﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Text;

namespace ParagonRCC_MVC3
{
    public static class ParagonHelpers
    {
        public static IDictionary<string, string> ToDictionary(this string source, string rowDelimiter, string colDelimiter)
        {
            Dictionary<string, string> rv = new Dictionary<string,string>();
            var vals = source.Split(rowDelimiter.ToCharArray());
            foreach (var val in vals)
            {
                string[] splits = val.Split(colDelimiter.ToCharArray(), 2);
                rv.Add(splits[0], splits[1]);
            }
            return rv;
        }

        public static string ToString(this IDictionary<string, string> source, string rowDelimiter, string colDelimiter)
        {
            List<string> list = new List<string>();
            StringBuilder sb = new StringBuilder();
            foreach (var kvp in source)
            {
                list.Add(string.Join("=", kvp.Key, kvp.Value));
            }
            string[] arr = list.ToArray();
            return string.Join(";", arr);
        }

        public static MvcHtmlString ActionSpan(this HtmlHelper html, string action, string controllerName,
            object routeValues, string spanText, object anchorAttributes, object spanAttributes)
        {
            var rValues = new RouteValueDictionary(routeValues);
            IDictionary<string, object> aAttrs = new RouteValueDictionary(anchorAttributes);
            IDictionary<string, object> sAttrs = new RouteValueDictionary(spanAttributes);

            return html.ActionSpan(action, controllerName, rValues, spanText, aAttrs, sAttrs);
        }

        public static MvcHtmlString ActionSpan(this HtmlHelper html, string action, string controllerName,
            object routeValues, string spanText, IDictionary<string, object> anchorAttributes, IDictionary<string, object> spanAttributes)
        {
            if (string.IsNullOrEmpty(action))
                throw new ArgumentNullException("action");

            var url = new UrlHelper(html.ViewContext.RequestContext);

            TagBuilder spanBuilder = null;
            if (!string.IsNullOrEmpty(spanText))
            {
                spanBuilder = new TagBuilder("span");
                spanBuilder.InnerHtml = html.Encode(spanText);
                if (null != spanAttributes)
                    spanBuilder.MergeAttributes(spanAttributes);
            }

            TagBuilder ancBuilder = new TagBuilder("a");
            if (string.IsNullOrEmpty(controllerName))
                ancBuilder.MergeAttribute("href", url.Action(action, routeValues));
            else
                ancBuilder.MergeAttribute("href", url.Action(action, controllerName, routeValues));
            if (null != anchorAttributes)
                ancBuilder.MergeAttributes(anchorAttributes);

            if (null == spanBuilder)
                ancBuilder.InnerHtml = string.Empty;
            else
                ancBuilder.InnerHtml = spanBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(ancBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string imagePath, string action,
            string controllerName, object routeValues, string spanText, object anchorAttributes,
            object imageAttributes, object spanAttributes)
        {
            var rValues = new RouteValueDictionary(routeValues);
            IDictionary<string, object> aAttrs = new RouteValueDictionary(anchorAttributes);
            IDictionary<string, object> iAttrs = new RouteValueDictionary(imageAttributes);
            IDictionary<string, object> sAttrs = new RouteValueDictionary(spanAttributes);

            return html.ActionImage(imagePath, action, controllerName, rValues, spanText, aAttrs, iAttrs, sAttrs);
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string imagePath, string action,
            string controllerName, RouteValueDictionary routeValues, string spanText,
            IDictionary<string, object> anchorAttributes, IDictionary<string, object> imageAttributes, IDictionary<string, object> spanAttributes)
        {
            if (string.IsNullOrEmpty(imagePath))
                throw new ArgumentNullException("imagePath");
            if (string.IsNullOrEmpty(action))
                throw new ArgumentNullException("action");

            var url = new UrlHelper(html.ViewContext.RequestContext);

            TagBuilder imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            if (null != imageAttributes)
                imgBuilder.MergeAttributes(imageAttributes);

            TagBuilder spanBuilder = null;
            if (!string.IsNullOrEmpty(spanText))
            {
                spanBuilder = new TagBuilder("span");
                spanBuilder.InnerHtml = html.Encode(spanText);
                if (null != spanAttributes)
                    spanBuilder.MergeAttributes(spanAttributes);
            }

            TagBuilder ancBuilder = new TagBuilder("a");
            if (string.IsNullOrEmpty(controllerName))
                ancBuilder.MergeAttribute("href", url.Action(action, routeValues));
            else
                ancBuilder.MergeAttribute("href", url.Action(action, controllerName, routeValues));
            if (null != anchorAttributes)
                ancBuilder.MergeAttributes(anchorAttributes);

            if (null == spanBuilder)
                ancBuilder.InnerHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);
            else
                ancBuilder.InnerHtml = imgBuilder.ToString(TagRenderMode.SelfClosing) + spanBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(ancBuilder.ToString(TagRenderMode.Normal));
        }

        /*
        public static MvcHtmlString ActionImage(this HtmlHelper html, string imagePath, string action,
            string controllerName, object routeValues, string spanText,
            IDictionary<string, object> anchorAttributes, IDictionary<string, object> imageAttributes, IDictionary<string, object> spanAttributes)
        {
            return ActionImage(html, imagePath, action, controllerName, 
                routeValues.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)),
                spanText, anchorAttributes, imageAttributes, spanAttributes);
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string imagePath, string action,
            string controllerName, object routeValues, string spanText,
            object anchorAttributes, object imageAttributes, object spanAttributes)
        {
            return ActionImage(html, imagePath, action, controllerName,
                routeValues.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)),
                spanText,
                anchorAttributes.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)),
                imageAttributes.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)),
                spanAttributes.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)));
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string imagePath, string action,
            object routeValues, string spanText,
            IDictionary<string, object> anchorAttributes, IDictionary<string, object> imageAttributes, IDictionary<string, object> spanAttributes)
        {
            return ActionImage(html, imagePath, action, null, 
                routeValues.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)), 
                spanText, anchorAttributes, imageAttributes, spanAttributes);
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string imagePath, string action,
            object routeValues, string spanText,
            object anchorAttributes, object imageAttributes, object spanAttributes)
        {
            return ActionImage(html, imagePath, action, null,
                routeValues.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)),
                spanText,
                anchorAttributes.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)), 
                imageAttributes.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)),
                spanAttributes.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(routeValues, null)));
        }
         * */
    }
}