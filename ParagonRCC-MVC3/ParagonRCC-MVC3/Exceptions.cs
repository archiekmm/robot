﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3
{
    public class ServerNotFoundException : Exception
    {
        public ServerNotFoundException(int id) : base(string.Format("The server with an id of {0} was not found.", id)) { }
    }

    public class RobotNotFoundException : Exception
    {
        public RobotNotFoundException(int id) : base(string.Format("The robot with an id of {0} was not found.", id)) { }
    }
}