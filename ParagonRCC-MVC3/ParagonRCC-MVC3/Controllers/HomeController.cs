﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParagonRCC_MVC3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to the Paragon Robot Control Center!";

            foreach (var s in ParagonRCC_MVC3.Models.Server.Servers)
            {
                try
                {
                    var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(s.Uri.ToString() + "/robots/");
                    x.Timeout = 2000;
                    x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                    var resp = (System.Net.HttpWebResponse)x.GetResponse();
                    var xs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
                    //var ms = new System.IO.MemoryStream(r);
                    var bots = (List<Paragon.RobotServices.Model.Robot>)xs.ReadObject(resp.GetResponseStream());
                    s.Robots.Clear();
                    s.Robots.AddRange(bots);
                    x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(s.Uri.ToString() + "/modules/");
                    x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                    var mResp = (System.Net.HttpWebResponse)x.GetResponse();
                    var xsm = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Module>));
                    //var msm = new System.IO.MemoryStream(m);
                    var modules = (List<Paragon.RobotServices.Model.Module>)xsm.ReadObject(mResp.GetResponseStream());
                    s.InputModules.Clear();
                    s.InputModules.AddRange(modules.Where(mm => mm.Type == "input"));
                    s.OutputModules.Clear();
                    s.OutputModules.AddRange(modules.Where(mm => mm.Type == "output"));
                }
                catch
                {
                }
            }

            return RedirectToAction("Details", "Server", new { id = ParagonRCC_MVC3.Models.Server.Servers.Count - 1 });
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
