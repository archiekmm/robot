﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ParagonRCC_MVC3.Models;

namespace ParagonRCC_MVC3.Controllers
{
    public class TransformController : Controller
    {
        //
        // GET: /Transform/

        public ActionResult Index(int mappingId, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
            //if (null == mapping)
            //    return new HttpNotFoundResult();
            //var list = mapping.Transforms.OrderBy(t => t.Order);
            object list = null;
            ViewBag.MappingId = mappingId;
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            if (Request.IsAjaxRequest())
                return PartialView(list);
            return View(list);
        }

        //
        // GET: /Transform/Details/5

        public ActionResult Details(int id, int mappingId, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
            //if (null == mapping)
            //    return new HttpNotFoundResult();
            //var item = mapping.Transforms.Where(t => t.Id == id).SingleOrDefault();
            //if (null == item)
            //    return new HttpNotFoundResult();
            object item = null;

            ViewBag.MappingId = mappingId;
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            if (Request.IsAjaxRequest())
                return PartialView("~/Views/Shared/Transform.cshtml", item);
            return View("~/Views/Shared/Transform.cshtml", item);
        }

        //
        // GET: /Transform/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Transform/Create

        [HttpPost]
        public ActionResult Create(int mappingId, int robotId, int serverId, string name, string value)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                //var newId = mapping.Transforms.Max(t => t.Id) + 1;
                //var item = new Transform() { Name = name, Value = value, Id = newId, Order = newId };
                //mapping.Transforms.Add(item);
                Transform item = null;

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                return RedirectToAction("Details", new { id = item.Id, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Transform/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Transform/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, int mappingId, int robotId, int serverId, string name, string value)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                ////var newId = mapping.Transforms.Max(t => t.Id) + 1;
                ////var item = new Transform() { Name = name, Value = value, Id = newId, Order = newId };
                //var item = mapping.Transforms.Where(t => t.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //item.Name = name;
                //item.Value = value;
                Transform item = null;
                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                return RedirectToAction("Details", new { id = item.Id, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Transform/Delete/5

        public ActionResult Delete(int id, int mappingId, int robotId, int serverId)
        {
            return View();
        }

        //
        // POST: /Transform/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id, int mappingId, int robotId, int serverId)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                //var item = mapping.Transforms.Where(t => t.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                Transform item = null;

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                if (Request.IsAjaxRequest())
                    return RedirectToAction("Refresh", new { mappingId = mappingId, robotId = robotId, serverId = serverId });
                return RedirectToAction("Index", new { mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Refresh(int mappingId, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
            //if (null == mapping)
            //    return new HttpNotFoundResult();
            //var list = mapping.Transforms.OrderBy(t => t.Order);
            IEnumerable<Transform> list = null;

            ViewBag.MappingId = mappingId;
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            if (Request.IsAjaxRequest())
                return PartialView(list);
            return View(list);
        }

        [HttpPost]
        public ActionResult Copy(int id, int mappingId, int robotId, int serverId)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                ////var newId = mapping.Transforms.Max(t => t.Id) + 1;
                ////var item = new Transform() { Name = name, Value = value, Id = newId, Order = newId };
                //var item = mapping.Transforms.Where(t => t.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //var newId = mapping.Transforms.Max(t => t.Id) + 1;
                //var newItem = new Transform() { Id = newId, Name = item.Name, Value = item.Value, Order = newId };
                //mapping.Transforms.Add(newItem);
                int newId = 0;

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                return RedirectToAction("Details", new { id = newId, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Order(int id, int mappingId, int robotId, int serverId, int order)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                ////var newId = mapping.Transforms.Max(t => t.Id) + 1;
                ////var item = new Transform() { Name = name, Value = value, Id = newId, Order = newId };
                //var item = mapping.Transforms.Where(t => t.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //item.Order = order;
                //save 
                Transform item = null;

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                return RedirectToAction("Details", new { id = item.Id, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }
    }
}
