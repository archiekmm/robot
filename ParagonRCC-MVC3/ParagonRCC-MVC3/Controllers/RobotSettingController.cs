﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace ParagonRCC_MVC3.Controllers
//{
//    public class RobotSettingController : Controller
//    {
//        //
//        // GET: /RobotSetting/

//        public ActionResult Index(int serverId, int robotId)
//        {
//            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
//            if (null == server)
//                return new HttpNotFoundResult();
//            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
//            if (null == robot)
//                return new HttpNotFoundResult();
//            var list = robot.Settings.GroupBy(s => s.Group);
//            //var l2 = robot.Settings.Select(s => s.Group).Distinct();
//            ViewData["robotId"] = robotId;
//            ViewData["serverId"] = serverId;
//            //ControllerContext.ParentActionViewContext.GetType();
            
//            if (Request.IsAjaxRequest())
//                return PartialView(list);
//            return View(list);
//        }

//        public ActionResult Group(int serverId, int robotId, string group)
//        {
//            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
//            if (null == server)
//                return new HttpNotFoundResult();
//            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
//            if (null == robot)
//                return new HttpNotFoundResult();
//            var list = robot.Settings.Where(s => s.Group == group);
//            //var l2 = robot.Settings.Select(s => s.Group).Distinct();
//            ViewData["robotId"] = robotId;
//            ViewData["serverId"] = serverId;
//            ViewData["groupName"] = group;
//            //ControllerContext.ParentActionViewContext.GetType();

//            if (Request.IsAjaxRequest())
//                return PartialView(list);
//            return View(list);
//        }

//        //
//        // GET: /RobotSetting/Details/5

//        public ActionResult Details(int serverId, int id, int robotId)
//        {
//            return View();
//        }

//        //
//        // GET: /RobotSetting/Create

//        public ActionResult Create(int serverId, int robotId)
//        {
//            return View();
//        } 

//        //
//        // POST: /RobotSetting/Create

//        [HttpPost]
//        public ActionResult Create(int serverId, int robotId, FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add insert logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }
        
//        //
//        // GET: /RobotSetting/Edit/5

//        public ActionResult Edit(int serverId, int robotId, int id)
//        {
//            return View();
//        }

//        //
//        // POST: /RobotSetting/Edit/5

//        [HttpPost]
//        public ActionResult Edit(int serverId, int robotId, int id, string value)
//        {
//            try
//            {
//                // TODO: Add update logic here
//                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
//                if (null == server)
//                    return new HttpNotFoundResult();
//                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
//                if (null == robot)
//                    return new HttpNotFoundResult();
//                var item = robot.Settings.Where(setting => setting.Id == id).SingleOrDefault();
//                if (null == item)
//                    return new HttpNotFoundResult();
//                item.Value = value;
 
//                return new HttpStatusCodeResult(200, "OK");
//            }
//            catch (Exception ex)
//            {
//                throw new HttpException("Exception while updating Robot Setting", ex);
//            }
//        }

//        //
//        // GET: /RobotSetting/Delete/5

//        public ActionResult Delete(int serverId, int robotId, int id)
//        {
//            return View();
//        }

//        //
//        // POST: /RobotSetting/Delete/5

//        [HttpPost]
//        public ActionResult Delete(int serverId, int robotId, int id, FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add delete logic here
 
//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }
//    }
//}
