﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParagonRCC_MVC3.Controllers
{
    public class LookupController : Controller
    {
        //
        // GET: /Lookup/

        public ActionResult Index(int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            if (Request.IsAjaxRequest())
                return PartialView(robot.Lookup);
            return View(robot.Lookup);
        }

        //
        // GET: /Lookup/Details/5

        public ActionResult Details(int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();

            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            if (Request.IsAjaxRequest())
                return PartialView(robot.Lookup);
            return View(robot.Lookup);
        }

        //
        // GET: /Lookup/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Lookup/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Lookup/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Lookup/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Lookup/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Lookup/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
