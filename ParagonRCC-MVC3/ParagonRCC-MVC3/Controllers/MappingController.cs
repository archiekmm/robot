﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ParagonRCC_MVC3.Models;

namespace ParagonRCC_MVC3.Controllers
{
    public class MappingController : Controller
    {
        //
        // GET: /Mapping/

        public ActionResult Index(int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            if (Request.IsAjaxRequest())
                return PartialView(robot.Mapping);
            return View(robot.Mapping);
        }

        //
        // GET: /Mapping/Details/5

        public ActionResult Details(int id, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var item = robot.Mappings.Where(m => m.Id == id).SingleOrDefault();
            //if (null == item)
            //    return new HttpNotFoundResult();
            object item = null;

            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;
            ViewBag.MappingId = id;

            if (Request.IsAjaxRequest())
                return PartialView(item);
            return View(item);
        }

        //
        // GET: /Mapping/Create

        public ActionResult Create(int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            var item = new Mapping();
            if (null == item)
                return new HttpNotFoundResult();

            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            if (Request.IsAjaxRequest())
                return PartialView(item);
            return View(item);
        } 

        //
        // POST: /Mapping/Create

        [HttpPost]
        public ActionResult Create(string name, int robotId, int serverId)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                    throw new ArgumentNullException("newName");
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //int newId = robot.Mappings.Max(m => m.Id) + 1;
                //var mapping = new Mapping() { Id = newId, Name = name, Order = newId, Transforms = new List<Transform>(), Conditions = new List<Condition>() };
                //robot.Mappings.Add(mapping);
                Mapping mapping = null;
                if (Request.IsAjaxRequest())
                {
                    Response.AddHeader("Location", Url.Action("Details", new { id = mapping.Id, robotId = robotId, serverId = serverId }));
                    return new HttpStatusCodeResult(299, "Created");
                }
                return RedirectToAction("Details", new { id = mapping.Id, robotId = robotId, serverId = serverId });
            }
            catch
            {
                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(400, "name cannot be null or empty");
                return View();
            }
        }

        public ActionResult Edit(int id, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var item = robot.Mappings.Where(m => m.Id == id).SingleOrDefault();
            //if (null == item)
            //    return new HttpNotFoundResult();
            Mapping item = null;

            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;
            ViewBag.MappingId = id;

            if (Request.IsAjaxRequest())
                return PartialView(item);
            return View(item);
        }

        //
        // POST: /Mapping/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, int robotId, int serverId, string name)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                    throw new ArgumentNullException("name");
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var item = robot.Mappings.Where(m => m.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();

                //item.Name = name;
                // save
                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(200);
                return RedirectToAction("Index");
            }
            catch
            {
                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(400, "name is required and must not be empty");
                return View();
            }
        }

        //
        // GET: /Mapping/Delete/5
 
        public ActionResult Delete(int id, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var item = robot.Mappings.Where(m => m.Id == id).SingleOrDefault();
            //if (null == item)
            //    return new HttpNotFoundResult();
            Mapping item = null;

            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;
            ViewBag.MappingId = id;

            if (Request.IsAjaxRequest())
                return PartialView(item);
            return View(item);
        }

        //
        // POST: /Mapping/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id, int robotId, int serverId)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var item = robot.Mappings.Where(m => m.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();

                //robot.Mappings.Remove(item);
                // save
                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(200);
                return RedirectToAction("Index");
            }
            catch
            {
                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(400);
                return View();
            }
        }

        [HttpPost]
        public ActionResult Copy(string name, int robotId, int serverId, int id)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                    throw new ArgumentNullException("newName");
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var item = robot.Mappings.Where(m => m.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //int newId = robot.Mappings.Max(m => m.Id) + 1;
                //var mapping = new Mapping() { Id = newId, Name = name, Order = newId, Transforms = new List<Transform>(), Conditions = new List<Condition>() };
                //foreach (var c in item.Conditions)
                //    mapping.Conditions.Add(
                //        new Condition( 
                //            ((mapping.Conditions.Count > 0) ? (mapping.Conditions.Max(cc => cc.Id) + 1) : 1), c));
                //foreach (var t in item.Transforms)
                //    mapping.Transforms.Add(
                //        new Transform(
                //            ((mapping.Transforms.Count > 0) ? (mapping.Transforms.Max(tt => tt.Id) + 1) : 1), t));
                //robot.Mappings.Add(mapping);
                Mapping mapping = null;
                if (Request.IsAjaxRequest())
                {
                    Response.AddHeader("Location", Url.Action("Details", new { id = mapping.Id, robotId = robotId, serverId = serverId }));
                    return new HttpStatusCodeResult(299, "Created");
                }
                return RedirectToAction("Details", new { id = mapping.Id, robotId = robotId, serverId = serverId });
            }
            catch (Exception ex)
            {
                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(400, ex.ToString());
                return View();
            }
        }

        [HttpPost]
        public ActionResult Order(int order, int robotId, int serverId, int id)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var item = robot.Mappings.Where(m => m.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //item.Order = order;
                // save
                Mapping item = null;

                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(200); //, "Created");
                return RedirectToAction("Details", new { id = item.Id, robotId = robotId, serverId = serverId });
            }
            catch
            {
                if (Request.IsAjaxRequest())
                    return new HttpStatusCodeResult(400);
                return View();
            }
        }

    }
}
