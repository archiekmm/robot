﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ParagonRCC_MVC3.Models;
using Paragon.RobotServices.Model;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO;
using System.Text;
using System.Reflection;
using System.Security.Cryptography;
using System.Xml;
using System.Configuration;

namespace ParagonRCC_MVC3.Controllers
{
    [System.Runtime.InteropServices.GuidAttribute("8091134D-5916-469F-B0E8-D47B1AF4E18A")]
    public class RobotController : Controller
    {
        private string filePath;
        private string FilePath
        {
            get
            {
                return Path.Combine(Server.MapPath("~/App_Data"), filePath);
            }
            set
            {
                filePath = value;
            }
        }

        string exportFilePath = ConfigurationManager.AppSettings["ExportXml"].ToString();
        string importFilePath = ConfigurationManager.AppSettings["ImportXml"].ToString();

        public string plainText;
        public string passPhrase = "Pas5pr@se";
        public string saltValue = "s@1tValue";
        public string hashAlgorithm = "MD5";
        public int passwordIterations = 2;
        public string initVector = "@1B2c3D4e5F6g7H8";
        public int keySize = 256;

        private object _value;

        protected ParagonRCC_MVC3.Models.Server _server;

        protected Robot FindRobot(int serverId, int robotId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).FirstOrDefault();
            if (server == null)
                throw new ServerNotFoundException(serverId);
            _server = server;
            var item = server.Robots.Where(r => r.Id == robotId).FirstOrDefault();
            ViewData["robotId"] = robotId;
            ViewData["serverId"] = serverId;
            if (item == null)
                throw new RobotNotFoundException(robotId);
            return item;
        }

        //
        // GET: /Robot/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Robot/Details/5

        public ActionResult Details(int id, int serverId)
        {
            try
            {
                Robot item = FindRobot(serverId, id);
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).Single();
                var url = server.Uri.ToString();
                try
                {
                    var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{0}", id));
                    x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                    var resp = (System.Net.HttpWebResponse)x.GetResponse();
                    var xs = new System.Runtime.Serialization.DataContractSerializer(typeof(Paragon.RobotServices.Model.Robot));
                    //var ms = new System.IO.MemoryStream(r);
                    var bot = (Paragon.RobotServices.Model.Robot)xs.ReadObject(resp.GetResponseStream());
                    item = server.Robots[id] = bot;
                }
                catch (Exception ex)
                {
                    return new HttpStatusCodeResult(503, ex.ToString());
                }
                if (Request.IsAjaxRequest())
                    return PartialView(item);
                return View(item);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //
        // GET: /Robot/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Robot/Create

        [HttpPost]
        public ActionResult Create(Robot item)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Robot/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Robot/Edit/5

        [HttpPost]
        public ActionResult Edit(Robot item)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Robot/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Robot/Delete/5

        [HttpPost]
        public ActionResult Delete(Robot item)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Logging(int id, int serverId)
        {
            try
            {
                var item = FindRobot(serverId, id);
                IEnumerable<LogEntry> list = null;
                var url = _server.Uri.ToString();
                //var url = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).Single().Uri.ToString();
                var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{0}/logs", id));
                x.Method = "GET";
                x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                System.Net.HttpWebResponse resp = null;
                try
                {
                    resp = (System.Net.HttpWebResponse)x.GetResponse();
                }
                catch (Exception ex)
                {
                    return new HttpStatusCodeResult(503, ex.ToString());
                }
                var dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.LogEntry>));
                list = (List<Paragon.RobotServices.Model.LogEntry>)dcs.ReadObject(resp.GetResponseStream());

                bool asc = false;
                // severities logic
                if (null != Request)
                {
                    if (Request.Params["critical"] == null)
                        list = list.Where(l => l.Severity != LogSeverity.Critical);
                    if (Request.Params["error"] == null)
                        list = list.Where(l => l.Severity != LogSeverity.Error);
                    if (Request.Params["warning"] == null)
                        list = list.Where(l => l.Severity != LogSeverity.Warning);
                    if (Request.Params["normal"] == null)
                        list = list.Where(l => l.Severity != LogSeverity.Normal);
                    if (Request.Params["debug"] == null)
                        list = list.Where(l => l.Severity != LogSeverity.Debug);
                    if ((null != Request.Params["ascending"]) && (Request.Params["ascending"].Contains("true")))
                        asc = true;
                }
                if (asc)
                    list = list.OrderBy(s => s.EventDate);
                else
                    list = list.OrderByDescending(s => s.Id).OrderByDescending(s => s.EventDate);

                if (Request.IsAjaxRequest())
                    return PartialView(list);
                return View(list);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Scheduling(int id, int serverId)
        {
            try
            {
                var item = FindRobot(serverId, id);
                IEnumerable<Schedule> list = item.Schedules;

                if (Request.IsAjaxRequest())
                    return PartialView(list);
                return View(list);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Notification(int id, int serverId)
        {
            try
            {
                var item = FindRobot(serverId, id);
                //var notification = item.Notification;
                var notification = item.Notifier;

                if (Request.IsAjaxRequest())
                    return PartialView(notification);
                return View(notification);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Normalization(int id, int serverId)
        {
            try
            {
                var item = FindRobot(serverId, id);
                //var normalization = item.Normalization;
                var normalization = item.Normalizer;

                ViewBag.TiffCompression = new SelectList(new[] { new { id = 1, name = "None" }, new { id = 2, name = "Group 3" }, new { id = 3, name = "Group 4" } }, "id", "name",
                    normalization.TiffCompression);
                ViewBag.Strips = new SelectList(new[] { new { id = 1, name = "Multiple" }, new { id = 2, name = "Single" } }, "id", "name",
                    normalization.Strips);
                ViewBag.FillOrder = new SelectList(new[] { new { id = 1, name = "Standard" }, new { id = 2, name = "Reverse" } }, "id", "name",
                    normalization.FillOrder);
                ViewBag.MatchResolutions = new SelectList(new[] { new { id = 1, name = "None" }, new { id = 2, name = "Match horizontal resolutions" }, new { id = 3, name = "Match vertical resolutions" }, new { id = 4, name = "Match both resolutions" } }, "id", "name",
                    normalization.MatchResolutions);
                ViewBag.RoundResolutions = new SelectList(new[] { new { id = 1, name = "None" }, new { id = 2, name = "Round horizontal resolutions" }, new { id = 3, name = "Round vertical resolutions" }, new { id = 4, name = "Round both resolutions" } }, "id", "name",
                    normalization.RoundResolutions);
                ViewBag.Rotate = new SelectList(new[] { new { id = 1, name = "None" }, new { id = 2, name = "Rotate 90 clockwise" }, new { id = 3, name = "Rotate 180" }, new { id = 4, name = "Rotate 270 clockwise" } }, "id", "name",
                    normalization.RotateImages);
                ViewBag.Mirror = new SelectList(new[] { new { id = 1, name = "None" }, new { id = 2, name = "Mirror horizontally" }, new { id = 3, name = "Mirror vertically" }, new { id = 4, name = "Mirror both" } }, "id", "name",
                    normalization.MirrorImages);
                ViewBag.ColorReduction = new SelectList(new[] { new { id = 1, name = "None" }, new { id = 2, name = "monochrome" }, new { id = 3, name = "16 colors" }, new { id = 3, name = "256 colors" }, new { id = 3, name = "16 greys" }, new { id = 3, name = "256 greys" } }, "id", "name",
                    normalization.ColorReduction);

                if (Request.IsAjaxRequest())
                    return PartialView(normalization);
                return View(normalization);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult TestLogin(int id, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).FirstOrDefault();
            if (server == null)
                return new HttpNotFoundResult();
            var item = server.Robots.Where(r => r.Id == id).FirstOrDefault();
            if (item == null)
                return new HttpNotFoundResult();
            ViewData["robotId"] = id;
            ViewData["robotName"] = item.Name;
            ViewData["serverId"] = serverId;

            ViewBag.Providers = new SelectList(new[] { new { id = "Kofax", name = "Kofax" }, new { id = "Captiva", name = "Captiva" } }, "id", "name");
            ViewBag.Fields = new List<string> { "UserId", "Password", "Server" };
            if (Request.IsAjaxRequest())
                return PartialView("~/Views/Shared/GeneralLogon.cshtml");
            return View("~/Views/Shared/GeneralLogon.cshtml");
        }

        /*
        public ActionResult Mappings(int id, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).FirstOrDefault();
            if (server == null)
                return new HttpNotFoundResult();
            var item = server.Robots.Where(r => r.Id == id).FirstOrDefault();
            if (item == null)
                return new HttpNotFoundResult();
            ViewData["robotId"] = id;
            ViewData["robotName"] = item.Name;
            ViewData["serverId"] = serverId;

            //ViewBag.Conditionals = new SelectList(Conditional.Conditionals, "Id", "Name");
            //ViewBag.Connectors = new SelectList(Connector.Connectors, "Id", "Name");
            ViewBag.Conditionals = new SelectList(new []
                {
                new { Id = 1, Name = "Equals"},
                new { Id = 2, Name = "NotEquals"},
                new { Id = 3, Name = "IsBlank"},
                new { Id = 4, Name = "IsNotBlank"},
                new { Id = 5, Name = "StartsWith"},
                new { Id = 6, Name = "EndsWith"},
                new { Id = 7, Name = "GreaterThan"},
                new { Id = 8, Name = "GreaterThanOrEqual"},
                new { Id = 9, Name = "LessThan"},
                new { Id = 10, Name = "LessThanOrEqual"},
                new { Id = 11, Name = "Contains"},
                new { Id = 12, Name = "InList"},
                new { Id = 13, Name = "NotInList"},
                new { Id = 14, Name = "Like"},
                new { Id = 15, Name = "NotLike" }
                }, "Id", "Name");
            ViewBag.Connectors = new SelectList(new [] 
                {
                    new { Id = 1, Name = "And" },
                    new { Id = 2, Name = "Or" }
                }, "Id", "Name");
            var list = item.Mappings.OrderBy(m => m.Order);
            if (Request.IsAjaxRequest())
                return PartialView(list);
            return View(list);
        }
        
        public ActionResult Settings(int id, int serverId)
        {
            return RedirectToAction("Index", "RobotSetting", new { robotId = id, serverId = serverId });
        }
        */

        public ActionResult Mappings(int serverId, int id)
        {
            try
            {
                var item = FindRobot(serverId, id);
                var mappingStr = item.Mapping;
                ViewBag.Connectors = ConditionController.Connectors;
                ViewBag.Conditionals = ConditionController.Conditionals;

                ViewData["category"] = "mapping"; // special category for the Mapping robot setting
                //                if (string.IsNullOrEmpty(mappingStr))
                //                {
                //                    mappingStr = 
                //                        @"<mappings>
                //                            <mapping id='1' order='1' name='default'></mapping>
                //                        </mappings>";
                //                }
                var mappings = Mapping.FromXml(mappingStr);
                if (Request.IsAjaxRequest())
                    return PartialView(mappings);
                return View(mappings);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Lookup(int serverId, int id)
        {
            try
            {
                var item = FindRobot(serverId, id);
                var lookup = item.Lookup;
                ViewData["category"] = "lookup";
                ViewBag.Connectors = ConditionController.Connectors;
                ViewBag.Conditionals = ConditionController.Conditionals;

                //ViewBag.Connectors = ConditionController.Connectors;
                //ViewBag.Conditionals = ConditionController.Conditionals;
                if (string.IsNullOrEmpty(lookup))
                {
                    //                                <condition id='1' order='1' op1='SourceFolder' op2='\\share\folder' conditionalId='1' connectorId='1' />
                    lookup =
                        @"<mappings>
                            <mapping id='1' name='default' order='1'>
                                <transform id='1' order='1' name='_Provider' value='SQL Server' />
                                <transform id='2' order='2' name='_Server' value='SQLPROD01' />
                                <transform id='3' order='3' name='_Database' value='MyDB' />
                                <transform id='4' order='4' name='_UserID' value='admin' />
                                <transform id='5' order='5' name='_Password' value='pass' />
                                <transform id='6' order='6' name='_UseTrustedConn' value='false' />
                            </mapping>
                        </mappings>";
                }
                var mappings = Mapping.FromXml(lookup);
                if (Request.IsAjaxRequest())
                    return PartialView("Mappings", mappings);
                return View("Mappings", mappings);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Input(int serverId, int id)
        {
            try
            {
                var item = FindRobot(serverId, id);
                //var list = item.InputSettings.GroupBy(s => s.Group);
                var ss = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                var module = ss.InputModules.Where(im => im.Id == item.InputModuleId).Single();
                var settings = item.InputSettings;
                foreach (var s in settings)
                {
                    var ms = module.Settings.Where(mss => mss.Id == s.Key).FirstOrDefault();
                    if (null != ms)
                    {
                        switch (ms.SettingTypeId)
                        {
                            case 8:
                                ms.Value = EncryptDecryptLoginPwd(s.Value, false);
                                break;
                            case 9:
                                ms.Value = Decryptdata(s.Value);
                                break;
                            default:
                                ms.Value = s.Value;
                                break;
                        }
                    }
                }
                var list = module.Settings.GroupBy(set => set.Group);
                ViewData["category"] = "input";
                if (Request.IsAjaxRequest())
                    return PartialView("Settings", list);
                return View("Settings", list);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Output(int serverId, int id)
        {
            try
            {
                var item = FindRobot(serverId, id);
                //var list = item.OutputSettings.GroupBy(s => s.Group);
                var ss = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                var module = ss.OutputModules.Where(im => im.Id == item.OutputModuleId).Single();
                var settings = item.OutputSettings;
                foreach (var s in settings)
                {
                    var ms = module.Settings.Where(mss => mss.Id == s.Key).FirstOrDefault();
                    if (null != ms)
                    {
                        switch (ms.SettingTypeId)
                        {
                            case 8:
                                ms.Value = EncryptDecryptLoginPwd(s.Value, false);
                                break;
                            case 9:
                                ms.Value = Decryptdata(s.Value);
                                break;
                            default:
                                ms.Value = s.Value;
                                break;
                        }
                    }
                }
                var list = module.Settings.GroupBy(set => set.Group);

                ViewData["category"] = "output";
                if (Request.IsAjaxRequest())
                    return PartialView("Settings", list);
                return View("Settings", list);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Audit(int id, int serverId)
        {
            try
            {
                var item = FindRobot(serverId, id);
                if (item.Audit.Login.Password != null)
                    item.Audit.Login.Password = Decryptdata(item.Audit.Login.Password);
                if (Request.IsAjaxRequest())
                    return PartialView(item.Audit);
                return View(item.Audit);
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Save(int serverId, int id, RobotSaveModel robot)
        {
            try
            {
                var item = FindRobot(serverId, id);

                if (robot.Input != null)
                    foreach (var newSetting in robot.Input)
                    {
                        //item.InputSettings.Where(s => s.Id == newSetting.Id).Single().Value = newSetting.Value;
                        switch (newSetting.Type)
                        {
                            case 8:
                                
                                item.InputSettings[newSetting.Id] = EncryptDecryptLoginPwd(newSetting.Value, true);
                                break;
                            case 9:
                                item.InputSettings[newSetting.Id] = EncryptPwd(newSetting.Value);
                                break;
                            default:
                                item.InputSettings[newSetting.Id] = newSetting.Value;
                                break;
                        }
                    }
                if (robot.Lookup != null)
                    item.Lookup = robot.Lookup;
                if (robot.Mapping != null)
                    item.Mapping = robot.Mapping;
                if (robot.Name != null)
                    item.Name = robot.Name;
                if (robot.Normalization != null)
                    item.Normalizer = robot.Normalization;
                if (robot.Notification != null)
                    item.Notifier = robot.Notification;
                if (robot.Output != null)
                    foreach (var newSetting in robot.Output)
                    {
                        switch (newSetting.Type)
                        {
                            case 8:
                                item.OutputSettings[newSetting.Id] = EncryptDecryptLoginPwd(newSetting.Value, true);
                                break;
                            case 9:
                                item.OutputSettings[newSetting.Id] = EncryptPwd(newSetting.Value);
                                break;
                            default:
                                item.OutputSettings[newSetting.Id] = newSetting.Value;
                                break;
                        }
                    }

                if (robot.Audit != null)
                {
                    string encryptAuditPwd = EncryptPwd(robot.Audit.Password);
                    robot.Audit.Password = encryptAuditPwd;
                    item.Audit = new Paragon.RobotServices.Model.Audit(robot.Audit);
                }
                if (robot.Schedules != null)
                {
                    item.Schedules.Clear();
                    foreach (var sch in robot.Schedules)
                    {
                        item.Schedules.Add(new Schedule(sch));
                    }
                }

                var url = _server.Uri.ToString();
                //var url = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).Single().Uri.ToString();
                var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{0}", id));
                x.Method = "PUT";
                var xs = new System.Runtime.Serialization.DataContractSerializer(typeof(Paragon.RobotServices.Model.Robot));
                var ms = new System.IO.MemoryStream();
                xs.WriteObject(ms, item);
                ms.Position = 0;
                x.ContentLength = ms.Length;
                x.ContentType = "text/xml";
                x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                x.GetRequestStream().Write(ms.GetBuffer(), 0, (int)ms.Length);
                var resp = (System.Net.HttpWebResponse)x.GetResponse();
            }
            catch (ServerNotFoundException sEx)
            {
                return new HttpNotFoundResult(sEx.ToString());
            }
            catch (RobotNotFoundException rEx)
            {
                return new HttpNotFoundResult(rEx.ToString());
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(503, ex.ToString());
            }

            return new HttpStatusCodeResult(200);
        }

        public ActionResult PostToService(int serverId, string message, int robotId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).Single();
            var url = server.Uri.ToString();
            var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{1}/{0}", robotId, message));
            x.Method = "POST";
            x.ContentLength = 0;
            x.ContentType = "application/x-ww-form-urlencoded";
            x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
            try
            {
                var resp = (System.Net.HttpWebResponse)x.GetResponse();
                //x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{0}", robotId));
                //x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                //resp = (System.Net.HttpWebResponse)x.GetResponse();
                //////////////////////var xs = new System.Runtime.Serialization.DataContractSerializer(typeof(bool));
                //var ms = new System.IO.MemoryStream(r);
                /////////////////////var bot = Convert.ToBoolean(xs.ReadObject(resp.GetResponseStream()));
                // server.Robots[robotId] = bot;

                return RedirectToAction("Details", new { serverId = serverId, id = robotId });
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(503, ex.ToString());
            }
        }

        public ActionResult PostToService(int serverId, string message, int robotId, string val)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).Single();
            var url = server.Uri.ToString();
            var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{1}?id={0}&val={2}", robotId, message, val));
            x.Method = "POST";
            x.ContentLength = 0;
            x.ContentType = "application/x-ww-form-urlencoded";
            x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
            try
            {
                var resp = (System.Net.HttpWebResponse)x.GetResponse();
                return RedirectToAction("Details", new { serverId = serverId, id = robotId });
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(503, ex.ToString());
            }
        }

        public List<ChartData> GetChartDataFromService(int serverId, string message, int robotId, string fromDate, string toDate, string viewType)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).Single();
            var url = server.Uri.ToString();
            var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{1}?id={0}&fromDate={2}&toDate={3}&viewType={4}", robotId, message, fromDate, toDate, viewType));
            x.Method = "POST";
            x.ContentLength = 0;
            x.ContentType = "application/x-ww-form-urlencoded";
            x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
            try
            {
                var resp = (System.Net.HttpWebResponse)x.GetResponse();
                var xs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<ChartData>));
                var bot = (List<ChartData>)xs.ReadObject(resp.GetResponseStream());
                return bot;
            }
            catch (Exception ex)
            {
                return new List<ChartData>();
            }
        }

        public ActionResult Start(int serverId, int id)
        {
            return PostToService(serverId, "start", id);
        }

        public ActionResult Stop(int serverId, int id)
        {
            return PostToService(serverId, "stop", id);
        }

        public ActionResult ScheduledToggle(int serverId, int id, bool isSchedule)
        {
            string schedule = isSchedule == true ? "true" : "false";
            return PostToService(serverId, "StartScheduling", id, schedule);
        }

        public ActionResult Pause(int serverId, int id)
        {
            return PostToService(serverId, "pause", id);
        }

        public ActionResult Resume(int serverId, int id)
        {
            return PostToService(serverId, "Resume", id);
        }

        public ActionResult SaveLicense(int serverId, int id, string licenseKey)
        {
            return PostToService(serverId, "SaveLicenseKey", id, licenseKey);
        }

        public ActionResult DisplayChart(int serverId, int id, string robotName)
        {
            ViewData["robotId"] = id;
            ViewData["robotServerId"] = serverId;
            ViewData["robotName"] = robotName;
            return View();
        }

        public ActionResult ExportImport(int serverId, int id, int inputModuleId, int outputModuleId)
        {
            ViewData["robotId"] = id;
            ViewData["serverId"] = serverId;
            ViewData["inputModuleId"] = inputModuleId;
            ViewData["outputModuleId"] = outputModuleId;

            Robot item = FindRobot(serverId, id);
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).Single();
            var url = server.Uri.ToString();
            try
            {
                var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url + string.Format("/robots/{0}", id));
                x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
                var resp = (System.Net.HttpWebResponse)x.GetResponse();
                var xs = new System.Runtime.Serialization.DataContractSerializer(typeof(Paragon.RobotServices.Model.Robot));
                var bot = (Paragon.RobotServices.Model.Robot)xs.ReadObject(resp.GetResponseStream());
                item = server.Robots[id] = bot;
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(503, ex.ToString());
            }
            if (Request.IsAjaxRequest())
                return PartialView(item);
            return View(item);
        }

        [HttpPost]
        public ActionResult ExportSetting(int serverId, int id, int inputModuleId, int outputModuleId, List<string> tabNameCollection)
        {         
            XmlDocument document = new XmlDocument();
            XmlDocument xmlImport = new XmlDocument();
            XmlNode Root = xmlImport.AppendChild(xmlImport.CreateElement("Robot"));
            
            document.Load(@exportFilePath);
            if (((document.GetElementsByTagName("InputModuleId").Item(0).InnerText) == inputModuleId.ToString()) && ((document.GetElementsByTagName("OutputModuleId").Item(0).InnerText) == outputModuleId.ToString()))
            {
                XmlNode importInputModuleId = xmlImport.ImportNode(document.GetElementsByTagName("InputModuleId").Item(0).Clone(), true);
                Root.AppendChild(importInputModuleId);
                XmlNode importOutputModuleId = xmlImport.ImportNode(document.GetElementsByTagName("OutputModuleId").Item(0).Clone(), true);
                Root.AppendChild(importOutputModuleId);
                foreach (string tabName in tabNameCollection)
                {
                    XmlNode importTabCollection = xmlImport.ImportNode(document.GetElementsByTagName(tabName).Item(0).Clone(), true);
                    Root.AppendChild(importTabCollection);
                }
                xmlImport.Save(@importFilePath);
            }
            
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ImportSetting(int serverId, int id, int inputModuleId, int outputModuleId, List<string> tabNameCollection)
        {
            XmlDocument currentDoc = new XmlDocument();
            currentDoc.Load(@exportFilePath);
            XmlDocument xmlExport = new XmlDocument();
            xmlExport.Load(@importFilePath);
            if (((currentDoc.GetElementsByTagName("InputModuleId").Item(0).InnerText) == inputModuleId.ToString()) && ((currentDoc.GetElementsByTagName("OutputModuleId").Item(0).InnerText) == outputModuleId.ToString()))
            {
                foreach (string tabName in tabNameCollection)
                {
                    XmlNode currentTabCollection = currentDoc.GetElementsByTagName(tabName).Item(0);
                    if (xmlExport.GetElementsByTagName(tabName).Item(0) != null)
                    {
                        XmlNode exportTabCollection = xmlExport.GetElementsByTagName(tabName).Item(0);
                        currentTabCollection.InnerXml = exportTabCollection.InnerXml;
                    }
                }
            }
            currentDoc.Save(@exportFilePath);
            
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public FileResult CreateChart(string chartImage, string result, string robotid, string serverid, string startDate, string endDate, string viewType)
        {
            SeriesChartType chartType;
            if (chartImage == "Pie")
                chartType = SeriesChartType.Pie;
            else
                chartType = SeriesChartType.Column;

            if (string.IsNullOrEmpty(startDate))
            {
                endDate = DateTime.Now.ToString("yyyy-MM-dd");
                startDate = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).ToString("yyyy-MM-dd");
            }

            viewType = viewType != null ? viewType : "Day";

            List<ChartData> listChartData = GetChartDataFromService(Convert.ToInt32(serverid), "chartdata", Convert.ToInt32(robotid), startDate, endDate, viewType);

            Chart chart = new Chart();
            chart.Width = 700;
            chart.Height = 300;
            chart.BackColor = Color.FromArgb(211, 223, 240);
            chart.BorderlineDashStyle = ChartDashStyle.Solid;
            chart.BackSecondaryColor = Color.White;
            chart.BackGradientStyle = GradientStyle.TopBottom;
            chart.BorderlineWidth = 1;
            chart.Palette = ChartColorPalette.BrightPastel;
            chart.BorderlineColor = Color.FromArgb(26, 59, 105);
            chart.RenderType = RenderType.BinaryStreaming;
            chart.BorderSkin.SkinStyle = BorderSkinStyle.Emboss;
            chart.AntiAliasing = AntiAliasingStyles.All;
            chart.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal;
            chart.Titles.Add(CreateTitle(result));
            chart.Legends.Add(CreateLegend());

            if (chartType == SeriesChartType.Column)
            {
                Series seriesDetailSuccess = new Series("Success");
                seriesDetailSuccess.ChartType = SeriesChartType.Column;
                DataPoint pointSuccess;
                DataPoint pointFailure;
                foreach (ChartData chartData in listChartData)
                {
                    pointSuccess = new DataPoint();
                    pointSuccess.AxisLabel = chartData.ViewType.ToString();
                    pointSuccess.YValues = new double[] { double.Parse(chartData.SuccessCount.ToString()) };
                    seriesDetailSuccess.Points.Add(pointSuccess);
                }
                chart.Series.Add(seriesDetailSuccess);

                Series seriesDetailFailure = new Series("Error");
                seriesDetailFailure.ChartType = SeriesChartType.Column;
                foreach (ChartData chartData in listChartData)
                {
                    pointFailure = new DataPoint();
                    pointFailure.AxisLabel = chartData.ViewType.ToString();
                    pointFailure.YValues = new double[] { double.Parse(chartData.FailureCount.ToString()) };
                    seriesDetailFailure.Points.Add(pointFailure);
                }
                chart.Series.Add(seriesDetailFailure);
            }
            else
                chart.Series.Add(CreateSeries(listChartData, chartType, result));

            chart.ChartAreas.Add(CreateChartArea());

            MemoryStream ms = new MemoryStream();
            chart.SaveImage(ms);
            return File(ms.GetBuffer(), @"image/png");
        }

        public Title CreateTitle(string chartTitle)
        {
            Title title = new Title();
            switch (chartTitle)
            {
                case "Success":
                    title.Text = "Successful count of documents";
                    break;
                case "Failure":
                    title.Text = "Unsuccessful count of documents";
                    break;
                default:
                    title.Text = "Successful/Unsuccessful count of documents";
                    break;
            }

            title.ShadowColor = Color.FromArgb(32, 0, 0, 0);
            title.Font = new Font("Trebuchet MS", 14F, FontStyle.Bold);
            title.ShadowOffset = 3;
            title.ForeColor = Color.FromArgb(26, 59, 105);

            return title;
        }

        public Legend CreateLegend()
        {
            Legend legend = new Legend();
            legend.Docking = Docking.Bottom;
            legend.Alignment = StringAlignment.Center;
            legend.BackColor = Color.Transparent;
            legend.Font = new Font(new FontFamily("Trebuchet MS"), 9);
            legend.LegendStyle = LegendStyle.Row;

            return legend;
        }

        public Series CreateSeries(IList<ChartData> listChartData, SeriesChartType chartType, string result)
        {
            Series seriesDetail = new Series();
            seriesDetail.Name = "Pie Chart";
            seriesDetail.IsValueShownAsLabel = false;
            seriesDetail.Color = Color.FromArgb(198, 99, 99);
            seriesDetail.ChartType = chartType;
            seriesDetail.BorderWidth = 2;
            seriesDetail["DrawingStyle"] = "Cylinder";
            seriesDetail["PieDrawingStyle"] = "SoftEdge";
            DataPoint point;

            foreach (ChartData chartData in listChartData)
            {
                point = new DataPoint();
                point.AxisLabel = chartData.ViewType.ToString();
                point.IsValueShownAsLabel = true;

                if (result == "Success")
                    point.YValues = new double[] { double.Parse(chartData.SuccessCount.ToString()) };
                else
                    point.YValues = new double[] { double.Parse(chartData.FailureCount.ToString()) };
                seriesDetail.Points.Add(point);
            }

            return seriesDetail;
        }

        public ChartArea CreateChartArea()
        {
            ChartArea chartArea = new ChartArea();
            chartArea.BackColor = Color.Transparent;
            chartArea.AxisX.IsLabelAutoFit = false;
            chartArea.AxisY.IsLabelAutoFit = false;
            chartArea.AxisX.LabelStyle.Font = new Font("Verdana,Arial,Helvetica,sans-serif", 8F, FontStyle.Regular);
            chartArea.AxisY.LabelStyle.Font = new Font("Verdana,Arial,Helvetica,sans-serif", 8F, FontStyle.Regular);
            chartArea.AxisY.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.Interval = 1;

            return chartArea;
        }

        public ActionResult Export(string robotid, string serverid, string startDate, string endDate, string viewType)
        {
            if ((string.IsNullOrEmpty(startDate)) || (startDate == "undefined"))
            {
                endDate = DateTime.Now.ToString("yyyy-MM-dd");
                startDate = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).ToString("yyyy-MM-dd");
            }

            if ((viewType == null) || (viewType == "undefined"))
            {
                viewType = "Day";
            }
            List<ChartData> myList = GetChartDataFromService(Convert.ToInt32(serverid), "chartdata", Convert.ToInt32(robotid), startDate, endDate, viewType);

            CreateExcelBytes(myList);
            byte[] ccdPdfStreamByte = null;
            ccdPdfStreamByte = GetBytesFromFile(FilePath);
            MemoryStream excelStreamData = new MemoryStream();
            excelStreamData.Write(ccdPdfStreamByte, 0, ccdPdfStreamByte.Length);
            int bufSize = (int)excelStreamData.Length;
            byte[] buf = new byte[bufSize];
            excelStreamData.Position = 0;
            int bytesRead = excelStreamData.Read(buf, 0, bufSize);

            if (System.IO.File.Exists(FilePath))
            {
                System.IO.File.Delete(FilePath);
            }

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.BufferOutput = true;
            Response.AddHeader("Content-disposition", "attachment;filename=chart.xls");
            Response.OutputStream.Write(buf, 0, bytesRead);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            Response.End();

            return View();
        }

        public void CreateExcelBytes(List<ChartData> list)
        {
            filePath = "temp" + Guid.NewGuid() + ".xls";
            Microsoft.Office.Interop.Excel.Application _excelApplication;
            Microsoft.Office.Interop.Excel.Workbook _workBook;
            _excelApplication = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbooks _workBooks = _excelApplication.Workbooks;
            _workBook = _workBooks.Add();
            Microsoft.Office.Interop.Excel.Sheets _excelSheets = _workBook.Worksheets;
            Microsoft.Office.Interop.Excel.Worksheet _excelSheet = _excelSheets.get_Item(1);


            int myRowCnt = list.Count;
            object[,] myExcelData = new object[myRowCnt + 1, 3];
            int row = 1;
            foreach (ChartData dbr in list)
            {
                myExcelData[row, 0] = dbr.ViewType.ToString();
                myExcelData[row, 1] = dbr.SuccessCount;
                myExcelData[row, 2] = dbr.FailureCount;

                row++;
            }

            Microsoft.Office.Interop.Excel.Range _excelRange;
            _excelRange = _excelSheet.get_Range("A1", Missing.Value);
            _excelRange = _excelRange.get_Resize(myRowCnt + 1, 3);
            _excelRange.set_Value(Missing.Value, myExcelData);
            _excelRange.EntireColumn.AutoFit();

            object[] headerName = { "ViewType", "Success", "Unsuccess" };
            _excelRange = _excelSheet.get_Range("A1", "C1");
            _excelRange.set_Value(Missing.Value, headerName);
            _excelRange.EntireColumn.AutoFit();

            _value = Missing.Value;

            _workBook.SaveAs(FilePath, _value, _value,
                _value, _value, _value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                _value, _value, _value, _value, null);
            _workBook.Close(false, _value, _value);
            _excelApplication.Quit();
        }

        public static byte[] GetBytesFromFile(string fullFilePath)
        {
            FileStream fs = System.IO.File.OpenRead(fullFilePath);
            try
            {
                byte[] bytes = new byte[fs.Length];
                fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                fs.Close();
                return bytes;
            }
            finally
            {
                fs.Close();
            }

        }

        private string EncryptDecryptLoginPwd(string setting, bool encrypt)
        {
            string password = string.Empty;
            string[] settingValueCollection = setting.Split('&');
            Dictionary<string, string> results = new Dictionary<string, string>();
            StringBuilder newSettingValue = new StringBuilder();
            foreach (string settingValue in settingValueCollection)
            {
                string[] settingKeyValue = settingValue.Split('=');
                if (settingKeyValue[0] == "password")
                {
                    if (encrypt)
                    {
                        password = EncryptPwd(settingKeyValue[1]);
                        results.Add(settingKeyValue[0], password);
                    }
                    else
                    {
                        password = Decryptdata(settingKeyValue[1]);
                        results.Add(settingKeyValue[0], password);
                    }
                }
                else
                {
                    results.Add(settingKeyValue[0], settingKeyValue[1]);
                }
            }
            foreach (var result in results)
            {
                newSettingValue.Append(result.Key);
                newSettingValue.Append("=");
                newSettingValue.Append(result.Value);
                newSettingValue.Append("&");
            }
            setting = newSettingValue.ToString().TrimEnd('&');
            return setting;
        }

        public string EncryptPwd(string plainText)
        {
            plainText = HttpUtility.UrlDecode(plainText);
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string cipherText = Convert.ToBase64String(cipherTextBytes);
            if (cipherText.Contains('='))
                        cipherText = cipherText.Replace("=", "*");
            return cipherText;
        }

        public string Decryptdata(string cipherText)
        {
            if (cipherText.Contains('*'))
                cipherText = cipherText.Replace("*", "=");
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            string plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            return plainText;
        }
    }
}
