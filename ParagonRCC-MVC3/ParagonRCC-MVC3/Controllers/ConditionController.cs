﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ParagonRCC_MVC3.Models;

namespace ParagonRCC_MVC3.Controllers
{
    public class ConditionController : Controller
    {
        public static List<Conditional> Conditionals = new List<Conditional> 
                {
                new Conditional() { Id = 1, Name = "Equals"},
                new Conditional() { Id = 2, Name = "NotEquals"},
                new Conditional() { Id = 3, Name = "IsBlank"},
                new Conditional() { Id = 4, Name = "IsNotBlank"},
                new Conditional() { Id = 5, Name = "StartsWith"},
                new Conditional() { Id = 6, Name = "EndsWith"},
                new Conditional() { Id = 7, Name = "GreaterThan"},
                new Conditional() { Id = 8, Name = "GreaterThanOrEqual"},
                new Conditional() { Id = 9, Name = "LessThan"},
                new Conditional() { Id = 10, Name = "LessThanOrEqual"},
                new Conditional() { Id = 11, Name = "Contains"},
                new Conditional() { Id = 12, Name = "InList"},
                new Conditional() { Id = 13, Name = "NotInList"},
                new Conditional() { Id = 14, Name = "Like"},
                new Conditional() { Id = 15, Name = "NotLike" }
                };

            /* new SelectList(new[]
                {
                new { Id = 1, Name = "Equals"},
                new { Id = 2, Name = "NotEquals"},
                new { Id = 3, Name = "IsBlank"},
                new { Id = 4, Name = "IsNotBlank"},
                new { Id = 5, Name = "StartsWith"},
                new { Id = 6, Name = "EndsWith"},
                new { Id = 7, Name = "GreaterThan"},
                new { Id = 8, Name = "GreaterThanOrEqual"},
                new { Id = 9, Name = "LessThan"},
                new { Id = 10, Name = "LessThanOrEqual"},
                new { Id = 11, Name = "Contains"},
                new { Id = 12, Name = "InList"},
                new { Id = 13, Name = "NotInList"},
                new { Id = 14, Name = "Like"},
                new { Id = 15, Name = "NotLike" }
                }, "Id", "Name"); */

        public static List<Connector> Connectors = new List<Connector> {
                    new Connector() { Id = 1, Name = "And" },
                    new Connector() { Id = 2, Name = "Or" }
                };
                /* new SelectList(new[] 
                {
                    new { Id = 1, Name = "And" },
                    new { Id = 2, Name = "Or" }
                }, "Id", "Name"); */

        //
        // GET: /Condition/

        public ActionResult Index(int mappingId, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
            //if (null == mapping)
            //    return new HttpNotFoundResult();
            //var list = mapping.Conditions.OrderBy(c => c.Order);
            object list = null;
            ViewBag.MappingId = mappingId;
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            ViewBag.Connectors = ConditionController.Connectors;
            ViewBag.Conditionals = ConditionController.Conditionals;

            if (Request.IsAjaxRequest())
                return PartialView(list);
            return View(list);
        }

        //
        // GET: /Condition/Details/5

        public ActionResult Details(int id, int mappingId, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
            //if (null == mapping)
            //    return new HttpNotFoundResult();
            //var item = mapping.Conditions.Where(c => c.Id == id).SingleOrDefault();
            //if (null == item)
            //    return new HttpNotFoundResult();
            object item = null;

            ViewBag.MappingId = mappingId;
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            //ViewBag.ConnectorId = new SelectList(ConditionController.Connectors, "Id", "Name", item.ConnectorId);
            //ViewBag.ConditionalId = new SelectList(ConditionController.Conditionals, "Id", "Name", item.ConditionalId);

            if (Request.IsAjaxRequest())
                return PartialView("~/Views/Shared/Condition.cshtml",item);
            return View("~/Views/Shared/Condition.cshtml", item);
        }

        //
        // GET: /Condition/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Condition/Create

        [HttpPost]
        public ActionResult Create(int mappingId, int robotId, int serverId) //, int conditionalId, int connectorId, string operand1, string operand2)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                //var newId = mapping.Conditions.Max(t => t.Id) + 1;
                //var item = new Condition() { Id = newId, Order = newId };
                //mapping.Conditions.Add(item);
                Condition item = null;

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                ViewBag.Connectors = ConditionController.Connectors;
                ViewBag.Conditionals = ConditionController.Conditionals;

                return RedirectToAction("Details", new { id = item.Id, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Condition/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Condition/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, int mappingId, int robotId, int serverId, int conditionalId, int connectorId, string operand1, string operand2)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                //var item = mapping.Conditions.Where(c => c.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //item.ConditionalId = conditionalId;
                //item.ConnectorId = connectorId;
                //item.Operand1 = operand1;
                //item.Operand2 = operand2;
                Condition item = null;

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                ViewBag.Connectors = ConditionController.Connectors;
                ViewBag.Conditionals = ConditionController.Conditionals;

                return RedirectToAction("Details", new { id = item.Id, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Condition/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Condition/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, int mappingId, int robotId, int serverId)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                //var item = mapping.Conditions.Where(c => c.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //mapping.Conditions.Remove(item);

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                ViewBag.Connectors = ConditionController.Connectors;
                ViewBag.Conditionals = ConditionController.Conditionals;

                if (Request.IsAjaxRequest())
                    return RedirectToAction("Refresh", new { mappingId = mappingId, robotId = robotId, serverId = serverId });
                return RedirectToAction("Index", new { mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Refresh(int mappingId, int robotId, int serverId)
        {
            var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
            if (null == server)
                return new HttpNotFoundResult();
            var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
            if (null == robot)
                return new HttpNotFoundResult();
            //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
            //if (null == mapping)
            //    return new HttpNotFoundResult();
            //var list = mapping.Conditions.OrderBy(c => c.Order);
            IEnumerable<Condition> list = null;

            ViewBag.MappingId = mappingId;
            ViewBag.RobotId = robotId;
            ViewBag.ServerId = serverId;

            ViewBag.Connectors = ConditionController.Connectors;
            ViewBag.Conditionals = ConditionController.Conditionals;

            if (Request.IsAjaxRequest())
                return PartialView(list);
            return View(list);
        }

        [HttpPost]
        public ActionResult Copy(int id, int mappingId, int robotId, int serverId)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                //var item = mapping.Conditions.Where(c => c.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //var newId = mapping.Conditions.Max(c => c.Id) + 1;
                //var newItem = new Condition(newId, item);
                //mapping.Conditions.Add(newItem);
                int newId = 0;

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                ViewBag.Connectors = ConditionController.Connectors;
                ViewBag.Conditionals = ConditionController.Conditionals;

                return RedirectToAction("Details", new { id = newId, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Order(int id, int mappingId, int robotId, int serverId, int order)
        {
            try
            {
                var server = ParagonRCC_MVC3.Models.Server.Servers.Where(s => s.Id == serverId).SingleOrDefault();
                if (null == server)
                    return new HttpNotFoundResult();
                var robot = server.Robots.Where(r => r.Id == robotId).SingleOrDefault();
                if (null == robot)
                    return new HttpNotFoundResult();
                //var mapping = robot.Mappings.Where(m => m.Id == mappingId).SingleOrDefault();
                //if (null == mapping)
                //    return new HttpNotFoundResult();
                //var item = mapping.Conditions.Where(c => c.Id == id).SingleOrDefault();
                //if (null == item)
                //    return new HttpNotFoundResult();
                //item.Order = order;
                //save 

                ViewBag.MappingId = mappingId;
                ViewBag.RobotId = robotId;
                ViewBag.ServerId = serverId;

                ViewBag.Connectors = ConditionController.Connectors;
                ViewBag.Conditionals = ConditionController.Conditionals;

                return RedirectToAction("Details", new { id = id, mappingId = mappingId, robotId = robotId, serverId = serverId });
            }
            catch
            {
                return new HttpNotFoundResult();
            }
        }
    }
}
