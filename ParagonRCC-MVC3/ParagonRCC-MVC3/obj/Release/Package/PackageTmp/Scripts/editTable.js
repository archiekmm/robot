﻿function editTable(node, settings) {
    if (node.length <= 0) return;
    if (!settings) return;
    /* assumtptions about table structure:
    */
    /*
    theader = $(node).find('thead');
    var thead;
    if (theader.length > 0) {
        thead = $(theader);
    } else {
        
    }
    */
    var cspan = $(node).find(".create");
    cspan.addClass("ui-icon")
        .addClass("ui-icon-plusthick")
        .attr("title", "Create")
        .bind("click", settings.create);
    var rspan = $(node).find(".refresh");
    rspan.addClass("ui-icon")
        .addClass("ui-icon-refresh")
        .attr("title", "Refresh")
        .bind("click", settings.refresh);
    var espan = $(node).find(".edit");
    espan.addClass("ui-icon")
        .addClass("ui-icon-pencil")
        .attr("title", "Edit")
        .bind("click", settings.edit);
    var dspan = $(node).find(".delete");
    dspan.addClass("ui-icon")
        .addClass("ui-icon-delete")
        .attr("title", "Delete")
        .bind("click", settings.del);
    var copySpan = $(node).find(".copy");
    copySpan.addClass("ui-icon")
        .addClass("ui-icon-copy")
        .attr("title", "Copy")
        .bind("click", settings.copy);
}