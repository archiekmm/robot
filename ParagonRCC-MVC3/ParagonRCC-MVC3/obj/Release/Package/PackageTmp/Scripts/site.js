﻿// OnLoad:
var $dirty;
$(function () {
    window.onbeforeunload = function() { 
        if (checkDirty()) return 'Data not saved, are you sure you want to navigate away from the control center?';
    }
    /*
    makeMappingDialog("#mappingEditDialog", "Edit", editMapping);
    makeMappingDialog("#mappingCreateDialog", "Create", createMapping);
    makeMappingDialog("#mappingCopyDialog", "Copy", copyMapping);
    makeMappingDialog("#mappingDeleteDialog", "Delete", deleteMapping);
    makeNVPDialog();
    */
    resizeViewport();
    $(window).resize(function () {
        clearTimeout(resizeTimer);
        clearTimeout(resizeTabsTimer);
        resizeTimer = setTimeout(resizeViewport, 1);
        resizeTabsTimer = setTimeout(resizeTabs, 1);
    });
    /*
    $(document).on('click', 'a.serverLink', showServerDetails);
    $(document).on('click', 'a.robotLink', showServerDetails);
    $(document).on('click', 'tr.robotLink', showServerDetails);
    */
    // a primitive way to set the dirty flag
    $(document).on('change', 'input, select', function(event) { checkDirty.flag = true; }); //function(event) { $dirty = true; });
    $(document).on('click', 'a.nodeLink', function (event) { nodeClick(event); });
    $(document).on('click', 'tr.nodeLink', function (event) { nodeClick(event); });
    $(document).on('click', 'a.actionLink', nodeClick);
    $(document).on('click', 'a.reload', function (event) { nodeReload(event); });
    //$(document).on('click', '.mappingTabs .ui-tabs-nav span.ui-icon-pencil', function (event) { mappingEdit(event); });
    //$(document).on('click', '.mappingTabs .ui-tabs-nav span.ui-icon-copy', function (event) { mappingCopy(event); });
    //$(document).on('click', '.mappingTabs .ui-tabs-nav span.ui-icon-trash', function (event) { mappingDelete(event); });
    // Note:  the following does not trump the anchor's click event!
    $(document).on('click', '.mappingTabs .ui-tabs-nav li', mappingTabClick);
    /*
    $(document).on('click', '.ui-tabs-nav li', function(event) { 
        if (event.target == event.currentTarget) { // li itself was clicked
            if ($(event.currentTarget).children('a').length > 0) {
                $(event.currentTarget).children('a').first().click();
            }
        }
        if ($(event.target).hasClass("ui-icon-plusthick")) {
            //alert("Do create.");
            $("#mappingCreateDialog").find("input[name='url']").val($(event.target).text());
            $("#mappingCreateDialog").dialog("open");
        }
        if ($(event.target).hasClass("ui-icon-copy")) {
            //alert("Do copy.");
            $("#mappingCopyDialog").find("input[name='url']").val($(event.target).text());
            $("#mappingCopyDialog").find("input[name='tabId']").val($(event.currentTarget).children('a').first().attr('href'));
            $("#mappingCopyDialog").dialog("open");
        }
        if ($(event.target).hasClass("ui-icon-trash")) {
            //alert("Do delete.");
            $("#mappingDeleteDialog").find("input[name='url']").val($(event.target).text());
            $("#mappingDeleteDialog").find("input[name='tabId']").val($(event.currentTarget).children('a').first().attr('href'));
            $("#mappingDeleteDialog").dialog("open");
        }
        if ($(event.target).hasClass("ui-icon-pencil")) {
            //alert("Do edit.");
            $("#mappingEditDialog").find("input[name='url']").val($(event.target).text());
            $("#mappingEditDialog").find("input[name='tabId']").val($(event.currentTarget).children('a').first().attr('href'));
            $("#mappingEditDialog").dialog("open");
        }
        return false;
    });
    */
    $(document).on('click', '.conditions tr span.ui-icon-plusthick', condRowCreate);
    $(document).on('click', '.transforms tr span.ui-icon-plusthick', tranRowCreate);
    $(document).on('click', '.nvp tr span.ui-icon-plusthick', nvpRowCreate);
    $(document).on('click', '.conditions tr span.ui-icon-copy', condRowCopy);
    $(document).on('click', '.transforms tr span.ui-icon-copy', tranRowCopy);
    $(document).on('click', '.nvp tr span.ui-icon-copy', nvpRowCopy);
    $(document).on('click', '.conditions tr span.ui-icon-trash', condRowDelete);
    $(document).on('click', '.transforms tr span.ui-icon-trash', tranRowDelete);
    $(document).on('click', '.nvp tr span.ui-icon-trash', nvpRowDelete);
    $(document).on('click', '.scheduleTable span.ui-icon.schedule', schedClick);
    $(document).on('click', '.schedElementTable span.ui-icon.scheduleElement', schedElemClick);

/*
    $(document).on('click', 'tr span.ui-icon-plusthick', tableRowCreate);
    $(document).on('click', 'tr span.ui-icon-copy', tableRowCopy);
    $(document).on('click', 'tr span.ui-icon-pencil', tableRowSave);
    $(document).on('click', 'tr span.ui-icon-trash', tableRowDelete);
*/
});

function schedClick(event) {
    console.log("schedClick");
    console.log($(event.target));
    console.log($(event.currentTarget));
    if ($(event.target).hasClass("ui-icon-plusthick")) {
        var newDiv = $(event.target).closest('table').find('tbody:first').append($("#scheduleTemplate > tbody > tr").clone());
    }
    if ($(event.target).hasClass("ui-icon-copy")) {
        var newDiv = $(event.target).closest('tbody').append($(event.target).closest('tr').clone());
    }
    if ($(event.target).hasClass("ui-icon-trash")) {
        $(event.target).closest('tr').remove();
    }
    return false;
}

function schedElemClick(event) {
    console.log("schedElemClick");
    console.log($(event.target));
    console.log($(event.currentTarget));
    if ($(event.target).hasClass("ui-icon-plusthick")) {
        var newDiv = $(event.target).closest('table').find('tbody').append($("#schedElementTemplate tr").clone());
    }
    if ($(event.target).hasClass("ui-icon-copy")) {
        var newDiv = $(event.target).closest('tbody').append($(event.target).closest('tr').clone());
    }
    if ($(event.target).hasClass("ui-icon-trash")) {
        $(event.target).closest('tr').remove();
    }
    return false;
}

var resizeTimer;
var resizeTabsTimer;
function resizeViewport() {
    var winHeight = $(window).height();
    var mainPBM = $("#main").outerHeight() - $("#main").height();
    var hHeight = $("#header").outerHeight();
    var fHeight = $("#footer").outerHeight();
    $("#main").height(winHeight - (hHeight + fHeight + mainPBM));
    $("#main").css("top", hHeight);
    $("#tree").height($("#main").height()).children().height($("main").height());
    $("#content").height($("#main").height()).children().height($("main").height());
    $("#content").css("left", $("#tree").outerWidth());
    ////alert("Content left is " + $("#content").css("left"));
    $("#content").width($(window).width() - $("#tree").outerWidth());
    ////alert("Content width is " + $("#content").width());

}

function nodeClick(event) {
    if (checkDirty()) {
        if (confirm("Save changes before loading next item?")) {
            saveChanges(event);
            changeContent(event);
        } else {
            if (confirm("Changes not saved, load anyway?")) {
                changeContent(event);
            }
        }
    } else {
        changeContent(event);
    }

    event.preventDefault();
    return false;
}

function checkDirty() {
    if (checkDirty.flag) return true;
    else return false;
}

function saveChanges(event) {
    // post new values
    var robot = {
        "Audit": {},
        "Name" : $(".robotForm .robotName").val(),
        "Notification" : {},
        "Schedules" : [],
        "Input" : [],
        "Normalization" : {},
        "Lookup" : "",
        "Mapping" : "",
        "Output" : []
    };
    addToRobot(robot, ".notificationForm", "Notification");
    addSchedule(robot, ".scheduleForm", "Schedules");
    addToRobot(robot, ".normalizationForm", "Normalization");
    addToRobot(robot, ".auditForm", "Audit");
    $(".inputForm").find(".settingModel").each( function(i) {  
        var type = $(this).find(".settingType").text();
        robot.Input[i] = {}; 
        robot.Input[i].Id = $(this).find(".settingKey").text();
        robot.Input[i].Type = type;
        robot.Input[i].Value = serializeSetting(type, $(this).find(".settingValue"));  
        //return false;  
    });
    if ($(".lookupForm").length > 0)
        robot.Lookup = serializeSetting("7", ".lookupForm");
    if ($(".mappingForm").length > 0)
        robot.Mapping = serializeSetting("7", ".mappingForm");
    $(".outputForm").find(".settingModel").each( function(i) {  
        var type = $(this).find(".settingType").text();
        robot.Output[i] = {}; 
        robot.Output[i].Id = $(this).find(".settingKey").text();
        robot.Output[i].Type = type;
        robot.Output[i].Value = serializeSetting(type, $(this).find(".settingValue"));  
        //return false;  
    });
    $.ajax({
        url: $("a[href='#saveRobot']").find("span").text(),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify( { robot: robot }),
        success: function() {
            alert("save complete.");
            checkDirty.flag = false;
        }
    });
}

function addSchedule(robot, selector, section) {
    $(selector).find(".scheduleTable > tbody > tr").each(function(i) {
        robot[section][i] = {};
        robot[section][i].Name = $(this).find("input[name='name']").first().val();
        robot[section][i].Elements = [];
        $(this).find(".schedElementTable > tbody > tr").each(function(j) {
            robot[section][i].Elements[j] = {};
            robot[section][i].Elements[j]["ScheduleElementId"] = j;
            $(this).find("input,select,textarea").each(function() {
                var name = $(this).attr("name");
                if (name.substring(0, 2) == "is")
                    name = "Is" + name.charAt(2).toUpperCase() + name.substring(3);
                else
                    name = name.charAt(0).toUpperCase() + name.substring(1);
                if ($(this).is("[type='checkbox']")) {
                    robot[section][i].Elements[j][name] = $(this).is(":checked");
                } else {
                    robot[section][i].Elements[j][name] = $(this).val();
                }
            });
        });
    });
}

function addToRobot(robot, selector, section) {
    $(selector).find("input,select,textarea").each(function() {
        var name = $(this).attr("name");
        name = name.charAt(0).toUpperCase() + name.substring(1);
        if ($(this).is("[type='checkbox']")) {
            robot[section][name] = $(this).is(":checked"); 
        } else {
            robot[section][name] = $(this).val(); 
        }
    });
}

function serializeSetting(type, element) {
    switch (type) {
        case '1':
            return $(element).find("input[type='checkbox']").first().is(":checked");
        case '2':
            return parseInt($(element).find("input[type='text']").first().val());
        case '3':
            return $(element).find("input[type='text']").first().val();
        case '4':
            return $(element).find("textarea").first().val();
        case '5':
            return $(element).find("select").first().val();
        case '6':
            var rv = "";
            var self = element;
            $(self).find("tr").each(function(i) {
                if (i > 0) 
                    rv += ";"
                rv += $(this).find("input[name='name']").first().val() + "=" + 
                    $(this).find("input[name='value']").first().val();
            });
            return rv;
        case '7':
            var rv = "<mappings>";
            $(element).find(".mappingTabs .mapping").each(function(i) {
                var id = $(this).find(".id").first().text();
                var order = i + 1;
                var name = $(this).find(".name").first().text();
                rv += "<mapping id='" + id + "' order='" + order + "' name='" + name + "'>";
                $(this).find(".conditions tbody tr").each(function(j) {
                    rv += "<condition id='" + $(this).find(".id").text() + "' ";
                    rv += "order='" + (j + 1) + "' ";
                    rv += "op1='" + $(this).find(".operand1").val() + "' ";
                    rv += "op2='" + $(this).find(".operand2").val() + "' ";
                    rv += "conditionalId='" + $(this).find(".conditional").val() + "' ";
                    rv += "connectorId='" + $(this).find(".connector").val() + "' ";
                    rv += " />"
                });
                $(this).find(".transforms tbody tr").each(function(k) {
                    rv += "<transform id='" + $(this).find(".id").text() + "' ";
                    rv += "order='" + (k + 1) + "' ";
                    rv += "name='" + $(this).find(".name").val() + "' ";
                    rv += "value='" + $(this).find(".value").val() + "' ";
                    rv += " />"
                });
                rv += "</mapping>"
            });
            rv += "</mappings>"
            return rv;
        case '8':
            return $(element).find("input,select").serialize();
    }
}

function changeContent(event) {
    var href = $(event.currentTarget).attr('href');
/*
    $("#content").load(href, function(resp, stat, xhr) {
        if (stat == "success") {
            makeRobotTabs();
            resizeTabs();
        }
    });
*/
    $.ajax({
        url: href,
        type: 'GET',
        success: function (htmlData) {
            $("#content").html(htmlData);
            //setTimeout(resizeTabs, 0);
        },
        error: function (jqxhr, textStatus, errorThrown) {
            var message = "There was an error while loading " + href + ".\n";
            message += "Status value is " + textStatus + ".\n";
            message += "Thrown error was " + errorThrown + ".\n";
            message = "<p>" + message + "</p>";
            $("#content").html(message);
        }, 
        cache: false
    });
} 

function resizeTabs() {
    $(".ui-tabs, .ui-tabs > div").each(function (i, el) {
        // get siblings
        // determine my height:
        //   my parent's innerHeight minus (my outerHeight minus my innerHeight) 
        //      minus each of my siblings' outerHeight
        //added minus 10 to reduce the parent height by removing height of the bottom space bar
        var theHeight = $(this).parent().height()-10;
        theHeight -= ($(this).outerHeight() - $(this).height());
        $(this).siblings(":visible").each(function () { theHeight -= $(this).height(); });
        // set height
        $(this).height(theHeight);
        // determine my width:
        //   my parent's innerWidth minus (my outerWidth minus my innerWidth)
        var theWidth = $(this).parent().width();
        theWidth -= ($(this).outerWidth() - $(this).width());
        // set width
        $(this).width(theWidth);
    });
}

function nodeReload(event) {
    var href = $(event.currentTarget).attr('href');
    var form = $(event.currentTarget).siblings('input');
    var formData = $(form).serialize();
    href = href + "&" + formData;
    $.ajax({
        url: href,
        type: 'GET',
        success: function (htmlData) {
            $(".logForm:visible").replaceWith(htmlData);
        },
        error: function (jqxhr, textStatus, errorThrown) {
            var message = "There was an error while loading " + href + ".\n";
            message += "Status value is " + textStatus + ".\n";
            message += "Thrown error was " + errorThrown + ".\n";
            message = "<p>" + message + "</p>";
            $("#robotForm").html(message);
        }
    });
    event.preventDefault();
    return false;
}

function reloadTab(selector) {
    $(selector).tabs("load", $(selector).tabs( "option", "selected"));
}

//function addTab(server, robot, name, tabId) {
function createMapping(dialogEl) {
    //var server = $(el).children("input[name='serverId']").val();
    //var robot = $(el).children("input[name='robotId']").val();
    var href = dialogEl.find("input[name='url']").val();
    var name = dialogEl.find("input[name='name']").val();
    var tabId = $(".mappingTabs:visible").tabs("length") - 1;
    if (name) {
        // post new mapping
        $.ajax({
            url: href + "&name=" + name,
            type: 'POST',
            success: function (htmldata, txtStatus, xhr) {
                ////alert("call to " + href + "returned:\n" + htmldata);
                ////alert("call to " + href + "returned:\n" + xhr.getResponseHeader('Location'));
                $(".mappingTabs:visible").tabs("add", xhr.getResponseHeader('Location'), name, tabId);
                $(".mappingTabs:visible").tabs("select", parseInt(tabId));
            },
            error: function (xhr, txtStatus, errorThrown) {
                //alert("Error while creating tab:\n" + errorThrown);
            },
        });
    }
}

function editMapping(dialogEl) {
    var href = dialogEl.find("input[name='url']").val()
    var name = dialogEl.find("input[name='name']").val();
    var tabId = dialogEl.find("input[name='tabId']").val();
    if (name) {
        $.ajax({
            url: href + "&name=" + name,
            type: 'POST',
            success: function() {
                $(".mappingTabs:visible a[href='" + tabId + "'] span").text(name);
            },
            error: function(xhr, txtStatus, errorThrown) {
                //alert("Error while updating mapping name:\n" + errorThrown);
            }
        });
    }
}

function copyMapping(dialogEl) {
    var href = dialogEl.find("input[name='url']").val();
    var name = dialogEl.find("input[name='name']").val();
    var tabId = $(".mappingTabs:visible").tabs("length") - 1;
    if (name) {
        // post new mapping
        $.ajax({
            url: href + "&name=" + name,
            type: 'POST',
            success: function (htmldata, txtStatus, xhr) {
                ////alert("call to " + href + "returned:\n" + htmldata);
                ////alert("call to " + href + "returned:\n" + xhr.getResponseHeader('Location'));
                $(".mappingTabs:visible").tabs("add", xhr.getResponseHeader('Location'), name, tabId);
                $(".mappingTabs:visible").tabs("select", parseInt(tabId));
            },
            error: function (xhr, txtStatus, errorThrown) {
                //alert("Error while copying tab:\n" + errorThrown);
            },
        });
    }
}

function deleteMapping(dialogEl) {
    var href = dialogEl.find("input[name='url']").val()
    var tabId = dialogEl.find("input[name='tabId']").val();
    $.ajax({
        url: href,
        type: 'POST',
        success: function() {
            //$(".mappingTabs:visible a[href='" + tabId + "'] span").text(name);
            $(".mappingTabs:visible").tabs("select", 0);
            $(".mappingTabs:visible").tabs("remove", tabId);
        },
        error: function(xhr, txtStatus, errorThrown) {
            //alert("Error while deleting mapping name:\n" + errorThrown);
        }
    });
}

// makeMappingDialog("#mappingCreateDialog", "Create", createMapping);
// makeMappingDialog("#mappingEditDialog", "Edit", editMapping);
function makeMappingDialog(elementId, buttonName, buttonFunc) {
    var el = $(elementId);
    var funcName = buttonName;
    var btns = { };
    btns[funcName] = function() {
        buttonFunc($(this));
        $(this).dialog("close");
    };
    btns["Cancel"] = function() {
        $(this).dialog("close");
    };
    el.dialog({
        autoOpen: false,
        modal: true,
        buttons: btns,
        open : function() {
            if (funcName != "Delete") {
                el.find("input[name='name']").focus();
            }
        },
        close: function() {
            el.children("form")[0].reset();
        }
    });
}

function makeNVPDialog() {
    $("#nvpAddDialog").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Ok" : function () {
                var tbody = $(this).data("tbody");
                var rowTemplate = '<tr><th><input type="text" name="key" value="#{name}" readonly="readonly" /></th><td><input type="text" name="value" value="#{value}" /></td><td><span class="ui-icon ui-icon-trash" title="Delete"></span><span class="ui-icon ui-icon-copy" title="Copy"></span></td></tr>';
                var name = $(this).find("input[name='name']").val();
                var value = $(this).find("input[name='value']").val();
                tbody.append(rowTemplate.replace( /#\{name\}/g, name ).replace( /#\{value\}/g, value ));
                $(this).dialog("close");
            },
            "Cancel" : function () {
                $(this).dialog("close");
            }
        },
        open : function() {
            $(this).find("input[name='name']").focus();
        },
        close: function() {
            $(this).children("form")[0].reset();
        }
    });
}

var $mappingsTabId;
function getNextMappingTabId() {
    if ($mappingsTabId) {
        $mappingsTabId += 1;
    } else {
        $mappingsTabId = 1;
    }
    return "mappingTab" + $mappingsTabId;
}

function makeMappingTabs(element) {
    $(element).tabs({
        tabTemplate : '<li class="sortable" title="drag to sort"><a href="#{href}"><span>#{label}</span></a><span class="ui-icon ui-icon-trash" title="Delete"></span><span class="ui-icon ui-icon-copy" title="Copy"></span></li>',
        select: function (event, ui) {
            if ($(ui.tab).attr('href') == '#newTab') {
                //$("#mappingCreateDialog").find("input[name='url']").val($(ui.tab).find("span.ui-icon-plusthick").text());
                //$("#mappingCreateDialog").dialog("open");
                var name = prompt("Enter name of mapping to create", "New Mapping");
                if (name) {
                    var tabDiv = $(event.target).closest(".mappingTabs");
                    var tabIdx = $(tabDiv).tabs("length") - 1;
                    var tabId = getNextMappingTabId();
                    var newDiv = $("#mappingTabTemplate").clone().attr("id", tabId).appendTo(".mappingTabs");
                    var maxId = 1;
                    $(tabDiv).find(".mapping .id").each(function() {
                        if ($(this).text() > maxId)
                            maxId = $(this).text();
                    });
                    $(newDiv).find(".id").first().text(maxId + 1);
                    $(newDiv).find(".name").first().text(name);
                    $(tabDiv).tabs("add", "#" + tabId, name, tabIdx);
                    $(tabDiv).tabs("select", tabIdx);
                }
                return false;
            }
        }
    }).addClass('ui-tabs-vertical ui-helper-clearfix')
    .find(".ui-tabs-nav").sortable({ 
        /* update: sortMappings, */
        revert: true, 
        scroll: true, 
        opacity: 0.7, 
        axis: 'y', 
        containment: 'parent', 
        cursor: 'move', 
        items: '.sortable'
        } );
    $(element).children("li").removeClass('ui-corner-top').addClass('ui-corner-left');
    //resizeMappingPanel($(".mappingTabs:visible"));
}

function resizeMappingPanel(node) {
    var panel = $(node); //var panel = $("#mappingForm");
    var parent = $(node).parent(); // $("#ui-tabs-x");
    var tree = $(node).children('ul'); // $("#mappingTree");
    var content = $(node).children('div'); //$("#mappingContent");
    panel.css('width', '100%');
    tree.css('width', '20%');
    content.css('width', '79%');
    content.css('padding', '0');
    panel.outerHeight(parent.innerHeight());
    tree.outerHeight(panel.innerHeight());
    content.outerHeight(panel.innerHeight());
}

function mappingEdit(event) {
    //alert("mappingEdit called");
    //alert( $(event.currentTarget) );
    return false;
}

function mappingCopy(event) {
    //alert("mappingCopy called");
    //alert( $(event.currentTarget) );
    //alert("source url is " + $.data($(event.currentTarget).siblings('a')[0], "load.tabs"));
    var destUrl = $.data($(event.currentTarget).siblings('a')[0], "load.tabs").replace('Details', 'Copy');
    //alert("dest url is " + destUrl);
    return false;
}

function mappingDelete(event) {
    //alert("mappingDelete called");
    //alert( $(event.currentTarget) );
    //alert("source url is " + $.data($(event.currentTarget).siblings('a')[0], "load.tabs"));
    var destUrl = $.data($(event.currentTarget).siblings('a')[0], "load.tabs").replace('Details', 'Delete');
    //alert("dest url is " + destUrl);

    return false;
}

function makeRobotTabs() {
    $(".robotTabs").tabs({
        load: function(event, ui) {
            if (($(ui.panel).find(".inputForm").length > 0) || ($(ui.panel).find(".outputForm").length > 0) ) {
                //$(".settingForm:visible").tabs({
                $(ui.panel).children("div").tabs({
                    select: function (event, ui) {
                        if ($(ui.tab).attr('href') == '#Save') {
                            var href = $(ui.tab).find("span .ui-icon-disk").text();
                            saveSettings(href, $(".settingForm:visible div"));
                            return false;
                        }
                    }
                });
                //$(".settingForm:visible .sortable").sortable({
                $(ui.panel).find(".sortable").sortable({
                    revert: true,
                    scroll: true,
                    opacity: 0.7,
                    axis: 'y',
                    containment: 'parent',
                    cursor: 'move'
                });
            }
        }, 
        select: function (event, ui) {
            if ($(ui.tab).attr('href') == "#saveRobot") {
                saveChanges(event);
                return false;
            }
        },
        show: function (event, ui) {
            resizeTabs();
        },
        ajaxOptions: {
            error: function (xhr, status, index, anchor) {
                $(anchor.hash).html("Error loading tab.");
            }, 
            cache: true
        },
        cache: true
    });
}

/*
function sortMappings(event, ui) {
    $(".mappingTabs:visible").find('a').each( function(i) { 
        if ($.data($(this)[0], "href.tabs") ) {
            $.ajax({
                url:  ($.data($(this)[0], "href.tabs") + "&order=" + (i+1)).replace('Details', 'Order'),
                type: 'POST',
                error: function(xhr, txtStatus, errorThrown) {
                    //alert("Error while sorting mappings:\n" + errorThrown);
                }
            });
        }
    });
    //alert("mappings sorted.");
}
*/

function tableRefresh(event) {
    var span = $(event.target);
    var tbody = span.closest("table").find("tbody");
    var href = span.text();
    if (href) {
            $.ajax({
            url: href,
            type: 'GET',
            success: function (htmldata) {
                tbody.replaceWith(htmldata);
            },
            error: function(xhr, txtStatus, errorThrown) {
                alert("Error while refreshing table:\n" + errorThrown);
            }
        });
    } else {
        
        alert("No link given for table refresh; aborting.");
    }
}

function tableRowCreate(event) {
    var span = $(event.target);
    var tbody = span.closest("table").find("tbody");
    var href = span.text();
    if (href) {
        $.ajax({
            url: href,
            type: 'POST',
            success: function (htmldata) {
                tbody.append(htmldata);
            },
            error: function(xhr, txtStatus, errorThrown) {
                alert("Error while creating row:\n" + errorThrown);
            }
        });
    } else {
        $("#nvpAddDialog").data("tbody", tbody);
        $("#nvpAddDialog").dialog("open");
    }
}


function tableRowSave(event) {
    var span = $(event.target);
    var tbody = span.closest("table").find("tbody");
    var href = span.text();
    if (href) {
        var data = span.closest("tr").find("input,select").serialize();
        $.ajax({
            url: href + "&" + data,
            type: 'POST',
            success: function (htmldata) {
                alert("Row saved.");
            },
            error: function(xhr, txtStatus, errorThrown) {
                alert("Error while saving row:\n" + errorThrown);
            }
        });
    }
}

function tableRowCopy(event) {
    var span = $(event.target);
    var tbody = span.closest("table").find("tbody");
    var href = span.text();
    if (href) {
        $.ajax({
            url: href,
            type: 'POST',
            success: function (htmldata) {
                tbody.append(htmldata);
            },
            error: function(xhr, txtStatus, errorThrown) {
                alert("Error while copying table row:\n" + errorThrown);
            }
        });
    } else {
        $("#nvpAddDialog").data("tbody", tbody);
        $("#nvpAddDialog input[name='name']").val(span.closest("tr").find("th input").first().val());
        $("#nvpAddDialog input[name='value']").val(span.closest("tr").find("td input").first().val());
        $("#nvpAddDialog").dialog("open");
    }
}

function tableRowDelete(event) {
    var span = $(event.target);
    var tbody = span.closest("table").find("tbody");
    var href = span.text();
    if (href) {
        $.ajax({
            url: href,
            type: 'POST',
            success: function (htmldata) {
                span.closest("tr").remove();
            },
            error: function(xhr, txtStatus, errorThrown) {
                alert("Error while deleting row:\n" + errorThrown);
            }
        });
    } else {
        span.closest("tr").remove();
    }
}


function sortTable(event, ui) {
    $(this).children("tr").each(function (i) {
        var href = ($(this).find("td:first-child span").text() + "&order=" + (i+1)).replace('Details', 'Order');
        //alert("sorting condition:  " + href);
        $.ajax({
            url: href,
            type: 'POST',
            error: function(xhr, txtStatus, errorThrown) {
                //alert("Error while sorting conditions:\n" + errorThrown);
            }
        });
    });
    //alert("conditions sorted.");
}

/*
function sortTransforms(event, ui) {
    $(".transforms:visible table tbody tr").each(function (i) {
        var href = ($(this).find("td:first-child span").text() + "&order=" + (i+1)).replace('Details', 'Order');
        //alert("sorting transform:  " + href);
        $.ajax({
            url: href,
            type: 'POST',
            error: function(xhr, txtStatus, errorThrown) {
                //alert("Error while sorting transforms:\n" + errorThrown);
            }
        });
    });
    //alert("transforms sorted.");
}
*/
/*
function saveSettings() {
    var rows = $(".settingForm:visible div > table tbody > tr");
    $(".settingForm").data("numDone", rows.length);
    $(".settingForm").data("errors", []);
    rows.each( function(i) {
        var href = $(this).find(".ui-helper-hidden span").text() + "&value=";
        if ($(this).find("table").length > 0) {
            var value = "";
            $(this).find("table tbody tr").each(function (i) {
                if (i > 0) {
                    value += ";"
                }
                value += $(this).find("th input").first().val() + "=" + $(this).find("td input").first().val();
            });
            href += encodeURIComponent(value);
        } else {
            if ($(this).find("input[type='checkbox']").length > 0) {
                if ($(this).find("input[type='checkbox']:checked")) {
                    href += "true";
                } else {
                    href += "false";
                }
            } else {
                href += encodeURIComponent($(this).find("input,select,textarea").val());
            }
        }
        $.ajax({
            url: href,
            type: 'POST',
            success: function (htmlData) {
                //$("#content").html(htmlData);
                //setTimeout(resizeTabs, 0);
            },
            error: function (jqxhr, textStatus, errorThrown) {
               var errors = $(".settingForm").data("errors");
               errors.push("Error while saving setting #" + i +":  " + errorThrown);
               $(".settingForm").data("errors", errors);
           },
           complete: function() {
               var numDone = $(".settingForm").data("numDone");
               $(".settingForm").data("numDone", numDone - 1);
           }
       });
    });
    finishSaveSettings();
}

function finishSaveSettings() {
    var numDone = $(".settingForm").data("numDone");
    if (numDone > 0) {
        setTimeout(finishSaveSettings, 10);
    } else {
        var errors = $(".settingForm").data("errors");
        if (errors.length > 0) {
            alert("Errors occurred while saving setttings:\n" + errors.join("\n"));
        } else {
            alert("Savings saved successfully.");
        }
    }
}
*/
function mappingTabClick(event) {
    var name, tabIdx, tabDiv, tabId;
    if (event.target == event.currentTarget) { 
        // li itself was clicked, so act like the anchor was clicked, if it has one
        if ($(event.currentTarget).children('a').length > 0) {
            $(event.currentTarget).children('a').first().click();
        }
    }
    // creation handled by the tabs' 'select' function
    if ($(event.target).hasClass("ui-icon-copy")) {
        //alert("Do copy.");
        name = prompt("Enter name of copy");
        if (name) {
            tabDiv = $(event.target).closest(".mappingTabs");
            tabIdx = $(tabDiv).tabs("length") - 1;
            tabId = getNextMappingTabId();
            var copyFrom = $(event.currentTarget).children('a').first().attr('href');
            var newDiv = $(copyFrom).clone().attr("id", tabId).appendTo(tabDiv);
            var maxId = 1;
            $(tabDiv).find(".mapping .id").each(function() {
                if ($(this).text() > maxId)
                    maxId = $(this).text();
            });
            $(newDiv).find(".id").first().text(maxId + 1);
            newDiv.find(".name").first().text(name);
            $(tabDiv).tabs("add", "#" + tabId, name, tabIdx);
            $(tabDiv).tabs("select", tabIdx);
        }
    }
    if ($(event.target).hasClass("ui-icon-trash")) {
        //alert("Do delete.");
        tabDiv = $(event.target).closest(".mappingTabs");
        $(tabDiv).tabs("remove", $(event.currentTarget).children('a').first().attr('href'));
    }
    if ($(event.target).hasClass("ui-icon-pencil")) {
        //alert("Do edit.");
        name = prompt("Enter new name");
        if (name) {
            $(event.target).parent().find('a span').first().text(name);
        }
    }
    return false;
}

function condRowCreate(event) {
    var newDiv = $(event.target).closest('table').find('tbody').append($("#conditionRowTemplate tr").clone());
    var maxId = 1;
    $(event.target).closest('table span.id').each(function() {
        if ($(this).text() > maxId)
            maxId = $(this).text();
    });
    $(newDiv).find('span.id').text(maxId);
}

function tranRowCreate(event) {
    var newDiv = $(event.target).closest('table').find('tbody').append($("#transformRowTemplate tr").clone());
    var maxId = 1;
    $(event.target).closest('table span.id').each(function() {
        if ($(this).text() > maxId)
            maxId = $(this).text();
    });
    $(newDiv).find('span.id').text(maxId);
}

function nvpRowCreate(event) {
    $(event.target).closest('table').find('tbody').append($('<tr><td><input name="name" /></td><td><input name="value" /></td></tr>'));
}

function condRowCopy(event) {
    var newDiv = $(event.target).closest('tbody').append($(event.target).closest('tr').clone());
    var maxId = 1;
    $(event.target).closest('table span.id').each(function() {
        if ($(this).text() > maxId)
            maxId = $(this).text();
    });
    $(newDiv).find('span.id').text(maxId);
}

function tranRowCopy(event) {
    var newDiv = $(event.target).closest('tbody').append($(event.target).closest('tr').clone());
    var maxId = 1;
    $(event.target).closest('table span.id').each(function() {
        if ($(this).text() > maxId)
            maxId = $(this).text();
    });
    $(newDiv).find('span.id').text(maxId);
}

function nvpRowCopy(event) {
    $(event.target).closest('tbody').append($(event.target).closest('tr').clone());
}

function condRowDelete(event) {
    $(event.target).closest('tr').remove();
}

function tranRowDelete(event) {
    $(event.target).closest('tr').remove();
}

function nvpRowDelete(event) {
    $(event.target).closest('tr').remove();
}

