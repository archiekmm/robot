﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace ParagonRCC_MVC3.Models
{
    public class Login
    {
        public string Provider;
        public string Server;
        public string Database;
        public string UserId;
        public string Password;

        public Login() { }

        public Login(string details)
        {
            string[] kvpItems = details.Split("&".ToCharArray());
            foreach (var item in kvpItems)
            {
                string[] kvp = item.Split("=".ToCharArray(), 2);
                switch (kvp[0].ToLower())
                {
                    case "provider":
                        this.Provider = kvp[1];
                        break;
                    case "server":
                        this.Server = kvp[1];
                        break;
                    case "database":
                        this.Database = kvp[1];
                        break;
                    case "userid":
                        this.UserId = kvp[1];
                        break;
                    case "password":
                        this.Password = kvp[1];
                        //Decryptdata(kvp[1]);
                        break;
                }
            }
        }

        
    }
}