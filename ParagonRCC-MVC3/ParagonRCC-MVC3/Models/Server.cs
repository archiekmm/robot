﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Paragon.RobotServices.Model;

namespace ParagonRCC_MVC3.Models
{
    public class Server
    {
        public static readonly List<Server> Servers = new List<Server> 
        { 
            new Server() { Name = "PAE Production", Host = "localhost", Port = "8080", Id = 1}, 
            new Server() { Name = "PAE QA", Host = "localhost", Port = "8081", Id = 2} 
        };

        public Server()
        {
            InputModules = new List<Paragon.RobotServices.Model.Module>();
            OutputModules = new List<Paragon.RobotServices.Model.Module>();
            Robots = new List<Paragon.RobotServices.Model.Robot>();
            //InputModules = new List<Paragon.RobotServices.Model.Module>();
            //InputModules.Add(new Module("InputFS.dll") { Id = 1, Name = "FileSystem", Type = "FileSystem"});
            //InputModules.Add(new Module("InputUnisys.dll") { Id = 2, Name = "Unisys", Type = "DMS"});
            //InputModules.Add(new Module("InputSQLServer.dll") { Id = 3, Name = "Database", Type = "Database"});
            //InputModules.Add(new Module("InputEmail.dll") { Id = 4, Name = "Email", Type = "Email"});

            //OutputModules = new List<Module>
            //    {
            //        new Module("OutputFS.dll") { Id = 1, Name = "FileSystem", Type = "FileSystem"},
            //        new Module("OutputUnisys.dll") { Id = 2, Name = "Unisys", Type = "DMS"},
            //        new Module("OutputSQLServer.dll") { Id = 3, Name = "Database", Type = "Database"},
            //        new Module("OutputEmail.dll") { Id = 4, Name = "Email", Type = "Email"},
            //        new Module("OutputKofax.dll") { Id = 5, Name = "Kofax", Type = "Capture"},
            //        new Module("OutputEDI.dll") { Id = 6, Name = "EDI", Type = "EDI"}
            //    };

            //Robots = new List<Robot>
            //    {
            //        //new Robot(2, 2) { Name = "Unisys Auto Foldering", Id = 1, Status = RobotStatus.Errors},
            //        //new Robot(1, 5) { Name = "FS Import to Kofax", Id = 2, Status = RobotStatus.Stopped},
            //        //new Robot(1, 2) { Name = "FS Import to Unisys", Id = 3, Status = RobotStatus.Running}
            //        new Robot() { Name = "Unisys Auto Foldering", Id = 1, Status = "errors", InputModuleId = 2, OutputModuleId = 2},
            //        new Robot() { Name = "FS Import to Kofax", Id = 2, Status = "stopped", InputModuleId = 1, OutputModuleId = 5},
            //        new Robot() { Name = "FS Import to Unisys", Id = 3, Status = "running", InputModuleId = 1, OutputModuleId = 2}
            //    };
        }

        public string Name { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public int Id { get; set; }
        public Uri Uri { get; set; }

        public List<Paragon.RobotServices.Model.Robot> Robots { get; protected set; }
        public List<Paragon.RobotServices.Model.Module> InputModules { get; protected set; }
        public List<Paragon.RobotServices.Model.Module> OutputModules { get; protected set; }
    }
}