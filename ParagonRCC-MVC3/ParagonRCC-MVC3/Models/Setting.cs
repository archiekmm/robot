﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    public class Setting
    {
        public int Id;
        public string Name;
        public string Value;
        public string Type;
    }
}