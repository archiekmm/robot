﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace ParagonRCC_MVC3.Models
{
    public class ConnectionConfiguration : Dictionary<string, string>
    {
        public ConnectionConfiguration(string connectionString) : base( connectionString.ToDictionary(";", "="))
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            if (this.ContainsKey("ProviderName"))
                ProviderName = this["ProviderName"];
        }

        public static ConnectionConfiguration Create(string connectionString)
        {
            return new ConnectionConfiguration(connectionString);
        }

        public string ProviderName { get; protected set; }

        public override string ToString()
        {
            return ((IDictionary<string, string>) this).ToString();
        }
    }
}