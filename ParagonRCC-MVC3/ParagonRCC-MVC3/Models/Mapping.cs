﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ParagonRCC_MVC3.Models
{
    public class Mapping
    {
        public int Id;
        public string Name;
        public int Order;
        public List<Condition> Conditions = new List<Condition> { };
        public List<Transform> Transforms = new List<Transform> { };

        public Mapping() { }

        public Mapping(XElement el)
        {
            if (el.Attribute("id") != null)
                int.TryParse(el.Attribute("id").Value, out this.Id);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'id' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("order") != null)
                int.TryParse(el.Attribute("order").Value, out this.Order);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'order' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("name") != null)
                this.Name = el.Attribute("name").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'name' attribute.", el.ToString()));
            Conditions.AddRange(Condition.FromElements(el.Elements("condition")));
            Transforms.AddRange(Transform.FromElements(el.Elements("transform")));
        }
        
        public static List<Mapping> FromXml(string xml)
        {
            if (string.IsNullOrEmpty(xml))
                xml = @"<mappings><mapping id='1' order='1' name='default'></mapping></mappings>";
            List<Mapping> rv = new List<Mapping>();
            try
            {
                XElement root = XElement.Parse(xml);
                rv.AddRange(root.Elements("mapping").Select(e => new Mapping(e)));
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return rv;
        }
    }
}