﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    public enum LogSeverity
    {
        Debug,
        Normal,
        Warning,
        Error,
        Critical
    }

    public class LogEntry
    {
        public int Id;
        public DateTime EventDate;
        public LogSeverity Severity;
        public string Message;
    }
}