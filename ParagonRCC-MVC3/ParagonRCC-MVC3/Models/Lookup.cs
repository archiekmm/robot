﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    public class Lookup
    {
        public int Id;
        
        public bool LookupEnabled;
        public bool ErrorNoMatch;
        public bool ErrorNoResults;
        public int LookupMatchTypeId;
        public string ConnectionString;
        public string Query;
        public ICollection<Mapping> Mappings = new List<Mapping> 
            { 
                new Mapping()
                {
                    Id = 1,
                    Order = 1,
                    Name = "Test",
                    Conditions = new List<Condition> 
                    {
                        new Condition() { Id = 1, Operand1 = "SourceFolder", ConditionalId = 1, Operand2 = @"C:\Windows", ConnectorId = 1 }
                    },
                    Transforms = new List<Transform>
                    {
                        new Transform() { Id = 1, Name = "TName", Value = "TValue" }
                    }
                },
                new Mapping()
                {
                    Id = 2,
                    Order = 2,
                    Name = "Test2",
                    Conditions = new List<Condition> 
                    {
                        new Condition() { Id = 2, Operand1 = "Folder", ConditionalId = 1, Operand2 = @"C:\Users", ConnectorId = 1 }
                    },
                    Transforms = new List<Transform>
                    {
                        new Transform() { Id = 2, Name = "2Name", Value = "2Value" }
                    }
                }

            };

    }

    public class LookupMatchType
    {
        public int Id;
        public string Name;

        public static readonly ICollection<LookupMatchType> Types = new List<LookupMatchType>
        {
            new LookupMatchType() { Id = 1, Name = "Single" },
            new LookupMatchType() { Id = 2, Name = "Multiple" }
        };
    }

}