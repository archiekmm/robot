﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    /*
    public class Module
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<Setting> Settings { get; protected set; }
        
        public string Type { get; set; }
        public string File { get; protected set; }

        public Module(string file)
        {
            if (string.IsNullOrEmpty(file)) 
                throw new ArgumentNullException(file);
            File = file;
            Settings = BuildSettings(file);
        }

        public IList<Setting> BuildSettings(string file)
        {
            switch (file) 
            {
                case "InputFS.dll":
                    return InputFS();
                case "InputUnisys.dll":
                    return InputUnisys();
                case "InputSQLServer.dll":
                    return InputSQLServer();
                case "InputEmail.dll":
                    return InputEmail();
                case "OutputFS.dll":
                    return OutputFS();
                case "OutputUnisys.dll":
                    return OutputUnisys();
                case "OutputSQLServer.dll":
                    return OutputSQLServer();
                case "OutputEmail.dll":
                    return OutputEmail();
                case "OutputKofax.dll":
                    return OutputKofax();
                case "OutputEDI.dll":
                    return OutputEDI();
                default:
                    throw new ArgumentOutOfRangeException(file);
            }
        }

        public IList<Setting> InputFS()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="Folders", Group="Folders", Label="Input Folders", Value=@"Input1=C:\Input;Input2=\\share\name\", SettingTypeId=6 },
                    new Setting() { Id=2, Name="Method", Group="Folders", Label="Poll Method", Value="1", SettingTypeId=5 },
                    new Setting() { Id=3, Name="Depth", Group="Folders", Label="Depth", Value="3;5", SettingTypeId=2, HelpText = "Subfolder depth; use -1 for 'all' or 0 to disable" },
                    new Setting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            var poll = rv.Where(s => s.Label == "Poll Method").Single();
            poll.Values.Add("Round Robin", "1");
            poll.Values.Add("Whole Folders First", "2");
            return rv;
        }

        public IList<Setting> InputUnisys()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new Setting() { Id=2, Name="Workset", Group="Worksets", Label="Input Workset", Value="AutoFolder", SettingTypeId=3 },
                    new Setting() { Id=3, Name="Discard", Group="Worksets", Label="Remove from Workflow", Value="false", SettingTypeId=1 },
                    new Setting() { Id=4, Name="Destination", Group="Worksets", Label="Dest Workset", Value="", SettingTypeId=3, DependsOnId=3, EnabledOnValues="true" },
                    new Setting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            return rv;
        }
        public IList<Setting> InputSQLServer()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new Setting() { Id=2, Name="Folders", Group="Folders", Label="Input Folders", Value=@"Input1=C:\Input;Input2=\\share\name\", SettingTypeId=6 },
                    new Setting() { Id=3, Name="Method", Group="Folders", Label="Poll Method", Value="1", SettingTypeId=5 },
                    new Setting() { Id=4, Name="Depth", Group="Folders", Label="Depth", Value="3;5", SettingTypeId=2, HelpText = "Subfolder depth; use -1 for 'all' or 0 to disable" },
                    new Setting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            var poll = rv.Where(s => s.Label == "Poll Method").Single();
            poll.Values.Add("Round Robin", "1");
            poll.Values.Add("Whole Folders First", "2");
            return rv;
        }
        public IList<Setting> InputEmail()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new Setting() { Id=3, Name="Protocol", Group="Server", Label="Protocol", Value="1", SettingTypeId=5 },
                    new Setting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new Setting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new Setting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            var poll = rv.Where(s => s.Label == "Protocol").Single();
            poll.Values.Add("IMAP", "1");
            poll.Values.Add("POP3", "2");
            return rv;
        }
        public IList<Setting> OutputFS()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="test", Group="General", Label="Test", Value="false", SettingTypeId=1 },
                    new Setting() { Id=2, Name="test2", Group="General", Label="Test 2", Value="3", SettingTypeId=3 },
                    new Setting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new Setting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new Setting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            return rv;
        }
        public IList<Setting> OutputUnisys()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=6, Name="Sharing", Group="Misc", Label="Login Sharing", Value="false", SettingTypeId=1},
                    new Setting() { Id=1, Name="Login", Group="Misc", Label="Login", Value="", SettingTypeId=8, EnabledOnValues="false", DependsOnId=6 },
                    new Setting() { Id=2, Name="Workset", Group="Worksets", Label="Input Workset", Value="AutoFolder", SettingTypeId=3 },
                    new Setting() { Id=3, Name="Discard", Group="Worksets", Label="Remove from Workflow", Value="false", SettingTypeId=1 },
                    new Setting() { Id=4, Name="Destination", Group="Worksets", Label="Dest Workset", Value="", SettingTypeId=3, DependsOnId=3, EnabledOnValues="true" },
                    new Setting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            return rv;
        }
        public IList<Setting> OutputSQLServer()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="test", Group="General", Label="Test", Value="false", SettingTypeId=1 },
                    new Setting() { Id=2, Name="test2", Group="General", Label="Test 2", Value="3", SettingTypeId=3 },
                    new Setting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new Setting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new Setting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            return rv;
        }
        public IList<Setting> OutputEmail()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new Setting() { Id=2, Name="Protocol", Group="Server", Label="Protocol", Value="1", SettingTypeId=5 },
                    new Setting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new Setting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new Setting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            var proto = rv.Where(setting => setting.Name == "Protocol").Single();
            proto.Values.Add("SMTP", "1");
            return rv;
        }
        public IList<Setting> OutputKofax()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new Setting() { Id=2, Name="BatchSize", Group="Batch", Label="Batch size", Value="20", SettingTypeId=2 },
                    new Setting() { Id=3, Name="Mappings", Group="Mappings", Label="Mappings", Value="", SettingTypeId=7 },
                };
            return rv;
        }
        public IList<Setting> OutputEDI()
        {
            var rv = new List<Setting>
                {
                    new Setting() { Id=1, Name="test", Group="General", Label="Test", Value="false", SettingTypeId=1 },
                    new Setting() { Id=2, Name="test2", Group="General", Label="Test 2", Value="3", SettingTypeId=3 },
                    new Setting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new Setting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new Setting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            return rv;
        }
    }

    public class Setting
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
        public string HelpText { get; set; }
        public readonly Dictionary<string, string> Values = new Dictionary<string, string>();
        public int DependsOnId { get; set; }
        public string EnabledOnValues { get; set; }
        public int SettingTypeId { get; set; }

        public Setting() { }

        public Setting(Setting s)
        {
            Id = s.Id;
            Name = s.Name;
            Group = s.Group;
            Value = s.Value;
            Label = s.Label;
            HelpText = s.HelpText;
            DependsOnId = s.DependsOnId;
            EnabledOnValues = s.EnabledOnValues;
            SettingTypeId = s.SettingTypeId;
            foreach (var kvp in s.Values)
                Values.Add(kvp.Key, kvp.Value);
        }
    }

    public class SettingType
    {
        public int Id { get; protected set;}
        public string Type { get; protected set;}

        public readonly static List<SettingType> Types = new List<SettingType>
            {
                new SettingType(1, "bool"),
                new SettingType(2, "int"),
                new SettingType(3, "string"),
                new SettingType(4, "list"),
                new SettingType(5, "selectList"),
                new SettingType(6, "dict"),
                new SettingType(7, "mapping"),
                new SettingType(8, "login")
            };

        public SettingType(int id, string type)
        {
            Id = id;
            Type = type;
        }
    }
    */
}