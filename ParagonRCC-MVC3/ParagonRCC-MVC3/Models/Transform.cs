﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ParagonRCC_MVC3.Models
{
    public class Transform
    {
        public int Id;
        public string Name;
        public string Value;
        public int Order;

        public Transform() { }

        public Transform(int newId, Transform t)
        {
            this.Id = newId;
            this.Name = t.Name;
            this.Value = t.Value;
            this.Order = newId;
        }

        public Transform(XElement el)
        {
            if (el.Attribute("id") != null)
                int.TryParse(el.Attribute("id").Value, out this.Id);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'id' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("order") != null)
                int.TryParse(el.Attribute("order").Value, out this.Order);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'order' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("name") != null)
                this.Name = el.Attribute("name").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'name' attribute.", el.ToString()));
            if (el.Attribute("value") != null)
                this.Value = el.Attribute("value").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'value' attribute.", el.ToString()));
        }

        public static List<Transform> FromElements(IEnumerable<XElement> els)
        {
            List<Transform> rv = new List<Transform>();
            rv.AddRange(els.Select(e => new Transform(e)));
            return rv;
        }
    }
}