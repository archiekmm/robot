﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    public class Audit
    {
        public int Id { get; set; }
        public bool Enabled { get; set; }
        public Login Login { get; set; }
        public string AuditTableName { get; set; }
        public string IdFieldName { get; set; }
        public string SuccessIndicator { get; set; }
        public string ErrorIndicator { get; set; }

        public Audit() 
        {
            Enabled = false;
            Login = new Login();
        }

        public Audit(AuditSaveSettings save)
        {
            this.Id = save.Id;
            this.Enabled = save.Enabled;
            this.Login = new Login(save.AsConnString());
            this.AuditTableName = save.AuditTableName;
            this.IdFieldName = save.IdFieldName;
            this.SuccessIndicator = save.SuccessIndicator;
            this.ErrorIndicator = save.ErrorIndicator;
        }
    }

    public class AuditSaveSettings
    {
        public int Id { get; set;}
        public bool Enabled {get; set;}
        public string Provider {get; set;}
        public string Server {get; set;}
        public string Database {get; set;}
        public string UserId {get; set;}
        public string Password {get; set;}
        public string AuditTableName { get; set; }
        public string IdFieldName { get; set; }
        public string SuccessIndicator { get; set; }
        public string ErrorIndicator { get; set; }

        public string AsConnString()
        {
            return string.Format("provider={0}&server={1}&database={2}&userid={3}&password={4}",
                Provider, Server, Database, UserId, Password);
        }
    }
}