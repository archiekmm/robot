﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    public enum RobotStatus
    {
        Warnings,
        Errors,
        Stopped,
        Running
    }
    /*
    public class Robot
    {
        protected static int lastId = 0;
        protected Server server;

        public Robot(Server s, int inputModuleId, int outputModuleId)
        {
            server = s;
            if (!s.InputModules.Select(m => m.Id).Contains(inputModuleId))
                throw new ArgumentOutOfRangeException(string.Format("Input Module Id {0} doesn't exist on server {1}", inputModuleId, s.Name));
            if (!s.OutputModules.Select(m => m.Id).Contains(outputModuleId))
                throw new ArgumentOutOfRangeException(string.Format("Output Module Id {0} doesn't exist on server {1}", outputModuleId, s.Name));

            this.InputModuleId = inputModuleId;
            this.OutputModuleId = outputModuleId;
            InputSettings = new List<Setting>();
            OutputSettings = new List<Setting>();

            foreach (Setting set in s.InputModules.Where(m => m.Id == inputModuleId).Single().Settings)
                InputSettings.Add(new Setting(set));
            foreach (Setting set in s.OutputModules.Where(m => m.Id == outputModuleId).Single().Settings)
                OutputSettings.Add(new Setting(set));

            this.Audit = new Audit();
        }

        public int Id;
        public string Name;
        public RobotStatus Status;
        public ICollection<LogEntry> Logs = new List<LogEntry>
            {
                new LogEntry() { Id = 1, EventDate = DateTime.Now.AddSeconds(-100), Severity = LogSeverity.Critical, Message = "Critical message 1!" },
                new LogEntry() { Id = 2, EventDate = DateTime.Now.AddSeconds(-90), Severity = LogSeverity.Debug, Message = "Debug message 2!" },
                new LogEntry() { Id = 3, EventDate = DateTime.Now.AddSeconds(-80), Severity = LogSeverity.Error, Message = "Error message 3!" },
                new LogEntry() { Id = 4, EventDate = DateTime.Now.AddSeconds(-70), Severity = LogSeverity.Warning, Message = "Warning message 4!" },
                new LogEntry() { Id = 5, EventDate = DateTime.Now.AddSeconds(-60), Severity = LogSeverity.Normal, Message = "Normal message 5!" },
                new LogEntry() { Id = 6, EventDate = DateTime.Now.AddSeconds(-50), Severity = LogSeverity.Critical, Message = "Critical message 6!" },
                new LogEntry() { Id = 7, EventDate = DateTime.Now.AddSeconds(-40), Severity = LogSeverity.Debug, Message = "Debug message 7!" },
                new LogEntry() { Id = 8, EventDate = DateTime.Now.AddSeconds(-30), Severity = LogSeverity.Error, Message = "Error message 8!" },
                new LogEntry() { Id = 9, EventDate = DateTime.Now.AddSeconds(-20), Severity = LogSeverity.Warning, Message = "Warning message 9!" },
                new LogEntry() { Id = 10, EventDate = DateTime.Now.AddSeconds(-10), Severity = LogSeverity.Normal, Message = "Normal message 10!" },
            };
        public ICollection<Schedule> Schedules = new List<Schedule> 
            {
                new Schedule() { Id = 1, Name = "8AM - 5PM M-F"}
            };

        public Notification Notification = new Notification();
        public Normalization Normalization = new Normalization();
        
        public string Mapping;
        public string Lookup;

        public int InputModuleId { get; protected set; }
        public int OutputModuleId { get; protected set; }

        public IList<Setting> InputSettings { get; protected set; }
        public IList<Setting> OutputSettings { get; protected set; }

        public Audit Audit { get; set; }
    }
    public class RobotSaveModel
    {
        public string Name { get; set; } 
        //public Dictionary<int, Schedule> Schedules = new Dictionary<int, Schedule>();
        public Notification Notification { get; set; }
        public Normalization Normalization { get; set; }
        public string Mapping {get; set;}
        public string Lookup { get; set; }
        public List<SettingSaveModel> Input { get; set; }
        public List<SettingSaveModel> Output { get; set; }
        public AuditSaveSettings Audit { get; set; }
        public List<Paragon.RobotServices.Model.Schedule> Schedules { get; set; }
    }

    public class SettingSaveModel
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
    }
    */
}