﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    public class Condition
    {
        public int Id;
        public string Operand1;
        public string Operand2;
        public int ConnectorId;// {get; set; }
        public int ConditionalId;// {get; set; }
        public int Order;

        public Condition() { ConditionalId = 1; ConnectorId = 1; }

        public Condition(int newId, Condition c)
        {
            this.Id = newId;
            this.Operand1 = c.Operand1;
            this.Operand2 = c.Operand2;
            this.ConditionalId = c.ConditionalId;
            this.ConnectorId = c.ConnectorId;
            this.Order = newId;
        }

        public Condition(XElement el)
        {
            if (el.Attribute("id") != null)
                int.TryParse(el.Attribute("id").Value, out this.Id);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'id' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("order") != null)
                int.TryParse(el.Attribute("order").Value, out this.Order);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'order' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("op1") != null)
                this.Operand1 = el.Attribute("op1").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'op1' attribute.", el.ToString()));
            if (el.Attribute("op2") != null)
                this.Operand2 = el.Attribute("op2").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'op2' attribute.", el.ToString()));
            if (el.Attribute("conditionalId") != null)
                int.TryParse(el.Attribute("conditionalId").Value, out this.ConditionalId);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'conditionalId' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("connectorId") != null)
                int.TryParse(el.Attribute("connectorId").Value, out this.ConnectorId);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'connectorId' attribute, or the attribute is not a number.", el.ToString()));
        }

        public static List<Condition> FromElements(IEnumerable<XElement> els)
        {
            List<Condition> rv = new List<Condition>();
            rv.AddRange(els.Select(e => new Condition(e)));
            return rv;
        }
    }

    public class Connector
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static readonly ReadOnlyCollection<Connector> Connectors = new ReadOnlyCollection<Connector>( new List<Connector>
            {
                new Connector() { Id = 1, Name = "And" },
                new Connector() { Id = 2, Name = "Or" }
            }
        );
    }

    public class Conditional
    {
        public int Id {get; set; }
        public string Name {get; set; }

        public static readonly ReadOnlyCollection<Conditional> Conditionals = new ReadOnlyCollection<Conditional>(new List<Conditional>
            {
                new Conditional() { Id = 1, Name = "Equals"},
                new Conditional() { Id = 2, Name = "NotEquals"},
                new Conditional() { Id = 3, Name = "IsBlank"},
                new Conditional() { Id = 4, Name = "IsNotBlank"},
                new Conditional() { Id = 5, Name = "StartsWith"},
                new Conditional() { Id = 6, Name = "EndsWith"},
                new Conditional() { Id = 7, Name = "GreaterThan"},
                new Conditional() { Id = 8, Name = "GreaterThanOrEqual"},
                new Conditional() { Id = 9, Name = "LessThan"},
                new Conditional() { Id = 10, Name = "LessThanOrEqual"},
                new Conditional() { Id = 11, Name = "Contains"},
                new Conditional() { Id = 12, Name = "InList"},
                new Conditional() { Id = 13, Name = "NotInList"},
                new Conditional() { Id = 14, Name = "Like"},
                new Conditional() { Id = 15, Name = "NotLike" }
            }
        );
    }
}