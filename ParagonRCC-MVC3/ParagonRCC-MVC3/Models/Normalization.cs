﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParagonRCC_MVC3.Models
{
    /*
    public class Normalization
    {
        public int NormalizationId {get; set;}
        public string Name {get; set;}
        public bool Normalize {get; set;}
        public bool ForceTifExtension {get; set;}
        public int TiffCompression {get; set;}
        public int Strips {get; set;}
        public int FillOrder {get; set;}
        public int MatchResolutions {get; set;}
        public int RoundResolutions {get; set;}
        public int RotateImages {get; set;}
        public int InvertImages {get; set;}
        public int MirrorImages {get; set;}
        public int ColorReduction {get; set;}

        public Normalization()
        {
            NormalizationId = 0;
            Name = "Normalization";
            Normalize = true;
            ForceTifExtension = true;
            TiffCompression = 1;
            Strips = 1;
            FillOrder = 1;
            MatchResolutions = 1;
            RoundResolutions = 1;
            RotateImages = 1;
            InvertImages = 1;
            MirrorImages = 1;
            ColorReduction = 1;
        }
    }

    public class TiffCompression
    {
        public int TiffCompressionId;
        public string Name;
    }

    public class Strips
    {
        public int StripsId;
        public string Name;
    }

    public class FillOrder
    {
        public int FillOrderId;
        public string Name;
    }

    public class MatchResolutions
    {
        public int MatchResolutionsId;
        public string Name;
    }

    public class RoundResolutions
    {
        public int RoundResolutionsId;
        public string Name;
    }

    public class RotateImages
    {
        public int RotateImagesId;
        public string Name;
    }

    public class InvertImages
    {
        public int InvertImagesId;
        public string Name;
    }

    public class MirrorImages
    {
        public int MirrorImagesId;
        public string Name;
    }

    public class ColorReduction
    {
        public int ColorReductionId;
        public string Name;
    }

    */
}