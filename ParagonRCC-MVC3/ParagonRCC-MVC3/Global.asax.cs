﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml.Linq;
using Paragon.RobotServices.Model;

namespace ParagonRCC_MVC3
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected static string xmlPath;

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            xmlPath = Server.MapPath("servers.xml");
            XElement el = XElement.Load(xmlPath);
            //var rdcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
            //var mdcs = new System.Runtime.Serialization.DataContractSerializer(typeof(IEnumerable<Paragon.RobotServices.Model.Module>));
            //System.IO.FileStream rms = new System.IO.FileStream(Server.MapPath("robots.xml"), System.IO.FileMode.Create);
            //System.IO.FileStream mms = new System.IO.FileStream(Server.MapPath("modules.xml"), System.IO.FileMode.Create);
            //var server = ParagonRCC_MVC3.Models.Server.Servers.First();
            //var mods = server.InputModules.Concat(server.OutputModules);
            //rdcs.WriteObject(rms, ParagonRCC_MVC3.Models.Server.Servers.First().Robots);
            //mdcs.WriteObject(mms, mods);
            //rms.Close();
            //mms.Close();
            ParagonRCC_MVC3.Models.Server.Servers.Clear();
            ParagonRCC_MVC3.Models.Server.Servers.AddRange(
                    el.Elements().Select((e, i) =>
                    {
                        Uri url = new Uri(e.Value);
                        return new ParagonRCC_MVC3.Models.Server() { Name = e.Name.LocalName.Replace("_", " "), Host = url.Host, Id = i, Port = url.Port.ToString(), Uri = url };
                    })
                );
            //foreach (var s in ParagonRCC_MVC3.Models.Server.Servers)
            //{
            //    try
            //    {
            //        var x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(s.Uri.ToString() + "/robots/");
            //        x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
            //        var resp = (System.Net.HttpWebResponse)x.GetResponse();
            //        var xs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
            //        //var ms = new System.IO.MemoryStream(r);
            //        var bots = (List<Paragon.RobotServices.Model.Robot>)xs.ReadObject(resp.GetResponseStream());
            //        s.Robots.Clear();
            //        s.Robots.AddRange(bots);
            //        x = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(s.Uri.ToString() + "/modules/");
            //        x.CachePolicy = new System.Net.Cache.HttpRequestCachePolicy(System.Net.Cache.HttpRequestCacheLevel.NoCacheNoStore);
            //        var mResp = (System.Net.HttpWebResponse)x.GetResponse();
            //        var xsm = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Module>));
            //        //var msm = new System.IO.MemoryStream(m);
            //        var modules = (List<Paragon.RobotServices.Model.Module>)xsm.ReadObject(mResp.GetResponseStream());
            //        s.InputModules.Clear();
            //        s.InputModules.AddRange(modules.Where(mm => mm.Type == "input"));
            //        s.OutputModules.Clear();
            //        s.OutputModules.AddRange(modules.Where(mm => mm.Type == "output"));
            //    }
            //    catch
            //    {
            //    }
            //}
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_End()
        {
            XElement servers = new XElement("servers");
            foreach (var s in ParagonRCC_MVC3.Models.Server.Servers)
                servers.Add(new XElement(s.Name.Replace(" ", "_"), new Uri("http://" + s.Host + ":" + s.Port)));
            servers.Save(xmlPath);
        }
    }

}