﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public class Consumer : IOutputModule
    {
        public long Process(IWorkitem item)
        {
            return 1;
        }

        public long ProcessBatch(IEnumerable<IWorkitem> items)
        {
            return items.Count();
        }

        public bool SupportsBatching
        {
            get { return true; }
        }

        public int Id
        {
            get;
            set;
        }

        protected string _name;
        public string Name
        {
            get
            {
                return "Consumer";
            }
            set { _name = value; }
        }

        public string FileName
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().Location;
            }
            set { _name = value; }
        }

        public string Type
        {
            get
            {
                return "output";
            }
            set { _name = value; }
        }

        public Consumer(IDictionary<int, string> settings)
        {
            settings.FirstOrDefault();
        }


        public bool RequiresConnection
        {
            get { return false; }
        }

        public IConnector Connector
        {
            get { return null; }
            set { }
        }

        public bool Connect()
        {
            throw new NotSupportedException();
        }

        public bool IsConnected
        {
            get { return false; }
        }

        public bool ShareConnector { get { return false; } }

        public void Disconnect()
        {
            throw new NotSupportedException();
        }

        public IList<Paragon.RobotServices.Model.LogEntry> Logger { get; set; }
    }
}
