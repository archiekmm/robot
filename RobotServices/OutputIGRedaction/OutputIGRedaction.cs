﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;
using Paragon.RobotServices.Plugin;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using NLog;



namespace Paragon.RobotServices.Plugin
{
    public class OutputIGRedaction : IOutputModule
    {
        protected IList<Model.LogEntry> logger;
        protected int id;
        protected string name;
        protected string fileName;
        protected string Mappings = "";
        protected string kind;
        protected string outputDirectory;
        protected bool isRobotconnected;
        private bool replaceOriginal;
        private string startPhrases;
        private string endPhrases;
        private bool doDeskew;
        ParagonIGRedaction.clsProcess paraIGRedaction = new ParagonIGRedaction.clsProcess();
        static Logger nlogger = LogManager.GetLogger("OutputIGRedaction");
        private string comments = "";
        private bool exceptionFlag = false;

        public OutputIGRedaction(IDictionary<int, string> settings)
        {
            isRobotconnected = false;
           outputDirectory = settings[1];
           bool.TryParse(settings[2], out replaceOriginal);
           bool.TryParse(settings[3], out doDeskew);
           startPhrases = settings[4];
           endPhrases = settings[5];
           bool.TryParse(settings[6], out exceptionFlag);
        }

        public long Process(IWorkitem item)
        {
            long rv = 0;
            nlogger.Log(LogLevel.Debug, "OutputIGREdaction:Processing item");
            comments = "";
            if (Mappings.Length > 0)
            {
                try
                {
                    var mappings = Mapping.FromXml(Mappings);
                    if (mappings.Count > 0)
                        //foreach (var index in item.Indexes)
                        for (int i = 0; i < item.Indexes.Count; i++)
                        {
                            IDictionary<string, string> temp = mappings.TranslateFields(item.Indexes[i]);
                            item.Indexes[i] = temp;
                        }

                }
                catch (Exception ex)
                {
                    Logger.Add(new LogEntry()
                    {
                        Severity = LogSeverity.Warning,
                        Message = string.Format("Invalid mappings defined for robot, unable to apply mappings.  Exception was:  \n{0}", ex.ToString()),
                        Id = Logger.Count + 1,
                        EventDate = DateTime.Now
                    });
                    nlogger.Log(LogLevel.Error, string.Format("Invalid mappings defined for robot, unable to apply mappings.  Exception was:  \n{0}", ex.ToString()));

                }
            }
            else
            {
                ////Logger.Add(new LogEntry()
                //{
                //    Severity = LogSeverity.Debug,
                //    Message = string.Format("No mappings to apply to {0}", item.Id),
                //    Id = Logger.Count + 1,
                //    EventDate = DateTime.Now
                //});
            }

            Document asDoc = null;


            if (null == item)
                return rv;

            nlogger.Log(LogLevel.Debug, "OutputIGREdaction:Processing item");

            if (item is Document)
                asDoc = item as Document;
            else
            {
                item.Status = WorkitemStatus.Error;
                item.ErrorSource = "OutputParaImage.Process";
                item.ErrorDescription = "Only Document workitems can be processed by OutputIGRedaction.";
                nlogger.Log(LogLevel.Error, "Only Document workitems can be processed by OutputIGRedaction.");
                return rv;
            }

            if (item.Status.Equals(WorkitemStatus.Error))
            {
                item.Status = WorkitemStatus.Error;
                nlogger.Log(LogLevel.Error, "Error while processing item in OutputIGRedaction.");
                return rv;
            }

            if (asDoc.Images[0] == null)
            {
                item.Status = WorkitemStatus.Successful;
                //item.ErrorSource = "OutputParaImage.Process";
                //item.ErrorDescription = "Only Document workitems can be processed by OutputParaImage.";
                return rv;
            }

            string tempfile = SaveImagesToTempFile(asDoc);
            Console.WriteLine("Temp file copied from Input module " + tempfile);
            nlogger.Log(LogLevel.Debug, "Copied the image to a temp location: " + tempfile);
            try
            {

                FileInfo fsInfo = new FileInfo(tempfile);

                if (fsInfo.Length > 2000000)
                {
                    asDoc.Status = WorkitemStatus.Error;
                    asDoc.ErrorDescription = "OutputIGRedaction.startUp error. Sending documents for exception processing as image size greater than 1.5M";
                    asDoc.ErrorSource = "paraIGRedaction.startUp";
                    rv = 0;
                    return rv;
                }

                string Param;
                if(doDeskew)
                    Param = "Web" + "|" + fsInfo.Directory + "|" + fsInfo.Name+ "|1";
                else
                    Param = "Web" + "|" + fsInfo.Directory + "|" + fsInfo.Name + "|";

                Console.WriteLine("Parameters passed to redaction process  :" + Param);
                nlogger.Log(LogLevel.Debug, "Redaction command sent to ParaIGREdaction dll: " + Param);

                if(!paraIGRedaction.startUp(Param))
                {
                    asDoc.Status = WorkitemStatus.Error;
                    asDoc.ErrorDescription = "OutputIGRedaction.startUp error. Sending documents for exception processing";
                    asDoc.ErrorSource = "paraIGRedaction.startUp";
                    rv = 0;
                    return rv;
                }

                String redactedFile;
                //51992001.tif-redacted-.tif
                redactedFile = fsInfo.Name + "-redacted-" + fsInfo.Extension;
                Console.WriteLine("After redaction process redacted file Path :" + redactedFile);
                nlogger.Log(LogLevel.Debug, "After redaction process redacted file Path :" + redactedFile);

                asDoc.Status = WorkitemStatus.Successful;
                asDoc.Id = asDoc.Id;
                rv = 1;
                DirectoryInfo imagePath;
                FileInfo imageFile;

                nlogger.Log(LogLevel.Debug, "Post Processing redacted file");
                //Save the image in output directory
                try
                {
                    string fileName = redactedFile;
                    string sourcePath = fsInfo.DirectoryName;
                    string targetPath = outputDirectory;
                    string strtemp = fsInfo.Extension;
                    // Use Path class to manipulate file and directory paths. 
                    string sourceFile = System.IO.Path.Combine(sourcePath, fileName);

                    strtemp = asDoc.Name + fsInfo.Extension;

                    string destFile = System.IO.Path.Combine(targetPath,strtemp);
                    string tempBackup = System.IO.Path.Combine(targetPath,strtemp+".back");

                    // To copy a folder's contents to a new location: 
                    // Create a new target folder, if necessary. 
                    if (!System.IO.Directory.Exists(targetPath))
                    {
                        System.IO.Directory.CreateDirectory(targetPath);
                    }
                    if (System.IO.File.Exists(sourceFile))
                    {
                        nlogger.Log(LogLevel.Debug, "Redaction Rules succeeded");
                        comments = "Redactions found";
                        asDoc.Indexes[0].Add("OutputComments", "Redactions found");
                        if (replaceOriginal)
                        {

                            if (asDoc.Indexes[0].ContainsKey("ObjectFullPath"))
                            {
                                nlogger.Log(LogLevel.Debug, "Copying :" + sourceFile + ": to destination " + asDoc.Indexes[0]["ObjectFullPath"]);
                                System.IO.File.Copy(sourceFile, asDoc.Indexes[0]["ObjectFullPath"], true);
                                nlogger.Log(LogLevel.Debug, "After copying file to destination");

                            }
                            if ((asDoc.Indexes[0].ContainsKey("filename")))
                            {
                                nlogger.Log(LogLevel.Debug, "Copying :" + sourceFile + ": to destination " + asDoc.Indexes[0]["filename"]);
                                System.IO.File.Copy(sourceFile, asDoc.Indexes[0]["filename"], true);
                                nlogger.Log(LogLevel.Debug, "After copying file to destination");
                            }
                        }

                        else
                        {
                            System.IO.File.Copy(sourceFile, destFile, true);
                            nlogger.Log(LogLevel.Debug, "Redacted file:" + sourceFile + " copied to following destination : " + destFile);
                        }

                        //cleaning up file in working directory
                        System.IO.File.Delete(sourceFile);
                        System.IO.File.Delete(tempfile);
                    }
                    else
                    {
                        comments = "No Redactions found";

                        asDoc.Indexes[0].Add("OutputComments", "No Redactions found");
                        nlogger.Log(LogLevel.Debug, "Redaction Rules failed and no file to redact:");
                        if (exceptionFlag)
                        {
                            asDoc.Status = WorkitemStatus.Error;
                            asDoc.ErrorDescription = "OutputIGRedaction.No Redactions found. Sending item to unsuccessful folder";
                            asDoc.ErrorSource = "paraIGRedaction.AfterRedaction";
                            rv = 0;
                            return rv;
                        }
                        else
                        {
                            if (replaceOriginal)
                            {

                                if (asDoc.Indexes[0].ContainsKey("ObjectFullPath"))
                                {
                                    nlogger.Log(LogLevel.Debug, "Copying :" + tempfile + ": to destination " + asDoc.Indexes[0]["ObjectFullPath"]);
                                    System.IO.File.Copy(tempfile, asDoc.Indexes[0]["ObjectFullPath"], true);
                                    nlogger.Log(LogLevel.Debug, "After copying file to destination");

                                }
                                if ((asDoc.Indexes[0].ContainsKey("filename")))
                                {
                                    nlogger.Log(LogLevel.Debug, "Copying :" + tempfile + ": to destination " + asDoc.Indexes[0]["filename"]);
                                    System.IO.File.Copy(tempfile, asDoc.Indexes[0]["filename"], true);
                                    nlogger.Log(LogLevel.Debug, "After copying file to destination");
                                }
                            }
                            else
                            {
                                System.IO.File.Copy(tempfile, destFile, true);
                                nlogger.Log(LogLevel.Debug, "Redacted file:" + sourceFile + " copied to following destination : " + destFile);
                            }
                            System.IO.File.Delete(tempfile);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //throw new InvalidOperationException(string.Format("File Copy failed"), ex);
                   asDoc.Status = WorkitemStatus.Error;
                   asDoc.ErrorDescription = ex.ToString();
                   asDoc.ErrorSource = "File Copy failed";
                   nlogger.Log(LogLevel.Error, string.Format("File Copy failes.  Exception was:  \n{0}", ex.ToString()));
                }
             
                
            }

            catch (Exception ex)
            {
                asDoc.Status = WorkitemStatus.Error;
                
                asDoc.ErrorDescription = ex.ToString();
                asDoc.ErrorSource = "OutputIGRedaction.Process";
                rv = 0;
            }           
            return rv;
        }

        public string SaveImagesToTempFile(Document doc)
        {
            string rv = System.IO.Path.GetTempFileName();
            string tempFile = rv;
            Console.WriteLine("SaveImagesToTempFile: " + doc.Images[0].Name);
            string ext = FileExtensions.MimeTypes.Where(mt => mt.Value == doc.Images[0].MimeType).First().Key;
            Console.WriteLine("Extention after mime conversion :" + ext + ": Extension before mime conversion : " + doc.Images[0].MimeType);
            if (ext.Contains("jpe"))
                ext = "jpg";

            rv = System.IO.Path.ChangeExtension(rv, ext);
            //// copy first image to temp file
            //if (ext.Contains("tif"))
            //{
            //    MemoryStream imageStream = new MemoryStream();
            //    Image first = Image.FromStream(new MemoryStream(doc.Images[0].Data));
            //    first.Save(imageStream, ImageFormat.Tiff);
            //    Image tiff = Image.FromStream(imageStream);
            //    ImageCodecInfo tiffInfo = ImageCodecInfo.GetImageEncoders().First(ici => ici.MimeType == "image/tiff");
            //    EncoderParameters firstParams = new EncoderParameters(2);
            //    firstParams.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            //    firstParams.Param[1] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
            //    tiff.Save(rv, tiffInfo, firstParams);

            //    // copy subsequent images to temp file

            //    EncoderParameters restParams = new EncoderParameters(2);
            //    restParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
            //    restParams.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            //    for (int i = 1; i < doc.Images.Count; i++)
            //    {
            //        tiff.SaveAdd(Image.FromStream(new MemoryStream(doc.Images[i].Data)), restParams);
            //    }

            //    // flush the file
            //    EncoderParameters flushParams = new EncoderParameters(1);
            //    flushParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.Flush);
            //    tiff.SaveAdd(flushParams);
            //}
            //else
            //{
                FileStream fs = new FileStream(rv, FileMode.Create);
                fs.Write(doc.Images[0].Data, 0, doc.Images[0].Data.Length);
                fs.Flush();
                fs.Close();
                fs = null;
            //}

            if (System.IO.File.Exists(tempFile))
            {
                System.IO.File.Delete(tempFile);
            }
            return rv;
        }

        public long ProcessBatch(IEnumerable<IWorkitem> items)
        {
            throw new NotSupportedException();
        }

        public bool SupportsBatching
        {
            get { return false; }
        }

        public bool RequiresConnection
        {
            get { return true; }
        }

        public IConnector Connector
        {
            get
            {
                throw new NotSupportedException();
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        public void SetIGConnector(ParagonIGRedaction.clsProcess item)
        {

                paraIGRedaction = item;
        }
        public ParagonIGRedaction.clsProcess GetIGConnector()
        {

            return paraIGRedaction;
        }
        public bool ShareConnector
        {
            get { return false; }
        }

        public IList<Model.LogEntry> Logger
        {
            get
            {
                return logger;
            }
            set
            {
                if (null == value)
                    throw new ArgumentNullException("Logger");
                logger = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("Name");
                name = value;
            }
        }

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("FileName");
                fileName = value;
            }
        }

        public string Type
        {
            get
            {
                return kind;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("Type");
                kind = value;
            }
        }

        public bool Connect()
        {
            if (!isRobotconnected)
            {
                //paraIGRedaction.Initialize();
                isRobotconnected = true;
            }
            return true;
        }

        public bool IsConnected
        {
            get { return isRobotconnected; }
        }

        public void Disconnect()
        {
            //session.LogoutEx(ref Domain);
        }

    }
}
