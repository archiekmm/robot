﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Paragon.RobotServices.Plugin
{
    public class InputUeWI : IInputModule, IDisposable
    {
        // Setting Names
        public const string cInputWorkset = "Queue Name";
        public const string cLogin = "login";

        protected UnisysImagingConnector conn = null;
        protected IDictionary<int, string> _settings;

        public InputUeWI(IDictionary<int, string> settings)
        {
            string loginStr = settings[1].Replace("&", ";");
            conn = new UnisysImagingConnector(loginStr);
            _settings = settings;
            //conn.Connect();
        }

        public void Dispose()
        {
            if (null != conn)
            {
                if (conn.IsConnected)
                    conn.Disconnect();
                conn = null;
            }
        }

        public IWorkitem Get()
        {
            if (!conn.IsConnected)
                return null;

            return conn.GetNext(_settings[2]);
        }

        public IEnumerable<IWorkitem> GetBatch()
        {
            throw new NotSupportedException();
        }

        public void Delete(IWorkitem item)
        {
            throw new NotSupportedException();
        }

        public void Move(IWorkitem item, System.IO.DirectoryInfo whereTo)
        {
            throw new NotSupportedException();
        }

        public void UpdateStatus(IWorkitem item, WorkitemStatus status)
        {
            item.Status = status;
        }

        public bool SupportsMove
        {
            get { return false; }
        }

        public bool SupportsDelete
        {
            get { return false; }
        }

        public bool SupportsUpdateStatus
        {
            get { return true; }
        }

        public bool SupportsBatching
        {
            get { return false; }
        }

        public int Id { get; set; }

        public string Name
        {
            get { return "InputUeWI"; }
            set { }
        }

        public string FileName
        {
            get { return this.GetType().Assembly.Location; }
            set { }
        }

        public string Type
        {
            get { return "input"; }
            set { }
        }

        public bool RequiresConnection
        {
            get { return true; }
        }

        public bool Connect()
        {
            return conn.Connect();
        }

        public bool IsConnected { get { return conn.IsConnected; } }

        public void Disconnect()
        {
            conn.Disconnect();
        }

        public IConnector Connector
        {
            get { return conn; }
            set
            {
                conn = (UnisysImagingConnector)value;
                //if (value is UnisysImagingConnector)
                //    conn = value as UnisysImagingConnector;
                //else
                //    throw new ArgumentException("conn must of type UnisysImagingConnector");
            }
        }

        public void PostProcess(IWorkitem item)
        {
            
        }
    }
}
