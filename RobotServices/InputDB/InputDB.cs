﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Paragon.RobotServices.Model;
using System.Data.SqlClient;
using NLog;

namespace Paragon.RobotServices.Plugin
{
    public class InputDB: IInputModule
    {
        static Logger nlogger = LogManager.GetLogger("InputDirectory"); 
        // settings fields
        private DirectoryInfo workDir;
        private string strProvider = "DB Provider";
        private string strServer = "Server";
        private string strDatabase = "Database Name";
        private string strUserID = "User ID";
        private string strPassword = "Password";
        private string strQuery = "Selection Query";
        private string strSuccessUpdateQuery = "Success Update Query";
        private string strErrorUpdateQuery = "Error Update Query";
        private bool metadataOnly = false;
        private string indexSeparator = "\r\n\r\n";
        private DirectoryInfo archiveDir = null;
        private bool archiveFlag = false;
        private DirectoryInfo errorDir = null;
        private string mappingsText = null;
        private SqlConnection dbConnection = null;
        private string imageFileExtension = null; 


        public const string cWorkPath = "Work Path";
        public const string cProvider = "DB Provider";
        public const string cServer = "Server";
        public const string cDatabase = "Database Name";
        public const string cUserID = "User ID";
        public const string cPassword = "Password";
        public const string cQuery = "Selection Query";
        public const string cUpdateQuery = "Update Query";
        public const string cArchive = "Archive";
        public const string cArchivePath = "Archive Path";
        public const string cArchiveFlag = "Archive Workitems";
        public const string cUnsuccessfulPath = "Unsuccessful Path";
 
        protected int count = 0;

        public InputDB(IDictionary<int, string> settings)
        {
            
            workDir = new DirectoryInfo(settings[1]);
            if (!(workDir.Exists))
                throw new InvalidOperationException(string.Format("Working directory \"{0}\" does not exist.", settings[1]));

            strProvider = settings[2];
            strServer = settings[3];
            strDatabase = settings[4];
            strUserID= settings[5];
            strPassword = settings[6];
            strQuery = settings[7];
            strSuccessUpdateQuery = settings[8];
            strErrorUpdateQuery = settings[9];

            bool.TryParse(settings[10], out archiveFlag);

            if ((archiveFlag) && ((!settings.ContainsKey(11)) || (string.IsNullOrEmpty(settings[11]))))
                throw new ArgumentException("Archive Path setting must be included when Archive Flag is true.");
            archiveDir = new DirectoryInfo(settings[11]);
            if (!archiveDir.Exists)
                throw new ArgumentException(string.Format("Archive Path '{0}' must exist.", settings[11]));
            errorDir = new DirectoryInfo(settings[12]);
            if (!errorDir.Exists)
                throw new ArgumentException(string.Format("Unsuccessful Path '{0}' must exist.", settings[12]));
        }
        public IWorkitem Get()
        {
            SqlDataReader myReader = null;
            SqlCommand myCommand = null;
            try
            {
                Document workitem = null;
                string strObjectName = null;
                nlogger.Log(LogLevel.Debug, "Getting item from InputDB. Selection Query:" + strQuery);
                
                myCommand = new SqlCommand(strQuery,dbConnection);
                myReader = myCommand.ExecuteReader();
                Dictionary<string, string> workitemIndexes = new Dictionary<string, string>();

                while (myReader.Read())
                {
                    workitem = new Document();
                    Console.WriteLine("ObjectName : " + myReader["ObjectName"].ToString());
                    strObjectName = myReader["ObjectName"].ToString();
                    workitemIndexes.Add("Destbatchid", (myReader["Destbatchid"].ToString()));
                    workitemIndexes.Add("ObjectName", (myReader["ObjectName"].ToString()));
                    workitemIndexes.Add("ObjectID", (myReader["ObjectID"].ToString()));
                    workitemIndexes.Add("ObjectFullPath", (myReader["ObjectFullPath"].ToString()));
                    workitemIndexes.Add("ObjectFormat", (myReader["ObjectFormat"].ToString()));
                    workitemIndexes.Add("Creation_date", (myReader["Creation_date"].ToString()));
                   // workitemIndexes.Add("Status", (myReader["Status"].ToString()));
                    workitemIndexes.Add("Comments", (myReader["Comments"].ToString()));
                    workitem.Indexes.Add(workitemIndexes);

                    workitem.Id = workitem.Name = strObjectName;
                    workitem.Status = WorkitemStatus.NotProcessed;

                    FileInfo imageFile = new FileInfo(myReader["ObjectFullPath"].ToString());
                    nlogger.Log(LogLevel.Debug, "Getting item from InputDB. Selection Query:" + strQuery);

                    Console.WriteLine("InputDB log : imageFile Found" + imageFile);
                    Console.WriteLine("Workitem Name : " + workitem.Name);

                    // move files to working directory
                    if ((imageFile != null) && imageFile.Exists)
                    {
                        nlogger.Log(LogLevel.Debug, "Getting image : " + imageFile.Name);
                        imageFile.CopyTo(Path.Combine(workDir.FullName, imageFile.Name));
                        workitem.Id = workitem.Name = Path.GetFileNameWithoutExtension(imageFile.Name);
                        imageFileExtension = imageFile.Extension;
                        Console.WriteLine("InputDirectory log : imageFile is moved to working directory :" + workDir.FullName + imageFile.Name);
                        // add images to the workitem
                        workitem.Images.AddRange(Page.GetPages(imageFile.FullName));
                    }
                    else
                    {
                        nlogger.Log(LogLevel.Error, "Image doesn't exist setting status to error. ");
                        workitem.Status = WorkitemStatus.Error;
                        PostProcess(workitem);
                        return workitem;
                    }
                    IEnumerable<Mapping> mappings = null;
                    if (!string.IsNullOrEmpty(mappingsText))
                    {
                        try
                        {
                            mappings = Mapping.FromXml(mappingsText);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                        }
                    }

                    if (null != mappings)
                    {
                        for (int i = 0; i < workitem.Indexes.Count; ++i)
                            workitem.Indexes[i] = mappings.TranslateFields(workitem.Indexes[i]);
                    }
                }
                myReader.Close();
                myReader.Dispose();
                
               // workitem.Status = WorkitemStatus.Successful;
                return workitem;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
               
                if(myReader !=null)
                    myReader.Close();
                return null;
            }
            

        }

        public IEnumerable<IWorkitem> GetBatch()
        {
            List<IWorkitem> rv = new List<IWorkitem>();
            for (int i = 0; i < 20; i++)
                rv.Add(this.Get());
            return rv;
        }

        public void Delete(IWorkitem item)
        {
            // construct file names
            string baseName = item.Name;
            string imageFileName = item.Name + imageFileExtension;

            // delete the files, if they exist

            if (File.Exists(Path.Combine(workDir.FullName, imageFileName)))
            {
                File.Delete(Path.Combine(workDir.FullName, imageFileName));
            }
        }

        public void Move(IWorkitem item, System.IO.DirectoryInfo whereTo)
        {
            if (null == item)
                throw new ArgumentNullException("item cannot be null.");
            if (null == whereTo)
                throw new ArgumentNullException("destination cannot be null.");
            string imageFileName = item.Name + imageFileExtension;
            if (File.Exists(Path.Combine(workDir.FullName, imageFileName)))
            {
                if (File.Exists(Path.Combine(whereTo.FullName, imageFileName)))
                    File.Delete(Path.Combine(whereTo.FullName, imageFileName));
                File.Move(Path.Combine(workDir.FullName, imageFileName),
                    Path.Combine(whereTo.FullName, imageFileName));               
            }

            //create index file
            StringBuilder sb = new StringBuilder();
            string indexFileName = item.Name + ".txt";
            foreach (var index in item.Indexes)
            {
                foreach (var kvp in index)
                {
                      sb.Append(kvp.Key).Append("=").Append(kvp.Value);
                      sb.Append("\r\n");
                }
            }
            File.WriteAllText(Path.Combine(whereTo.FullName, indexFileName), sb.ToString());
        }

        public void UpdateStatus(IWorkitem item, WorkitemStatus status)
        {
            item.Status = status;
        }
        public void UpdateWorkitem(IWorkitem item)
        {
            SqlDataReader myReader = null;
            SqlCommand myCommand = null;
            try
            {
                string strUpdateQuery;
               string imageFileName = item.Name + imageFileExtension;
               string imageFullPath="";
               String comments = "";

                if (item.Indexes[0].ContainsKey("ObjectFullPath"))
                {
                    imageFullPath = item.Indexes[0]["ObjectFullPath"];
                }
                if (item.Indexes[0].ContainsKey("OutputComments"))
                {
                    comments = item.Indexes[0]["OutputComments"];
                }
                if (item.Status == WorkitemStatus.Successful)
                {
                    if (strSuccessUpdateQuery.Contains("{"))
                    {
                        strUpdateQuery = string.Format(strSuccessUpdateQuery, item.Name, item.Name, imageFullPath, imageFileExtension);

                    }
                    else if(strSuccessUpdateQuery.Contains("["))
                    {
                        strUpdateQuery = strSuccessUpdateQuery.Replace("[", "{").Replace("]", "}");
                        strUpdateQuery = string.Format(strUpdateQuery, comments, imageFullPath);
                    }
                    else
                    {
                        strUpdateQuery = strSuccessUpdateQuery + "'" + imageFullPath + "'";
                    }
                    
                }
                else
                {
                    if (strErrorUpdateQuery.Contains("{"))
                    {
                        strUpdateQuery = string.Format(strErrorUpdateQuery, item.Name, item.Name, imageFullPath, imageFileExtension);

                    }
                    else if (strErrorUpdateQuery.Contains("["))
                    {
                        strUpdateQuery = strErrorUpdateQuery.Replace("[", "{").Replace("]", "}");
                        string tempError = item.ErrorDescription;
                        if (tempError != null)
                        {
                            if (tempError.Length > 150)
                                tempError = tempError.Substring(0, 150);

                        }
                        else
                            tempError = "No error raised";

                        strUpdateQuery = string.Format(strUpdateQuery, tempError, imageFullPath);

                    }
                    else
                    {
                        strUpdateQuery = strErrorUpdateQuery + "'" + imageFullPath + "'"; 
                    }
                }
                nlogger.Log(LogLevel.Debug, "Update query : " + strUpdateQuery);

                Console.WriteLine("Updating item status: " + strUpdateQuery);

                myCommand = new SqlCommand(strUpdateQuery, dbConnection);
                myReader = myCommand.ExecuteReader();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public bool SupportsMove
        {
            get { return true; }
        }

        public bool SupportsDelete
        {
            get { return true; }
        }

        public bool SupportsUpdateStatus
        {
            get { return true; }
        }

        public bool SupportsBatching
        {
            get { return true; }
        }

        public int Id
        {
            get;
            set;
        }

        protected string _name;
        public string Name
        {
            get { return "InputDB"; }
            set { _name = value; }
        }

        public string FileName
        {
            get { return System.Reflection.Assembly.GetExecutingAssembly().Location; }
            set { _name = value; }
        }

        public string Type
        {
            get { return "input"; }
            set { _name = value; }
        }

        public bool RequiresConnection
        {
            get { return true; }
        }

        public bool Connect()
        {
            ////dbConnection = new SqlConnection("Suser id=" + strUserID + ";" +
            //                                      "password=" + strPassword + ";server=" + strServer+ ";" +
            //                                      "Trusted_Connection=yes;" +
            //                                      "database=" + strDatabase + "; " +
            //                                      "connection timeout=30");
            dbConnection = new SqlConnection("Server=" + strServer + ";Database=" + strDatabase + ";User Id=" + strUserID + ";Password=" + strPassword + ";MultipleActiveResultSets=true");

            try
            {
                dbConnection.Open();
                nlogger.Log(LogLevel.Debug, "Opening DB Connection " );
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }

        }

        public bool IsConnected 
        { get 
          {

                return true;
 
          } 
        }


        public void PostProcess(IWorkitem item)
        {
            if ((item.Status == WorkitemStatus.Successful))
            {
                if (archiveFlag)
                {
                    DirectoryInfo realArchiveDir = new DirectoryInfo(
                        Path.Combine(archiveDir.FullName,
                            DateTime.Today.ToString("yyyy-MM-dd")));
                    if (!realArchiveDir.Exists)
                        realArchiveDir.Create();

                    Move(item, realArchiveDir);
                    UpdateWorkitem(item);
                }
                else
                {
                    UpdateWorkitem(item);
                }

            }
            else
            {
                DirectoryInfo realErrorDir = new DirectoryInfo(
                    Path.Combine(errorDir.FullName,
                        DateTime.Today.ToString("yyyy-MM-dd")));
                if (!realErrorDir.Exists)
                    realErrorDir.Create();

                Move(item, realErrorDir);
                UpdateWorkitem(item);
            }
        }

        public IConnector Connector
        {
            get { return null; }
        }

        public void Disconnect()
        {
            try
            {
                if(dbConnection != null)
                    dbConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
