﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Plugin;
using CALTypeLib;
using System.Net.Mime;

namespace Paragon.RobotServices.Plugin
{
    public class UnisysImagingConnector : ImagingConnector
    {
        // Unisys data:  client, clientList, etc.
        protected CALMaster _master = new CALTypeLib.CALMaster();
        protected CALClient _client = null;
        protected CALClientList _list = null;
        protected const int calNoRetain = 0;
        protected const string calWildcard = "%";

        public UnisysImagingConnector(IDictionary<string, string> login)
            : base(login)
        {
            // TODO:  use settings to configure, e.g., impersonation, enterprise query, and so on
        }

        public UnisysImagingConnector(System.Data.Common.DbConnectionStringBuilder dcsb)
            : base(dcsb)
        {
        }

        public UnisysImagingConnector(string loginStr)
            : base(new System.Data.Common.DbConnectionStringBuilder() { ConnectionString = loginStr.ToLower() })
        {
        }

        public override bool Connect()
        {
            try
            {
                foreach (var kvp in _login)
                    Console.WriteLine("key is {0}, value is {1}", kvp.Key, kvp.Value);
                _client = _master.CreateClient(_login["userid"], _login["password"], _login["server"], null);
                _list = (CALClientList)_client.ClientList;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _client = null;
                _list = null;
                return false;
            }
        }

        public override void Disconnect()
        {
            try
            {
                _list.Clear(ClientListClearConstants.calClearAbortNew);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            _list = null;
            try
            {
                _client.Destroy(DestroyConstants.calDestroyAbortChanges);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            _client = null;
        }

        public override bool IsConnected
        {
            get { return (null != _client); }
        }

        public override void Dispose()
        {
            if (null != _list)
                _list.Clear(ClientListClearConstants.calClearAbortNew);
            _list = null;
            if (null != _client)
                _client.Destroy(DestroyConstants.calDestroyAbortChanges);
            _client = null;
            _master = null;
        }

        public override IEnumerable<Document> FindDocuments(IDictionary<string, string> searchIndexes)
        {
            var itemIds = FindItemIds(searchIndexes, false);
            Console.WriteLine("Found {0} items.", itemIds.Count);
            foreach (string id in itemIds)
            {
                Document d = RetrieveDocument(id);
                if (null != d)
                    yield return d;
            }
        }

        public IList<string> FindItemIds(IDictionary<string, string> searchIndexes, bool trueFoldersFalseDocuments)
        {
            List<string> rv = new List<string>();

            // TODO:  Configure search parameters:  Enterprise query, etc.

            CALIndexFields searchFields = new CALIndexFields();
            if (null == searchFields)
                return rv; // an empty list

            foreach (var kvp in searchIndexes)
            {
                searchFields.set_Value(kvp.Key, kvp.Value);
                Console.WriteLine("Set search field {0} to {1}", kvp.Key, kvp.Value);

            }

            CALQueryResults results = (CALQueryResults) _client.Query(calWildcard,
                (trueFoldersFalseDocuments ? ObjTypeConstants.calObjTypeFolder : ObjTypeConstants.calObjTypeDocument), 
                LocationConstants.calLocationEnterprise, 500, searchFields);

            for (int i = 0; i < results.Count; ++i)
            {
                CALEnumItem item = results.get_Item(i + 1);
                CALWorkitemInfo info = (CALWorkitemInfo)item.Info;
                rv.Add(info.ObjID.InternalName);
                info = null;
                item = null;
            }
            results = null;
            searchFields = null;
            return rv;
        }

        public override string CreateDocument(Document doc)
        {
            return CreateDocument(doc, false);
        }

        public string CreateDocument(Document doc, bool sendToWorklfow)
        {
            #region create the document
            CALWorkitemInfo info = null;
            CALClientListItem clientListItem = null;
            try
            {
                info = (CALWorkitemInfo)_client.CreateDocument(doc.Name, doc.Class);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
            if (null == info)
                return null;

            clientListItem = (CALClientListItem) _list.Add(info.ObjID, calNoRetain);
            if (null == clientListItem)
                return null;

            #endregion

            // open it
            CALDocument calDoc = (CALDocument)clientListItem.Open(OpenConstants.calOpenReadWrite);
            if (null == calDoc)
                return null;

            #region set indexes
            /*
            if (doc.Indexes.Count > 1)
             * warning:  Unisys only supports a single index for a document.
             **/

            // TODO:  verify indexes

            CALFormFields fields = (CALFormFields)calDoc.FormFields(FieldConstants.calFieldsNoViews);
            if (null == fields)
            {
                // warning: no index fields found for document
                Console.WriteLine("No form field indexes found for doc class" + doc.Class + ".");
            }
            else
                UpdateIndexes((CALFormFields)calDoc.FormFields(FieldConstants.calFieldsNoViews), doc.Indexes); //calDoc, doc); // 
            #endregion

            ImportPages(calDoc, doc);

            #region save and close doc

            calDoc.Save();
            calDoc.RefreshInfo();

            string rv = calDoc.Info.ObjID.InternalName;
            calDoc.Close(CloseConstants.calCloseSaveChanges);
            calDoc = null;
            
            if (sendToWorklfow)
                clientListItem.SendToDefault(SendConstants.calSendDiscard);
            else
                _list.Remove(clientListItem);
            #endregion
            doc.Id = rv;
            Console.WriteLine("Document with id {0} created.", doc.Id);
            return rv;
        }

        protected CALClientListItem GetListItem(string docId)
        {
            if (string.IsNullOrEmpty(docId))
                throw new ArgumentNullException("docid");

            CALClientListItem listItem = null;
            CALObjID id = new CALObjID() { InternalName = docId };

            Console.WriteLine("GetListItem:  searching client list");
            listItem = _list.Find(id);

            // check to see if it's already on the list
            if (listItem == null) // still not there?  check workflow
            {
                Console.WriteLine("GetListItem:  checking workflow");
                CALWorkitemWorkstepList list = (CALWorkitemWorkstepList)_client.FindInWorkflow(id);
                if (list.Count > 0)
                {
                    CALEnumItem item = list.get_Item(1);
                    listItem = (CALClientListItem)item.Retrieve(calNoRetain);
                    item = null;
                }
                list = null;
            }

            if (listItem == null) // nope, not on the list, add it
            {
                Console.WriteLine("GetListItem:  adding (outside workflow)");
                try
                {
                    listItem = (CALClientListItem)_list.Add(id, calNoRetain);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("GetListItem:  " + ex.ToString());
                    listItem = null;
                }
            }

            id = null;
            return listItem;
        }

        protected void UpdateIndexes( CALFormFields formFields, IEnumerable<IDictionary<string, string>> indexes) //CALDocument calDoc, ImagingDocument doc)  
        {
            //CALFormFields formFields = (CALFormFields)calDoc.FormFields(FieldConstants.calFieldsNoViews);
            //IEnumerable<IDictionary<string, string>> indexes = doc.Indexes;
            foreach (CALFormField f in formFields)
            {
                for (int i = 0; i < Math.Min(indexes.Count(), f.Info.Rows); ++i)
                {
                    IDictionary<string, string> current = indexes.ElementAt(i);
                    string fName = f.Info.Name;
                    string fValue = (current.ContainsKey(fName) ? current[fName] : string.Empty);
                    if (fValue == string.Empty)
                        continue;
                    Console.WriteLine("UnisysImagingConnector:UpdateIndexes setting index value (i is {0}, value is {1}", i, fValue);
                    if (f.Info.Rows == 1)
                        f.set_Value(Type.Missing, fValue.Substring(0, Math.Min(f.Info.Length, fValue.Length)));
                    else
                        f.set_Value((i+1), fValue.Substring(0, Math.Min(f.Info.Length, fValue.Length)));
                }
            }
        }

        public override bool UpdateIndexes(IWorkitem doc)
        {
            bool rv = false;

            CALClientListItem listItem = GetListItem(doc.Id);
            if (null == listItem)
                return rv;

            CALDocument calDoc = null;
            // already open?  grab it
            if ((listItem.Info.Status & ObjStatusConstants.calObjStatusOpen) == ObjStatusConstants.calObjStatusOpen)
                calDoc = (CALDocument)listItem.OpenedItem;
            else // have to open it
                calDoc = (CALDocument)listItem.Open(OpenConstants.calOpenReadWrite);

            if (null == calDoc)
            {
                _list.Remove(listItem);
                listItem = null;
                return rv;
            }

            try
            {
                UpdateIndexes((CALFormFields)calDoc.FormFields(FieldConstants.calFieldsNoViews), doc.Indexes);
                rv = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                rv = false;
            }

            calDoc.Close(CloseConstants.calCloseSaveChanges);
            calDoc = null;
            _list.Remove(listItem);
            listItem = null;
            return rv;
        }

        protected void ImportPages(CALDocument calDoc, Document doc)
        {
            // need to "bounce" the pages off of the file system
            //  (Unisys can only import files, not bytes or byte streams)
            foreach (var page in doc.Images)
            {
                string filename = System.IO.Path.GetTempFileName();
                System.IO.File.Delete(filename);
                switch (page.MimeType)
                {
                    case MediaTypeNames.Application.Pdf:
                        filename = System.IO.Path.ChangeExtension(filename, "pdf");
                        break;
                    case MediaTypeNames.Application.Octet:
                        filename = System.IO.Path.ChangeExtension(filename, "bin");
                        break;
                    case MediaTypeNames.Application.Rtf:
                    case MediaTypeNames.Text.RichText:
                        filename = System.IO.Path.ChangeExtension(filename, "rtf");
                        break;
                    case MediaTypeNames.Image.Gif:
                        filename = System.IO.Path.ChangeExtension(filename, "gif");
                        break;
                    case MediaTypeNames.Image.Jpeg:
                        filename = System.IO.Path.ChangeExtension(filename, "jpg");
                        break;
                    case MediaTypeNames.Text.Html:
                        filename = System.IO.Path.ChangeExtension(filename, "html");
                        break;
                    case MediaTypeNames.Text.Plain:
                        filename = System.IO.Path.ChangeExtension(filename, "txt");
                        break;
                    case MediaTypeNames.Image.Tiff:
                    default:
                        filename = System.IO.Path.ChangeExtension(filename, "tif");
                        break;
                }
                System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create);
                fs.Write(page.Data, 0, page.Data.Count());
                fs.Close();
                calDoc.ImportPages(filename, string.Empty, null);
                System.IO.File.Delete(filename);
            }

        }
        public override Document RetrieveDocument(string docId)
        {
            if (string.IsNullOrEmpty(docId))
                throw new ArgumentNullException("docid");

            CALClientListItem listItem = null;
            try
            {
                listItem = GetListItem(docId);
            }
            catch (Exception ex)
            {
                return null;
            }
            if (null == listItem)
                return null;

            CALDocument calDoc = null;
            // already open?  grab it
            if ((listItem.Info.Status & ObjStatusConstants.calObjStatusOpen) == ObjStatusConstants.calObjStatusOpen)
                calDoc = (CALDocument)listItem.OpenedItem;
            else // have to open it
                calDoc = (CALDocument)listItem.Open(OpenConstants.calOpenReadOnly & OpenConstants.calOpenArchive);

            if (null == calDoc)
            {
                _list.Remove(listItem);
                listItem = null;
                return null;
            }
            Console.WriteLine("creating new Document instance.");
            Document id = new Document();
            id.Name = calDoc.Info.Name;
            id.Class = calDoc.Class;
            id.Id = calDoc.Info.ObjID.InternalName;
            Console.WriteLine("copying form fields."); 
            CALFormFields ff = (CALFormFields)calDoc.FormFields(FieldConstants.calFieldsNoViews);
            int indexCount = 1;
            for (int i = 1; i <= ff.Count; i++)
            {
                CALFormField f = (CALFormField)ff.Item[i];
                if (indexCount < f.Info.Rows)
                    indexCount = f.Info.Rows;
            }
            for (int i = 1; i <= indexCount; ++i)
            {
                IDictionary<string, string> curIndex = new Dictionary<string, string>();
                foreach(CALFormField f in ff)
                    if (f.Info.Rows >= i)
                        curIndex.Add(f.Info.Name, f.get_Value(i));
                id.Indexes.Add(curIndex);
            }
            ff = null;
            try
            {
                Console.WriteLine("copying pages.");
                string filename = System.IO.Path.GetTempFileName();
                if (System.IO.File.Exists(filename))
                    System.IO.File.Delete(filename);
                filename = System.IO.Path.ChangeExtension(filename, "tif");
                Console.WriteLine("filename is {0}", filename);
                if ((null == calDoc) || (calDoc.Pages.Count == 0))
                    Console.WriteLine("caldoc is null or has no pages.");
                else
                {
                    Console.WriteLine("calling caldoc.exportpages.");
                    calDoc.ExportPages(filename, Type.Missing, Type.Missing);
                }
                if (System.IO.File.Exists(filename))
                {
                    Console.WriteLine("exporting page data", filename);
                    byte[] pageData = System.IO.File.ReadAllBytes(filename);
                    System.IO.File.Delete(filename);
                    id.Images.Add(new Page("Pages", System.Net.Mime.MediaTypeNames.Image.Tiff, pageData));
                }
                Console.WriteLine("closing and releasing document.");
                calDoc.Close(CloseConstants.calCloseAbortChanges);
                calDoc = null;
                Console.WriteLine("removing listitem from client list.");
                if (null != _list)
                    _list.Remove(listItem);
                listItem = null;
                Console.WriteLine("listitem removed.");
            }
            catch (Exception ex)
            {
                //Console.WriteLine("RetrieveDocument exception:  " + ex.ToString());
                Console.WriteLine("RetrieveDocument exception:  Data: {0}, HelpLink: {1}, Inner Ex: {2}, Message: {3}, Source: {4}, StackTrace: {5}, Target Site: {6}",
                    ex.Data, ex.HelpLink, ((null == ex.InnerException) ? string.Empty : ex.InnerException.ToString()),
                    ex.Message, ex.Source, ex.StackTrace, ex.TargetSite);
                Console.WriteLine("page copy interrupted.");
            }
            Console.WriteLine("returning document.");
            return id;
        }

        public override bool UpdateDocument(Document doc, bool replacePages)
        {
            CALClientListItem listItem = GetListItem(doc.Id);
            if (null == listItem)
                return false;
            CALDocument calDoc = null;
            try
            {
                calDoc = (CALDocument)listItem.Open(OpenConstants.calOpenReadWrite);

                // it'd be really nice to know if the indexes or images changed

                // update indexes
                UpdateIndexes((CALFormFields)calDoc.FormFields(FieldConstants.calFieldsNoViews), doc.Indexes); //calDoc, doc); // 

                // update pages, if requested (via replacement, yuck!)
                if (replacePages)
                {
                    CALPages docPages = (CALPages)calDoc.Pages;
                    while (docPages.Count > 0)
                        docPages.Remove(docPages.get_Item(1));

                    ImportPages(calDoc, doc);
                }
            }
            catch
            {
                _list.Remove(listItem);
                listItem = null;
                return false;
            }

            return true;
        }

        public override bool DeleteDocument(string docId)
        {
            CALClientListItem item = GetListItem(docId);
            if (null == item)
                return false;
            item.Open(OpenConstants.calOpenReadWrite);
            _list.DeleteWorkitem(item, DeleteWorkitemConstants.calDeleteWorkitemSystemDelete);
            item = null;
            return true;
        }

        public override bool ImportPage(string docId, string pageName, string mimeType, byte[] pageData)
        {
            throw new NotImplementedException();
        }

        public override byte[] ExportPage(string docId, string pageName)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Folder> FindFolders(IDictionary<string, string> searchIndexes)
        {
            var itemIds = FindItemIds(searchIndexes, true);
            Console.WriteLine("Found {0} items.", itemIds.Count);
            foreach (string id in itemIds)
            {
                Folder f = RetrieveFolder(id);
                if (null != f)
                    yield return f;
            }
        }

        public override string CreateFolder(Folder folder)
        {
            return CreateFolder(folder, false);
        }

        public string CreateFolder(Folder folder, bool sendToWorkflow)
        {
            #region create the document
            CALWorkitemInfo info = null;
            CALClientListItem clientListItem = null;
            try
            {
                info = (CALWorkitemInfo)_client.CreateFolder(folder.Name,folder.Class);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
            if (null == info)
                return null;

            clientListItem = (CALClientListItem)_list.Add(info.ObjID, calNoRetain);
            if (null == clientListItem)
                return null;

            #endregion

            // open it
            CALFolder calFolder = (CALFolder)clientListItem.Open(OpenConstants.calOpenReadWrite);
            if (null == calFolder)
                return null;

            #region set indexes
            /*
            if (doc.Indexes.Count > 1)
             * warning:  Unisys only supports a single index for a document.
             **/

            // TODO:  verify indexes

            CALFormFields fields = (CALFormFields)calFolder.FormFields(FieldConstants.calFieldsNoViews);
            if (null == fields)
            {
                // warning: no index fields found for document
                Console.WriteLine("No form field indexes found for doc class" + folder.Class + ".");
            }
            else
                UpdateIndexes((CALFormFields)calFolder.FormFields(FieldConstants.calFieldsNoViews), folder.Indexes); //calDoc, doc); // 
            #endregion

            // add children

            CALFolderChildren kids = ((CALFolderChildren)calFolder.Children);
            int ord = 0;
            foreach (string docId in folder.Documents.Select(d => d.Id))
            {
                ord = kids.get_Count(ObjTypeConstants.calObjTypeAll) + 1;
                kids.Add(new CALObjID() { InternalName = docId }, ord);
            }
            foreach (string folderId in folder.Folders.Select(f => f.Id))
            {
                ord = kids.get_Count(ObjTypeConstants.calObjTypeAll) + 1;
                kids.Add(new CALObjID() { InternalName = folderId }, ord);
            }

            #region save and close folder

            calFolder.Save();
            calFolder.RefreshInfo();
            string rv = calFolder.Info.ObjID.InternalName;
            calFolder.Close(CloseConstants.calCloseSaveChanges);
            calFolder = null;

            if (sendToWorkflow)
                clientListItem.SendToDefault(SendConstants.calSendDiscard);
            else 
                _list.Remove(clientListItem);
            #endregion
            folder.Id = rv;
            Console.WriteLine("folder with id {0} created.", rv);
            return rv;
            
        }

        public override Folder RetrieveFolder(string folderId)
        {
            CALClientListItem listItem = null;
            try
            {
                listItem = GetListItem(folderId);
            }
            catch (Exception ex)
            {
                return null;
            }
            if (null == listItem)
                return null;

            // get folder info
            CALFolder calFolder = null;
            // already open?  grab it
            if ((listItem.Info.Status & ObjStatusConstants.calObjStatusOpen) == ObjStatusConstants.calObjStatusOpen)
                calFolder = (CALFolder)listItem.OpenedItem;
            else // have to open it
                calFolder = (CALFolder)listItem.Open(OpenConstants.calOpenReadOnly & OpenConstants.calOpenArchive);

            if (null == calFolder)
            {
                _list.Remove(listItem);
                listItem = null;
                return null;
            }
            Folder fi = new Folder();
            
            // copy folder info
            fi.Name = calFolder.Info.Name;
            fi.Class = calFolder.Class;
            fi.Id = calFolder.Info.ObjID.InternalName;

            // copy index info
            CALFormFields ff = (CALFormFields)calFolder.FormFields(FieldConstants.calFieldsNoViews);
            int indexCount = ff.get_Item(1).Info.Rows;
            for (int i = 0; i < indexCount; ++i)
            {
                IDictionary<string, string> curIndex = new Dictionary<string, string>();
                foreach (CALFormField f in ff)
                    curIndex.Add(f.Info.Name, f.get_Value(i + 1));
                fi.Indexes.Add(curIndex);
            }
            ff = null;

            // copy child info
            CALFolderChildren kids = (CALFolderChildren)calFolder.Children;
            foreach (CALFolderChild fchild in kids.FolderItems)
                fi.Folders.Add(this.RetrieveFolder(fchild.Info.ObjID.InternalName));

            foreach (CALFolderChild dchild in kids.DocumentItems)
                fi.Documents.Add(this.RetrieveDocument(dchild.Info.ObjID.InternalName));
            kids = null;

            // clean up
            calFolder.Close(CloseConstants.calCloseAbortChanges);
            calFolder = null;
            _list.Remove(listItem);
            listItem = null;
            return fi;
        }

        public override bool UpdateFolder(Folder folder)
        {
            CALClientListItem listItem = GetListItem(folder.Id);
            if (null == listItem)
                return false;
            CALFolder calFolder = null;
            CALFolderChildren kids = null;
            try
            {
                calFolder = (CALFolder)listItem.Open(OpenConstants.calOpenReadWrite);

                // it'd be really nice to know if the indexes or images changed

                // update indexes
                UpdateIndexes((CALFormFields)calFolder.FormFields(FieldConstants.calFieldsNoViews), folder.Indexes); //calDoc, doc); // 

                // replace children

                kids = (CALFolderChildren)calFolder.Children;
                while (kids.AllItems.Count > 0)
                    kids.Remove(kids.AllItems.get_Item(1));

                foreach (var fChild in folder.Folders)
                    kids.Add(fChild.Id, 0);

                foreach (var dChild in folder.Documents)
                    kids.Add(dChild.Id, 0);

                //foreach (CALFolderChild fchild in kids.FolderItems)
                //    folder.FolderIds.Add(fchild.Info.ObjID.InternalName);

                //foreach (CALFolderChild dchild in kids.DocumentItems)
                //    folder.DocumentIds.Add(dchild.Info.ObjID.InternalName);

            }
            catch
            {
                kids = null;
                calFolder.Close(CloseConstants.calCloseAbortChanges);
                calFolder = null;
                _list.Remove(listItem);
                listItem = null;
                return false;
            }
            calFolder.Save();
            kids = null;
            calFolder.Close(CloseConstants.calCloseAbortChanges);
            calFolder = null;
            _list.Remove(listItem);
            listItem = null;
            return true;
        }

        public override bool DeleteFolder(string folderId)
        {
            CALClientListItem listItem = GetListItem(folderId);
            if (null == listItem)
                return false;
            listItem.Open(OpenConstants.calOpenReadWrite);
            _list.DeleteWorkitem(listItem, DeleteWorkitemConstants.calDeleteWorkitemSystemDelete);
            listItem = null;
            return true;
        }

        public override bool AddToFolder(string folderId, string itemId)
        {
            bool rv = false;
            CALClientListItem listItem = GetListItem(folderId);
            if (null == listItem)
                return false;
            if (listItem.Info.Type != ObjTypeConstants.calObjTypeFolder)
                return false;
            CALFolder calFolder = null;
            try
            {
                calFolder = (CALFolder)listItem.Open(OpenConstants.calOpenReadWrite);
                CALFolderChildren kids = (CALFolderChildren)calFolder.Children;
                kids.Add(itemId, 0);
                rv = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                rv = false;
            }
            finally
            {
                if (null != calFolder)
                    calFolder.Close(CloseConstants.calCloseAbortChanges);
                calFolder = null;
                listItem = null;
            }
            return rv;

        }

        public override bool RemoveFromFolder(string folderId, string itemId)
        {
            bool rv = false;
            CALClientListItem listItem = GetListItem(folderId);
            if (null == listItem)
                return false;
            if (listItem.Info.Type != ObjTypeConstants.calObjTypeFolder)
                return false;
            CALFolder calFolder = null;
            CALFolderChildren kids = null;
            CALFolderChild childToRemove = null;
            try
            {
                calFolder = (CALFolder)listItem.Open(OpenConstants.calOpenReadWrite);
                kids = (CALFolderChildren)calFolder.Children;
                foreach (CALFolderChild child in kids.AllItems)
                    if (child.Info.ObjID.InternalName == itemId)
                    {
                        childToRemove = child;
                        break;
                    }
                if (null != childToRemove)
                    kids.Remove(childToRemove);
                rv = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                rv = false;
            }
            finally
            {
                childToRemove = null;
                kids = null;
                if (null != calFolder)
                    calFolder.Close(CloseConstants.calCloseAbortChanges);
                calFolder = null;
                listItem = null;
            }
            return rv;
        }

        public IWorkitem GetNext(string queueName)
        {
            CALQueue queue;
            CALClientListItem item = null;
            try
            {
                queue = new CALQueue();
                queue.Client = _client;
                queue.Name = queueName;
                queue.Type = QueueTypeConstants.calQueueTypeWorkset;

                while (true)
                {
                    try
                    {
                        item = queue.GetNext(0);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("queue is empty"))
                            item = null;
                    }
                    if (null == item)
                        return null;
                    if (item.Info.Type == ObjTypeConstants.calObjTypeDocument)
                        return RetrieveDocument(item.Info.ObjID.InternalName);
                    if (item.Info.Type == ObjTypeConstants.calObjTypeFolder)
                        return RetrieveFolder(item.Info.ObjID.InternalName);
                    item.SendToDefault(SendConstants.calSendDiscard);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception for queue {0}", queueName);
                Console.WriteLine(ex.ToString());
                return null;
            }
            finally
            {
                queue = null;
                item = null;
            }

        }

        public bool RouteWorkitem(IWorkitem item)
        {
            return RouteWorkitem(item, "_default");
        }

        public bool RouteWorkitem(IWorkitem item, string workstep)
        {
            return RouteWorkitem(item, workstep, false);
        }

        public bool RouteWorkitem(IWorkitem workitem, string workstep, bool saveChanges)
        {
            bool rv = false;

            Console.WriteLine("Routing workitem {0} ({1}) to {2} (save is {3}).", workitem.Name, workitem.Id, workstep, saveChanges);
            CALClientListItem item = null;

            try
            {

                CALObjID id = new CALObjID() { InternalName = workitem.Id };

                item = _list.Find(id);
                if (null != item)
                {
                    try
                    {
                        _list.Remove(item);
                        item = null;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Routeworkitem, while trying to remove from client list, " + ex.ToString());
                    }
                }

                if (null == item)
                {
                    CALWorkitemWorkstepList list = (CALWorkitemWorkstepList)_client.FindInWorkflow(id);
                    if ((null == list) || (list.Count == 0))
                        item = GetListItem(workitem.Id);
                    else
                    {
                        CALEnumItem enumItem = list.Item[1];
                        item = enumItem.Retrieve(0);
                    }
                }

                if (null == item)
                    item = _list.Add(id, 0);

                if (null != item.OpenedItem)
                    {
                        if (saveChanges)
                            item.OpenedItem.Close(CloseConstants.calCloseSaveChanges);
                        else
                            item.OpenedItem.Close(CloseConstants.calCloseAbortChanges);
                    }
                try
                {
                    if (workstep == "_default")
                        item.SendToDefault(SendConstants.calSendDiscard);
                    else
                    {
                        CALQueue queue = new CALQueue() { Client = _client, Type = QueueTypeConstants.calQueueTypeWorkstep, Name = workstep };
                        item.Send(queue, SendConstants.calSendDiscard);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("RouteWorkitem while sending, exception is " + ex.ToString());
                }
                rv = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in RouteWorkitem:  " + ex.ToString());
                rv = false;
            }
            return rv;
        }

        public void SetName(IWorkitem item, string newName)
        {
            if (string.IsNullOrEmpty(newName))
                throw new ArgumentNullException("new name cannot be null or empty.");

            CALClientListItem listItem = GetListItem(item.Id);
            if (listItem.Info.Type == ObjTypeConstants.calObjTypeFolder)
            {
                CALFolder itemHandle;
                if (null != listItem.OpenedItem)
                    itemHandle = (CALFolder)listItem.OpenedItem;
                else
                    itemHandle = (CALFolder)listItem.Open(OpenConstants.calOpenReadWrite);
                itemHandle.SetName(newName);
                itemHandle.Save();
                itemHandle.RefreshInfo();
                itemHandle.Close(CloseConstants.calCloseSaveChanges);
            }
            else if (listItem.Info.Type == ObjTypeConstants.calObjTypeDocument)
            {
                CALDocument itemHandle;
                if (null != listItem.OpenedItem)
                    itemHandle = (CALDocument)listItem.OpenedItem;
                else
                    itemHandle = (CALDocument)listItem.Open(OpenConstants.calOpenReadWrite);
                itemHandle.SetName(newName);
                itemHandle.Save();
                itemHandle.RefreshInfo();
                itemHandle.Close(CloseConstants.calCloseSaveChanges);
            }
            if (null != listItem)
                _list.Remove(listItem);
        }

        public void PlaceInError(IWorkitem item)
        {
            CALClientListItem listItem = GetListItem(item.Id);
            listItem.PlaceInError(PlaceInErrorConstants.calPlaceInErrorDiscard);
        }

        public void PlaceInError(IWorkitem item, string comment)
        {
            CALClientListItem listItem = GetListItem(item.Id);
            listItem.PlaceInError(PlaceInErrorConstants.calPlaceInErrorDiscard, comment);
        }
    }
}
