﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Paragon.RobotServices.Model;

namespace Paragon.RobotServices.Plugin
{
    public class InputRedaction: IInputModule
    {
       protected class InputFileInfo
        {
            public FileInfo File { get; set; }
            public DirectoryInfo Directory { get; set; }
            public string FolderAlias { get; set; }
            public string SubfolderPath { get; set; }
        }

        // settings fields
        private DirectoryInfo workDir;
        private Dictionary<string, DirectoryInfo> inputDirs = new Dictionary<string, DirectoryInfo>();
        //private List<KeyValuePair<string, DirectoryInfo>> inputDirs = new List<KeyValuePair<string, DirectoryInfo>>();
        private bool includeSubfolders = true;
        private bool roundRobinSequence = false;
        private string imageFileExtension = ".tif";
        private int fileCreationDelay = 10; // seconds
        private bool includeIndexFile = true;
        private string indexFileExtension = ".idx";
        private bool stripImageExtension = false;
        private bool checkIndexFileFirst = false;
        private IndexFieldPairSeparators separatorType = IndexFieldPairSeparators.CrLf;
        private string separatorOther = null;
        private string valueSeparator = "=";
        private bool indexValuesOnly = false;
        private List<string> indexFieldNames = new List<string>();
        private bool errorIfNameValueCountsDontMatch = true;
        private bool metadataOnly = false;
        private string indexSeparator = "\r\n\r\n";
        private DirectoryInfo archiveDir = null;
        private bool archiveFlag = false;
        private DirectoryInfo errorDir = null;
        private string mappingsText = null;

        // setting name constants
        public const string cWorkPath = "Work Path";
        public const string cInputFolders = "Input Folders";
        public const string cIncludeSubfolders = "Include Subfolders";
        public const string cFolderSequencing = "Round-robin Sequencing";
        public const string cImageFileExtension = "Image File Extension";
        public const string cFileCreationDelay = "File Creation Delay";
        public const string cIncludeIndexFile = "Include Index File";
        public const string cIndexFileExtension = "Index File Extension";
        public const string cDontStripImageExtension = "Strip Image File Extension";
        public const string cCheckForIndexFileFirst = "Check For Index File First";
        public const string cPairSeparatorType = "Pair Separator Type";
        public const string cPairSeparatorOther = "Pair Separator Other";
        public const string cValueSeparator = "Value Separator";
        public const string cIndexComment = "Index Comment";
        public const string cOnlyMetaData = "Include Only Meta Data";
        public const string cIndexValuesOnly = "Index File Contains Only Values";
        public const string cIndexFieldNames = "Index Field Names";
        public const string cErrorIfNameValuesDontMatch = "Error If Names And Values Do Not Match";
        public const string cIndexFileValueComment = "Index Field Value Comment";
        public const string cErrorComment = "Error Comment";
        public const string cIndexSeparator = "Multiple Indexes Separator";
        public const string cArchivePath = "Archive Path";
        public const string cArchiveFlag = "Archive Workitems";
        public const string cUnsuccessfulPath = "Unsuccessful Path";

        // index into inputDirs of last folder checked
        private int lastFolderChecked = -1;
        private string lastFolderName = null;
        private DirectoryInfo lastFolder = null;

   

        public InputRedaction(IDictionary<int, string> settings)
        {
            //if (!(settings.ContainsKey(cWorkPath)))
            //    throw new InvalidOperationException(string.Format("Setting '%0' was not provided.", cWorkPath));
            workDir = new DirectoryInfo(settings[1]);
            if (!(workDir.Exists))
                throw new InvalidOperationException(string.Format("Working directory \"{0}\" does not exist.", settings[0]));
            //if ((!(settings.ContainsKey(cInputFolders))) || (string.IsNullOrEmpty(settings[cInputFolders])))
            //    throw new InvalidOperationException(string.Format("List of Input Directories was empty.", settings[cWorkPath]));
            string[] idNamePairs = settings[2].Split(";".ToCharArray());
            if ((idNamePairs == null) || (idNamePairs.Length == 0))
                throw new InvalidOperationException(string.Format("List of Input Directories was empty.", settings[1]));
            foreach (string idNamePair in idNamePairs)
            {
                string[] idSplitPairs = idNamePair.Split("=".ToCharArray());
                if ((idSplitPairs == null) || (idSplitPairs.Length != 2))
                    throw new InvalidOperationException(string.Format("Input Directory specification (\"{0}\") missing an entry in format name=value.", idNamePair));
                inputDirs.Add(idSplitPairs[0], new DirectoryInfo(idSplitPairs[1]));
                if (!(inputDirs[idSplitPairs[0]].Exists))
                    throw new InvalidOperationException(string.Format("Input Directory named \"{0}\" at path \"%1\" does not exist.", idNamePair[0], idNamePair[1]));
            }
            //if (settings.ContainsKey(cIncludeSubfolders))
                bool.TryParse(settings[3], out includeSubfolders);
            //if (settings.ContainsKey(cFolderSequencing))
                bool.TryParse(settings[4], out roundRobinSequence);
            //if (settings.ContainsKey(cImageFileExtension))
            //{
                imageFileExtension = settings[5];
                if (!(imageFileExtension.StartsWith(".")))
                    imageFileExtension = "." + imageFileExtension;

                indexFileExtension = settings[6];
                if (!(indexFileExtension.StartsWith(".")))
                    indexFileExtension = "." + indexFileExtension;


                int.TryParse(settings[7], out fileCreationDelay);

                bool.TryParse(settings[8], out includeIndexFile);

                if (bool.TryParse(settings[9], out stripImageExtension))
                    stripImageExtension = !stripImageExtension;

                bool.TryParse(settings[10], out checkIndexFileFirst);

                object parsed;
                try
                {
                    parsed = Enum.Parse(typeof(IndexFieldPairSeparators), settings[11], true);
                    if (parsed != null)
                        separatorType = (IndexFieldPairSeparators)parsed;
                }
                catch
                { separatorType = IndexFieldPairSeparators.CrLf; }
            //}
            //if (settings.ContainsKey(cPairSeparatorOther))
                separatorOther = settings[12];
            //if (settings.ContainsKey(cValueSeparator))
                valueSeparator = settings[13];
            //if (settings.ContainsKey(cIndexValuesOnly))
                bool.TryParse(settings[14], out indexValuesOnly);
            //if ((settings.ContainsKey(cIndexFieldNames)) && (!(string.IsNullOrEmpty(settings[cIndexFieldNames]))))
            if (!string.IsNullOrEmpty(settings[15]))
                foreach (string fieldName in settings[15].Split(",".ToCharArray()))
                    indexFieldNames.Add(fieldName);
            //if (settings.ContainsKey(cErrorIfNameValuesDontMatch))
                bool.TryParse(settings[16], out errorIfNameValueCountsDontMatch);
            //if ((settings.ContainsKey(cIndexSeparator)) && (!(string.IsNullOrEmpty(settings[cIndexSeparator]))))
                indexSeparator = settings[17];
            //if ((settings.ContainsKey(cArchiveFlag)) && (!(string.IsNullOrEmpty(settings[cArchiveFlag]))))
            //{
                bool.TryParse(settings[18], out archiveFlag);
                if ((archiveFlag) && ((!settings.ContainsKey(19)) || (string.IsNullOrEmpty(settings[19]))))
                    throw new ArgumentException("Archive Path setting must be included when Archive Flag is true.");
                archiveDir = new DirectoryInfo(settings[19]);
                if (!archiveDir.Exists)
                    throw new ArgumentException(string.Format("Archive Path '{0}' must exist.", settings[19]));
                errorDir = new DirectoryInfo(settings[20]);
                if (!errorDir.Exists)
                    throw new ArgumentException(string.Format("Unsuccessful Path '{0}' must exist.", settings[20]));
                mappingsText = settings[21];
            //}
        }

        #region IInputModule Members

        public IWorkitem Get()
        {
            //throw new NotImplementedException();

            // log:  getting incoming file
            InputFileInfo incomingFile = GetIncomingFile();

            // log:  converting file to workitem
            if (incomingFile == null)
                return null;
            else
                return WorkitemFromFileInfo(incomingFile);
        }

        protected InputFileInfo GetIncomingFile()
        {
            if (inputDirs.Count == 0)
                throw new InvalidOperationException("No input folders defined!");

            if (inputDirs.Count == 1)
                return CheckDirectory(inputDirs.First());

            int startIndex = -1;
            if ((lastFolderChecked > -1) && (lastFolderChecked < inputDirs.Count))
                startIndex = lastFolderChecked;
            else
                startIndex = 0;

            if ((roundRobinSequence) && (lastFolderChecked > -1))
            {
                startIndex += 1;
                if (startIndex >= inputDirs.Count)
                    startIndex = 0;
            }

            InputFileInfo incomingFile = null;
            int curIndex = startIndex;
            while (incomingFile == null)
            {
                KeyValuePair<string, DirectoryInfo> curDir = inputDirs.ElementAt(curIndex);
                incomingFile = CheckDirectory(curDir);
                if (incomingFile == null)
                {
                    curIndex += 1;
                    if (curIndex >= inputDirs.Count)
                        curIndex = 0;
                    if (curIndex == startIndex)
                        break;
                }
            }
            if (incomingFile != null)
                lastFolderChecked = curIndex;

            return incomingFile;
        }

        protected InputFileInfo CheckDirectory(KeyValuePair<string, DirectoryInfo> dirToCheck)
        {
            FileInfo incomingFile = null;
            InputFileInfo fileInfo = null;
            string pattern = "*";
            if ((checkIndexFileFirst) || (metadataOnly))
            {
                if (indexFileExtension.StartsWith("."))
                    pattern += indexFileExtension;
                else
                    pattern += "." + indexFileExtension;
                incomingFile = dirToCheck.Value.GetFiles(pattern, SearchOption.AllDirectories).FirstOrDefault();
            }
            else
            {
                if(imageFileExtension.Contains("*"))
                   pattern += "*.*";
                else if(imageFileExtension.StartsWith("."))
                    pattern += imageFileExtension;
                else
                    pattern += "." + imageFileExtension;

                incomingFile = dirToCheck.Value.GetFiles(pattern, SearchOption.AllDirectories).FirstOrDefault();
            }

            if (incomingFile != null)
            {
                fileInfo = new InputFileInfo();
                fileInfo.File = incomingFile;
                fileInfo.FolderAlias = dirToCheck.Key;
                fileInfo.Directory = dirToCheck.Value;
                if (incomingFile.DirectoryName == dirToCheck.Value.FullName)
                    fileInfo.SubfolderPath = "";
                else
                    fileInfo.SubfolderPath = incomingFile.DirectoryName.Substring(dirToCheck.Value.FullName.Length + 1);
            }
            return fileInfo;
        }

        protected IWorkitem WorkitemFromFileInfo(InputFileInfo fileInfo)
        {
            //IWorkitem = 
            bool indexFileFirst = includeIndexFile && checkIndexFileFirst;
            //if (indexFileFirst || metadataOnly)
                // log:  found incoming index file 
            // else
                // log:  found incoming image file

            if (fileCreationDelay > 0)
                System.Threading.Thread.Sleep(fileCreationDelay * 1000);

            // log:  ensuring original file still exists

            if (!(fileInfo.File.Exists))
                throw new InvalidOperationException(string.Format("Incoming file '{0}' no longer exists.", fileInfo.File.FullName));

            // create workitem object

            Document workitem = new Document();

            workitem.Id = workitem.Name = Path.GetFileNameWithoutExtension(fileInfo.File.Name);
            workitem.Status = WorkitemStatus.NotProcessed;
            FileInfo indexFile = null;
            FileInfo imageFile = null;

            if (indexFileFirst)
            {
                indexFile = fileInfo.File;
                if (!metadataOnly)
                {
                    string imagePattern = Path.GetFileNameWithoutExtension(fileInfo.File.Name) + "*" + 
                        imageFileExtension;
                    if (fileInfo.SubfolderPath.Length > 0)
                        imageFile = fileInfo.Directory.GetDirectories(fileInfo.SubfolderPath).First().GetFiles(imagePattern).FirstOrDefault();
                    else
                        imageFile = fileInfo.Directory.GetFiles(imagePattern).FirstOrDefault();
                    if ((imageFile == null) || (!imageFile.Exists))
                        throw new InvalidOperationException(string.Format("No image files associated with index file '{0}' were found.", fileInfo.File.Name));
                }
            }
            else // image file first
            {
                imageFile = fileInfo.File;
                if (includeIndexFile)
                {
                    string indexPattern = Path.GetFileNameWithoutExtension(fileInfo.File.Name) + "*" +
                        indexFileExtension;
                    //indexFile = fileInfo.Directory.GetFiles(indexPattern).FirstOrDefault();
                    if (fileInfo.SubfolderPath.Length > 0)
                        indexFile = fileInfo.Directory.GetDirectories(fileInfo.SubfolderPath).First().GetFiles(indexPattern).FirstOrDefault();
                    else
                        indexFile = fileInfo.Directory.GetFiles(indexPattern).FirstOrDefault();
                    if ((indexFile == null) || !indexFile.Exists)
                        throw new InvalidOperationException(string.Format("Unable to find corresponding index file for '{0}'", fileInfo.File.Name));
                }
            }

            // move files to working directory
            if ((imageFile != null) && imageFile.Exists)
                imageFile.MoveTo(Path.Combine(workDir.FullName, imageFile.Name));
            if ((indexFile != null) && indexFile.Exists)
                indexFile.MoveTo(Path.Combine(workDir.FullName, indexFile.Name));

            // add images to the workitem
            if (!metadataOnly)
            {
                //FileStream fs = new FileStream(imageFile.FullName, FileMode.Open, FileAccess.Read);
                //workitem.Images.Add(System.Drawing.Image.FromFile(imageFile.FullName));
                workitem.Images.AddRange(Page.GetPages(imageFile.FullName));
                //fs.Close();
            }
            if (includeIndexFile)
            {
                string indexString = File.ReadAllText(indexFile.FullName);
                foreach (var index in ParseIndexes(indexString))
                    workitem.Indexes.Add(index);
            }

            // make sure at least "system" indexes exist
            if (workitem.Indexes.Count == 0)
            {
                Dictionary<string, string> systemIndexes = new Dictionary<string, string>();
                systemIndexes.Add("_SourceFolder", fileInfo.FolderAlias);
                systemIndexes.Add("_SubfolderPath", fileInfo.SubfolderPath);
                workitem.Indexes.Add(systemIndexes);
            }
            else
                foreach (var index in workitem.Indexes)
                {
                    index.Add("_SourceFolder", fileInfo.FolderAlias);
                    index.Add("_SubfolderPath", fileInfo.SubfolderPath);
                }

            IEnumerable<Mapping> mappings = null;
            if (!string.IsNullOrEmpty(mappingsText))
            {
                try
                {
                    mappings = Mapping.FromXml(mappingsText);
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }

            if (null != mappings)
            {
                for (int i = 0; i < workitem.Indexes.Count; ++i)
                    workitem.Indexes[i] = mappings.TranslateFields(workitem.Indexes[i]);
            }

            return workitem;
        }

        protected IEnumerable<Dictionary<string, string>> ParseIndexes(string indexesString)
        {
            List<Dictionary<string, string>> rv = new List<Dictionary<string, string>>();
            string[] unparsedIndexes = indexesString.Split(new string[] {indexSeparator}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string unparsedIndex in unparsedIndexes)
            {
                string[] indexPairs;
                // fill indexPairs
                switch (separatorType)
                {
                    case IndexFieldPairSeparators.Cr:
                        indexPairs = unparsedIndex.Split("\r".ToCharArray());
                        break;
                    case IndexFieldPairSeparators.CrLf:
                        indexPairs = unparsedIndex.Split(new string[] {"\r\n"},  StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case IndexFieldPairSeparators.CrOrLf:
                        string temp = unparsedIndex.Replace("\n", "\r");
                        temp = temp.Replace("\r\r", "\r");
                        indexPairs = temp.Split("\r".ToCharArray());
                        break;
                    case IndexFieldPairSeparators.Lf:
                        indexPairs = unparsedIndex.Split("\n".ToCharArray());
                        break;
                    default:
                        indexPairs = unparsedIndex.Split(new string[] {separatorOther}, StringSplitOptions.RemoveEmptyEntries);
                        break;
                }
                // create index dictionary
                Dictionary<string, string> indexDict = new Dictionary<string, string>();
                // populate dict
                if (indexValuesOnly)
                {
                    if (errorIfNameValueCountsDontMatch)
                        if (indexFieldNames.Count != indexPairs.Length)
                            throw new InvalidOperationException("Names count and values count mismatch while parsing index file contents " + indexesString);
                    for (int i = 0; i < indexFieldNames.Count; ++i)
                        if (i < indexPairs.Length)
                            indexDict.Add(indexFieldNames[i], indexPairs[i]);
                }
                else
                    foreach (string indexPair in indexPairs)
                    {
                        string[] indexParts = indexPair.Split(valueSeparator.ToCharArray());
                        if ((indexParts != null) && (indexParts.Length >= 2))
                            indexDict.Add(indexParts[0], indexParts[1]);
                    }
                // add to indexes list
                rv.Add(indexDict);
            }
            return rv;
        }

        public IEnumerable<IWorkitem> GetBatch()
        {
            throw new NotSupportedException();
        }

        public void Delete(IWorkitem item)
        {
            // construct file names
            string baseName = item.Name;
            string indexFileName = item.Name + indexFileExtension;
            string imageFileName = item.Name + imageFileExtension;

            // delete the files, if they exist
            if (File.Exists(Path.Combine(workDir.FullName, indexFileName)))
            {
                File.Delete(Path.Combine(workDir.FullName, indexFileName));
            }
            if (File.Exists(Path.Combine(workDir.FullName, imageFileName)))
            {
                File.Delete(Path.Combine(workDir.FullName, imageFileName));
            }
        }

        public void Move(IWorkitem item, System.IO.DirectoryInfo whereTo)
        {
            if (null == item)
                throw new ArgumentNullException("item cannot be null.");
            if (null == whereTo)
                throw new ArgumentNullException("destination cannot be null.");
            // flags so that we don't write files if they already exist
            bool imageFileMoved = false;
            bool indexFileMoved = false;
            // construct file names
            string baseName = item.Name;
            string indexFileName = item.Name + indexFileExtension;
            string imageFileName = item.Name + imageFileExtension;

            //check we have a wild card character in image file extension.
            //We will then need to find file from teh work path and then move

            if (imageFileExtension.Contains(".*"))
            {

                FileInfo tempFile = workDir.GetFiles(imageFileName, SearchOption.AllDirectories).FirstOrDefault();               
                if (tempFile != null)
                {
                    imageFileName = tempFile.Name;
                }
            }
   
            // move the files, if they exist
            if (File.Exists(Path.Combine(workDir.FullName, indexFileName)))
            {
                if (File.Exists(Path.Combine(whereTo.FullName, indexFileName)))
                    File.Delete(Path.Combine(whereTo.FullName, indexFileName));
                File.Move(Path.Combine(workDir.FullName, indexFileName),
                    Path.Combine(whereTo.FullName, indexFileName));
                indexFileMoved = true;
            }
            if (File.Exists(Path.Combine(workDir.FullName, imageFileName)))
            {
                if (File.Exists(Path.Combine(whereTo.FullName, imageFileName)))
                    File.Delete(Path.Combine(whereTo.FullName, imageFileName));
                File.Move(Path.Combine(workDir.FullName, imageFileName),
                    Path.Combine(whereTo.FullName, imageFileName));
                imageFileMoved = true;
            }

            // create them in whereTo even if they aren't in the workDir
            if (!imageFileMoved)
            {
                if (item is Document)
                {
                    Document doc = item as Document;
                    if (File.Exists(Path.Combine(whereTo.FullName, imageFileName)))
                        File.Delete(Path.Combine(whereTo.FullName, imageFileName));
                    doc.SaveImagesToTiff(Path.Combine(whereTo.FullName, imageFileName));
                }
            }
            if (!indexFileMoved)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var index in item.Indexes)
                {
                    foreach (var kvp in index)
                    {
                        if (!kvp.Key.StartsWith("_"))
                        {
                            sb.Append(kvp.Key).Append(valueSeparator).Append(kvp.Value);
                            switch (separatorType)
                            {
                                case IndexFieldPairSeparators.Cr:
                                    sb.Append("\r");
                                    break;
                                case IndexFieldPairSeparators.CrLf:
                                    sb.Append("\r\n");
                                    break;
                                case IndexFieldPairSeparators.CrOrLf:
                                case IndexFieldPairSeparators.Lf:
                                    sb.Append("\n");
                                    break;
                                default:
                                    sb.Append(separatorOther);
                                    break;
                            }
                        }
                    }
                    if (item.Indexes.IndexOf(index) < (item.Indexes.Count - 1))
                        sb.Append(indexSeparator);
                }
                File.WriteAllText(Path.Combine(whereTo.FullName, indexFileName), sb.ToString());
            }

        }

        public void UpdateStatus(IWorkitem item, WorkitemStatus status)
        {
            item.Status = status;
        }

        public bool SupportsMove
        {
            get { return true; }
        }

        public bool SupportsDelete
        {
            get { return true; }
        }

        public bool SupportsUpdateStatus
        {
            get { return true; }
        }

        public bool SupportsBatching
        {
            get { return false; }
        }

        #endregion

        #region IIoCommon Members

        public bool Connect()
        {
            throw new NotSupportedException();
        }

        public bool Connected
        {
            get { throw new NotSupportedException(); }
        }

        public void Disconnect()
        {
            throw new NotSupportedException();
        }

        public bool RequiresConnection
        {
            get { return true; }
        }

        #endregion


        public void PostProcess(IWorkitem item)
        {
            if ((item.Status == WorkitemStatus.Successful) && (archiveFlag))
            {
                DirectoryInfo realArchiveDir = new DirectoryInfo(
                    Path.Combine(archiveDir.FullName, 
                        DateTime.Today.ToString("yyyy-MM-dd"),
                        item.Indexes[0]["_SourceFolder"], 
                        item.Indexes[0]["_SubfolderPath"]));
                if (!realArchiveDir.Exists)
                    realArchiveDir.Create();
                Move(item, realArchiveDir);
            }
            else
            {
                DirectoryInfo realErrorDir = new DirectoryInfo(
                    Path.Combine(errorDir.FullName, 
                        DateTime.Today.ToString("yyyy-MM-dd"),
                        item.Indexes[0]["_SourceFolder"],
                        item.Indexes[0]["_SubfolderPath"]));
                if (!realErrorDir.Exists)
                    realErrorDir.Create();
                Move(item, realErrorDir);
            }
        }

        public IConnector Connector
        {
            get { throw new NotImplementedException(); }
        }

        public int Id
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string FileName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Type
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public bool IsConnected
        {
            get { throw new NotImplementedException(); }
        }


    }
}
