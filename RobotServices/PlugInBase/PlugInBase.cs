﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{

    // Note to inheritors:  When you create a concrete Plugin class,
    // make sure to create a constructor taking a single integer argument.
    // This constructor will be called when the plugin is created.
    // The argument's value will be the id of the module associated with the
    // plug-in.  This value is used to lookup the settings for the module.
    // (Retrieve the field id's from the ModuleSettings service/table.
    //  This gives a settings dictionary for the plug-in.)
    public abstract class PlugInBase
    {
        public abstract long Run();
    }
}
