﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;
using Paragon.RobotServices.Plugin;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;


namespace Paragon.RobotServices.Plugin
{
    public class OutputParaImage : IOutputModule
    {
        protected ParagonImagingAPI.ParaImgSession session;
        protected IList<Model.LogEntry> logger;
        protected int id;
        protected string name;
        protected string fileName;
        protected string kind;

        protected string Username;
        protected string Password;
        protected string Server;
        protected string Url;
        protected string Port = "8080";
        protected string Domain = "jdbc/Paragon";
        protected string Mappings = "";

        //public OutputParaImage(IDictionary<string, string> settings)
        //{
        //    if (settings.ContainsKey("Username") && !string.IsNullOrEmpty(settings["Username"]))
        //        Username = settings["Username"];
        //    else
        //        throw new ArgumentException("settings must have a 'Username' entry.");
        //    if (settings.ContainsKey("Password") && !string.IsNullOrEmpty(settings["Password"]))
        //        Password = settings["Password"];
        //    else
        //        throw new ArgumentException("settings must have a 'Password' entry.");
        //    if (settings.ContainsKey("Server") && !string.IsNullOrEmpty(settings["Server"]))
        //        Server = settings["Server"];
        //    else
        //        throw new ArgumentException("settings must have a 'Server' entry.");
        //    if (settings.ContainsKey("Url") && !string.IsNullOrEmpty(settings["Url"]))
        //        Url = settings["Url"];
        //    else
        //        throw new ArgumentException("settings must have a 'Url' entry.");
        //    if (settings.ContainsKey("Port") && !string.IsNullOrEmpty(settings["Port"]))
        //        Port = settings["Port"];
        //    if (settings.ContainsKey("Domain") && !string.IsNullOrEmpty(settings["Domain"]))
        //        Domain = settings["Domain"];

        //    session = new ParagonImagingAPI.ParaImgSession();
        //}

        public OutputParaImage(IDictionary<int, string> settings)
        {
            //if (settings.ContainsKey("Username") && !string.IsNullOrEmpty(settings["Username"]))
                Username = settings[1];
            //else
            //    throw new ArgumentException("settings must have a 'Username' entry.");
            //if (settings.ContainsKey("Password") && !string.IsNullOrEmpty(settings["Password"]))
                Password = settings[2];
            //else
            //    throw new ArgumentException("settings must have a 'Password' entry.");
            //if (settings.ContainsKey("Server") && !string.IsNullOrEmpty(settings["Server"]))
                Server = settings[3];
            //else
            //    throw new ArgumentException("settings must have a 'Server' entry.");
            //if (settings.ContainsKey("Url") && !string.IsNullOrEmpty(settings["Url"]))
                Url = settings[4];
            //else
            //    throw new ArgumentException("settings must have a 'Url' entry.");
            if (settings.ContainsKey(5) && !string.IsNullOrEmpty(settings[5]))
                Port = settings[5];
            if (settings.ContainsKey(6) && !string.IsNullOrEmpty(settings[6]))
                Domain = settings[6];
            if (settings.ContainsKey(7) && !string.IsNullOrEmpty(settings[7]))
                Mappings = settings[7];
            session = new ParagonImagingAPI.ParaImgSession();
        }

        public long Process(IWorkitem item)
        {
            long rv = 0;

            if (Mappings.Length > 0)
            {
                try
                {
                    var mappings = Mapping.FromXml(Mappings);
                    if (mappings.Count > 0)
                        //foreach (var index in item.Indexes)
                        for (int i = 0; i < item.Indexes.Count; i++)
                        {
                            IDictionary<string, string> temp = mappings.TranslateFields(item.Indexes[i]);
                            item.Indexes[i] = temp;
                        }

                }
                catch (Exception ex)
                {
                    Logger.Add(new LogEntry()
                    {
                        Severity = LogSeverity.Warning,
                        Message = string.Format("Invalid mappings defined for robot, unable to apply mappings.  Exception was:  \n{0}", ex.ToString()),
                        Id = Logger.Count + 1,
                        EventDate = DateTime.Now
                    });
                }
            } else
                Logger.Add(new LogEntry()
                {
                    Severity = LogSeverity.Debug,
                    Message = string.Format("No mappings to apply to {0}", item.Id),
                    Id = Logger.Count + 1,
                    EventDate = DateTime.Now
                });

            Document asDoc = null;
            ParagonImagingAPI.ParaImgDocClass docClass = null;

            if (null == item)
                return rv;
            
            if (item is Document)
                asDoc = item as Document;
            else
            {
                item.Status = WorkitemStatus.Error;
                item.ErrorSource = "OutputParaImage.Process";
                item.ErrorDescription = "Only Document workitems can be processed by OutputParaImage.";
                return rv;
            }

            if (item.Indexes[0].ContainsKey("_Class") && string.IsNullOrEmpty(item.Class))
                item.Class = item.Indexes[0]["_Class"];
            if (item.Indexes[0].ContainsKey("_Name") && string.IsNullOrEmpty(item.Name))
                item.Name = item.Indexes[0]["_Name"];

            if (string.IsNullOrEmpty(item.Class))
            {
                item.Status = WorkitemStatus.Error;
                item.ErrorSource = "OutputParaImage.Process";
                item.ErrorDescription = "ParaImage documents must have a class.";
                return rv;
            }

            for (int i = 1; i <= session.DocumentClasses.Count; i++)
                if (session.DocumentClasses[i].szDocumentClassName == asDoc.Class)
                    docClass = session.DocumentClasses[i];
            
            if (null == docClass)
            {
                item.Status = WorkitemStatus.Error;
                item.ErrorSource = "OutputParaImage.Process";
                item.ErrorDescription = string.Format("Document class of {0} does not exist in {1}.", asDoc.Class, session.DataSource);
                return rv;
            }

            string tempfile = SaveImagesToTempFile(asDoc);

            ParagonImagingAPI.ParaImgDocument doc = new ParagonImagingAPI.ParaImgDocument();

            ParagonImagingAPI.ParaImgAttributes attrs = new ParagonImagingAPI.ParaImgAttributes();
            foreach (ParagonImagingAPI.ParaImgAttribute att in docClass.attributes)
            {
                if (att.IsRequired)
                    if (!asDoc.Indexes[0].ContainsKey(att.Name))
                    {
                        string errMessage = string.Format("Workitem does not have a value for the required attribute '{0}'", att.Name);
                        if (string.IsNullOrEmpty(asDoc.ErrorDescription ))
                            asDoc.ErrorDescription = errMessage;
                        else
                            asDoc.ErrorDescription += Environment.NewLine + errMessage;
                        asDoc.Status = WorkitemStatus.Error;
                        asDoc.ErrorSource = "OutputParaImage.Process";
                    }
                if (asDoc.Indexes[0].ContainsKey(att.Name))
                    attrs.Add(att.Name, att.DataType, att.Protected, asDoc.Indexes[0][att.Name]);
            }
            if (!string.IsNullOrEmpty(asDoc.ErrorDescription))
                return rv;

            try
            {
                string rc = doc.CreateDocument(asDoc.Class, 
                    ((asDoc.Name.Length > 50) ? asDoc.Name.Substring(0, 49) : asDoc.Name),
                    tempfile, attrs, ref session);
                asDoc.Status = WorkitemStatus.Successful;
                asDoc.Id = rc;
                rv = 1;
            }
            catch (Exception ex)
            {
                asDoc.Status = WorkitemStatus.Error;
                asDoc.ErrorDescription = ex.ToString();
                asDoc.ErrorSource = "OutputParaImage.Process";
            }
            File.Delete(tempfile);
            return rv;
        }

        public string SaveImagesToTempFile(Document doc)
        {
            string rv = System.IO.Path.GetTempFileName();
            string ext = FileExtensions.MimeTypes.Where(mt => mt.Value == doc.Images[0].MimeType).First().Key;
            rv = System.IO.Path.ChangeExtension(rv, ext);
            // copy first image to temp file
            if (ext.Contains("tif"))
            {
                MemoryStream imageStream = new MemoryStream();
                Image first = Image.FromStream(new MemoryStream(doc.Images[0].Data));
                first.Save(imageStream, ImageFormat.Tiff);
                Image tiff = Image.FromStream(imageStream);
                ImageCodecInfo tiffInfo = ImageCodecInfo.GetImageEncoders().First(ici => ici.MimeType == "image/tiff");
                EncoderParameters firstParams = new EncoderParameters(2);
                firstParams.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
                firstParams.Param[1] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
                tiff.Save(rv, tiffInfo, firstParams);

                // copy subsequent images to temp file

                EncoderParameters restParams = new EncoderParameters(2);
                restParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
                restParams.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
                for (int i = 1; i < doc.Images.Count; i++)
                {
                    tiff.SaveAdd(Image.FromStream(new MemoryStream(doc.Images[i].Data)), restParams);
                }

                // flush the file
                EncoderParameters flushParams = new EncoderParameters(1);
                flushParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.Flush);
                tiff.SaveAdd(flushParams);
            }
            else
            {
                FileStream fs = new FileStream(rv, FileMode.Create);
                fs.Write(doc.Images[0].Data, 0, doc.Images[0].Data.Length);
                fs.Flush();
                fs.Close();
                fs = null;
            }
            return rv;
        }

        public long ProcessBatch(IEnumerable<IWorkitem> items)
        {
            throw new NotSupportedException();
        }

        public bool SupportsBatching
        {
            get { return false; }
        }

        public bool RequiresConnection
        {
            get { return true; }
        }

        public IConnector Connector
        {
            get
            {
                throw new NotSupportedException();
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        public bool ShareConnector
        {
            get { return false; }
        }

        public IList<Model.LogEntry> Logger
        {
            get
            {
                return logger;
            }
            set
            {
                if (null == value)
                    throw new ArgumentNullException("Logger");
                logger = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("Name");
                name = value;
            }
        }

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("FileName");
                fileName = value;
            }
        }

        public string Type
        {
            get
            {
                return kind;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("Type");
                kind = value;
            }
        }

        public bool Connect()
        {
            //bool logValue = true;
            //bool logValue = false;
            //session.set_Logging(ref logValue);
            bool rv = false;
            Exception caught = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    if (session.LoginEx(Username, Password, Server, Url) > 0)
                    {
                        rv = true;
                        return rv;
                    }
                }
                catch (Exception ex)
                {
                    caught = ex;
                }
            }
            logger.Add(new LogEntry() { Severity = LogSeverity.Error, EventDate = DateTime.Now, Id = logger.Max(le => le.Id) + 1, 
                Message = string.Format("Unable to log into ParaImage after 5 attempts, exception was:\n{0}", caught.ToString()) });
            return false;            
        }

        public bool IsConnected
        {
            get { return session.IsLoggedIn; }
        }

        public void Disconnect()
        {
            session.LogoutEx(ref Domain);
        }
    }
}
