﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Captures;
using Paragon.Robot.Model.Capture.KofaxAscentCapture;

namespace Paragon.RobotServices.Captures.Kofax
{
    public class KofaxCaptureConnector : CaptureConnector
    {
        protected Application _app = null;
        protected Login _login = null;

        public KofaxCaptureConnector(Capture c, IEnumerable<Settings.Setting> settings, IEnumerable<MappingGroups.MappingGroup> mappings)
            : base(c, settings, mappings)
        {
            _login = new Login();
            if (_login.RequiresLogin)
                _app = _login.Logon(c.UserId, c.Password);
            else
                _app = _login.ActiveApplication;
        }

        public override void Dispose()
        {
            _app = null;
            _login.Logout();
            _login = null;
        }

        public override IDictionary<string, string> CreateBatch(IEnumerable<byte[]> images, IDictionary<string, string> inputs)
        {
            IDictionary<string, string> batchSettings = PreProcess(inputs);
            Batch b = PrepareBatch(batchSettings, inputs);
            IDictionary<string, string> rv = ImportFiles(b, batchSettings, inputs, images);
            CloseBatch(rv["Status"] == "Successful", batchSettings);
            return rv;
        }

        protected IDictionary<string, string> ErrorDictionary(int code, string shortMsg, string longMsg)
        {
            Dictionary<string, string> rv = new Dictionary<string, string>();
            rv.Add("_Error.Code", code.ToString());
            rv.Add("_Error.Short", shortMsg);
            rv.Add("_Error.Long", longMsg);
            rv.Add("_Error", string.Format("{0}({1}):  {2}", rv["_Error.Code"],
                    rv["_Error.Short"], rv["_Error.Long"]));
            return rv;
        }

        protected IDictionary<string, string> PreProcess(IDictionary<string, string> inputs)
        {
            MappingGroups.MappingGroupService mgs = new MappingGroups.MappingGroupService();
            Dictionary<string, string> merged = new Dictionary<string, string>(inputs);
            foreach (var setting in _settings)
                merged[setting.Name] = setting.Value;
            return mgs.ConditionalTranslateFields(_groups.FirstOrDefault().GroupId, merged, false);
        }

        protected internal Batch PrepareBatch(IDictionary<string, string> batchSettings, IDictionary<string, string> inputs)
        {
            Batch b = _app.CreateBatch(batchSettings["_ClassName"], batchSettings["_Name"]);
            foreach (var f in b.BatchFields)
                if (inputs.ContainsKey(f.Name))
                {
                    if (inputs[f.Name].Length > f.Length)
                        f.Value = inputs[f.Name].Substring(0, f.Length);
                    else
                        f.Value = inputs[f.Name];
                }
            return b;
        }

        protected IDictionary<string, string> ImportFiles(Batch batch, IDictionary<string, string> batchSettings, 
            IDictionary<string, string> inputs, IEnumerable<byte[]> files)
        {
            Dictionary<string, string> rv = new Dictionary<string, string>();
            int pageCount = 0;
            bool importError = false;
            string errMessage = string.Empty;
            string docNames = string.Empty;
            try
            {
                foreach (var file in files)
                {
                    string fileName = System.IO.Path.GetTempFileName();
                    System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create);
                    fs.Write(file, 0, file.Length);
                    fs.Close();
                    var pages = batch.ImportFile(fileName);
                    pageCount += pages.Count;
                }
            }
            catch (Exception ex)
            {
                importError = true;
                errMessage = ex.Message;
            }

            if (!(batch.SupportsAutomaticSeparationAndFormId))
            {
                var doc = batch.CreateDocument();
                docNames += "," + doc.UniqueId.ToString();
                if (!(string.IsNullOrEmpty(batchSettings["formTypeName"])))
                    foreach (var ft in batch.FormTypes)
                        if (ft.Name == batchSettings["formTypeName"])
                        {
                            doc.FormType = ft;
                            break;
                        }
                foreach (var page in batch.LoosePages)
                    page.MoveToDocument(doc);
            }
            foreach (var doc in batch.Documents)
            {
                docNames += "," + doc.UniqueId.ToString();
                pageCount += doc.PageCount;
                if (null != doc.FormType)
                {
                    foreach (var f in doc.IndexFields)
                    {
                        if (inputs.ContainsKey(f.Name))
                        {
                            if (inputs[f.Name].Length > f.Length)
                                f.Value = inputs[f.Name].Substring(0, f.Length);
                            else
                                f.Value = inputs[f.Name];
                        }
                    }
                }
                else
                    rv.Add("Warning", "Batch does not separate automatically and no valid form type was given.  Document indexes were not updated.");
            }
            if (importError)
            {
                var setting = _settings.Where(s => s.Name == cImageErrorReject).FirstOrDefault();
                if ((null != setting) && ( setting.Value.ToLowerInvariant() == "true"))
                {
                    foreach (var doc in batch.Documents)
                    {
                        setting = _settings.Where(s => s.Name == cImageRejectNoteText).FirstOrDefault();
                        if ((null == setting) || (setting.Value.Length == 0))
                            doc.Note = "Warning:  Encountered early End of File while importing images.";
                        else
                            doc.Note = setting.Value;
                    }
                }
                rv.Add("Status", "Errors");
                rv.Add("Error", errMessage);
            }
            else
            {
                rv.Add("Status", "Successful");
                rv.Add("Error", string.Empty);
            }
            rv.Add("DestId", docNames.Substring(1, docNames.Length - 1));
            rv.Add("DestBatchId", batch.Id.ToString());
            rv.Add("DocumentCount", batch.DocumentCount.ToString());
            rv.Add("PageCount", pageCount.ToString());

            return rv;
        }

        public const string cImageErrorReject = "RejectBadImages";
        public const string cImageRejectNoteText = "RejectNoteText";
        public const string cBatchDestination = "BatchDestination";

        protected void CloseBatch(bool success, IDictionary<string, string> batchSettings)
        {
            if (success)
            {
                switch (batchSettings[cBatchDestination].ToLowerInvariant())
                {
                    case "SendToDefault":
                        _app.CloseBatch();
                        break;
                    case "SendToScan":
                        _app.SuspendBatch();
                        break;
                    default:
                        _app.RejectBatch("No destination specified");
                        break;
                }
            }
            else
                _app.DeleteBatch();
        }
    }
}
