﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public class Generator : IInputModule
    {
        protected int count = 0;
        public IWorkitem Get()
        {
            count++;
            System.Threading.Thread.Sleep(100);
            return new Document()
            {
                Id = count.ToString(),
                Name = string.Format("Workitem {0}", count),
                Status = WorkitemStatus.NotProcessed,
                DocumentCount = 0,
                PageCount = 0
            };
        }

        public IEnumerable<IWorkitem> GetBatch()
        {
            List<IWorkitem> rv = new List<IWorkitem>();
            for (int i = 0; i < 20; i++)
                rv.Add(this.Get());
            return rv;
        }

        public void Delete(IWorkitem item)
        {
            throw new NotImplementedException();
        }

        public void Move(IWorkitem item, System.IO.DirectoryInfo whereTo)
        {
            throw new NotImplementedException();
        }

        public void UpdateStatus(IWorkitem item, WorkitemStatus status)
        {
            throw new NotImplementedException();
        }

        public bool SupportsMove
        {
            get { return false; }
        }

        public bool SupportsDelete
        {
            get { return false; }
        }

        public bool SupportsUpdateStatus
        {
            get { return false; }
        }

        public bool SupportsBatching
        {
            get { return true; }
        }

        public int Id
        {
            get;
            set;
        }

        protected string _name;
        public string Name
        {
            get { return "Generator"; }
            set { _name = value; }
        }

        public string FileName
        {
            get { return System.Reflection.Assembly.GetExecutingAssembly().Location; }
            set { _name = value; }
        }

        public string Type
        {
            get { return "input"; }
            set { _name = value; }
        }

        public Generator(IDictionary<int, string> settings)
        {
            settings.FirstOrDefault();
        }

        public bool RequiresConnection
        {
            get { return false; }
        }

        public bool Connect()
        {
            throw new NotSupportedException();
        }

        public bool IsConnected { get { return false; } }


        public void PostProcess(IWorkitem item)
        {
            //throw new NotImplementedException();
        }

        public IConnector Connector
        {
            get { return null; }
        }

        public void Disconnect()
        {
            throw new NotSupportedException();
        }
    }
}
