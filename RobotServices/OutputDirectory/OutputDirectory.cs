﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Paragon.RobotServices.Fields;

namespace Paragon.RobotServices.Plugin.AutoImport.Output
{
    public class OutputDirectory : IOutputModule
    {
        private DirectoryInfo destDir;
        private int docSeqDigits = 6;
        private int batchSeqDigits = 6;
        private bool storeImages = true;
        private string imagePathMapping = string.Empty;
        private string imageFileExtension = ".tif";
        private string imageNameMapping = "[_DisplayName][_ImageFileExtension]";
        private bool storeAttachments = false;
        private string attxPathMapping = string.Empty;
        private string attxFileExtension = ".atx";
        private string attxNameMapping = "[_DisplayName][_AttxFileExtension]";
        private bool storeIndexes = true;
        private string idxPathMapping = string.Empty;
        private string idxFileExtension = ".idx";
        private string idxNameMapping = "[_DisplayName][_IndexFileExtension]";
        private bool createSubdirs = false;
        private IndexFieldPairSeparators pairSepType = IndexFieldPairSeparators.CrLf;
        private string pairSepOther = "\r\n";
        private string valueSep = "=";
        private bool indexValuesOnly = false;
        private bool storeFilePaths = false;
        private int lastDocSeqNum = 0;
        private int lastBatchSeqNum = 0;
        private bool storeMultipleIndexes = false;
        private string multiIndexSeparator = "\r\n\r\n";

        protected Paragon.RobotServices.Fields.FieldService _fs;

        public const string cDestinationBasePath = "Destination Base Path";
        public const string cDocumentSequenceDigits = "Document Sequence Number Digits";
        public const string cBatchSequenceDigits = "Batch Sequence Number Digits";
        public const string cStoreImages = "Image File Storage Method";
        public const string cImageFilePathMapping = "Image File Path Mapping";
        public const string cImageFileNameMapping = "Image File Name Mapping";
        public const string cImageFileExtension = "Image File Extension";
        public const string cStoreAttachments = "Attachment File Storage Method";
        public const string cAttachmentPathMapping = "Attachment File Path Mapping";
        public const string cAttachmentNameMapping = "Attachment File Name Mapping";
        public const string cAttachmentFileExtension = "Attachment File Extension";
        public const string cStoreIndexes = "Index File Storage Method";
        public const string cIndexFilePathMapping = "Index File Path Mapping";
        public const string cIndexFileNameMapping = "Index File Name Mapping";
        public const string cIndexFileExtension = "Index File Extension";
        public const string cCreateSubdirectories = "Create Subdirectories";
        public const string cPairSeparatorType = "Index Pair Separator Type";
        public const string cPairSeparatorOther = "Index Pair Separator Other";
        public const string cValueSeparatorType = "Index Value Separator Type";
        public const string cValueSeparatorOther = "Index Value Separator Other";
        public const string cIndexValuesOnly = "Index Values Only";
        public const string cStoreFilePaths = "Store File Paths with Indexes";
        public const string cLastDocumentSequence = "Last Document Sequence Number";
        public const string cLastBatchSequence = "Last Batch Sequence Number";
        public const string cStoreMultipleIndexes = "Store Multiple Indexes";
        public const string cMultipleIndexSeparator = "Multiple Index Separator";

        // FieldService instance is required for translating the *FilePath and *FileName setting values into concrete strings
        public OutputDirectory(IDictionary<string, string> settings) //, FieldService fs)
        {
            _fs = new FieldService();
            if (settings.ContainsKey(cDestinationBasePath) && !(string.IsNullOrEmpty(settings[cDestinationBasePath])))
            {
                try
                {
                    destDir = new DirectoryInfo(settings[cDestinationBasePath]);
                }
                catch
                { throw new ArgumentException(string.Format("The string '%0' is not a valid path.", settings[cDestinationBasePath])); }
                if (!(destDir.Exists))
                    throw new ArgumentException(string.Format("The path '%0' does not exist.", settings[cDestinationBasePath]));
            }
            else
                throw new ArgumentException("Destination path not given or is empty.");
            if (settings.ContainsKey(cDocumentSequenceDigits))
                if (!(int.TryParse(settings[cDocumentSequenceDigits], out docSeqDigits)))
                    throw new ArgumentException(string.Format("Value given for document sequence digits ('%0') is not an integer", settings[cDocumentSequenceDigits]));
            if (settings.ContainsKey(cBatchSequenceDigits))
                if (!(int.TryParse(settings[cBatchSequenceDigits], out batchSeqDigits)))
                    throw new ArgumentException(string.Format("Value given for batch sequence digits ('%0') is not an integer", settings[cBatchSequenceDigits]));
            if (settings.ContainsKey(cStoreImages))
                if (!(bool.TryParse(settings[cStoreImages], out storeImages)))
                    throw new ArgumentException(string.Format("Value given for store images ('%0') is not a valid boolean (true or false)", settings[cStoreImages]));
            if (settings.ContainsKey(cStoreAttachments))
                if (!(bool.TryParse(settings[cStoreAttachments], out storeAttachments)))
                    throw new ArgumentException(string.Format("Value given for store attachments ('%0') is not a valid boolean (true or false)", settings[cStoreAttachments]));
            if (settings.ContainsKey(cStoreIndexes))
                if (!(bool.TryParse(settings[cStoreIndexes], out storeIndexes)))
                    throw new ArgumentException(string.Format("Value given for store indexes ('%0') is not a valid boolean (true or false)", settings[cStoreIndexes]));
            if (settings.ContainsKey(cStoreMultipleIndexes))
                if (!(bool.TryParse(settings[cStoreMultipleIndexes], out storeMultipleIndexes)))
                    throw new ArgumentException(string.Format("Value given for store multi-indexes ('%0') is not a valid boolean (true or false)", settings[cStoreMultipleIndexes]));
            if (settings.ContainsKey(cStoreFilePaths))
                if (!(bool.TryParse(settings[cStoreFilePaths], out storeFilePaths)))
                    throw new ArgumentException(string.Format("Value given for store file paths ('%0') is not a valid boolean (true or false)", settings[cStoreFilePaths]));
            if (settings.ContainsKey(cImageFilePathMapping))
            {
                imagePathMapping = settings[cImageFilePathMapping];
            }
            if (settings.ContainsKey(cImageFileNameMapping))
            {
                if (string.IsNullOrEmpty(settings[cImageFileNameMapping]))
                    throw new ArgumentException("Value given for image name mapping was null or empty.");
                else
                    imageNameMapping = settings[cImageFileNameMapping];
            }
            if (settings.ContainsKey(cAttachmentPathMapping))
            {
                attxPathMapping = settings[cAttachmentPathMapping];
            }
            if (settings.ContainsKey(cAttachmentNameMapping))
            {
                if (string.IsNullOrEmpty(settings[cAttachmentNameMapping]))
                    throw new ArgumentException("Value given for attachment name mapping was null or empty.");
                else
                    attxNameMapping = settings[cAttachmentNameMapping];
            }
            if (settings.ContainsKey(cIndexFilePathMapping))
            {
                idxPathMapping = settings[cIndexFilePathMapping];
            }
            if (settings.ContainsKey(cIndexFileNameMapping))
            {
                if (string.IsNullOrEmpty(settings[cIndexFileNameMapping]))
                    throw new ArgumentException("Value given for index name mapping was null or empty.");
                else
                    idxNameMapping = settings[cIndexFileNameMapping];
            }
            if (settings.ContainsKey(cCreateSubdirectories))
                if (!(bool.TryParse(settings[cCreateSubdirectories], out createSubdirs)))
                    throw new ArgumentException(string.Format("Value given for create subdirectories ('%0') is not a valid boolean (true or false)", settings[cCreateSubdirectories]));
            if (settings.ContainsKey(cPairSeparatorType))
            {
                object parsed;
                try
                {
                    parsed = Enum.Parse(typeof(IndexFieldPairSeparators), settings[cPairSeparatorType], true);
                    if (parsed != null)
                        pairSepType = (IndexFieldPairSeparators)parsed;
                    switch (pairSepType)
                    {
                        case IndexFieldPairSeparators.Cr:
                        case IndexFieldPairSeparators.CrOrLf:
                            pairSepOther = "\r";
                            break;
                        case IndexFieldPairSeparators.Lf:
                            pairSepOther = "\n";
                            break;
                        case IndexFieldPairSeparators.CrLf:
                        default:
                            pairSepOther = "\r\n";
                            break;
                    }
                }
                catch
                { pairSepType = IndexFieldPairSeparators.CrLf; }
            }
            else if (settings.ContainsKey(cPairSeparatorOther))
            {
                if (!(string.IsNullOrEmpty(settings[cPairSeparatorOther])))
                    pairSepOther = settings[cPairSeparatorOther];
            }
            if (settings.ContainsKey(cIndexValuesOnly))
                if (!(bool.TryParse(settings[cIndexValuesOnly], out indexValuesOnly)))
                    throw new ArgumentException(string.Format("Value given for index values only ('%0') is not a valid boolean (true or false)", settings[cIndexValuesOnly]));
            if (settings.ContainsKey(cLastDocumentSequence))
                if (!(int.TryParse(settings[cLastDocumentSequence], out lastDocSeqNum)))
                    throw new ArgumentException(string.Format("Value given for last document sequence ('%0') is not a valid integer", settings[cLastDocumentSequence]));
            if (settings.ContainsKey(cLastBatchSequence))
                if (!(int.TryParse(settings[cLastBatchSequence], out lastBatchSeqNum)))
                    throw new ArgumentException(string.Format("Value given for last batch sequence ('%0') is not a valid integer", settings[cLastBatchSequence]));
            if (settings.ContainsKey(cMultipleIndexSeparator))
            {
                //if (string.IsNullOrEmpty(settings[cMultipleIndexSeparator]))
                //    throw new ArgumentException("Value given for multiple index separator ('%0') was null or empty.");
                //else
                    multiIndexSeparator = settings[cMultipleIndexSeparator];
            }
            if (settings.ContainsKey(cImageFileExtension))
            {
                if (string.IsNullOrEmpty(settings[cImageFileExtension]))
                    throw new ArgumentException("Value given for image file extension ('%0') was null or empty.");
                else
                    imageFileExtension = settings[cImageFileExtension];
            }
            if (settings.ContainsKey(cAttachmentFileExtension))
            {
                if (string.IsNullOrEmpty(settings[cAttachmentFileExtension]))
                    throw new ArgumentException("Value given for attachment file extension ('%0') was null or empty.");
                else
                    attxFileExtension = settings[cAttachmentFileExtension];
            }
            if (settings.ContainsKey(cIndexFileExtension))
            {
                if (string.IsNullOrEmpty(settings[cIndexFileExtension]))
                    throw new ArgumentException("Value given for index file extension ('%0') was null or empty.");
                else
                    idxFileExtension = settings[cIndexFileExtension];
            }

        }

        #region IOutputModule Members

        protected string GetNextInSequence(long lastSequence, int maxDigits)
        {
            if (maxDigits > 18)
                throw new ArgumentOutOfRangeException(string.Format("maxDigits must be 18 or less, was given %0", maxDigits));
            string format = "".PadRight(maxDigits, '0');
            long maxValue = (long)Math.Pow(10, maxDigits);
            if ((lastSequence > maxValue) || (lastSequence < 1))
                lastSequence = 1;
            else
                lastSequence += 1;

            return lastSequence.ToString(format);
        }

        public long Process(IWorkitem item)
        {
            List<Field> fields = new List<Field>();
            fields.Add(new Field() { Name = "_ImageFilePath", Value = imagePathMapping });
            fields.Add(new Field() { Name = "_ImageFileName", Value = imageNameMapping });
            fields.Add(new Field() { Name = "_IndexFilePath", Value = idxPathMapping });
            fields.Add(new Field() { Name = "_IndexFileName", Value = idxNameMapping });
            fields.Add(new Field() { Name = "_AttxFilePath", Value = attxPathMapping });
            fields.Add(new Field() { Name = "_AttxFileName", Value = attxNameMapping });

            Dictionary<string, string> sysFields = new Dictionary<string,string>();
            sysFields.Add("_WorkitemID", item.WorkitemId);
            sysFields.Add("_DisplayName", item.DisplayName);
            sysFields.Add("_Julian", DateTime.Today.DayOfYear.ToString());
            sysFields.Add("_DocSequence", GetNextInSequence(lastDocSeqNum, docSeqDigits));
            sysFields.Add("_ImageFileExtension", imageFileExtension );
            sysFields.Add("_IndexFileExtension", idxFileExtension);
            sysFields.Add("_AttxFileExtension", attxFileExtension);

            IDictionary<string, string> results = fields.TranslateFields(sysFields);
            DirectoryInfo imagePath;
            DirectoryInfo attxPath;
            DirectoryInfo indexPath;
            FileInfo imageFile;
            FileInfo attxFile;
            FileInfo indexFile;
            if (storeImages)
            {
                try
                {
                    imagePath = new DirectoryInfo(Path.Combine(destDir.FullName, results["_ImageFilePath"]));
                    imageFile = new FileInfo(Path.Combine(imagePath.FullName, results["_ImageFileName"]));
                    if (item.Images.Count == 1)
                    {
                        if (imageFile.Exists)
                            imageFile.Delete();
                        item.Images[0].Save(imageFile.FullName);
                    }
                    else
                        for (int i = 0; i < item.Images.Count; ++i)
                        {
                            string tempName = Path.GetFileNameWithoutExtension(imageFile.Name) + i.ToString() + Path.GetExtension(imageFile.Name);
                            imageFile = new FileInfo(Path.Combine(imagePath.FullName, tempName));
                            if (imageFile.Exists)
                                imageFile.Delete();
                            item.Images[i].Save(imageFile.FullName);
                        }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(string.Format("'%0', '%1', and '%2' cannot be combined into a valid, accessible path or file.", destDir.FullName, results["_ImageFilePath"], results["_ImageFileName"]), ex);
                }
            }
            if (storeAttachments)
            {
                try
                {
                    attxPath = new DirectoryInfo(Path.Combine(destDir.FullName, results["_AttxFilePath"]));
                    attxFile = new FileInfo(Path.Combine(attxPath.FullName, results["_AttxFileName"]));
                    if (item.Attachments.Count == 1)
                    {
                        if (attxFile.Exists)
                            attxFile.Delete();
                        FileStream afs = attxFile.OpenWrite();
                        afs.Write(item.Attachments[0], 0, (int)item.Attachments[0].Length);
                        afs.Close();
                    }
                    else
                        for (int i = 0; i < item.Attachments.Count; ++i)
                        {
                            string tempName = Path.GetFileNameWithoutExtension(attxFile.Name) + i.ToString() + Path.GetExtension(attxFile.Name);
                            attxFile = new FileInfo(Path.Combine(attxFile.FullName, tempName));
                            if (attxFile.Exists)
                                attxFile.Delete();
                            FileStream afs = attxFile.OpenWrite();
                            afs.Write(item.Attachments[i], 0, (int)item.Attachments[i].Length);
                            afs.Close();
                        }
                }
                catch
                {
                    throw new InvalidOperationException(string.Format("'%0', '%1', and '%2' cannot be combined into a valid, accessible path or file.", destDir.FullName, results["_AttxFilePath"], results["_AttxFileName"]));
                }
            }
            if (storeIndexes)
            {
                try
                {
                    indexPath = new DirectoryInfo(Path.Combine(destDir.FullName, results["_IndexFilePath"]));
                    indexFile = new FileInfo(Path.Combine(indexPath.FullName, results["_IndexFileName"]));
                    string outputStr = IndexesToString(item);
                    if (indexFile.Exists)
                        indexFile.Delete();
                    FileStream ifs = indexFile.OpenWrite();
                    byte[] outputBytes = Encoding.UTF8.GetBytes(outputStr);
                    ifs.Write(outputBytes, 0, outputBytes.Length);
                    ifs.Close();
                }
                catch
                {
                    throw new InvalidOperationException(string.Format("'%0', '%1', and '%2' cannot be combined into a valid, accessible path or file.", destDir.FullName, results["_IndexFilePath"], results["_IndexFileName"]));
                }
            }
            return 1;
        }

        protected string IndexesToString(IWorkitem item)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var idx in item.Indexes)
            {
                foreach (var kvp in idx)
                    sb.Append(kvp.Key).Append(valueSep).Append(kvp.Value).Append(pairSepOther);
                if (storeMultipleIndexes)
                    sb.Append(multiIndexSeparator);
                else
                    break;
            }
            return sb.ToString();
        }

        public long ProcessBatch(IEnumerable<IWorkitem> items)
        {
            throw new NotSupportedException();
        }

        public bool SupportsBatching
        {
            get { return false; }
        }

        #endregion

        #region IIoCommon Members

        public bool Connect()
        {
            throw new NotSupportedException();
        }

        public bool Connected
        {
            get { throw new NotSupportedException(); }
        }

        public void Disconnect()
        {
            throw new NotSupportedException();
        }

        public bool RequiresConnection
        {
            get { return false; }
        }

        #endregion
    }
}
