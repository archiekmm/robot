﻿@echo off

net user ParagonRobot P@r@g0nSyst3ms /add /comment:"User account for Paragon's RobotService" /passwordchg:no /expires:never 
netsh http add urlacl url=http://+:8080/ user="ParagonRobot"

icacls "%programdata%\Paragon Systems\RobotServices\*.xml" /grant ParagonRobot:(f)

