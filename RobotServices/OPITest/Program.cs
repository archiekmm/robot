﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Plugin;

namespace OPITest
{
    class Program
    {
       
         static void Main2(string[] args)
        {
            Dictionary<int, string> outputSettings = new Dictionary<int, string>();
            outputSettings.Add(1, "paraadm1");
            outputSettings.Add(2, "paraadm");
            outputSettings.Add(3, "va-vpc-pidemo");
            outputSettings.Add(4, "ParagonImagingInterface/ParagonImaging");

            List<Paragon.RobotServices.Model.LogEntry> Log = new List<Paragon.RobotServices.Model.LogEntry>();

            OutputIGRedaction opi = new OutputIGRedaction(outputSettings);
            opi.Logger = Log;

            opi.Connect();

            Document doc = new Document();
            doc.Class = "CONTRACT";
            doc.Name = "OPI Test " + DateTime.Now.ToString("yyyy-mm-dd-hh-MM");
            doc.Indexes.Add(new Dictionary<string, string>());
            doc.Indexes[0].Add("First Name", "Bobby");
            doc.Indexes[0].Add("Last Name", "Miller");
            doc.Indexes[0].Add("Loan Amount", "$1,000.00");
            doc.Indexes[0].Add("Contract Date", "08/27/2012");

            doc.Images.AddRange(Page.GetPages("4PageDoc.tif"));  //.Add(new Page("4PageDoc.tif"));
            
            opi.Process(doc);
            Console.WriteLine("Doc status is {0}", doc.Status);
            if (doc.Status == WorkitemStatus.Error)
            {
                Console.WriteLine("Source is {0}", doc.ErrorSource);
                Console.WriteLine("Error is {0}", doc.ErrorDescription);
            }
            else
                Console.WriteLine("Doc id is {0}", doc.Id);

            Console.WriteLine("Done.");
            Console.ReadLine();

        }

        static void Main(string[] args)
        {
            Dictionary<int, string> inputSettings = new Dictionary<int, string>();
            inputSettings.Add(1, @"C:\Test\Work");
            inputSettings.Add(2, @"Source=C:\Test\Input");
            inputSettings.Add(3, "false");
            inputSettings.Add(4, "false");
            inputSettings.Add(5, ".tif");
            inputSettings.Add(6, ".txt");
            inputSettings.Add(7, "1");
            inputSettings.Add(8, "false");
            inputSettings.Add(9, "false");
            inputSettings.Add(10, "false");
            inputSettings.Add(11, "CrLf");
            inputSettings.Add(12, "\r\n");
            inputSettings.Add(13, "=");
            inputSettings.Add(14, "false");
            inputSettings.Add(15, "");
            inputSettings.Add(16, "true");
            inputSettings.Add(17, "\r\n\r\n");
            inputSettings.Add(18, "true");
            inputSettings.Add(19, @"C:\Test\Archive");
            inputSettings.Add(20, @"C:\Test\Archive");
            inputSettings.Add(21, "");

            IInputModule id = new InputDirectory(inputSettings);
            

            Dictionary<int, string> outputSettings = new Dictionary<int, string>();
            outputSettings.Add(1, @"C:\AAAOutput");
            outputSettings.Add(2, "paraadm");
            outputSettings.Add(3, "va-vpc-pidemo");
            outputSettings.Add(4, "ParagonImagingInterface/ParagonImaging");

           List<Paragon.RobotServices.Model.LogEntry> Log = new List<Paragon.RobotServices.Model.LogEntry>();

            OutputIGRedaction opi = new OutputIGRedaction(outputSettings);
            opi.Logger = Log;

            opi.Connect();

            IWorkitem item = id.Get();

            //item.Class = "CONTRACT";
            opi.Process(item);

            if (item.Status == WorkitemStatus.Successful)
            {
                Console.WriteLine("Workitem processed successfully.");
                id.PostProcess(item);
            }
            else
                Console.WriteLine("Workitem errored:\n  {0}\n  {1}\n", item.ErrorSource, item.ErrorDescription);
                

            Console.WriteLine("Done.");
            Console.ReadLine();
        }
    }

}
