﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Plugin;
using System.Xml;
using System.Xml.Linq;

namespace UnisysConnectorTest
{
    class Program
    {
        static void Main(string[] args)
        {
            
            

        }

        static void Main1(string[] args)
        {
            System.Data.Common.DbConnectionStringBuilder dbsb = new System.Data.Common.DbConnectionStringBuilder();
            dbsb.ConnectionString = "userid=virtualadmin;password=admin123;server=se2img;";
            foreach (var k in dbsb.Keys)
                Console.WriteLine("Key {0}: {1}", k, dbsb[(string)k]);

            UnisysImagingConnector conn = new UnisysImagingConnector(dbsb.ConnectionString);
            conn.Connect();
            try
            {
                IWorkitem item = conn.GetNext("Autofoldering");
                if (null == item)
                    Console.WriteLine("item was null.");
                else
                    Console.WriteLine("item retrieved with id '{0}'", item.Id);
                conn.Disconnect();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught" + ex.ToString());
            }
            Console.WriteLine("done.");
            Console.Read();
        }

        static void Main2(string[] args)
        {
            Dictionary<string, string> login = new Dictionary<string, string>() {
                {"userid" , "virtualadmin"}, 
                {"password", "admin123"},
                {"domain" , "SE2IMG"}
            };

            Dictionary<string, string> settings = new Dictionary<string, string>();

            IImagingConnector uConn = new UnisysImagingConnector(login);

            string sourceDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            try
            {
                uConn.Connect();
                Console.WriteLine("uConn.IsConnected is {0}.", uConn.IsConnected);

                //uConn.AddToFolder(fId, dId);
                Document item = new Document();
                item.Class = "MFDOC";
                item.Name = "MFDOC " + DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss.ffffff");
                Dictionary<string, string> idx = new Dictionary<string, string>()
                {
                    {"ACCOUNT_NUM", "1234567890"}, {"TAX_ID", "987654321"},
                };
                item.Indexes.Add(idx);
                item.Images.AddRange(Page.GetPages(System.IO.Path.Combine(sourceDir, "Sample1.tif")));

                Console.WriteLine("Creating document.");

                uConn.CreateDocument(item);
                string docId = item.Id;
                item = null;
                item = uConn.RetrieveDocument(docId);
                Console.WriteLine("Document with id {0} created.", docId);

                Folder folder = new Folder();
                folder.Class = "MFFOLDER";
                folder.Name = "MFFOLDER " + DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss.ffffff");
                folder.Indexes.Add(idx);

                Console.WriteLine("Creating folder.");
                uConn.CreateFolder(folder);
                string foldId = folder.Id;
                folder = null;
                folder = uConn.RetrieveFolder(foldId);
                Console.WriteLine("folder  with id {0} created.", foldId);

                uConn.AddToFolder(foldId, docId);
                Console.WriteLine("doc added to folder.");
                uConn.RemoveFromFolder(foldId, docId);

                Console.WriteLine("finding documents");
                foreach (var doc in uConn.FindDocuments(idx))
                {
                    Console.WriteLine(doc.Id);
                }

                Console.WriteLine("finding folders");
                foreach (var doc in uConn.FindFolders(idx))
                {
                    Console.WriteLine(doc.Id);
                }

                Console.WriteLine("updating indexes");
                
                if (item.Indexes[0].ContainsKey("LAST_NAME"))
                    item.Indexes[0]["LAST_NAME"] = "Miller";
                else
                    item.Indexes[0].Add("LAST_NAME", "Miller");
                uConn.UpdateIndexes(item);

                Console.WriteLine("deleting document");

                uConn.DeleteDocument(docId);
                Console.WriteLine("deleting folder");
                uConn.DeleteFolder(foldId);

                //uConn.UpdateImages(item);

                //uConn.updateIndexes(folder);

                //uConn.UpdateImages(item);
                //uConn.UpdateChildren(folder);
                //uConn.UpdateDocuments(folder);
                //uConn.UpdateFolders(folder);

                //uConn.ExportPage(dId, pageName);
                //uConn.ImportPage(dId, pageName, mimeType, pageData);
                //uConn.UpdateDocument(iDoc, replacePages);
                //uConn.UpdateFolder(iFold);
                

                uConn.Disconnect();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                uConn.Disconnect();
            }

            Console.WriteLine("done.");
            Console.Read();
        }

        //public static Workitem FromDocument(ImagingDocument doc)
        //{
        //    Workitem rv = new Workitem();

        //    rv.Class = doc.Class;
        //    rv.Id = doc.DocumentId;
        //    rv.DocumentCount = 1;
        //    rv.Name = doc.Name;
        //    rv.Indexes.AddRange(doc.Indexes.Select(s => s));
        //    rv.Images.AddRange(doc.Pages.Select(p => new WorkitemPage(p.Name, p.MimeType, p.ImageData)));
        //    rv.Status = WorkitemStatus.NotProcessed;

        //    return rv;
        //}

        //public static ImagingDocument FromWorkitem(Workitem item)
        //{
        //    ImagingDocument rv = new ImagingDocument();

        //    rv.Class = item.Class;
        //    rv.DocumentId = item.Id;
        //    rv.Name = item.Name;
        //    rv.Indexes.AddRange(rv.Indexes.Select(s => s));
        //    rv.Pages.AddRange(item.Images.Select((im, i) => new ImagingPage() { PageId = i.ToString(), ImageData = im.Data, MimeType = im.MimeType, Name = im.Name }));

        //    return rv;
        //}
    }

    public static class Extensions
    {
        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            foreach (T item in items)
                list.Add(item);
        }
    }
}
