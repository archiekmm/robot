﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;
//using Paragon.RobotServices.Service;
using System.Xml.Linq;

namespace Paragon.RobotServices.Plugin
{
    //                            item          "childGroup"             "childInfo"
    //using ChildWorkItem = Tuple<IWorkitem, Dictionary<string, string>, Dictionary<string, string>>;
    using ChildWorkItem = Tuple<IWorkitem, Mapping, Dictionary<string, string>>;
    public class FolderingOutputUeWI : IOutputModule
    {
        protected const string cAnalyst = "ANALYST";
        protected const string cAA_Analyst = "AA_ANALYST";
        protected const string cRP_Analyst = "RP_ANALYST";
        protected const string cMA_Analyst = "MA_ANALYST";
        protected const string cUserName = "UserName";

        protected const string cLoginName = "Login";
        protected string _login;
        protected bool _shareLogin = false;

        protected const string cHoldWorkstepName = "Hold Workstep Name";
        protected const string cAuditWorkstepName = "Audit Workstep Name";
        protected const string cRemoveWorkstepName = "Remove Workstep Name";
        protected const string cExceptionWorkstepName = "Exception Workstep Name";

        protected string holdWorkstepName;
        protected string auditWorkstepName;
        protected string removeWorkstepName;
        protected string exceptionWorkstepName;

        protected const string cHoldEnabled = "Hold Enabled";
        protected const string cHoldInterval = "Hold Interval";

        protected bool holdProcessingEnabled = false;
        protected int holdProcessingInterval = 60;

        protected const string cAuditEnabled = "Enable Audit Processing";
        protected const string cAuditInterval = "Audit Processing Interval";
        protected const string cAuditLogin = "Audit Login";
        protected const string cShowAuditQueries = "Show Audit Queries";
        protected const string cAuditQuery = "Audit Query";

        protected bool auditProcessingEnabled;
        protected int auditProcessingInterval;
        protected string auditLogin;
        protected bool auditProcessingShowQueries;
        protected string auditProcessingQuery;

        protected const string cFolderGroupsSetting = "Folder Group Mappings";
        protected const string cChildGroupsSetting = "Child Group Mappings";

        protected List<Mapping> folderGroupMappings;
        protected List<Mapping> childGroupMappings;

        protected const string cProcessMultipleMappings = "Multiple Mappings";
        protected bool processMultipleMappings = false;

        protected DateTime holdProcessingDue;
        protected DateTime auditProcessingDue;

        public FolderingOutputUeWI(IDictionary<int, string> settings)
        {
            #region commented
            // verify configuration
            /*
            if (!settings.ContainsKey(cLoginName))
                throw new ArgumentException("settings must contain a login");
            else
                _login = settings[cLoginName];

            if (!settings.ContainsKey(cFolderGroupsSetting))
                throw new ArgumentException("settings must contain folder group mappings");
            else
                folderGroupMappings = Mapping.FromXml(settings[cFolderGroupsSetting]);

            if (!settings.ContainsKey(cChildGroupsSetting))
                throw new ArgumentException("settings must contain child group mappings");
            else
                childGroupMappings = Mapping.FromXml(settings[cChildGroupsSetting]);

            if (!settings.ContainsKey(cInputWorksetName))
                throw new ArgumentException("settings must contain an input worket name");
            else
                inputWorksetName = settings[cInputWorksetName];

            if (!settings.ContainsKey(cHoldWorkstepName))
                throw new ArgumentException("settings must contain a hold workstep name");
            else
                holdWorkstepName = settings[cHoldWorkstepName];

            if (!settings.ContainsKey(cAuditWorkstepName))
                throw new ArgumentException("settings must contain an audit workstep name");
            else
                auditWorkstepName = settings[cAuditWorkstepName];

            if (!settings.ContainsKey(cExceptionWorkstepName))
                throw new ArgumentException("settings must contain an exception workstep name");
            else
                exceptionWorkstepName = settings[cExceptionWorkstepName];

            if (!settings.ContainsKey(cHoldEnabled))
                throw new ArgumentException("settings must contain a hold enabled name");
            else
                bool.TryParse(settings[cHoldEnabled], out holdProcessingEnabled);

            if (!settings.ContainsKey(cHoldInterval))
                throw new ArgumentException("settings must contain an exception workstep name");
            else
                int.TryParse(settings[cHoldInterval], out holdProcessingInterval);

            if (!settings.ContainsKey(cProcessMultipleMappings))
                throw new ArgumentException("settings must contain an exception workstep name");
            else
                bool.TryParse(settings[cProcessMultipleMappings], out processMultipleMappings);

            if (!settings.ContainsKey(cAuditEnabled))
                throw new ArgumentException("settings must contain an exception workstep name");
            else
                bool.TryParse(settings[cAuditEnabled], out auditProcessingEnabled);

            if (!settings.ContainsKey(cAuditInterval))
                throw new ArgumentException("settings must contain an exception workstep name");
            else
                int.TryParse(settings[cAuditInterval], out auditProcessingInterval);

            if (!settings.ContainsKey(cShowAuditQueries))
                throw new ArgumentException("settings must contain an exception workstep name");
            else
                bool.TryParse(settings[cShowAuditQueries], out auditProcessingShowQueries);

            if (!(settings.ContainsKey(cAuditQuery)))
                throw new ArgumentException("settings must contain an audit query");
            else
                auditProcessingQuery = settings[cAuditQuery];
             */
            #endregion

            bool.TryParse(settings[1], out _shareLogin);
            _login = settings[2].Replace("&", ";");
            holdWorkstepName = settings[3];
            auditWorkstepName = settings[4];
            removeWorkstepName = settings[5];
            exceptionWorkstepName = settings[6];
            bool.TryParse(settings[7], out holdProcessingEnabled);
            int.TryParse(settings[8], out holdProcessingInterval);
            bool.TryParse(settings[9], out auditProcessingEnabled);
            auditLogin = settings[10];
            int.TryParse(settings[11], out auditProcessingInterval);
            bool.TryParse(settings[12], out auditProcessingShowQueries);
            auditProcessingQuery = settings[13];
            bool.TryParse(settings[14], out processMultipleMappings);
            folderGroupMappings = Mapping.FromXml(settings[15]);
            childGroupMappings = Mapping.FromXml(settings[16]);
        }

        public long Process(IWorkitem item)
        {
            Console.WriteLine("Foldering output:  processing item {0}", item.Id);
            if (item is Document)
            {
                Console.WriteLine("item is a document. Id is {0}, Class is {1}, Name is {2}.\n Index:", item.Id, item.Class, item.Name);
                foreach (var kvp in item.Indexes[0])
                    Console.Write("{0}: {1}", kvp.Key, kvp.Value);
                Console.WriteLine();
                
                ProcessDocument(item as Document);
            }
            if (item is Folder)
            {
                Console.WriteLine("item is a folder. Id is {0}, Class is {1}, Name is {2}.\n Index:", item.Id, item.Class, item.Name);
                foreach (var kvp in item.Indexes[0])
                    Console.Write("{0}: {1}", kvp.Key, kvp.Value);
                Console.WriteLine();
                ProcessFolder(item as Folder);
            }
            Console.WriteLine("item processing complete, statis was {0}, checking hold and audit queues", item.Status);

            if ((holdProcessingEnabled) && (DateTime.Now >= holdProcessingDue))
            {
                Console.WriteLine("Hold Processing");
                PerformHoldProcessing();
            }
            if ((auditProcessingEnabled) && (DateTime.Now >= auditProcessingDue))
            {
                Console.WriteLine("Audit Processing");
                PerformAuditProcessing();
            }
            if (item.Status == WorkitemStatus.Successful)
                return 1;
            else
                return 0;
        }

        protected void ProcessDocument(Document doc)
        {
            if (doc.Class.StartsWith("AA"))
                conn.RouteWorkitem(doc, auditWorkstepName);
            else if (doc.Class.StartsWith("MF"))
            {
                Console.WriteLine("Processing MF Document");
                ProcessMFDoc(doc);
            }
            else
            {
                switch (doc.Class)
                {
                    case "HRASSOC":
                        ProcessHRASSOCDoc(doc);
                        break;
                    case "HRDOC":
                    case "HRBNDOC":
                        ProcessHROtherDoc(doc);
                        break;
                    default:
                        conn.RouteWorkitem(doc, "_default", false);
                        doc.Status = WorkitemStatus.Error;
                        doc.ErrorSource = "ProcessDocument";
                        doc.ErrorDescription = string.Format("unsupported document class '{0}'", doc.Class);
                        break;
                }
            }
        }

        protected void ProcessFolder(Folder folder)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");
            if (string.IsNullOrEmpty(folder.Id))
            {
                folder.Status = WorkitemStatus.Error;
                folder.ErrorSource = "ProcessFolder";
                folder.ErrorDescription = "Folder must exist in Unisys to be processed.";
                return;
            }

            Dictionary<string, string> folderInfoTagged = new Dictionary<string, string>();
            foreach (var kvp in folder.Indexes[0])
                folderInfoTagged.Add("folder." + kvp.Key, kvp.Value);

            IDictionary<string, string> folderGroup = null;
            foreach (var m in folderGroupMappings)
                if (m.Conditions.Evaluate(folder.Indexes[0]))
                {
                    folderGroup = new Dictionary<string, string>();
                    foreach (var t in m.Transforms)
                        folderGroup.Add(t.Name, t.Value);
                    folderGroup.Add("Name", m.Name);
                    break;
                }
            if (null == folderGroup)
            {
                folder.Status = WorkitemStatus.Error;
                folder.ErrorSource = "ProcessFolder";
                folder.ErrorDescription = "Folder did not match any group definitions.";
                return;
            }
            else
            {
                Log(LogSeverity.Debug, string.Format("Folder {0} mapped to group {1}", folder.Id, folderGroup["Name"]));
            }
                //folderGroup.Dump();

            folderInfoTagged.Add("_FolderGroup", folderGroup["Name"]);
            //folderGroup["Name"].Dump();

            if (folderGroup["_Action"] == "route")
            {
                string dest = null;
                switch (folderGroup["_Destination"])
                {
                    case "{default}":
                    case "_default":
                        dest = "_default";
                        break;
                    case "{hold}":
                    case "_hold":
                        dest = holdWorkstepName;
                        break;
                    case "{remove}":
                    case "_remove":
                        dest = removeWorkstepName;
                        break;
                    case "{exception}":
                    case "_exception":
                        dest = exceptionWorkstepName;
                        break;
                    default:
                        dest = folderGroup["_Destination"];
                        break;
                }
                //string.Format("conn.RouteWorkitem(Folder (ID={0}), {1}, false);", folder.Id, dest).Dump();
                conn.RouteWorkitem(folder, dest, false);
                folder.Status = WorkitemStatus.Successful;
                //"folder action was 'route', not processing child items.".Dump();
                Log(LogSeverity.Normal, string.Format("Action for folder {0} was 'route', not processing child items.", folder.Id));
                return;
            }

            // folder not routed, process children
            List<Tuple<IWorkitem, Mapping, Dictionary<string, string>>> childItems = new List<Tuple<IWorkitem, Mapping, Dictionary<string, string>>>();
            foreach (var doc in folder.Documents)
            {
                //Dictionary<string, string> childGroup = null;
                Mapping childGroup = null;
                foreach (Mapping cm in childGroupMappings)
                    if (cm.Conditions.Evaluate(doc.Indexes[0]))
                    {
                        childGroup = cm;
                        break;
                    }
                //{
                //    childGroup = new Dictionary<string, string>();
                //    foreach (var t in cm.Transforms)
                //        childGroup.Add(t.Name, t.Value);
                //    childGroup.Add("Name", cm.Name);
                //}
                if (null == childGroup)
                {
                    folder.Status = WorkitemStatus.Error;
                    folder.ErrorDescription = string.Format("folder child with id '{0}' did not match any child mappings groups.", doc.Id);
                    folder.ErrorSource = "ProcessFolder";
                    Log(LogSeverity.Error, folder.ErrorDescription);
                    //"conn.RouteWorkitem(folder, exceptionWorkstepName);".Dump();
                    conn.RouteWorkitem(folder, exceptionWorkstepName);
                    return;
                }
                Dictionary<string, string> childInfo = new Dictionary<string, string>();
                foreach (var kvp in doc.Indexes[0])
                    childInfo.Add(kvp.Key, kvp.Value);
                foreach (var kvp in folderInfoTagged)
                    childInfo.Add(kvp.Key, kvp.Value);
                Tuple<IWorkitem, Mapping, Dictionary<string, string>> childItem = new Tuple<IWorkitem, Mapping, Dictionary<string, string>>(doc, childGroup, childInfo);
                childItems.Add(childItem);
                Log(LogSeverity.Error, string.Format("Folder (ID={0}): added document child (ID={1} to list for processing.", folder.Id, doc.Id));
                //childInfo.Dump();
            }

            foreach (var fold in folder.Folders)
            {
                Mapping childGroup = null;
                foreach (Mapping cm in childGroupMappings)
                    if (cm.Conditions.Evaluate(fold.Indexes[0]))
                    {
                        childGroup = cm;
                        break;
                    }
                //{
                //    childGroup = new Dictionary<string, string>();
                //    foreach (var t in cm.Transforms)
                //        childGroup.Add(t.Name, t.Value);
                //    childGroup.Add("Name", cm.Name);
                //}
                if (null == childGroup)
                {
                    folder.Status = WorkitemStatus.Error;
                    folder.ErrorDescription = string.Format("folder child with id '{0}' did not match any child mappings groups.", fold.Id);
                    folder.ErrorSource = "ProcessFolder";
                    Log(LogSeverity.Error, folder.ErrorDescription);//.Dump();
                    //"conn.RouteWorkitem(folder, exceptionWorkstepName);".Dump();
                    conn.RouteWorkitem(folder, exceptionWorkstepName);
                    return;
                }
                Dictionary<string, string> childInfo = new Dictionary<string, string>();
                foreach (var kvp in fold.Indexes[0])
                    childInfo.Add(kvp.Key, kvp.Value);
                foreach (var kvp in folderInfoTagged)
                    childInfo.Add(kvp.Key, kvp.Value);
                Tuple<IWorkitem, Mapping, Dictionary<string, string>> childItem = new Tuple<IWorkitem, Mapping, Dictionary<string, string>>(fold, childGroup, childInfo);
                childItems.Add(childItem);
                Log(LogSeverity.Debug, string.Format("Folder (ID={0}): added folder child (ID={1}) to list for processing.", folder.Id, fold.Id)); //.Dump();
                //childInfo.Dump();
            }

            // if we got this far, all child items are valid
            Log(LogSeverity.Debug, string.Format("Processing folder child items for folder {0}", folder.Id)); //.Dump();
            foreach (var item in childItems)
            {
                IDictionary<string, string> updates = item.Item2.TranslateFields(item.Item1.Indexes[0]);
                bool anyUpdates = false;
                foreach (var kvp in updates)
                {
                    if ((item.Item1.Indexes[0].ContainsKey(kvp.Key)) && (item.Item1.Indexes[0][kvp.Key] != kvp.Value))
                    {
                        item.Item1.Indexes[0][kvp.Key] = kvp.Value;
                        anyUpdates = true;
                    }
                }
                if (anyUpdates)
                    conn.UpdateIndexes(item.Item1);
                //string.Format("conn.UpdateIndexes(Id={0})", item.Item1.Id).Dump();
                switch (updates["_Action"].ToLower())
                {
                    case "none":
                    case "keep":
                    default:
                        break;
                    case "remove":
                    case "removeonly":
                        //string.Format("conn.RemoveFromFolder(folderId={0}, childId={1})", folder.Id, item.Item1.Id).Dump();
                        if (item.Item1 is Document)
                            folder.Documents.Remove((Document)item.Item1);
                        else if (item.Item1 is Folder)
                            folder.Folders.Remove((Folder)item.Item1);
                        // conn.UpdateFolder will do the actual adding/removing
                        break;
                    case "route":
                    case "routeonly":
                        //string.Format("conn.RouteWorkitem(childId={0}, dest={1})", item.Item1.Id, updates["_Destination"]).Dump();
                        conn.RouteWorkitem(item.Item1, updates["_Destination"]);
                        break;
                    case "routeandremove":
                        //string.Format("conn.RemoveFromFolder(folderId={0}, childId={1})", folder.Id, item.Item1.Id).Dump();
                        //string.Format("conn.RouteWorkitem(childId={0}, dest={1})", item.Item1.Id, updates["_Destination"]).Dump();
                        if (item.Item1 is Document)
                            folder.Documents.Remove((Document)item.Item1);
                        else if (item.Item1 is Folder)
                            folder.Folders.Remove((Folder)item.Item1);
                        conn.RouteWorkitem(item.Item1, updates["_Destination"]);
                        break;
                }
            }
            IDictionary<string, string> folderUpdates = folderGroupMappings.TranslateFields(folder.Indexes[0]);
            if (null != folderUpdates)
            {
                foreach (var kvp in folderUpdates)
                    if ((folder.Indexes[0].ContainsKey(kvp.Key)) && (folder.Indexes[0][kvp.Key] != kvp.Value))
                        folder.Indexes[0][kvp.Key] = kvp.Value;
            }
            //string.Format("conn.UpdateFolder(Id={0})", folder.Id).Dump();
            conn.UpdateFolder(folder);
            switch (folderUpdates["_FinalAction"].ToLower())
            {
                case "delete":
                    //"delete".Dump();
                    conn.DeleteFolder(folder.Id);
                    break;
                case "route":
                    //"route".Dump();
                    conn.RouteWorkitem(folder, folderUpdates["_Destination"], true);
                    break;
                case "routespecial":
                default:
                    //"routeSpecial".Dump();
                    /**/
                    int count = folder.Documents.Count + folder.Folders.Count;
                    if (count == 0)
                        conn.DeleteFolder(folder.Id);
                    else if (count == 1)
                    {
                        IWorkitem item;
                        if (folder.Documents.Count == 1)
                            item = folder.Documents[0];
                        else
                            item = folder.Folders[0];
                        conn.RemoveFromFolder(folder.Id, item.Id);
                        conn.RouteWorkitem(item, folderUpdates["_Destination"]);
                        conn.DeleteFolder(folder.Id);
                    }
                    else
                        conn.RouteWorkitem(folder, folderUpdates["_Destination"], true);
                        /* */
                    break;
            }
            folder.Status = WorkitemStatus.Successful;
        }

        public long ProcessBatch(IEnumerable<IWorkitem> items)
        {
            throw new NotSupportedException();
        }

        public bool SupportsBatching
        {
            get { return false; }
        }

        public int Id { get; set; }

        public string Name 
        { 
            get { return "FolderingOutputUeWI"; }
            set { }
        }

        public string FileName
        {
            get { return this.GetType().Assembly.Location; }
            set { }
        }

        public string Type
        {
            get { return "output"; }
            set { }
        }

        protected UnisysImagingConnector conn = null;

        public bool RequiresConnection
        {
            get { return true; }
        }

        public IConnector Connector
        {
            get { return conn; }
            set
            {
                conn = (UnisysImagingConnector)value;
                //if (conn is UnisysImagingConnector)
                //    conn = value as UnisysImagingConnector;
                //else
                //    throw new ArgumentException("Connector must be a UnisysImagingConnector");
            }
        }

        public bool ShareConnector { get { return _shareLogin; } }

        public bool Connect()
        {
            //if (null == conn)
            //    conn = new UnisysImagingConnector();

            if (conn.IsConnected)
                conn.Disconnect();
            return conn.Connect();
        }

        public bool IsConnected
        {
            get { return conn.IsConnected; }
        }

        public void Disconnect()
        {
            conn.Disconnect();
        }

        protected void ProcessMFDoc(Document doc)
        {
            string route = string.Empty;
            if (doc.Indexes[0].ContainsKey("ROUTE"))
                route = doc.Indexes[0]["ROUTE"].ToUpper();
            Log(LogSeverity.Debug, string.Format("ProcessMFDoc:  routing doc {0}({1}) to {2}", doc.Name, doc.Id, route));
            if ("COMPLETE" == route)
            {
                Log(LogSeverity.Debug, "Foldering completed MF Document");
                Console.WriteLine("Foldering completed MF Document");
                FolderCompletedDocument(doc);
            }
            else if ((string.Empty == route) || ("MUTUAL FUNDS" == route))
            {
                Log(LogSeverity.Debug, "ROUTE empty or 'MUTUAL FUNDS', routing MF Document to Mutual Funds");
                conn.RouteWorkitem(doc, "Mutual Funds");
            }
            else
            {
                Log(LogSeverity.Debug, 
                    string.Format("ROUTE is {0}, (not empty, 'MUTUAL FUNDS', or 'COMPLETE', routing MF Document to Hold (Queue {1}).", route, holdWorkstepName));
                conn.RouteWorkitem(doc, holdWorkstepName);
            }
            Log(LogSeverity.Debug,"exiting ProcessMFDoc");
        }

        protected string[] MfApplMasterFolderFields = new string[] { "LAST_NAME", "FIRST_NAME", "MIDDLE_INITIAL", "TAX_ID", "SSN2", "DATE" };
        protected string[] MfApplMfAcctFields = new string[] { "LAST_NAME", "FIRST_NAME", "MIDDLE_INITIAL", "TAX_ID", "SSN2", "DATE", "ACCOUNT_NUM", "TYPE" };
        protected string[] MfMasterFolderSearchNames = new string[] { "TAX_ID" };
        protected string[] MfTransNames = new string[] { "ACCOUNT_NUM", "YEAR" };
        protected string[] MfAcctSearchNames = new string[] { "ACCOUNT_NUM", "TAX_ID" };
        protected string[] MfMasterFolderCopyNames = new string[] { "ACCOUNT_NUM", "TAX_ID", "DATE" };
        protected string[] MfAcctCopyNames = new string[] { "ACCOUNT_NUM", "TAX_ID", "DATE", "TYPE" };
        protected string[] HrAssocMasterSearchNames = new string[] { "LAST_NAME", "ASSOCIATE_NUMBER" };
        protected string[] HrAssocMasterCopyNames = new string[] { "LAST_NAME", "ASSOCIATE_NUMBER", "FIRST_NAME", "MIDDLE_INITIAL", "DATE" };
        protected string[] HrAssocSubCopyNames = new string[] { "ASSOCIATE_NUMBER" };
        protected string[] HrOtherSubNames = new string[] { "ASSOCIATE_NUMBER", "TYPE" };
        protected string[] HrOtherMasterSearchNames = new string[] { "ASSOCIATE_NUMBER" };
        protected string[] HrOtherMasterCopyNames = new string[] { "ASSOCIATE_NUMBER", "DATE" };

        public void FolderCompletedDocument(Document doc)
        {
            try
            {
                if (doc.Class.ToUpper() == "MFAPPL")
                {
                    if (!doc.Indexes[0].ContainsKey("TAX_ID") || string.IsNullOrEmpty(doc.Indexes[0]["TAX_ID"]))
                    {
                        doc.Status = WorkitemStatus.Error;
                        doc.ErrorSource = "FolderCompletedDocument";
                        doc.ErrorDescription = string.Format("MFAPPL document {0} ({1}) does not have a TAX_ID value ", doc.Name, doc.Id);
                        return;
                    }
                    Folder masterFolder = CreateFolderForDoc(doc, "MFFOLDER", doc.Indexes[0]["TAX_ID"], MfApplMasterFolderFields); //   new Folder();
                    Folder mfAcct = null;
                    if (doc.Indexes[0].ContainsKey("ACCOUNT_NUM") && (!(string.IsNullOrEmpty(doc.Indexes[0]["ACCOUNT_NUM"]))))
                    {
                        mfAcct = CreateFolderForDoc(doc, "MFACCT", doc.Indexes[0]["ACCOUNT_NUM"], MfApplMfAcctFields);
                        mfAcct.Documents.Add(doc);
                        conn.CreateFolder(mfAcct);
                        Console.WriteLine("Created MFACCT named {0}", mfAcct.Name);
                        masterFolder.Folders.Add(mfAcct);
                    }
                    else
                        masterFolder.Documents.Add(doc);
                    conn.CreateFolder(masterFolder);
                    Console.WriteLine("Created MFFOLDER named {0}", masterFolder.Name);
                    doc.Status = WorkitemStatus.Successful;
                }
                else // i.e., doc.CLASS != MFAPPL
                {
                    Folder mfFolder = null; // master
                    Folder mfTrans = null;
                    Folder mfAcct = null;
                    bool hasTaxId = (doc.Indexes[0].ContainsKey("TAX_ID") && !string.IsNullOrEmpty(doc.Indexes[0]["TAX_ID"]));

                    string mfTransId = ((doc.Indexes[0].ContainsKey("DATE") && !string.IsNullOrEmpty(doc.Indexes[0]["DATE"])) ?
                        doc.Indexes[0]["DATE"].Substring(0, 4) : string.Empty);
                    mfTrans = GetFolderForDoc(doc, conn, "MFTRANS", mfTransId, MfTransNames, MfTransNames);
                    if (null == mfTrans)
                        mfTrans = CreateFolderForDoc(doc, "MFTRANS", mfTransId, MfTransNames);

                    string mfAcctId = ((doc.Indexes[0].ContainsKey("ACCOUNT_NUM") && !string.IsNullOrEmpty(doc.Indexes[0]["ACCOUNT_NUM"])) ?
                        doc.Indexes[0]["ACCOUNT_NUM"] : string.Empty);
                    mfAcct = GetFolderForDoc(doc, conn, "MFACCT", mfAcctId, MfAcctSearchNames, MfAcctCopyNames);
                    if (null == mfAcct)
                        mfAcct = CreateFolderForDoc(doc, "MFACCT", mfAcctId, MfAcctCopyNames);

                    if (hasTaxId)
                    {
                        mfFolder = GetFolderForDoc(doc, conn, "MFFOLDER", doc.Indexes[0]["TAX_ID"], MfMasterFolderSearchNames, MfMasterFolderCopyNames);
                        if (null == mfFolder)
                            mfFolder = CreateFolderForDoc(doc, "MFFOLDER", doc.Indexes[0]["TAX_ID"], MfMasterFolderCopyNames);
                    }
                    Console.WriteLine("Folders created (in memory), updating document name");
                    doc.Name = doc.Indexes[0]["MFTRANS"] + " " + doc.Indexes[0]["DATE"];
                    conn.SetName(doc, doc.Name);
                    Console.WriteLine("Folders created (in memory), updating document name");

                    //string.Format("mfTrans {0} null.", (null == mfTrans) ? "is" : "is not").Dump();
                    if (!mfTrans.Documents.Any(d => d.Id == doc.Id))
                    {
                        mfTrans = RenameAndCloneIfFull(mfTrans, conn);
                        mfTrans.Documents.Add(doc);
                    }
                    if (string.IsNullOrEmpty(mfTrans.Id))
                        conn.CreateFolder(mfTrans);
                    else
                        conn.UpdateFolder(mfTrans);
                    Console.WriteLine("MfTrans created or updated.");

                    //string.Format("mfAcct {0} null.", (null == mfAcct) ? "is" : "is not").Dump();
                    if (!mfAcct.Folders.Any(f => f.Id == mfTrans.Id))
                    {
                        mfAcct = RenameAndCloneIfFull(mfAcct, conn);
                        mfAcct.Folders.Add(mfTrans);
                    }
                    if (string.IsNullOrEmpty(mfAcct.Id))
                        conn.CreateFolder(mfAcct);
                    else
                        conn.UpdateFolder(mfAcct);
                    Console.WriteLine("MfAcct created or updated.");

                    if (hasTaxId)
                    {
                        //string.Format("mfFolder {0} null.", (null == mfFolder) ? "is" : "is not").Dump();
                        if (!mfFolder.Folders.Any(f => f.Id == mfAcct.Id))
                        {
                            mfFolder = RenameAndCloneIfFull(mfFolder, conn);
                            mfFolder.Folders.Add(mfAcct);
                        }
                        if (string.IsNullOrEmpty(mfFolder.Id))
                            conn.CreateFolder(mfFolder);
                        else
                            conn.UpdateFolder(mfFolder);
                        Console.WriteLine("MfFolder created or updated.");
                    }
                    doc.Status = WorkitemStatus.Successful;
                }

                conn.RouteWorkitem(doc, removeWorkstepName);
            }
            catch (Exception ex)
            {
                Log(LogSeverity.Error, string.Format("Exception while foldering completed document " + ex.ToString()));
                Console.WriteLine(ex.ToString());
            }
        }

        protected void ProcessHRASSOCDoc(Document doc)
        {
            Folder masterFolder = null;
            Folder subFolder = null;
            string masterName = null;
            string subName = "Application";

            if ((!doc.Indexes[0].ContainsKey("ASSOCIATE_NUMBER")) || string.IsNullOrEmpty(doc.Indexes[0]["ASSOCIATE_NUMBER"]))
            {
                string error = string.Format("Document '{0}' does not contain an ASSOCIATE_NUMBER index value.", doc.Id);
                conn.PlaceInError(doc, error);
                doc.Status = WorkitemStatus.Error;
                doc.ErrorDescription = error;
                doc.ErrorSource = "ProcessHRASSOCDoc";
                return;
            }
            masterName = doc.Indexes[0]["ASSOCIATE_NUMBER"];
            masterFolder = GetFolderForDoc(doc, conn, "HRFOLDER", masterName, HrAssocMasterSearchNames, HrAssocMasterCopyNames);
            if (null != masterFolder)
            {
                string error = string.Format(
                    "While foldering document '{0}', a master folder was found for Last Name of '{1}' and Associate Number of '{2}', which shouldn't happen.",
                    doc.Id, doc.Indexes[0]["LAST_NAME"], doc.Indexes[0]["ASSOCIATE_NUMBER"]);
                conn.PlaceInError(doc, string.Format("HRFOLDER for {0} exists.", doc.Id));
                doc.Status = WorkitemStatus.Error;
                doc.ErrorDescription = error;
                doc.ErrorSource = "ProcessHRASSOCDoc";
                return;
            }
            else
            {
                masterFolder = CreateFolderForDoc(doc, "HRFOLDER", masterName, HrAssocMasterCopyNames);
                subFolder = CreateFolderForDoc(doc, "HRSUBFLD", subName, HrAssocSubCopyNames);
                subFolder.Documents.Add(doc);
                conn.CreateFolder(subFolder);
                masterFolder.Folders.Add(subFolder);
                conn.CreateFolder(masterFolder);
                doc.Status = WorkitemStatus.Successful;
                conn.RouteWorkitem(doc);
            }
        }

        protected void ProcessHROtherDoc(Document doc)
        {
            string hrMaster = "HRFOLDER";
            string hrSub = "HRSUBFLD";

            if (doc.Class == "HRBNDOC")
            {
                hrMaster = "HRBNFLDR";
                hrSub = "HRBNSUBF";
            }

            Folder masterFolder = null;
            Folder subFolder = null;
            bool newMasterFolder = false;
            bool newSubFolder = false;

            subFolder = GetFolderForDoc(doc, conn, hrSub, doc.Indexes[0]["TYPE"], HrOtherSubNames, HrOtherSubNames);
            if (null == subFolder)
            {
                newSubFolder = true;
                subFolder = CreateFolderForDoc(doc, hrSub, doc.Indexes[0]["TYPE"], HrOtherSubNames);
            }
            masterFolder = GetFolderForDoc(doc, conn, hrMaster, doc.Indexes[0]["ASSOCIATE_NUMBER"], HrOtherMasterSearchNames, HrOtherMasterCopyNames);
            if (null == masterFolder)
            {
                newMasterFolder = true;
                masterFolder = CreateFolderForDoc(doc, hrMaster, doc.Indexes[0]["ASSOCIATE_NUMBER"], HrOtherMasterCopyNames);
            }
            conn.SetName(doc, doc.Indexes[0]["DATE"]);
            //conn.UpdateDocument(doc, false);
            if (!subFolder.Documents.Any(d => d.Id == doc.Id))
            {
                //if ((subFolder.Documents.Count + subFolder.Folders.Count) > 398)
                //    subFolder = conn.CloneFolder(subFolder);
                subFolder.Documents.Add(doc);
            }
            if (newSubFolder)
                conn.CreateFolder(subFolder);
            else
                conn.UpdateFolder(subFolder);
            if (!masterFolder.Folders.Any(f => f.Id == subFolder.Id))
            {
                //if ((masterFolder.Documents.Count + masterFolder.Folders.Count) > 398)
                //    masterFolder = conn.CloneFolder(masterFolder);
                masterFolder.Folders.Add(subFolder);
            }
            if (newMasterFolder)
                conn.CreateFolder(masterFolder);
            else
                conn.UpdateFolder(masterFolder);
            conn.RouteWorkitem(doc);
            doc.Status = WorkitemStatus.Successful;
        }


        public void PerformHoldProcessing()
        {
            while (true)
            {
                IWorkitem item = conn.GetNext(holdWorkstepName);
                if (null == item)
                    break;
                conn.RouteWorkitem(item);
            }
            holdProcessingDue += new TimeSpan(holdProcessingInterval * TimeSpan.TicksPerSecond);
        }

        public void PerformAuditProcessing()
        {
            while (true)
            {
                IWorkitem item = conn.GetNext(auditWorkstepName);
                if (null == item)
                    break;

                Dictionary<string, string> fields = new Dictionary<string, string>(item.Indexes[0]);
                fields.Add("OBJECT_CLASS", item.Class);
                fields.Add("OBJECT_NAME", item.Name);
                fields.Add("OBJECT_ID", item.Id);

                bool analystRequired = false;
                string analystFieldName;
                string classPrefix = item.Class.Substring(0, 2);
                switch (classPrefix.ToUpper())
                {
                    case "AA":
                        analystFieldName = cAA_Analyst;
                        analystRequired = true;
                        break;
                    case "RP":
                        analystFieldName = cRP_Analyst;
                        analystRequired = true;
                        break;
                    case "MA":
                        analystFieldName = cMA_Analyst;
                        analystRequired = true;
                        break;
                    default:
                        analystFieldName = cAnalyst;
                        analystRequired = true;
                        break;
                }
                if (fields.ContainsKey(analystFieldName))
                {
                    if (string.IsNullOrEmpty(fields[analystFieldName]) && analystRequired)
                    {
                        conn.RouteWorkitem(item, exceptionWorkstepName);
                        continue;
                    }
                    fields.Add(cUserName, fields[analystFieldName]);
                }
                else if (analystRequired)
                {
                    conn.RouteWorkitem(item, exceptionWorkstepName);
                    continue;
                }
                if (!item.Indexes[0].ContainsKey("PASSFAIL"))
                {
                    conn.RouteWorkitem(item, exceptionWorkstepName);
                    continue;
                }
                Random r = new Random();
                if (r.Next(100) < 50)
                    item.Indexes[0]["PASSFAIL"] = "T";
                else
                    item.Indexes[0]["PASSFAIL"] = "F";

                conn.UpdateIndexes(item);
                conn.RouteWorkitem(item);
            }
            auditProcessingDue += new TimeSpan(auditProcessingInterval * TimeSpan.TicksPerSecond);
        }

        public IList<LogEntry> Logger { get; set; }

        protected void Log(LogSeverity sev, string message)
        {
            if (null != Logger)
                Logger.Add(new LogEntry()
                {
                    EventDate = DateTime.Now,
                    Id = Logger.Count,
                    Severity = sev,
                    Message = message
                });
        }

        //public Folder CreateFolderForDoc(Document doc, string folderClass, string foldersNameFieldFromDoc, string[] docFieldNamesToCopy)
        //{
        //    Folder rv = new Folder();
        //    rv.Class = folderClass;
        //    if (doc.Indexes[0].ContainsKey(foldersNameFieldFromDoc) && !(string.IsNullOrEmpty(doc.Indexes[0][foldersNameFieldFromDoc])))
        //        rv.Name = doc.Indexes[0][foldersNameFieldFromDoc];
        //    else
        //        rv.Name = Guid.NewGuid().ToString();
        //    if (rv.Indexes.Count == 0)
        //        rv.Indexes.Add(new Dictionary<string, string>());
        //    foreach (string name in docFieldNamesToCopy)
        //        if (doc.Indexes[0].ContainsKey(name))
        //            rv.Indexes[0].Add(name, doc.Indexes[0][name]); 
        //        else
        //            rv.Indexes[0].Add(name, string.Empty);
        //    return rv;
        //}

        public void CopyDictionaryFields(IDictionary<string, string> source, IDictionary<string, string> dest, string[] fieldNames)
        {
            CopyDictionaryFields(source, dest, fieldNames, true);
        }

        public void CopyDictionaryFields(IDictionary<string, string> source, IDictionary<string, string> dest, string[] fieldNames, bool includeEmptyFields)
        {
            foreach (string name in fieldNames)
            {
                if (name == "YEAR")
                {
                    if (source.ContainsKey("DATE") && (!string.IsNullOrEmpty(source["DATE"])))
                        dest.Add("YEAR", source["DATE"].Substring(0, 4));
                }
                else
                {
                    if (source.ContainsKey(name))
                    {
                        if (includeEmptyFields || (!string.IsNullOrEmpty(source[name])))
                            dest.Add(name, source[name]);
                    }
                    else if (includeEmptyFields)
                        dest.Add(name, string.Empty);
                }
            }
        }

        public Folder CreateFolderForDoc(Document doc, string className, string fName, string[] copyFieldNames)
        {
            Folder f = new Folder();
            f.Class = className;
            if (!string.IsNullOrEmpty(fName))
                f.Name = fName;
            else
                f.Name = Guid.NewGuid().ToString();
            if (f.Indexes.Count == 0)
                f.Indexes.Add(new Dictionary<string, string>());
            CopyDictionaryFields(doc.Indexes[0], f.Indexes[0], copyFieldNames);
            return f;
        }

        public Folder GetFolderForDoc(Document doc, UnisysImagingConnector conn, string className, string fName, string[] searchNames, string[] copyNames)
        {
            Folder f = null;
            Dictionary<string, string> searchIndexes = new Dictionary<string, string>();

            CopyDictionaryFields(doc.Indexes[0], searchIndexes, searchNames, false);

            if (!string.IsNullOrEmpty(className))
                searchIndexes.Add("OBJECT_CLASS", className);

            if (!string.IsNullOrEmpty(fName))
                searchIndexes.Add("OBJECT_NAME", fName + "%");

            var folders = conn.FindFolders(searchIndexes);

            Console.WriteLine("GetFolderForDoc:  found {0} existing folders", folders.Count());
            if (folders.Count() > 0)
            {
                f = folders.FirstOrDefault();
                Console.WriteLine("GetFolderForDoc:  cloning folder if full; folder item count is {0}", f.Documents.Count() + f.Folders.Count());
                f = RenameAndCloneIfFull(f, conn);
            }
            return f;
        }

        public Folder RenameAndCloneIfFull(Folder f, UnisysImagingConnector conn)
        {
            if (f.Documents.Count() + f.Folders.Count() >= 398)
            {
                Console.WriteLine("RenameAndCloneIfFull:  folder is full.  Cloning and renaming full folder {0} ({1})", f.Name, f.Id);
                Folder newFolder = new Folder();
                newFolder.Class = f.Class;
                newFolder.Name = f.Name;
                foreach (var dict in f.Indexes)
                {
                    var newIndex = new Dictionary<string, string>();
                    foreach (var kvp in dict)
                        newIndex.Add(kvp.Key, kvp.Value);
                    newFolder.Indexes.Add(newIndex);
                }
                f.Name = f.Name + "_" + DateTime.Now.ToString("MMdd");
                conn.SetName(f, f.Name);
                conn.CreateFolder(newFolder);
                return newFolder;
            }
            Console.WriteLine("RenameAndCloneIfFull:  folder is not full, returning original folder {0} ({1})", f.Name, f.Id);
            return f;
        }

    }

    public static class MappingExtensions
    {
        public static IDictionary<string, string> TranslateFields(this IEnumerable<Mapping> list, IDictionary<string, string> fields)
        {
            foreach (Mapping m in list)
            {
                if (m.Conditions.Evaluate(fields))
                    return m.TranslateFields(fields);
            }
            return null;
        }
    }
}
