﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestPlugin
{
    public class TestPlugin : Paragon.RobotServices.Plugin.PlugInBase
    {
        protected string sourceFolder = null;
        protected string destFolder = null;
        protected string origSource;
        protected string origDest;

        public TestPlugin(IDictionary<string, string> settings)
        {
            if (!(settings.ContainsKey("sourceFolder")))
                throw new ArgumentException("No sourceFolder setting given.");
            if (!(settings.ContainsKey("destFolder")))
                throw new ArgumentException("No destFolder setting given.");
            sourceFolder = origSource = settings["sourceFolder"];
            destFolder = origDest = settings["destFolder"];
            if (!(System.IO.Directory.Exists(sourceFolder)))
                throw new ArgumentException(string.Format("{0} is not a directory.", sourceFolder));
            if (!(System.IO.Directory.Exists(destFolder)))
                throw new ArgumentException(string.Format("{0} is not a directory.", destFolder));
        }

        public override long Run()
        {
            System.Console.WriteLine("Looping at " + DateTime.Now.ToString());
            string[] files = System.IO.Directory.GetFiles(sourceFolder, "*.*");
            if (files.Count() == 0)
            {
                string temp = sourceFolder;
                sourceFolder = destFolder;
                destFolder = temp;
                string[] files2 = System.IO.Directory.GetFiles(sourceFolder, "*.*");
                if (files2.Count() == 0)
                    return 0;
                else
                    files = files2;
            }
            foreach (string file in files)
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(file);
                Console.WriteLine("moving {0} to {1}.", fi.FullName, destFolder);
                string dest = System.IO.Path.Combine(destFolder, fi.Name);
                fi.MoveTo(dest);
            }
            return files.Count();
        }
    }
}
