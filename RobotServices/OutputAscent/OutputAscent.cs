﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Captures;

namespace Paragon.RobotServices.Plugin.AutoImport.Output
{
    public class OutputAscent : IOutputModule
    {
        protected CaptureService _cs = new CaptureService();
        protected IDictionary<string, string> _settings;

        public OutputAscent(IDictionary<string, string> settings)
        {
            _settings = settings;
        }

        #region IOutputModule Members

        public long Process(IWorkitem item)
        {
            int sessId = _cs.StartSession(int.Parse(_settings["captureId"]));
            List<byte[]> files = new List<byte[]>();
            //files.Add(System.IO.File.ReadAllBytes(item.Images.FullName));
            //var retDict = _cs.ImportStreams(sessId, item.IndexFields, files);
            //if (retDict["Status"] == "Successful")
            //    return long.Parse(retDict["PageCount"]);
            //else
                return 0;
        }

        public long ProcessBatch(IEnumerable<IWorkitem> items)
        {
            throw new NotImplementedException();
        }

        public bool SupportsBatching
        {
            get { return true; }
        }

        #endregion

        #region IIoCommon Members

        public bool Connect()
        {
            throw new NotImplementedException();
        }

        public bool Connected
        {
            get { throw new NotImplementedException(); }
        }

        public void Disconnect()
        {
            throw new NotImplementedException();
        }

        public bool RequiresConnection
        {
            get { return false; }
        }

        #endregion
    }
}
