﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class Schedule
    {
        [DataMember]
        public string Name;

        [DataMember]
        public int ScheduleId;

        [DataMember]
        public List<ScheduleElement> Elements = new List<ScheduleElement>();

        //public Schedule(ScheduleDAO s)
        //{
        //    this.Name = s.Name;
        //    this.ScheduleId = s.ScheduleId;
        //    foreach (var rs in s.RobotScheduleDAOs)
        //        this.RobotIds.Add(rs.RobotId);
        //    foreach (var se in s.ScheduleToScheduleElementDAOs)
        //        this.ScheduleElementIds.Add(se.ScheduleElementId);
        //}

        public Schedule()
        {
            //Elements.Add(new ScheduleElement()
            //{
            //    Start = (DateTime.MinValue + new TimeSpan(8, 0, 0)),
            //    End = DateTime.MaxValue, 
            //            Duration = new TimeSpan(10, 0, 0), Frequency = new TimeSpan(1, 0, 0, 0), 
            //            IsBlackout = false, Name = "Business Hours"});
            //Elements.Add(new ScheduleElement()
            //{
            //    Start = (DateTime.MinValue + new TimeSpan(6, 0, 0, 0)),
            //    End = DateTime.MaxValue, 
            //            Duration = new TimeSpan(2, 0, 0, 0), Frequency = new TimeSpan(7, 0, 0, 0), 
            //            IsBlackout = true, Name = "Weekends"});
        }

        public Schedule(ScheduleSaveModel model)
        {
            Name = model.Name;
            for (int i = 0; i < model.Elements.Count; i++)
            {
                var el = new ScheduleElement(model.Elements[i]);
                el.ScheduleElementId = i;
                el.Duration = model.Elements[i].Duration;
                if (el.Duration == "0")
                    el.Duration = "None";
                else if (el.Duration == "1")
                    el.Duration = "Daily";
                else if (el.Duration == "2")
                    el.Duration = "Weekly";
                else if (el.Duration == "3")
                    el.Duration = "Monthly";
                Elements.Add(el);
            }
        }
    }
}
