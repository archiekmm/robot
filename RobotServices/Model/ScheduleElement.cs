﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class ScheduleElement
    {
        [DataMember]
        public string Duration;

        [DataMember]
        public DateTime End;

        [DataMember]
        public TimeSpan Frequency;

        [DataMember]
        public DateTime Start;

        [DataMember]
        public string Name;

        [DataMember]
        public int ScheduleElementId;

        [DataMember]
        public bool IsBlackout;

        //public ScheduleElement(ScheduleElementDAO sed)
        //{
        //    this.Duration = sed.Duration;
        //    this.End = sed.End;
        //    this.Frequency = sed.Frequency;
        //    this.Name = sed.Name;
        //    this.ScheduleElementId = sed.ScheduleElementId;
        //    this.ScheduleKind = sed.ScheduleKind;
        //    this.Start = sed.Start;
        //}

        public ScheduleElement() { }

        public ScheduleElement(ScheduleElementSaveModel model)
        {
            Name = model.Name;
            IsBlackout = model.IsBlackout;
            if (!(DateTime.TryParse(model.Start, out Start)))
                Start = DateTime.MinValue;
            if (!(DateTime.TryParse(model.End, out End)))
                End = DateTime.MaxValue;
            Duration = model.Duration;
            if (!(TimeSpan.TryParse(model.Frequency, out Frequency)))
                Frequency = new TimeSpan(0);
        }
    }
}
