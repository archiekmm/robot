﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class UseLicense
    {
        [DataMember]
        public bool CanNormalize = false;
        [DataMember]
        public bool CanLookup = false;
        [DataMember]
        public bool CanMap = false;
        //[DataMember]
        //public DateTime ExpirationDate { get; set; }
        [DataMember]
        public DateTime ExpirationDate = DateTime.MaxValue;
        [DataMember]
        public string Message = string.Empty;
        [DataMember]
        public bool IsLiscenceExpired { get; set; }
    }
}