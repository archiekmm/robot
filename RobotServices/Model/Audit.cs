﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;
using System.Xml;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class Audit
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public bool Enabled { get; set; }
        [DataMember]
        public Login Login { get; set; }
        [DataMember]
        public string AuditTableName { get; set; }
        [DataMember]
        public string IdFieldName { get; set; }
        [DataMember]
        public string SuccessIndicator { get; set; }
        [DataMember]
        public string ErrorIndicator { get; set; }
        public string InsertQuery { get; set; }
        public string UpdateQuery { get; set; }
        public string SelectQueryByDay { get; set; }
        public string SelectQueryByMonth { get; set; }
        public string SelectQueryByYear { get; set; }

        public Audit()
        {
            Enabled = false;
            Login = new Login();
        }

        public Audit(AuditSaveSettings save)
        {
            this.Id = save.Id;
            this.Enabled = save.Enabled;
            this.Login = new Login(save.AsConnString());
            this.AuditTableName = save.AuditTableName;
            this.IdFieldName = save.IdFieldName;
            this.SuccessIndicator = save.SuccessIndicator;
            this.ErrorIndicator = save.ErrorIndicator;
        }
    }

    public class AuditSaveSettings
    {
        public int Id { get; set; }
        public bool Enabled { get; set; }
        public string Provider { get; set; }
        public string Server { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string AuditTableName { get; set; }
        public string IdFieldName { get; set; }
        public string SuccessIndicator { get; set; }
        public string ErrorIndicator { get; set; }

        public string AsConnString()
        {
            return string.Format("provider={0}&server={1}&database={2}&userid={3}&password={4}",
                Provider, Server, Database, UserId, Password);
        }
    }

    public class AuditInfo
    {
        public Guid InsertAuditInfo(Audit audit)
        {
            Guid id = new Guid();
            SqlConnection sqlConnection = GetConnectionString(audit);
            try
            {
                sqlConnection.Open();
                SqlCommand myCommand = new SqlCommand(audit.InsertQuery, sqlConnection);

                id = (Guid)myCommand.ExecuteScalar();

                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return id;
        }

        public void UpdateAuditInfo(Audit audit)
        {
            SqlConnection sqlConnection = GetConnectionString(audit);
            try
            {
                sqlConnection.Open();
                SqlCommand myCommand = new SqlCommand(audit.UpdateQuery, sqlConnection);
                myCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public int GetSuccessCount(Audit audit)
        {
            int count = 0;
            if (audit.Login.Provider != null)
            {
                SqlConnection sqlConnection = GetConnectionString(audit);
                try
                {
                    sqlConnection.Open();
                    SqlCommand myCommand = new SqlCommand("SELECT count(*) FROM robotinstance where status = 'Successful'", sqlConnection);
                    count = Convert.ToInt32(myCommand.ExecuteScalar());
                    sqlConnection.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                return count;
            }
            else
                return 0;
        }

        private static SqlConnection GetConnectionString(Audit audit)
        {
            string connectionString = "user id=" + audit.Login.UserId + ";password=" + audit.Login.Password + ";server=" + audit.Login.Server + ";Trusted_Connection=yes;database=" + audit.Login.Database;
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            return sqlConnection;
        }

        public List<ChartData> GetChartData(Audit audit, string viewType)
        {
            List<ChartData> listChartData = new List<ChartData>();
            SqlConnection sqlConnection = GetConnectionString(audit);
            sqlConnection.Open();
            SqlCommand cmd = sqlConnection.CreateCommand();
            switch (viewType)
            {
                case "Day":
                    cmd.CommandText = audit.SelectQueryByDay;
                    break;
                case "Month":
                    cmd.CommandText = audit.SelectQueryByMonth;;
                    break;
                case "Year":
                    cmd.CommandText = audit.SelectQueryByYear;
                    break;
                default:
                    cmd.CommandText = audit.SelectQueryByDay;
                    break;
            }
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ChartData chartData = new ChartData();
                chartData.ViewType = Convert.ToString(reader["view_type"]);
                chartData.FailureCount = Convert.ToInt32(reader["error"]);
                chartData.SuccessCount = Convert.ToInt32(reader["success"]);
                listChartData.Add(chartData);
            }
            reader.Close();
            sqlConnection.Close();

            return listChartData;
        }

        public static void SetAuditQueries(Audit audit)
        {
            XmlDocument document = new XmlDocument();
            document.Load(@"D:\Shweta\Personel\offshoreengagement\RobotServices-RCC\RobotServices\RobotServices\RobotService\bin\Debug\robots.xml");

            audit.InsertQuery = document.GetElementsByTagName("InsertQuery").Count == 0 ? string.Empty : document.GetElementsByTagName("InsertQuery").Item(0).InnerText;
            audit.UpdateQuery = document.GetElementsByTagName("UpdateQuery").Count == 0 ? string.Empty : document.GetElementsByTagName("UpdateQuery").Item(0).InnerText;
            audit.SelectQueryByDay = "select CAST(CAST(start_time AS DATE) as varchar(10)) as view_type, count(case status when 'Successful' then 1 end) success, count(case status when 'Error' then 1 end) error from {0} where CAST(start_time AS date) BETWEEN CAST('{1}' as date) and CAST('{2}' as date) group by CAST(start_time AS DATE) order by CAST(start_time AS DATE)";
            audit.SelectQueryByMonth = "SELECT DATENAME(month ,start_time) as view_type, count(case status when 'Successful' then 1 end) success, count(case status when 'Error' then 1 end) error from {0} where CAST(start_time AS date) BETWEEN CAST('{1}' as date) and CAST('{2}' as date) group by MONTH(CAST(start_time AS DATE) ) , DATENAME(month ,start_time)";
            audit.SelectQueryByYear = "SELECT year(CAST(start_time AS DATE)) as view_type, count(case status when 'Successful' then 1 end) success, count(case status when 'Error' then 1 end) error from {0} where CAST(start_time AS date) BETWEEN CAST('{1}' as date) and CAST('{2}' as date) group by year(CAST(start_time AS DATE) )";
        }

        //public static void SetAuditQueries(List<Robot> Robots)
        //{
        //    XmlDocument document = new XmlDocument();
        //    document.Load(@"D:\Shweta\Personel\offshoreengagement\RobotServices-RCC\RobotServices\RobotServices\RobotService\bin\Debug\robots.xml");

        //    foreach (Robot robot in Robots)
        //    {
        //        robot.Audit.InsertQuery = document.GetElementsByTagName("InsertQuery").Item(0).InnerText;
        //        robot.Audit.UpdateQuery = document.GetElementsByTagName("UpdateQuery").Item(0).InnerText;
        //    }
        //}
    }
}
