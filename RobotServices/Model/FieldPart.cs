﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Model
{
    internal enum FieldPartType
    {
        Literal,
        DateTime,
        Field
    }

    internal class FieldPart
    {
        public FieldPartType PartType = FieldPartType.Literal;
        public string LiteralText = string.Empty;
        public string FieldName = string.Empty;
        public bool Lookup = false;
        public Dictionary<string, string> LookupTable = null;
        public bool Wrap = false;
        public int WrapLength = 40;
        public bool Trim = false;
        public bool Numeric = false;
        public string NumericFormat = string.Empty;
        public bool DateField = false;
        public string DateFormat = string.Empty;
        public bool Strip = false;
        public string StripChars = string.Empty;
        public bool TruncateAt = false;
        public int TruncatePos = 0;
        public bool TruncateAfter = false;
        public string TruncateChars = string.Empty;
        public bool Substring = false;
        public int SubstringStart = 0;
        public int SubstringLength = 0;
        public bool IndexString = false;
        public int IndexValue = 0;
        public string IndexDelimiter = string.Empty;
        public bool StringFormat = false;
        public string StringFormatMask = string.Empty;
        public bool Fixed = false;
        public int FixedLength = 0;
        public bool ReplaceString = false;
        public string ReplaceText = string.Empty;
        public string ReplaceWithText = string.Empty;

        private const string cCommaString = ",";
        private const string cEscCommaString = @"\,";
        private const string cSubCommaString = "Â";
        private const string cLeftBraceString = "{";
        private const string cEscLeftBraceString = @"\{";
        private const string cSubLeftBraceString = "Å";
        private const string cRightBraceString = "}";
        private const string cEscRightBraceString = @"\}";
        private const string cSubRightBraceString = "Æ";
        private const string cColonString = ":";
        private const string cEscColonString = @"\:";
        private const string cSubColonString = "Ç";
        private const string cBarString = "|";
        private const string cEscBarString = @"\|";
        private const string cSubBarString = "È";
        private const string cLeftBracket = "[";
        private const string cEscLeftBracket = @"\[";
        private const string cSubLeftBracket = "Ò";
        private const string cRightBracket = "]";
        private const string cEscRightBracket = @"\]";
        private const string cSubRightBracket = "Ó";

        private char[] cCommaArray = ",".ToCharArray();
        private char[] cLeftBraceArray = "{".ToCharArray();
        private char[] cRightBraceArray = "}".ToCharArray();
        private char[] cColonArray = ":".ToCharArray();

        ~FieldPart()
        {
            if (LookupTable != null)
            {
                LookupTable.Clear();
                LookupTable = null;
            }
        }

        void ParseParameterStringByCommas(string value)
        {
            // sample input:  date{yyyy-mm-dd},strip{-},substring{0, 8),trim
            string[] parms = EncodeEscapedPivotChars(value).Split(cCommaArray);
            // results would be:
            //  parms[0] = date{yyyy-mm-dd}
            //  parms[1] = strip{-}
            //  parms[2] = substring{0, 8}
            //  parms[3] = trim
            foreach (string paramPart in parms)
            {
                // ignore empty functions (e.g., from "date{yyyy},,trim")
                if (paramPart != string.Empty)
                    ParseParameterFunction(paramPart);
            }
        }

        void ParseParameterFunction(string paramPart)
        {
            // sample input:  date{yyyy-mm-dd}
            string[] paramParts = paramPart.Split(cLeftBraceArray, 2);
            // results:
            //  paramparts[0] = date
            //  paramparts[1] = yyyy-mm-dd}
            // note:  either 1 or 2 items returned (since we pre-filter empty strings, zero is not possible)
            // strip right braces from the results, not being strict about them
            // to be strict, uncomment the following:
            //if (!paramParts[1].EndsWith("}"))
            //    throw new FormatException("invalid format for field part:  no right brace detected in section '"+paramPart+"'.");
            string arg;
            if (paramParts.Count() == 2)
                // so, commas in argument lists need to be escaped:  \,
                arg = DecodeEscapedPivotChars(paramParts[1].Replace("}", string.Empty));
            else // count == 1
                arg = string.Empty;

            // now, take the function and arguments and finish setting up the object
            switch (paramParts[0].ToLowerInvariant().Trim())
            {
                case "index":
                    SetupIndex(arg);
                    break;
                case "trim":
                    Trim = true;
                    break;
                case "wrap":
                    Wrap = true;
                    if (!int.TryParse(arg, out WrapLength))
                        WrapLength = 40;
                    break;
                case "numeric":
                    Numeric = true; NumericFormat = arg;
                    break;
                case "date":
                case "datetime":
                case "time":
                    DateField = true; DateFormat = arg;
                    break;
                case "lookup":
                    SetupLookup(arg);
                    break;
                case "strip":
                    Strip = true; StripChars = arg;
                    break;
                case "substring":
                    SetupSubstring(arg);
                    break;
                case "truncate":
                    SetupTruncate(arg);
                    break;
                case "format":
                    StringFormat = true; StringFormatMask = arg;
                    break;
                case "fixed":
                    Fixed = true;
                    if (!int.TryParse(arg, out FixedLength))
                        FixedLength = 0;
                    break;
                case "replace":
                    SetupReplace(arg);
                    break;
                default:
                    throw new FormatException("invalid function requested:  " + paramParts[0].ToLowerInvariant());
            }

        }

        internal void SetupIndex(string arg)
        {
            string[] subParts = arg.Split("|".ToCharArray());
            IndexString = true;
            if (!int.TryParse(subParts[0], out IndexValue))
                IndexValue = 0;
            if (subParts.Count() == 2)
                IndexDelimiter = subParts[1];
            else
                IndexDelimiter = ",";
        }

        internal void SetupLookup(string arg)
        {
            Lookup = true;
            string[] parmSubParts = arg.Split("|".ToCharArray());
            if (LookupTable == null)
                LookupTable = new Dictionary<string, string>();
            else
                LookupTable.Clear();
            foreach (string paramSubPart in parmSubParts)
                LookupTable.Add(paramSubPart.Split(":".ToCharArray())[0], paramSubPart.Split(":".ToCharArray())[1]);
        }

        internal void SetupSubstring(string arg)
        {
            string[] parmSubParts = arg.Split("|".ToCharArray());
            if (parmSubParts.Count() == 0)
                Substring = false;
            else if (parmSubParts.Count() == 1)
            {
                Substring = true;
                SubstringStart = int.Parse(parmSubParts[0]);
                SubstringLength = int.MaxValue - SubstringStart;
            }
            if (parmSubParts.Count() >= 2)
            {
                Substring = true;
                SubstringStart = int.Parse(parmSubParts[0]);
                SubstringLength = int.Parse(parmSubParts[1]);
            }

        }

        internal void SetupTruncate(string arg)
        {
            int temp = 0;
            if (int.TryParse(arg, out temp))
            {
                if (temp >= 1)
                {
                    TruncateAt = true;
                    TruncatePos = temp;
                }
            }
            else
            {
                TruncateAfter = true;
                TruncateChars = arg;
            }
        }

        internal void SetupReplace(string arg)
        {
            string[] parmSubParts = arg.Split("|".ToCharArray());
            if (parmSubParts.Count() == 1)
            {
                ReplaceString = true;
                ReplaceText = parmSubParts[0];
                ReplaceWithText = string.Empty;
            }
            if ((parmSubParts.Count() == 2) && (parmSubParts[0] != string.Empty))
            {
                ReplaceString = true;
                ReplaceText = parmSubParts[0];
                ReplaceWithText = parmSubParts[1];
            }
        }

        internal void OldSetParameters(string value)
        {
            List<string> parms = new List<string>();
            List<string> paramParts = new List<string>();
            List<string> parmSubParts = new List<string>();

            if ((value == null) || (value == string.Empty))
            {
                return;
            }

            value = EncodeEscapedPivotChars(value);
            parms.AddRange(value.Split(cCommaArray));
            foreach (string paramPart in parms)
            {
                paramParts.Clear();
                paramParts.AddRange(paramPart.Split(cLeftBraceArray, 2));

                // validate
                if (paramParts.Count == 2) // that is, the left brace is in the string
                {
                    if (paramParts[1].EndsWith("}"))
                    {
                        paramParts[1] = paramParts[1].Substring(0, paramParts[1].Length - 1);
                    }
                    else
                    {
                        throw new FormatException(string.Format("invalid format for field part:  no right braces detected in section '{0}' of string {1}", paramPart, value));
                    }
                    paramParts[1] = DecodeEscapedPivotChars(paramParts[1]);
                }
                else if ((paramPart == "numeric") || (paramPart == "trim") || (paramPart == "wrap") || (paramPart == "fixed"))
                {
                    paramParts.Add("");
                }
                else
                {
                    throw new FormatException(string.Format("invalid format for field part:  multiple left braces detected in section '{0}' of string {1}", paramPart, value));
                }

                // validation passed
                switch (paramParts[0].ToLowerInvariant())
                {
                    case "index":
                        parmSubParts.Clear();
                        parmSubParts.AddRange(paramParts[1].Split("|".ToCharArray()));
                        IndexString = true;
                        IndexValue = int.Parse(parmSubParts[0]);
                        if (parmSubParts.Count == 2)
                            IndexDelimiter = parmSubParts[1];
                        else
                            IndexDelimiter = ",";
                        break;
                    case "trim":
                        Trim = true;
                        break;
                    case "wrap":
                        Wrap = true;
                        if (!int.TryParse(paramParts[1], out WrapLength))
                            WrapLength = 40;
                        //if (paramParts[1] != string.Empty)
                        //    WrapLength = int.Parse(paramParts[1]);
                        break;
                    case "numeric":
                        Numeric = true;
                        NumericFormat = paramParts[1];
                        break;
                    case "date":
                    case "datetime":
                    case "time":
                        DateField = true;
                        DateFormat = paramParts[1];
                        break;
                    case "lookup":
                        Lookup = true;
                        parmSubParts.Clear();
                        parmSubParts.AddRange(paramParts[1].Split("|".ToCharArray()));
                        if (LookupTable == null)
                            LookupTable = new Dictionary<string, string>();
                        else
                            LookupTable.Clear();
                        foreach (string paramSubPart in parmSubParts)
                            LookupTable.Add(paramSubPart.Split(":".ToCharArray())[0], paramSubPart.Split(":".ToCharArray())[1]);
                        break;
                    case "strip":
                        Strip = true;
                        StripChars = paramParts[1];
                        break;
                    case "substring":
                        parmSubParts.Clear();
                        parmSubParts.AddRange(paramParts[1].Split("|".ToCharArray()));
                        if (parmSubParts.Count == 0)
                            Substring = false;
                        else if (parmSubParts.Count == 1)
                        {
                            Substring = true;
                            SubstringStart = int.Parse(parmSubParts[0]);
                            SubstringLength = int.MaxValue - SubstringStart;
                        }
                        if (parmSubParts.Count >= 2)
                        {
                            Substring = true;
                            SubstringStart = int.Parse(parmSubParts[0]);
                            SubstringLength = int.Parse(parmSubParts[1]);
                        }
                        break;
                    case "truncate":
                        int temp = 0;
                        if (int.TryParse(paramParts[1], out temp))
                        {
                            if (temp >= 1)
                            {
                                TruncateAt = true;
                                TruncatePos = temp;
                            }
                        }
                        else
                        {
                            TruncateAfter = true;
                            TruncateChars = paramParts[1];
                        }
                        break;
                    case "format":
                        StringFormat = true;
                        StringFormatMask = paramParts[1];
                        break;
                    case "fixed":
                        Fixed = true;
                        if (!int.TryParse(paramParts[1], out FixedLength))
                            FixedLength = 0;
                        //FixedLength = int.Parse(paramParts[1]);
                        break;
                    case "replace":
                        parmSubParts.Clear();
                        parmSubParts.AddRange(paramParts[1].Split("|".ToCharArray()));
                        if (parmSubParts.Count == 1)
                        {
                            ReplaceString = true;
                            ReplaceText = parmSubParts[0];
                            ReplaceWithText = string.Empty;
                        }
                        if ((parmSubParts.Count == 2) && (parmSubParts[0] != string.Empty))
                        {
                            ReplaceString = true;
                            ReplaceText = parmSubParts[0];
                            ReplaceWithText = parmSubParts[1];
                        }
                        break;
                }
            }
        }

        public string Parameters
        {
            set
            {
                // testing new set parameters
                //OldSetParameters(value);

                ParseParameterStringByCommas(value);
            }
        }

        internal static string EncodeEscapedPivotChars(string strInput)
        {
            return strInput
                .Replace(cEscBarString, cSubBarString)
                .Replace(cEscColonString, cSubColonString)
                .Replace(cEscCommaString, cSubCommaString)
                .Replace(cEscLeftBraceString, cSubLeftBraceString)
                .Replace(cEscLeftBracket, cSubLeftBracket)
                .Replace(cEscRightBraceString, cSubRightBraceString)
                .Replace(cEscRightBracket, cSubRightBracket);
        }

        internal static string DecodeEscapedPivotChars(string strInput)
        {
            return strInput
                .Replace(cSubBarString, cBarString)
                .Replace(cSubColonString, cColonString)
                .Replace(cSubCommaString, cCommaString)
                .Replace(cSubLeftBraceString, cLeftBraceString)
                .Replace(cSubLeftBracket, cLeftBracket)
                .Replace(cSubRightBraceString, cRightBraceString)
                .Replace(cSubRightBracket, cRightBracket);
        }

        public static FieldPart CreateLiteral(string text)
        {
            FieldPart rv = new FieldPart();
            rv.LiteralText = text;
            rv.PartType = FieldPartType.Literal;
            return rv;
        }

        public static FieldPart CreateField(string text)
        {
            if ((text == null) || (text == string.Empty))
                return null;

            // input is something like "Field:func{arg|arg2},func2{arg},func3"
            FieldPart rv = new FieldPart();

            // encode escaped values
            string encoded = EncodeEscapedPivotChars(text);

            // parse on colon, which separates field from functions
            string[] parts = encoded.Split(":".ToCharArray(), 2);

            // first part is either a field or a coded literal, such as [br] for newline
            // let's create that
            if ((parts.Count() == 1) || ((parts.Count() == 2) && (parts[1] == string.Empty)))
                // no colon in the field, or empty parameters, so no field functions used
                switch (parts[0].ToLowerInvariant())
                {
                    case "cr":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = "\n";
                        break;
                    case "lf":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = "\r";
                        break;
                    case "br":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = "\n\r";
                        break;
                    case "tab":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = "\t";
                        break;
                    case "comma":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = ",";
                        break;
                    case "quote":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = "\"";
                        break;
                    case "colon":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = ":";
                        break;
                    case "semi":
                        rv.PartType = FieldPartType.Literal;
                        rv.LiteralText = ";";
                        break;
                    default:
                        rv.PartType = FieldPartType.Field;
                        rv.FieldName = parts[0];
                        break;
                }
            else // there is a colon, and therefore functions on the field
            {
                // but, it might just be a date value
                switch (parts[0].ToLowerInvariant())
                {
                    case "date":
                    case "datetime":
                    case "time":
                        rv.PartType = FieldPartType.DateTime;
                        //rv.DateField = true;  hmm, not doing this in prior method, not done in RobotProc, either.
                        rv.DateFormat = parts[1];
                        break;
                    default:
                        rv.PartType = FieldPartType.Field;
                        rv.FieldName = parts[0];
                        rv.Parameters = parts[1];
                        break;
                }
            }
            return rv;
        }
    }
}
