﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class ScheduleSettings
    {
        [DataMember]
        public TimeSpan Duration;

        [DataMember]
        public DateTime End;

        [DataMember]
        public TimeSpan Frequency;
        [DataMember]
        public string Name;

        [DataMember]
        public int ScheduleElementId;

        [DataMember]
        public bool IsBlackout;
        [DataMember]
        public DateTime Start;

        //public ScheduleSettings() { }

        //public ScheduleSettings(ScheduleElementSaveModel model)
        //{
        //    Name = model.Name;
        //    IsBlackout = model.IsBlackout;
        //    if (!(DateTime.TryParse(model.Start, out Start)))
        //        Start = DateTime.MinValue;
        //    if (!(DateTime.TryParse(model.End, out End)))
        //        End = DateTime.MaxValue;
        //    if (!(TimeSpan.TryParse(model.Duration, out Duration)))
        //        Duration = new TimeSpan(0);
        //    if (!(TimeSpan.TryParse(model.Frequency, out Frequency)))
        //        Frequency = new TimeSpan(0);
        //}

        public ScheduleSettings()
        {
            Duration = new TimeSpan(0);
            End = DateTime.MaxValue;
            Frequency = new TimeSpan(0);
            Name = "Service Schedule";
            Start = DateTime.MinValue;

        }
    }
}
