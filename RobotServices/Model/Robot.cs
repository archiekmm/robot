﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class Robot
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public bool Repeats { get; set; }
        [DataMember]
        public int SleepTime { get; set; }
        [DataMember]
        public bool IsScheduled { get; set; }
        [DataMember]
        public string LicenseKey { get; set; }
        [DataMember]
        public List<Schedule> Schedules { get; set; }
        [DataMember]
        public NotificationSettings Notifier { get; set; }
        [DataMember]
        public NormalizerSettings Normalizer { get; set; }
        [DataMember]
        public string Lookup { get; set; }
        [DataMember]
        public string Mapping { get; set; }
        [DataMember]
        public Audit Audit { get; set; }
        [DataMember]
        public int InputModuleId { get; set; }
        [DataMember]
        public int OutputModuleId { get; set; }
        [DataMember]
        public Dictionary<int, string> InputSettings { get; set; }
        [DataMember]
        public Dictionary<int, string> OutputSettings { get; set; }
        [DataMember]
        public UseLicense Liscence { get; set; }
        [DataMember]
        public DateTime ExpDate { get; set; }
        [DataMember]
        public int SuccessCount { get; set; }
        [DataMember]
        public int FailureCount { get; set; }
        [DataMember]
        public List<ChartData> ItemsChartData { get; set; }
        public Robot()
        {
            Schedules = new List<Schedule>();
            //Schedules.Add(new Schedule());
            IsScheduled = false;
            LicenseKey = string.Empty;
            Notifier = new NotificationSettings();
            Normalizer = new NormalizerSettings();
            Audit = new Audit();
            InputSettings = new Dictionary<int, string>();
            OutputSettings = new Dictionary<int, string>();
            SuccessCount = 0;
            FailureCount = 0;
            ItemsChartData = new List<ChartData>();
        }
        
    }
    public class RobotSaveModel
    {
        public string Name { get; set; }
        //public Dictionary<int, Schedule> Schedules = new Dictionary<int, Schedule>();
        public NotificationSettings Notification { get; set; }
        public NormalizerSettings Normalization { get; set; }
        public string Mapping { get; set; }
        public string Lookup { get; set; }
        public List<SettingSaveModel> Input { get; set; }
        public List<SettingSaveModel> Output { get; set; }
        public AuditSaveSettings Audit { get; set; }
        public List<ScheduleSaveModel> Schedules { get; set; }
    }

    public class SettingSaveModel
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
    }

    public class ScheduleElementSaveModel
    {
        public string Name { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Duration { get; set; }
        public string Frequency { get; set; }
        public bool IsBlackout { get; set; }
    }

    public class ScheduleSaveModel
    {
        public string Name { get; set; }
        public List<ScheduleElementSaveModel> Elements { get; set; }
    }
}
