﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class ModuleSetting
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public string HelpText { get; set; }
        [DataMember]
        public IDictionary<string, string> Values { get; set; }
        [DataMember]
        public int DependsOnId { get; set; }
        [DataMember]
        public string EnabledOnValues { get; set; }
        [DataMember]
        public int SettingTypeId { get; set; }

        public ModuleSetting()
        {
            Values = new Dictionary<string, string>();
        }
    }
}
