﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Model
{
    public static class FieldsEval
    {
        public static IDictionary<string, string> TranslateFields(this IEnumerable<Field> fields, IDictionary<string, string> inputs, bool includeUnmapped)
        {
            Dictionary<string, string> rv = new Dictionary<string, string>();
            KeyValuePair<string, string> kvp; // = null;
            foreach (var field in fields)
            {
                kvp = field.TranslateField(inputs);
                // change in case of duplicates, we don't want an exception thrown.
                //rv.Add(kvp.Key, kvp.Value);
                rv[kvp.Key] = kvp.Value;
            }
            if (includeUnmapped)
                foreach (var kvp2 in inputs)
                    if (!(rv.ContainsKey(kvp2.Key)))
                        rv.Add(kvp2.Key, inputs[kvp2.Key]);
            return rv;
        }

        public static IDictionary<string, string> TranslateFields(this IEnumerable<Field> fields, IDictionary<string, string> inputs)
        {
            return fields.TranslateFields(inputs, true);
        }
    }
}
