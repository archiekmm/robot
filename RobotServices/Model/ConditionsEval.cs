﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Model
{
    public static class ConditionsEval
    {
        public static bool Evaluate(this IEnumerable<Condition> conditions, IDictionary<string, string> inputs)
        {
            bool rv = true;

            // No conditions === "unconditional" === always true
            if (conditions.Count() == 0)
                return true;

            foreach (var cond in conditions)
            {
                bool interim = cond.Evaluate(inputs);
                switch (cond.ConnectorId) //(cond.Connector)
                {
                    //case Connector.And: // AND
                    case 1:
                        rv = (rv && interim);
                        break;
                    //case Connector.Or: // OR
                    case 2:
                        rv = (rv || interim);
                        break;
                }
            }
            return rv;
        }
    }
}
