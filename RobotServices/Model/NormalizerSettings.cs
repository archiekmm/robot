﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization; 

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class NormalizerSettings
    {
        [DataMember]
        public int NormalizationId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool Normalize { get; set; }
        [DataMember]
        public bool ForceTifExtension { get; set; }
        [DataMember]
        public int TiffCompression { get; set; }
        [DataMember]
        public int Strips { get; set; }
        [DataMember]
        public int FillOrder { get; set; }
        [DataMember]
        public int MatchResolutions { get; set; }
        [DataMember]
        public int RoundResolutions { get; set; }
        [DataMember]
        public int RotateImages { get; set; }
        [DataMember]
        public int InvertImages { get; set; }
        [DataMember]
        public int MirrorImages { get; set; }
        [DataMember]
        public int ColorReduction { get; set; }

        public NormalizerSettings()
        {
            NormalizationId = 0;
            Name = "Normalization";
            Normalize = true;
            ForceTifExtension = true;
            TiffCompression = 1;
            Strips = 1;
            FillOrder = 1;
            MatchResolutions = 1;
            RoundResolutions = 1;
            RotateImages = 1;
            InvertImages = 1;
            MirrorImages = 1;
            ColorReduction = 1;
        }
    }
}
