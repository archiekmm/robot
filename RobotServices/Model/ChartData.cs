﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class ChartData
    {
        /// <summary>
        /// Total Count of items processed
        /// </summary>
        [DataMember]
        public int TotalCount { get; set; }

        /// <summary>
        /// Success count
        /// </summary>
        [DataMember]
        public int SuccessCount { get; set; }

        /// <summary>
        /// Error count
        /// </summary>
        [DataMember]
        public int FailureCount { get; set; }

        /// <summary>
        /// View type
        /// </summary>
        [DataMember]
        public string ViewType { get; set; }
    }
}
