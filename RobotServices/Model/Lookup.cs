﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Model
{
    public class Lookup
    {
        public IDictionary<string, string> DoLookup(IDictionary<string, string> indexes)
        {
            return null;
        }

        public Lookup(string xmlMapping)
        {
            Mappings = Mapping.FromXml(xmlMapping);
        }

        public List<Mapping> Mappings;

        public string ProviderName;
        public string ConnectionString;

    }
}
