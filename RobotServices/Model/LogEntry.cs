﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Model
{
    public enum LogSeverity
    {
        Debug,
        Normal,
        Warning,
        Error,
        Critical
    }

    public class LogEntry
    {
        public int Id;
        public DateTime EventDate;
        public LogSeverity Severity;
        public string Message;
    }
}
