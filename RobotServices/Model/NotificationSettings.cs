﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class NotificationSettings
    {
        [DataMember]
        public string SmtpServer { get; set; }
        [DataMember]
        public int SmtpPort { get; set; }
        [DataMember]
        public string Recipients { get; set; }
        [DataMember]
        public string From { get; set; }

        [DataMember]
        public bool NotifyStartEventLog { get; set; }
        [DataMember]
        public bool NotifyStartEmail { get; set; }
        [DataMember]
        public bool NotifyStopEventLog { get; set; }
        [DataMember]
        public bool NotifyStopEmail { get; set; }
        [DataMember]
        public bool NotifyUserAbortEventLog { get; set; }
        [DataMember]
        public bool NotifyUserAbortEmail { get; set; }
        [DataMember]
        public bool NotifyErrorAbortEventLog { get; set; }
        [DataMember]
        public bool NotifyErrorAbortEmail { get; set; }
        [DataMember]
        public bool NotifyAnyErrorEventLog { get; set; }
        [DataMember]
        public bool NotifyAnyErrorEmail { get; set; }

        public NotificationSettings()
        {
            SmtpServer = "smtp.mail.google.com";
            SmtpPort = 25;
            Recipients = "administrator@email.com";
            From = "admin@robot.com";

            NotifyStartEventLog = true;
            NotifyStartEmail = false;
            NotifyStopEventLog = true;
            NotifyStopEmail = false;
            NotifyUserAbortEventLog = true;
            NotifyUserAbortEmail = false;
            NotifyErrorAbortEventLog = true;
            NotifyErrorAbortEmail = true;
            NotifyAnyErrorEventLog = true;
            NotifyAnyErrorEmail = false;
        }
    }
}
