﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Model
{
    [DataContract]
    public class Module
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public IList<ModuleSetting> Settings { get; protected set; }

        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string File { get; set; }

        public Module()
        {
            Settings = new List<ModuleSetting>();
        }

        public Module(string file)
        {
            if (string.IsNullOrEmpty(file))
                throw new ArgumentNullException(file);
            File = file;
            Settings = BuildSettings(file);
        }

        public IList<ModuleSetting> BuildSettings(string file)
        {
            switch (file)
            {
                case "InputFS.dll":
                    return InputFS();
                case "InputUnisys.dll":
                    return InputUnisys();
                case "InputSQLServer.dll":
                    return InputSQLServer();
                case "InputEmail.dll":
                    return InputEmail();
                case "OutputFS.dll":
                    return OutputFS();
                case "OutputUnisys.dll":
                    return OutputUnisys();
                case "OutputSQLServer.dll":
                    return OutputSQLServer();
                case "OutputEmail.dll":
                    return OutputEmail();
                case "OutputKofax.dll":
                    return OutputKofax();
                case "OutputEDI.dll":
                    return OutputEDI();
                default:
                    throw new ArgumentOutOfRangeException(file);
            }
        }

        public IList<ModuleSetting> InputFS()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="Folders", Group="Folders", Label="Input Folders", Value=@"Input1=C:\Input;Input2=\\share\name\", SettingTypeId=6 },
                    new ModuleSetting() { Id=2, Name="Method", Group="Folders", Label="Poll Method", Value="1", SettingTypeId=5 },
                    new ModuleSetting() { Id=3, Name="Depth", Group="Folders", Label="Depth", Value="3;5", SettingTypeId=2, HelpText = "Subfolder depth; use -1 for 'all' or 0 to disable" },
                    new ModuleSetting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            var poll = rv.Where(s => s.Label == "Poll Method").Single();
            poll.Values.Add("Round Robin", "1");
            poll.Values.Add("Whole Folders First", "2");
            return rv;
        }

        public IList<ModuleSetting> InputUnisys()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new ModuleSetting() { Id=2, Name="Workset", Group="Worksets", Label="Input Workset", Value="AutoFolder", SettingTypeId=3 },
                    new ModuleSetting() { Id=3, Name="Discard", Group="Worksets", Label="Remove from Workflow", Value="false", SettingTypeId=1 },
                    new ModuleSetting() { Id=4, Name="Destination", Group="Worksets", Label="Dest Workset", Value="", SettingTypeId=3, DependsOnId=3, EnabledOnValues="true" },
                    new ModuleSetting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            return rv;
        }
        public IList<ModuleSetting> InputSQLServer()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new ModuleSetting() { Id=2, Name="Folders", Group="Folders", Label="Input Folders", Value=@"Input1=C:\Input;Input2=\\share\name\", SettingTypeId=6 },
                    new ModuleSetting() { Id=3, Name="Method", Group="Folders", Label="Poll Method", Value="1", SettingTypeId=5 },
                    new ModuleSetting() { Id=4, Name="Depth", Group="Folders", Label="Depth", Value="3;5", SettingTypeId=2, HelpText = "Subfolder depth; use -1 for 'all' or 0 to disable" },
                    new ModuleSetting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            var poll = rv.Where(s => s.Label == "Poll Method").Single();
            poll.Values.Add("Round Robin", "1");
            poll.Values.Add("Whole Folders First", "2");
            return rv;
        }
        public IList<ModuleSetting> InputEmail()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new ModuleSetting() { Id=3, Name="Protocol", Group="Server", Label="Protocol", Value="1", SettingTypeId=5 },
                    new ModuleSetting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new ModuleSetting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new ModuleSetting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            var poll = rv.Where(s => s.Label == "Protocol").Single();
            poll.Values.Add("IMAP", "1");
            poll.Values.Add("POP3", "2");
            return rv;
        }
        public IList<ModuleSetting> OutputFS()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="test", Group="General", Label="Test", Value="false", SettingTypeId=1 },
                    new ModuleSetting() { Id=2, Name="test2", Group="General", Label="Test 2", Value="3", SettingTypeId=3 },
                    new ModuleSetting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new ModuleSetting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new ModuleSetting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            return rv;
        }
        public IList<ModuleSetting> OutputUnisys()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=6, Name="Sharing", Group="Misc", Label="Login Sharing", Value="false", SettingTypeId=1},
                    new ModuleSetting() { Id=1, Name="Login", Group="Misc", Label="Login", Value="", SettingTypeId=8, EnabledOnValues="false", DependsOnId=6 },
                    new ModuleSetting() { Id=2, Name="Workset", Group="Worksets", Label="Input Workset", Value="AutoFolder", SettingTypeId=3 },
                    new ModuleSetting() { Id=3, Name="Discard", Group="Worksets", Label="Remove from Workflow", Value="false", SettingTypeId=1 },
                    new ModuleSetting() { Id=4, Name="Destination", Group="Worksets", Label="Dest Workset", Value="", SettingTypeId=3, DependsOnId=3, EnabledOnValues="true" },
                    new ModuleSetting() { Id=5, Name="Mappings", Group="Mappings", Label="Input Mappings", Value="", SettingTypeId=7 },
                };
            return rv;
        }
        public IList<ModuleSetting> OutputSQLServer()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="test", Group="General", Label="Test", Value="false", SettingTypeId=1 },
                    new ModuleSetting() { Id=2, Name="test2", Group="General", Label="Test 2", Value="3", SettingTypeId=3 },
                    new ModuleSetting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new ModuleSetting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new ModuleSetting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            return rv;
        }
        public IList<ModuleSetting> OutputEmail()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new ModuleSetting() { Id=2, Name="Protocol", Group="Server", Label="Protocol", Value="1", SettingTypeId=5 },
                    new ModuleSetting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new ModuleSetting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new ModuleSetting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            var proto = rv.Where(ModuleSetting => ModuleSetting.Name == "Protocol").Single();
            proto.Values.Add("SMTP", "1");
            return rv;
        }
        public IList<ModuleSetting> OutputKofax()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="Login", Group="Login", Label="Login", Value="", SettingTypeId=8 },
                    new ModuleSetting() { Id=2, Name="BatchSize", Group="Batch", Label="Batch size", Value="20", SettingTypeId=2 },
                    new ModuleSetting() { Id=3, Name="Mappings", Group="Mappings", Label="Mappings", Value="", SettingTypeId=7 },
                };
            return rv;
        }
        public IList<ModuleSetting> OutputEDI()
        {
            var rv = new List<ModuleSetting>
                {
                    new ModuleSetting() { Id=1, Name="test", Group="General", Label="Test", Value="false", SettingTypeId=1 },
                    new ModuleSetting() { Id=2, Name="test2", Group="General", Label="Test 2", Value="3", SettingTypeId=3 },
                    new ModuleSetting() { Id=3, Name="test3", Group="Other", Label="Test 3", Value="3;5", SettingTypeId=4 },
                    new ModuleSetting() { Id=4, Name="test4", Group="Other", Label="Test 4", Value="L1=V1;L2=V2", SettingTypeId=5 },
                    new ModuleSetting() { Id=5, Name="test5", Group="Other", Label="Test 5", Value="2", SettingTypeId=2 },
                };
            return rv;
        }

    }
}
