﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Paragon.RobotServices.Model
{
    public enum CompareMethod
    {
        Text,
        Binary
    }

    /*
    public enum Connector
    {
        And,
        Or
    }

    public enum Conditional
    {
        Equals,
        NotEquals,
        IsBlank,
        IsNotBlank,
        StartsWith,
        EndsWith,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Contains,
        InList,
        NotInList,
        Like,
        NotLike
    }

    */

    [DataContract]
    public class Condition
    {
        [DataMember]
        public int ConditionId;

        [DataMember]
        public string Operand1;

        //[DataMember]
        //public Conditional Conditional = Conditional.Equals;

        [DataMember]
        public int ConditionalId;

        [DataMember]
        public string Operand2;

        //[DataMember]
        //public Connector Connector = Connector.And;

        [DataMember]
        public int ConnectorId; // = Connector.And;

        [DataMember]
        public CompareMethod CompareMethod = CompareMethod.Text;

        //public Condition(ConditionDAO c)
        //{
        //    this.ConditionId = c.ConditionId;
        //    this.CompareMethod = c.CompareMethod;
        //    this.Conditional = c.Conditional;
        //    this.Connector = c.Connector;
        //    this.Operand1 = c.Operand1;
        //    this.Operand2 = c.Operand2;
        //}

        public Condition() { }

        public Condition(System.Xml.Linq.XElement el)
        {
            if (el.Attribute("id") != null)
                int.TryParse(el.Attribute("id").Value, out this.ConditionId);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'id' attribute, or the attribute is not a number.", el.ToString()));
            //if (el.Attribute("order") != null)
            //    int.TryParse(el.Attribute("order").Value, out this.Order);
            //else
            //    throw new ArgumentException(string.Format("element {0} does not have an 'order' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("op1") != null)
                this.Operand1 = el.Attribute("op1").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'op1' attribute.", el.ToString()));
            if (el.Attribute("op2") != null)
                this.Operand2 = el.Attribute("op2").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'op2' attribute.", el.ToString()));
            if (el.Attribute("conditionalId") != null)
            {
                //int.TryParse(el.Attribute("conditionalId").Value, out this.ConditionalId);
                //if (!Enum.TryParse<Conditional>(el.Attribute("conditionalId").Value, out this.Conditional))
                if (!int.TryParse(el.Attribute("conditionalId").Value, out this.ConditionalId))
                    throw new ArgumentException(string.Format("element's 'conditionalId' attribute value of '{0}' could not be parsed as a valid conditional (Equal, NotEqual, etc.)", el.Attribute("conditionalId").Value));
            }
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'conditionalId' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("connectorId") != null)
            {
                //int.TryParse(el.Attribute("connectorId").Value, out this.ConnectorId);
                //if (!Enum.TryParse<Connector>(el.Attribute("connectorId").Value, out this.Connector))
                if (!int.TryParse(el.Attribute("connectorId").Value, out this.ConnectorId))
                    throw new ArgumentException(string.Format("element's 'connectorId' attribute value of '{0}' could not be parsed as a valid connector (And, Or)", el.Attribute("conditionalId").Value));
            }
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'connectorId' attribute, or the attribute is not a number.", el.ToString()));
        }

        public static List<Condition> FromElements(IEnumerable<System.Xml.Linq.XElement> els)
        {
            List<Condition> rv = new List<Condition>();
            rv.AddRange(els.Select(e => new Condition(e)));
            return rv;
        }

        public bool Evaluate(IDictionary<string, string> inputFields)
        {
            List<Field> fields = new List<Field>();
            IDictionary<string, string> results;
            string op1Value = "", op2Value;
            bool result;

            // if the left operand contains a field name, get the value directly
            if (inputFields.ContainsKey(Operand1))
                op1Value = inputFields[Operand1];
            else  //otherwise, it needs to be translated
                fields.Add(new Field() { Name = "_op1", Value = Operand1 });

            // right operand is always translated
            fields.Add(new Field() { Name = "_op2", Value = Operand2 });

            results = fields.TranslateFields(inputFields);

            // now set the translated results
            if (results.ContainsKey("_op1"))
                op1Value = results["_op1"];
            op2Value = results["_op2"];

            // perform the comparison
            switch (ConditionalId) // (Conditional)
            {
                case 1:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant() == op2Value.ToLowerInvariant());
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) == double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) == DateTime.Parse(op2Value));
                        else
                            result = (op1Value == op2Value);
                    }
                    break;
                //case Conditional.NotEquals:
                case 2:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant() != op2Value.ToLowerInvariant());
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) != double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) != DateTime.Parse(op2Value));
                        else
                            result = (op1Value != op2Value);
                    }
                    break;
                //case Conditional.IsBlank:
                case 3:
                    result = (op1Value.Trim().Length == 0);
                    break;
                //case Conditional.IsNotBlank:
                case 4:
                    result = (op1Value.Trim().Length != 0);
                    break;
                //case Conditional.StartsWith:
                case 5:
                    if (CompareMethod == CompareMethod.Text)
                        result = op1Value.ToLowerInvariant().StartsWith(op2Value.ToLowerInvariant());
                    else
                        result = op1Value.StartsWith(op2Value);
                    break;
                //case Conditional.EndsWith:
                case 6:
                    if (CompareMethod == CompareMethod.Text)
                        result = op1Value.ToLowerInvariant().EndsWith(op2Value.ToLowerInvariant());
                    else
                        result = op1Value.EndsWith(op2Value);
                    break;
                //case Conditional.GreaterThan:
                case 7:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) > 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) > double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) > DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) > 0);
                    }
                    break;
                //case Conditional.GreaterThanOrEqual:
                case 8:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) >= 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) >= double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) >= DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) >= 0);
                    }
                    break;
                //case Conditional.LessThan:
                case 9:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) < 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) < double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) < DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) < 0);
                    }
                    break;
                //case Conditional.LessThanOrEqual:
                case 10:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) <= 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) <= double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) <= DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) <= 0);
                    }
                    break;
                //case Conditional.Contains:
                case 11:
                    if (op1Value.Length == 0)
                        result = false;
                    else
                        result = op1Value.Contains(op2Value);
                    break;
                //case Conditional.InList:
                case 12:
                    if (op1Value.Length == 0)
                        result = false;
                    else
                        result = ("," + op2Value + ",").Contains("," + op1Value + ",");
                    break;
                //case Conditional.NotInList:
                case 13:
                    if (op1Value.Length == 0)
                        result = false;
                    else
                        result = !("," + op2Value + ",").Contains("," + op1Value + ",");
                    break;
                //case Conditional.Like:
                case 14:
                    result = (System.Text.RegularExpressions.Regex.Match(op1Value, op2Value).Success);
                    break;
                //case Conditional.NotLike:
                case 15:
                    result = !(System.Text.RegularExpressions.Regex.Match(op1Value, op2Value).Success);
                    break;
                default:
                    result = false;
                    break;
            }

            return result;
        }

        private bool IsNumeric(string value)
        {
            double temp;
            return double.TryParse(value, out temp);
        }

        private bool IsDate(string value)
        {
            DateTime temp;
            return DateTime.TryParse(value, out temp);
        }

    }
    public class Connector
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static readonly ReadOnlyCollection<Connector> Connectors = new ReadOnlyCollection<Connector>(new List<Connector>
            {
                new Connector() { Id = 1, Name = "And" },
                new Connector() { Id = 2, Name = "Or" }
            }
        );
    }

    public class Conditional
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static readonly ReadOnlyCollection<Conditional> Conditionals = new ReadOnlyCollection<Conditional>(new List<Conditional>
            {
                new Conditional() { Id = 1, Name = "Equals"},
                new Conditional() { Id = 2, Name = "NotEquals"},
                new Conditional() { Id = 3, Name = "IsBlank"},
                new Conditional() { Id = 4, Name = "IsNotBlank"},
                new Conditional() { Id = 5, Name = "StartsWith"},
                new Conditional() { Id = 6, Name = "EndsWith"},
                new Conditional() { Id = 7, Name = "GreaterThan"},
                new Conditional() { Id = 8, Name = "GreaterThanOrEqual"},
                new Conditional() { Id = 9, Name = "LessThan"},
                new Conditional() { Id = 10, Name = "LessThanOrEqual"},
                new Conditional() { Id = 11, Name = "Contains"},
                new Conditional() { Id = 12, Name = "InList"},
                new Conditional() { Id = 13, Name = "NotInList"},
                new Conditional() { Id = 14, Name = "Like"},
                new Conditional() { Id = 15, Name = "NotLike" }
            }
        );
    }

}
