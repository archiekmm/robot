﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices
{
    public interface ICrudServiceFor<T>
    {
        IEnumerable<T> List();
        int Create(T newItem);
        T Retrieve(int itemId);
        bool Update(T item);
        bool Delete(int itemId);
    }
}
