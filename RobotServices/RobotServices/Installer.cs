﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ServiceProcess;
//using System.Configuration.Install;

namespace Paragon.RobotServices
{
    [RunInstaller(true)]
    public class Installer : System.Configuration.Install.Installer
    {
        private readonly ServiceProcessInstaller _installProcess;
        private readonly CustomServiceInstaller _installService;

        public Installer()
        {
            _installProcess = new ServiceProcessInstaller();
            _installProcess.Account = ServiceAccount.NetworkService;

            _installService = new CustomServiceInstaller(typeof(ServiceImplementation));
            _installService.StartType = ServiceStartMode.Automatic;

            // remove built-in EventLogInstlaler;
            _installService.Installers.Clear();

            Installers.Add(_installProcess);
            Installers.Add(_installService);
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            if ((Context.Parameters.ContainsKey("username")))
                _installProcess.Account = ServiceAccount.User;

            string svcName = _installService.GetServiceName(Context.Parameters);
            _installService.ServiceArguments = ("-service " + svcName + " " + _installService.ServiceArguments).Trim();

            // run the install
            base.Install(stateSaver);
        }

        public override void Rollback(System.Collections.IDictionary savedState)
        {
            _installService.ServiceName = (string)savedState["ServiceName"];
            base.Rollback(savedState);
        }

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            if ((null == savedState) ||  (!savedState.Contains("ServiceName")))
                _installService.ServiceName = "testSvc";
            else
                _installService.ServiceName = (string)savedState["ServiceName"];
            base.Uninstall(savedState);
        }
    }
}
