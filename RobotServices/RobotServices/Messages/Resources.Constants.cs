// ReSharper disable InconsistentNaming
#pragma warning disable 1591 //disable missing xml comments

namespace Paragon.RobotServices.Messages
{
    public enum Facilities
    {
        General = 256,
    }
    
    public enum Categories
    {
        General = 1,
    }
    
    public enum HResults : long
    {
        SERVICESTARTED = 0x41000003L,
        SERVICESTARTFAILEDEXCEPTION = 0xC1000002L,
        SERVICESTOPPED = 0x41000004L,
    }
}
