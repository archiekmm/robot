// CATEGORIES
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: CATEGORY_GENERAL
//
// MessageText:
//
// General
//
#define CATEGORY_GENERAL                 ((long)0x00000001L)

// MESSAGES
//
// MessageId: SERVICESTARTFAILEDEXCEPTION
//
// MessageText:
//
// The service %7 failed to start.%n
// %n
// More Information: %2%n
// Excpetion Type: %3%n
// Base Type: %4%n
// Stack Trace: %5%n
// %6
//
#define SERVICESTARTFAILEDEXCEPTION      ((long)0xC1000002L)

//
// MessageId: SERVICESTARTED
//
// MessageText:
//
// The service %7 started successfully%n
// %n
// More Information: %2%n
// Excpetion Type: %3%n
// Base Type: %4%n
// Stack Trace: %5%n
// %6
//
#define SERVICESTARTED                   ((long)0x41000003L)

//
// MessageId: SERVICESTOPPED
//
// MessageText:
//
// The service %7 stopped successfully%n
// %n
// More Information: %2%n
// Excpetion Type: %3%n
// Base Type: %4%n
// Stack Trace: %5%n
// %6
//
#define SERVICESTOPPED                   ((long)0x41000004L)

