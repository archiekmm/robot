MessageIdTypedef=long
LanguageNames=(English=0x409:MSG00409)

SeverityNames=(
    Success=0x0
    Information=0x1
    Warning=0x2
    Error=0x3
)

FacilityNames=(
    FACILITY_GENERAL=0x100
)

;// CATEGORIES

MessageId       = 0x1
SymbolicName    = CATEGORY_GENERAL
Language        = English
General
.

;// MESSAGES

MessageId       = 0x2
Severity        = Error
Facility        = FACILITY_GENERAL
SymbolicName    = SERVICESTARTFAILEDEXCEPTION
Language        = English
The service %7 failed to start.%n
%n
More Information: %2%n
Excpetion Type: %3%n
Base Type: %4%n
Stack Trace: %5%n
%6
.

MessageId       = 0x3
Severity        = Information
Facility        = FACILITY_GENERAL
SymbolicName    = SERVICESTARTED
Language        = English
The service %7 started successfully%n
%n
More Information: %2%n
Excpetion Type: %3%n
Base Type: %4%n
Stack Trace: %5%n
%6
.

MessageId       = 0x4
Severity        = Information
Facility        = FACILITY_GENERAL
SymbolicName    = SERVICESTOPPED
Language        = English
The service %7 stopped successfully%n
%n
More Information: %2%n
Excpetion Type: %3%n
Base Type: %4%n
Stack Trace: %5%n
%6
.

