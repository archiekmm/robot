﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Imaging
{
    // NOTE: If you change the interface name "IImagingConnection" here, you must also update the reference to "IImagingConnection" in App.config.
    [ServiceContract]
    public interface IImagingService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> imagingIds);

        [OperationContract]
        int Create(Imaging ic);

        [OperationContract]
        Imaging Retrieve(int imagingId);

        [OperationContract]
        IEnumerable<Imaging> RetrieveMany(IEnumerable<int> imagingIds);

        [OperationContract]
        bool Update(Imaging ic);

        [OperationContract]
        bool Delete(int imagingId);

        [OperationContract]
        int StartSession(int imagingId);

        [OperationContract]
        bool EndSession(int sessionId);

        [OperationContract]
        IList<string> FindDocuments(int sessionId, IDictionary<string, string> indexes);
        
        [OperationContract]
        string CreateDocument(int sessionId, ImagingDocument doc);

        [OperationContract]
        ImagingDocument RetrieveDocument(int sessionId, string docId);

        [OperationContract]
        bool UpdateDocument(int sessionId, ImagingDocument doc, bool replacePages);

        [OperationContract]
        bool DeleteDocument(int sessionId, string docId);

        [OperationContract]
        bool ImportPage(int sessionId, string docId, string pageName, string mimeType, byte[] pageData);

        [OperationContract]
        byte[] ExportPage(int sessionId, string docId, string pageName); // pageId?

        [OperationContract]
        IList<string> FindFolders(int sessionId, IDictionary<string, string> indexes);

        [OperationContract]
        string CreateFolder(int sessionId, ImagingFolder folder);

        [OperationContract]
        ImagingFolder RetrieveFolder(int sessionId, string folderId);

        [OperationContract]
        bool UpdateFolder(int sessionId, ImagingFolder folder);

        [OperationContract]
        bool DeleteFolder(int sessionId, string folderId);

        [OperationContract]
        bool AddToFolder(int sessionId, string folderId, string docId);

        [OperationContract]
        bool RemoveFromFolder(int sessionId, string folderId, string docId);

        // Note:  no direct Page operations
    }
}
