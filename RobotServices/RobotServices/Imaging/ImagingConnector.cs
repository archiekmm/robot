﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Imaging
{
    public abstract class ImagingConnector : IImagingConnector, IDisposable
    {
        protected Imaging _config;
        protected IEnumerable<Settings.Setting> _settings;

        public ImagingConnector(Imaging config, IEnumerable<Settings.Setting> settings)
        {
            _config = config;
            _settings = settings;
        }

        public abstract IList<string> FindDocuments(IDictionary<string, string> searchIndexes);
        public abstract string CreateDocument(ImagingDocument doc);
        public abstract ImagingDocument RetrieveDocument(string docId);
        public abstract bool UpdateDocument(ImagingDocument doc, bool replacePages);
        public abstract bool DeleteDocument(string docId);
        public abstract bool ImportPage(string docId, string pageName, string mimeType, byte[] pageData);
        public abstract byte[] ExportPage(string docId, string pageName);

        public abstract IList<string> FindFolders(IDictionary<string, string> searchIndexes);
        public abstract string CreateFolder(ImagingFolder folder);
        public abstract ImagingFolder RetrieveFolder(string folderId);
        public abstract bool UpdateFolder(ImagingFolder folder);
        public abstract bool DeleteFolder(string folderId);
        public abstract bool AddToFolder(string folderId, string docId);
        public abstract bool RemoveFromFolder(string folderId, string docId);

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }
}
