﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Imaging
{
    public interface IImagingCommand
    {
        IImagingCommandResult Execute();
    }
}
