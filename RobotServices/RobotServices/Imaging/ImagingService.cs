﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Paragon.RobotServices.Utilities;

namespace Paragon.RobotServices.Imaging
{
    //public class ImagingSession
    //{
    //    public ImagingConnector Connector;
    //}

    // NOTE: If you change the class name "ImagingConnection" here, you must also update the reference to "ImagingConnection" in App.config.
    public class ImagingService : IImagingService
    {
        protected ImagingClassesDataContext _idb = new ImagingClassesDataContext();

        protected static Dictionary<int, ImagingConnector> _sessions = new Dictionary<int, ImagingConnector>();

        #region IImagingService Members

        #region CRUD members

        public IEnumerable<int> ListAll()
        {
            return _idb.ImagingConnections
                .Select(ic => ic.ImagingId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _idb.ImagingConnections
                .Select(ic => new KeyValuePair<int, string>(ic.ImagingId, ic.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _idb.ImagingConnections.Where(ic => ids.Contains(ic.ImagingId))
                .Select(ic => new KeyValuePair<int, string>(ic.ImagingId, ic.Name));
        }

        public int Create(Imaging ic)
        {
            ImagingConnectionDAO iConn = new ImagingConnectionDAO(ic);
            if (null == iConn)
                return 0;
            _idb.ImagingConnections.InsertOnSubmit(iConn);
            _idb.SubmitChanges();
            return iConn.ImagingId;
        }

        public Imaging Retrieve(int imagingId)
        {
            var iConn = _idb.ImagingConnections.Where(ic => ic.ImagingId == imagingId).FirstOrDefault();
            if (null == iConn)
                return null;
            return new Imaging(iConn);
        }

        public IEnumerable<Imaging> RetrieveMany(IEnumerable<int> ids)
        {
            return _idb.ImagingConnections.Where(ic => ids.Contains(ic.ImagingId))
                .Select(ic => new Imaging(ic));
        }

        public bool Update(Imaging ic)
        {
            var iConn = _idb.ImagingConnections.Where(ic2 => ic2.ImagingId == ic.ImagingId).FirstOrDefault();
            if (null == iConn)
                return false;
            iConn.ConnectionString = ic.ConnectionString;
            iConn.Name = ic.Name;
            iConn.Password = ic.Password;
            iConn.UserId = ic.UserId;
            _idb.SubmitChanges();
            return true;
        }

        public bool Delete(int imagingId)
        {
            var iConn = _idb.ImagingConnections.Where(ic => ic.ImagingId == imagingId).FirstOrDefault();
            if (null == iConn)
                return false;
            foreach (var item in _idb.ModuleImagings.Where(mi => mi.ImagingId == imagingId))
                _idb.ModuleImagings.DeleteOnSubmit(item);
            _idb.ImagingConnections.DeleteOnSubmit(iConn);
            _idb.SubmitChanges();
            return true;
        }

        #endregion

        public int StartSession(int imagingId)
        {
            var iConn = _idb.ImagingConnections.Where(ic => ic.ImagingId == imagingId).FirstOrDefault();
            if (null == iConn)
                return -1;
            var config = new Imaging(iConn);
            Settings.SettingService ss = new Paragon.RobotServices.Settings.SettingService();
            var settings = ss.RetrieveMany(config.SettingIds.Keys);
            ImagingConnector conn = null;
            int newId = -1;
            try
            {
                Type connType = iConn.ImagingKind.LoadAssemblyAndGetType();
                conn = (ImagingConnector)System.Activator.CreateInstance(connType, config, settings);
                if (_sessions.Count == 0)
                    newId = 1;
                else
                    newId = _sessions.Max(kvp => kvp.Key) + 1;
                _sessions.Add(newId, conn );
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception while creating imaging connector. " + Environment.NewLine
                    + "Type:  {0}, Id:  {1}, Name:  {2}, User: {3}, ConnString: {4}, " + Environment.NewLine
                    + "Message:  {5}", iConn.ImagingKind, iConn.ImagingId,
                    iConn.Name, iConn.UserId, iConn.ConnectionString, ex.ToString());
            }
            return newId;
        }

        public bool EndSession(int sessionId)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            _sessions.Remove(sessionId);
            sess.Dispose();
            sess = null;
            return true;
        }

        public IList<string> FindDocuments(int sessionId, IDictionary<string, string> indexes)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return null;
            return sess.FindDocuments(indexes);
        }

        public string CreateDocument(int sessionId, ImagingDocument doc) 
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return null;
            return sess.CreateDocument(doc);
        }

        public ImagingDocument RetrieveDocument(int sessionId, string docId) 
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return null;
            return sess.RetrieveDocument(docId);
        }

        public bool UpdateDocument(int sessionId, ImagingDocument doc, bool replacePages)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            return sess.UpdateDocument(doc, replacePages);
        }

        public bool DeleteDocument(int sessionId, string docId)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            return sess.DeleteDocument(docId);
        }

        public bool ImportPage(int sessionId, string docId, string pageName, string mimeType, byte[] pageData)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            return sess.ImportPage(docId, pageName, mimeType, pageData);
        }

        public byte[] ExportPage(int sessionId, string docId, string pageName) // pageId?
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return null;
            return sess.ExportPage(docId, pageName);
        }

        public IList<string> FindFolders(int sessionId, IDictionary<string, string> indexes)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return null;
            return sess.FindFolders(indexes);
        }

        public string CreateFolder(int sessionId, ImagingFolder folder)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return null;
            return sess.CreateFolder(folder);
        }

        public ImagingFolder RetrieveFolder(int sessionId, string folderId)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return null;
            return sess.RetrieveFolder(folderId);
        }

        public bool UpdateFolder(int sessionId, ImagingFolder folder)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            return sess.UpdateFolder(folder);
        }

        public bool DeleteFolder(int sessionId, string folderId)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            return sess.DeleteFolder(folderId);
        }

        public bool AddToFolder(int sessionId, string folderId, string docId)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            return sess.AddToFolder(folderId, docId);
        }

        public bool RemoveFromFolder(int sessionId, string folderId, string docId)
        {
            var sess = _sessions.Where(kvp => kvp.Key == sessionId).Select(kvp => kvp.Value).FirstOrDefault();
            if (null == sess)
                return false;
            return sess.RemoveFromFolder(folderId, docId);
        }

        #endregion
    }
}
