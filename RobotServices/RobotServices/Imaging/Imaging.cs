﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Imaging
{
    [DataContract]
    public class Imaging
    {
        [DataMember]
        public string ConnectionString;

        [DataMember]
        public int ImagingId;

        [DataMember]
        public string Name;

        [DataMember]
        public string Password;

        [DataMember]
        public string UserId;

        [DataMember]
        public string ImagingKind;

        [DataMember]
        public Dictionary<int, string> SettingIds = new Dictionary<int, string>();
        //public List<int> SettingIds = new List<int>();

        public Imaging(ImagingConnectionDAO ic) 
        {
            this.ConnectionString = ic.ConnectionString;
            this.ImagingId = ic.ImagingId;
            this.Name = ic.Name;
            this.Password = ic.Password;
            this.UserId = ic.UserId;
            this.ImagingKind = ic.ImagingKind;
            foreach (var setting in ic.ImagingSettings)
                this.SettingIds.Add(setting.SettingId, setting.Setting.Name);
        }

        public Imaging() { }
    }
}
