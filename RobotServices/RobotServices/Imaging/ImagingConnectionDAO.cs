﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Imaging
{
    public partial class ImagingConnectionDAO
    {
        // protected Unisys cal = new Cal();

        public ImagingConnectionDAO(Imaging ic)
            : this()
        {
            this.ConnectionString = ic.ConnectionString;
            this.Name = ic.Name;
            this.Password = ic.Password;
            this.UserId = ic.UserId;
            this.ImagingKind = ic.ImagingKind;
            foreach (var settingId in ic.SettingIds)
                this.ImagingSettings.Add(new ImagingSettingDAO() { SettingId = settingId.Key, ImagingConnections = this });
        }


    }
}
