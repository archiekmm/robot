﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public interface IImagingConnector
    {
        IList<string> FindDocuments(IDictionary<string, string> searchIndexes);
        string CreateDocument(Document doc);
        Document RetrieveDocument(string docId);
        bool UpdateDocument(Document doc, bool replacePages);
        bool DeleteDocument(string docId);
        bool ImportPage(string docId, string pageName, string mimeType, byte[] pageData);
        byte[] ExportPage(string docId, string pageName);

        IList<string> FindFolders(IDictionary<string, string> searchIndexes);
        string CreateFolder(Folder folder);
        Folder RetrieveFolder(string folderId);
        bool UpdateFolder(Folder folder);
        bool DeleteFolder(string folderId);
        bool AddToFolder(string folderId, string docId);
        bool RemoveFromFolder(string folderId, string docId);

        IWorkitem AsWorkitem(Document doc);
        Document AsDocument(IWorkitem item);
    }
}
