﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Paragon.RobotServices.Utilities
{
    public class SynchronizedBool
    {
        protected ReaderWriterLockSlim boolLock = new ReaderWriterLockSlim();
        protected bool value;

        public bool Value
        {
            get
            {
                boolLock.EnterReadLock();
                try
                {
                    return this.value;
                }
                finally
                {
                    boolLock.ExitReadLock();
                }
            }
            set
            {
                boolLock.EnterWriteLock();
                try
                {
                    this.value = value;
                }
                finally
                {
                    boolLock.ExitWriteLock();
                }
            }
        }

        public SynchronizedBool(bool value)
        {
            this.value = value;
        }

        public static implicit operator bool(SynchronizedBool sb)
        {
            return sb.Value;
        }
    }

}
