﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Utilities
{
    public static class TypeLoader
    {
        public static Type LoadAssemblyAndGetType(this string typeName)
        {
            Type type = Type.GetType(typeName);
            if (null == type)
            {
                string asmName = string.Join(",", typeName.Split(",".ToCharArray()).Skip(1).ToArray()).Trim();
                System.Reflection.Assembly.Load(asmName);
                type= Type.GetType(typeName);
            }
            return type;
        }

        public static T CreateInstance<T>(this Type type, object[] constructorArgs)
        {
            return (T)System.Activator.CreateInstance(type, constructorArgs);
        }
    }
}
