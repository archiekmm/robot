﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Normalizers
{
    // NOTE: If you change the interface name "INormalizerService" here, you must also update the reference to "INormalizerService" in App.config.
    [ServiceContract]
    public interface INormalizerService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> normalizerIds);

        [OperationContract]
        int Create(Normalizer nc);

        [OperationContract]
        Normalizer Retrieve(int normalizerId);

        [OperationContract]
        IEnumerable<Normalizer> RetrieveMany(IEnumerable<int> normalizerIds);

        [OperationContract]
        bool Update(Normalizer nc);

        [OperationContract]
        bool Delete(int normalizerId);

        [OperationContract]
        byte[] NormalizeStream(int normalizerId, byte[] stream);
    }
}
