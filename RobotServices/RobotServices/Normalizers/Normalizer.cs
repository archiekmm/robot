﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Normalizers
{
    [DataContract]
    public class Normalizer
    {
        [DataMember]
        public System.Windows.Media.Imaging.TiffCompressOption TiffCompression;

        [DataMember]
        public int BitsPerPixel;
        
        [DataMember]
        public bool MatchXAndY;

        [DataMember]
        public string Name;

        [DataMember]
        public int NormalizerId;

        [DataMember]
        public int Rotation;

        [DataMember]
        public bool RoundResolution;

        [DataMember]
        public List<int> ModuleIds = new List<int>();

        public Normalizer() { }

        public Normalizer(NormalizerDAO n)
        {
            this.BitsPerPixel = n.BitsPerPixel;
            this.MatchXAndY = n.MatchXAndY;
            this.Name = n.Name;
            this.NormalizerId = n.NormalizerId;
            this.Rotation = n.Rotation;
            this.RoundResolution = n.RoundResolution;
            this.TiffCompression = n.TiffCompression;
            foreach (var x in n.ModuleNormalizerDAOs)
                this.ModuleIds.Add(x.ModuleId);
        }
    }
}
