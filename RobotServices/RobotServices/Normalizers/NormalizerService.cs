﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Normalizers
{
    // NOTE: If you change the class name "NormalizerService" here, you must also update the reference to "NormalizerService" in App.config.
    public class NormalizerService : INormalizerService
    {
        #region INormalizerService Members

        protected NormalizerClassesDataContext _ncdc = new NormalizerClassesDataContext();

        public IEnumerable<int> ListAll()
        {
            return _ncdc.Normalizers
                .Select(n => n.NormalizerId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _ncdc.Normalizers
                .Select(n => new KeyValuePair<int, string>(n.NormalizerId, n.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _ncdc.Normalizers.Where(n => ids.Contains(n.NormalizerId))
                .Select(n => new KeyValuePair<int, string>(n.NormalizerId, n.Name));
        }

        public int Create(Normalizer nc)
        {
            NormalizerDAO n = new NormalizerDAO(nc);
            if (null == n)
                return 0;
            _ncdc.Normalizers.InsertOnSubmit(n);
            _ncdc.SubmitChanges();
            return n.NormalizerId;
        }

        public Normalizer Retrieve(int normalizerId)
        {
            return _ncdc.Normalizers.Where(n => n.NormalizerId == normalizerId)
                .Select(n => new Normalizer(n)).FirstOrDefault();
        }

        public IEnumerable<Normalizer> RetrieveMany(IEnumerable<int> ids)
        {
            return _ncdc.Normalizers.Where(n => ids.Contains(n.NormalizerId))
                .Select(n => new Normalizer(n));
        }

        public bool Update(Normalizer nc)
        {
            NormalizerDAO n = _ncdc.Normalizers.Where(nn => nn.NormalizerId == nc.NormalizerId).FirstOrDefault();
            if (null == n)
                return false;
            
            n.BitsPerPixel = nc.BitsPerPixel;
            n.MatchXAndY = nc.MatchXAndY;
            n.Name = nc.Name;
            n.Rotation = nc.Rotation;
            n.RoundResolution = nc.RoundResolution;
            n.TiffCompression = nc.TiffCompression;

            // add new modules
            foreach (int mId in nc.ModuleIds)
                if (!(n.ModuleNormalizerDAOs.Any(mn => mn.ModuleId == mId)))
                    _ncdc.ModuleNormalizers.InsertOnSubmit(
                        new ModuleNormalizerDAO() { ModuleId = mId, NormalizerDAO = n });
            // remove old modules
            foreach (var mn in n.ModuleNormalizerDAOs)
                if (!(nc.ModuleIds.Contains(mn.ModuleId)))
                    _ncdc.ModuleNormalizers.DeleteOnSubmit(mn);

            _ncdc.SubmitChanges();
            return true;
        }

        public bool Delete(int normalizerId)
        {
            NormalizerDAO n = _ncdc.Normalizers.Where(nn => nn.NormalizerId == normalizerId).FirstOrDefault();
            if (null == n)
                return false;
            foreach (var nm in n.ModuleNormalizerDAOs)
                _ncdc.ModuleNormalizers.DeleteOnSubmit(nm);
            _ncdc.Normalizers.DeleteOnSubmit(n);
            _ncdc.SubmitChanges();
            return true;
        }

        public byte[] NormalizeStream(int normalizerId, byte[] s)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
