﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Normalizers
{
    public partial class NormalizerDAO
    {
        public NormalizerDAO(Normalizer nc)
            : this()
        {
            this.BitsPerPixel = nc.BitsPerPixel;
            this.MatchXAndY = nc.MatchXAndY;
            this.Name = nc.Name;
            this.Rotation = nc.Rotation;
            this.RoundResolution = nc.RoundResolution;
            this.TiffCompression = nc.TiffCompression;
        }


    }
}
