﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceProcess;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Security.AccessControl;
using System.ComponentModel;

namespace Paragon.RobotServices
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ServiceAttribute : Attribute
    {
        public readonly string ServiceName;
        public ServiceStartMode StartMode = ServiceStartMode.Manual;
        public int AutoRestartAttempts = 0;
        public int AutoRestartDelayMilliseconds = 1000;
        public int ResetFailureDelaySeconds = 86400;

        public ServiceAttribute() : this(null) { }
        public ServiceAttribute(string serviceName)
        {
            ServiceName = serviceName;
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class ServiceAccessAttribute : Attribute
    {
        public readonly WellKnownSidType Sid;
        public readonly AceQualifier Qualifier;
        public readonly ServiceAccessRights AccessMask;

        public ServiceAccessAttribute(WellKnownSidType sid, AceQualifier qualifier, ServiceAccessRights accessMask)
        {
            Sid = sid;
            Qualifier = qualifier;
            AccessMask = accessMask;
        }

    }

    [DisplayName("My Service Name")]
    [Description("This is a description")]
    [Service("testSvc", StartMode=ServiceStartMode.Automatic, AutoRestartAttempts=2)]
    [ServiceAccess(WellKnownSidType.BuiltinAdministratorsSid, AceQualifier.AccessAllowed, ServiceAccessRights.SERVICE_ALL_ACCESS)]
    [ServiceAccess(WellKnownSidType.BuiltinUsersSid, AceQualifier.AccessAllowed, ServiceAccessRights.GENERIC_READ | ServiceAccessRights.GENERIC_EXECUTE)]
    class ServiceImplementation : IDisposable
    {
        private List<ServiceHost> services = null;

        public void Start(ICollection<string> args)
        {
            services = new List<ServiceHost>();
            this.LoadServices(services);
            this.StartServices(services);
        }

        public void Stop()
        {
            this.StopServices(services);
            foreach (var s in services)
                s.Close();
            services = null;
        }

        public void Dispose()
        {
        }

        public IEnumerable<Type> GetServices()
        {
            Type t = typeof(Paragon.RobotServices.Robots.RobotService);
            List<Type> services = new List<Type>();
            System.Reflection.Assembly assem = t.Assembly;
            foreach (Type t2 in assem.GetTypes())
            {
                if ((t2.Name.EndsWith("Service")) &&
                        t2.GetInterfaces().Any(
                            i => (i.Namespace == t2.Namespace && i.Name == "I" + t2.Name)))
                    services.Add(t2);
            }
            System.Diagnostics.EventLog.WriteEntry("ServiceTemplate", "GetServices:  there were " + services.Count + " services found.");
            return services;
        }

        public void LoadServices(IList<ServiceHost> list)
        {
            System.UriBuilder b = new UriBuilder()
            {
                Scheme = "net.tcp",
                Host = "localhost",
                Port = Paragon.RobotServices.Properties.Settings.Default.Port
            };
            ServiceHost sh = null;
            foreach (Type t in GetServices())
            {
                //b.Path = @"/" + t.Name;
                b.Path = @"/" + t.FullName.Replace(".", "/");
                sh = new ServiceHost(t, b.Uri);
                list.Add(sh);
            }
            System.Diagnostics.EventLog.WriteEntry("ServiceTemplate", "LoadServices:  there were " + list.Count + " services loaded.");
        }

        public void StartServices(IList<ServiceHost> list)
        {
            string sSource = "ServiceTemplate";
            string sLog = "ServiceTemplate";
            if (!System.Diagnostics.EventLog.SourceExists(sSource))
                System.Diagnostics.EventLog.CreateEventSource(sSource, sLog);
            System.Diagnostics.EventLog.WriteEntry(sSource, "Working Directory is " + Environment.CurrentDirectory);
            System.Diagnostics.EventLog.WriteEntry(sSource, "Assembly Directory is " + this.GetType().Assembly.Location);
            foreach (var sh in list)
            {
                foreach (var addr in sh.BaseAddresses)
                    System.Diagnostics.EventLog.WriteEntry(sSource, string.Format("{0} is hosted at {1}", sh.Description, addr.ToString()));
                    //Console.WriteLine("{0} is hosted at {1}", sh.Description, addr.ToString());
                sh.Open();
            }
        }

        public void StopServices(IList<ServiceHost> list)
        {
            foreach (var sh in list)
                sh.Close();
        }


    }
}
