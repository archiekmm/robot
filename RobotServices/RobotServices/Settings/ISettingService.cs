﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Settings
{
    // NOTE: If you change the interface name "ISettingService" here, you must also update the reference to "ISettingService" in App.config.
    [ServiceContract]
    public interface ISettingService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> settingIds);

        [OperationContract]
        int Create(Setting sc);

        [OperationContract]
        Setting Retrieve(int settingId);

        [OperationContract]
        IEnumerable<Setting> RetrieveMany(IEnumerable<int> settingIds);

        [OperationContract]
        bool Update(Setting sc);

        [OperationContract]
        bool Delete(int settingId);

        // other service members, e.g., CheckSetting
    }
}
