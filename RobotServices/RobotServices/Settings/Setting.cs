﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Settings
{
    [DataContract]
    public class Setting
    {
        [DataMember]
        public int SettingId;

        [DataMember]
        public string Name;

        [DataMember]
        public string Value;

        [DataMember]
        public string Type;

        [DataMember]
        public string ValueList;

        [DataMember]
        public string EnableIf;

        [DataMember]
        public string DependsOn;

        public Setting(SettingDAO s)
        {
            this.SettingId = s.SettingId;
            this.DependsOn = s.DependsOn;
            this.EnableIf = s.EnableIf;
            this.Name = s.Name;
            this.SettingId = s.SettingId;
            this.Type = s.Type;
            this.Value = s.Value;
            this.ValueList = s.ValueList;
        }

        public Setting() { }
    }
}
