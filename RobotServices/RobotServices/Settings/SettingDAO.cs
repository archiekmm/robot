﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Settings
{
    public partial class SettingDAO
    {
        public SettingDAO(Setting sc)
            : this()
        {
            this.DependsOn = sc.DependsOn;
            this.EnableIf = sc.EnableIf;
            this.Name = sc.Name;
            this.Type = sc.Type;
            this.Value = sc.Value;
            this.ValueList = sc.ValueList;
        }
    }
}
