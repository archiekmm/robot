﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Settings
{
    // NOTE: If you change the class name "SettingService" here, you must also update the reference to "SettingService" in App.config.
    public class SettingService : ISettingService
    {
        protected SettingsClassesDataContext _scdc = new SettingsClassesDataContext();

        public IEnumerable<int> ListAll()
        {
            return _scdc.SettingDAOs.Select(dao => dao.SettingId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _scdc.SettingDAOs
                .Select(dao => new KeyValuePair<int, string>(dao.SettingId, dao.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _scdc.SettingDAOs.Where(s => ids.Contains(s.SettingId))
                .Select(dao => new KeyValuePair<int, string>(dao.SettingId, dao.Name));
        }

        public int Create(Setting sc)
        {
            SettingDAO dao = new SettingDAO(sc);
            if (null == dao)
                return 0;
            _scdc.SettingDAOs.InsertOnSubmit(dao);
            _scdc.SubmitChanges();
            return dao.SettingId;
        }

        public Setting Retrieve(int settingId)
        {
            return _scdc.SettingDAOs.Where(s => s.SettingId == settingId)
                .Select(s => new Setting(s)).FirstOrDefault();
        }

        public IEnumerable<Setting> RetrieveMany(IEnumerable<int> settingIds)
        {
            return _scdc.SettingDAOs.Where(s => settingIds.Contains(s.SettingId))
                .Select(s => new Setting(s));
        }

        public bool Update(Setting sc)
        {
            SettingDAO dao = _scdc.SettingDAOs.Where(s => s.SettingId == sc.SettingId).FirstOrDefault();
            if (null == dao)
                return false;
            dao.DependsOn = sc.DependsOn;
            dao.EnableIf = sc.EnableIf;
            dao.Name = sc.Name;
            dao.Type = sc.Type;
            dao.Value = sc.Value;
            dao.ValueList = sc.ValueList;
            _scdc.SubmitChanges();
            return true;
        }

        public bool Delete(int settingId)
        {
            SettingDAO dao = _scdc.SettingDAOs.Where(s => s.SettingId == settingId).FirstOrDefault();
            if (null == dao)
                return false;
            foreach (var mod in dao.ModuleSettingDAOs)
                _scdc.ModuleSettingDAOs.DeleteOnSubmit(mod);
            _scdc.SettingDAOs.DeleteOnSubmit(dao);
            _scdc.SubmitChanges();
            return true;
        }

        // check setting, etc.
    }
}
