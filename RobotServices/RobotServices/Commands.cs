﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceProcess;

namespace Paragon.RobotServices
{
    static class Commands
    {
        public static void Help(ICollection<string> args)
        {
        }

        public static void Run(ICollection<string> args)
        {
            using (ServiceImplementation svc = new ServiceImplementation())
            {
                svc.Start(args);
                Console.WriteLine("Press [Enter] to exit...");
                Console.ReadLine();
                svc.Stop();
            }
        }

        public static void RunAsService(ICollection<string> args)
        {
            ServiceBase.Run(new RobotWindowsService(args));
        }

        public static void Install(ICollection<string> args)
        {
            new InstallUtil(typeof(Program.Program).Assembly).Install(args);
        }

        public static void Uninstall(ICollection<string> args)
        {
            new InstallUtil(typeof(Program.Program).Assembly).Uninstall(args);
        }

    }
}
