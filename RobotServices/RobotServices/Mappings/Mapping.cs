﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Mappings
{
    [DataContract]
    public class Mapping
    {
        [DataMember]
        public int MappingId;

        [DataMember]
        public string Name;

        [DataMember]
        public List<int> ConditionIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> FieldIds = new Dictionary<int, string>();
        //public List<int> FieldIds = new List<int>();

        public Mapping(MappingDAO m)
        {
            this.MappingId = m.MappingId;
            this.Name = m.Name;
            foreach (var c in m.MappingsToConditions)
                ConditionIds.Add(c.ConditionId);
            foreach (var f in m.MappingsToFields)
                FieldIds.Add(f.FieldId, f.Field.Name);
        }

        public Mapping() { }

        public IDictionary<string, string> TranslateFields(IDictionary<string, string> inputs)
        {
            Conditions.ConditionService cs = new Conditions.ConditionService();
            Fields.FieldService fs = new Fields.FieldService();
            if (cs.Evaluate(this.ConditionIds, inputs))
            {
                IDictionary<string, string> rv = fs.TranslateFields(this.FieldIds.Keys, inputs);
                rv["_MapClassName"] = this.Name;
                return rv;
            }
            return inputs;
        }
    }
}
