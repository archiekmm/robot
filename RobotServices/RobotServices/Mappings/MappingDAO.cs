﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Mappings
{
    public partial class MappingDAO
    {
        public MappingDAO(Mapping mc)
            : this()
        {
            // never read the id from a user-given config
            // select, update, and delete look it up, create lets the database set it
            this.Name = mc.Name;
            if (mc.ConditionIds != null)
                foreach (int cId in mc.ConditionIds)
                    this.MappingsToConditions.Add(new MappingsToConditionDAO() { ConditionId = cId, Mapping = this });
            if (mc.FieldIds != null)
                foreach (var fId in mc.FieldIds)
                    this.MappingsToFields.Add(new MappingsToFieldDAO() { FieldId = fId.Key, Mapping = this });
        }

    }
}
