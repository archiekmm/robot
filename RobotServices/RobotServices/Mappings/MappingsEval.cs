﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Mappings
{
    public static class MappingsEval
    {
        public static IDictionary<string, string> TranslateFields(this IEnumerable<Mapping> mappings, IDictionary<string, string> inputs)
        {
            foreach (var mapping in mappings)
            {
                IDictionary<string, string> rv = mapping.TranslateFields(inputs);
                if (rv.ContainsKey("_MapClassName"))
                    return rv;
            }
            return inputs;
        }
    }
}
