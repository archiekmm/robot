﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Mappings
{
    public class MappingService : IMappingService
    {
        protected MappingClassesDataContext _mdb = new MappingClassesDataContext();

        #region CRUD operations

        public IEnumerable<int> ListAll()
        {
            return _mdb.Mappings
                .Select(m => m.MappingId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _mdb.Mappings
                .Select(m => new KeyValuePair<int, string>(m.MappingId, m.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _mdb.Mappings.Where(m => ids.Contains(m.MappingId))
                .Select(m => new KeyValuePair<int, string>(m.MappingId, m.Name));
        }

        public int Create(Mapping mc)
        {
            // conditions and fields are added by id during construction
            MappingDAO m = new MappingDAO(mc);
            _mdb.Mappings.InsertOnSubmit(m);
            _mdb.SubmitChanges();
            return m.MappingId;
        }

        public Mapping Retrieve(int mappingId)
        {
            return _mdb.Mappings.Where(m => m.MappingId == mappingId)
                .Select(m => new Mapping(m)).FirstOrDefault();
        }

        public IEnumerable<Mapping> RetrieveMany(IEnumerable<int> mappingIds)
        {
            return _mdb.Mappings.Where(m => mappingIds.Contains(m.MappingId))
                .Select(m => new Mapping(m));
        }
        
        public bool Update(Mapping mc)
        {
            MappingDAO m = _mdb.Mappings.Where(mm => mm.MappingId == mc.MappingId).FirstOrDefault();
            if (null == m)
                return false;
            m.Name = mc.Name;
            // Add conditions that don't already exist
            foreach (int condId in mc.ConditionIds)
                if (!(m.MappingsToConditions.Any(m2c => m2c.ConditionId == condId)))
                    _mdb.MappingsToConditions.InsertOnSubmit(new MappingsToConditionDAO() { ConditionId = condId, Mapping = m });
                    //m.MappingsToConditions.Add(new MappingsToCondition() { ConditionId = condId, Mapping = m });
            // Remove conditions that no longer exist
            foreach (var m2c in m.MappingsToConditions)
                if (!(mc.ConditionIds.Contains(m2c.ConditionId)))
                    _mdb.MappingsToConditions.DeleteOnSubmit(m2c);
                    //m.MappingsToConditions.Remove(m2c);
            // Add fields that don't already exist
            foreach (var fieldId in mc.FieldIds)
                if (!(m.MappingsToFields.Any(m2f => m2f.FieldId == fieldId.Key)))
                    _mdb.MappingsToFields.InsertOnSubmit(new MappingsToFieldDAO() { FieldId = fieldId.Key, Mapping = m });
                    //m.MappingsToFields.Add(new MappingsToField() { FieldId = fieldId, Mapping = m });
            // Remove fields that no longer exist
            foreach (var m2f in m.MappingsToFields)
                if (!(mc.ConditionIds.Contains(m2f.FieldId)))
                    _mdb.MappingsToFields.DeleteOnSubmit(m2f);
                    //m.MappingsToFields.Remove(m2f);
            _mdb.SubmitChanges();
            return false;
        }

        public bool Delete(int mappingId)
        {
            var m = _mdb.Mappings.Where(mm => mm.MappingId == mappingId).FirstOrDefault();
            if (null == m)
                return false;
            foreach (var m2c in m.MappingsToConditions)
                _mdb.MappingsToConditions.DeleteOnSubmit(m2c);
                //m.MappingsToConditions.Remove(m2c);
            foreach (var m2f in m.MappingsToFields)
                _mdb.MappingsToFields.DeleteOnSubmit(m2f);
                //m.MappingsToFields.Remove(m2f);
            _mdb.Mappings.DeleteOnSubmit(m);
            _mdb.SubmitChanges();
            return true;
        }

        #endregion

        public IDictionary<string, string> TranslateFields(int mappingId, IDictionary<string, string> inputs)
        {
            Mapping m = _mdb.Mappings.Where(mm => mm.MappingId == mappingId)
                .Select(mm => new Mapping(mm)).FirstOrDefault();
            if (null == m)
                return inputs;
            return m.TranslateFields(inputs);
        }

        public IDictionary<string, string> TranslateMany(IEnumerable<int> mappingIds, IDictionary<string, string> inputs)
        {
            return RetrieveMany(mappingIds).TranslateFields(inputs);
        }

    }
}
