﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Mappings
{
    [ServiceContract]
    public interface IMappingService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> mappingIds);

        [OperationContract]
        int Create(Mapping mc);

        [OperationContract]
        Mapping Retrieve(int mappingId);

        [OperationContract]
        IEnumerable<Mapping> RetrieveMany(IEnumerable<int> mappingIds);

        [OperationContract]
        bool Update(Mapping mc);

        [OperationContract]
        bool Delete(int mappingId);

        [OperationContract]
        IDictionary<string, string> TranslateFields(int mappingId, IDictionary<string, string> inputs);

        [OperationContract]
        IDictionary<string, string> TranslateMany(IEnumerable<int> mappingIds, IDictionary<string, string> inputs);

    }
}
