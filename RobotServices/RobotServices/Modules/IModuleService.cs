﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Modules
{
    // NOTE: If you change the interface name "IModuleService" here, you must also update the reference to "IModuleService" in App.config.
    [ServiceContract]
    public interface IModuleService //: ICrudServiceFor<Module>
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> mappingIds);

        // leaving here for the moment for copy/paste convenience.  move below Retrieve
        [OperationContract]
        IEnumerable<Module> RetrieveMany(IEnumerable<int> mappingIds);

        [OperationContract]
        int Create(Module mc);

        [OperationContract]
        Module Retrieve(int moduleId);

        [OperationContract]
        bool Update(Module mc);

        [OperationContract]
        bool Delete(int moduleId);

    }
}
