﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Modules
{
    [DataContract]
    public class Module
    {
        [DataMember]
        public int ModuleId;

        [DataMember]
        public string Name;

        [DataMember]
        public string ModuleKind;

        [DataMember]
        public string InputModule;

        [DataMember]
        public string OutputModule;

        [DataMember]
        public Dictionary<int, string> CaptureIds = new Dictionary<int, string>();
        //public List<int> CaptureIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> ImagingIds = new Dictionary<int, string>();
        //public List<int> ImagingIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> LookupIds = new Dictionary<int, string>();
        //public List<int> LookupIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> MappingIds = new Dictionary<int, string>();
        //public List<int> MappingIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> NormalizerIds = new Dictionary<int, string>();
        //public List<int> NormalizerIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> SettingIds = new Dictionary<int, string>();
        //public List<int> SettingIds = new List<int>();

        [DataMember]
        public List<int> LogIds = new List<int>();

        public Module() { }

        public Module(ModuleDAO m)
        {
            this.ModuleId = m.ModuleId;
            this.Name = m.Name;
            this.ModuleKind = m.ModuleKind;
            this.InputModule = m.InputModule;
            this.OutputModule = m.OutputModule;
            foreach (var mc in m.ModuleCaptures)
                CaptureIds.Add(mc.CaptureId, mc.CaptureConnection.Name);
            foreach (var mi in m.ModuleImagings)
                ImagingIds.Add(mi.ImagingId, mi.ImagingConnection.Name);
            foreach (var ml in m.ModuleLookups)
                LookupIds.Add(ml.LookupId, ml.Lookup.Name);
            foreach (var mm in m.ModuleMappings)
                MappingIds.Add(mm.MappingsId, mm.Mapping.Name);
            foreach (var mn in m.ModuleNormalizers)
                NormalizerIds.Add(mn.NormalizerId, mn.Normalizer.Name);
            foreach (var ms in m.ModuleSettings)
                SettingIds.Add(ms.SettingId, ms.Setting.Name);
            foreach (var ml in m.ModuleLogs)
                LogIds.Add(ml.LogId);
        }
    }
}
