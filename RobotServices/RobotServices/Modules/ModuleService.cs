﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Modules
{
    // NOTE: If you change the class name "ModuleService" here, you must also update the reference to "ModuleService" in App.config.
    public class ModuleService : IModuleService
    {
        protected ModuleClassesDataContext _mcdb = new ModuleClassesDataContext();

        #region IModuleService Members

        public IEnumerable<int> ListAll()
        {
            return _mcdb.Modules
                .Select(mod => mod.ModuleId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _mcdb.Modules
                .Select(mod => new KeyValuePair<int, string>(mod.ModuleId, mod.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _mcdb.Modules.Where(mod => ids.Contains(mod.ModuleId))
                .Select(mod => new KeyValuePair<int, string>(mod.ModuleId, mod.Name));
        }

        public int Create(Module mc)
        {
            ModuleDAO m = new ModuleDAO(mc);
            foreach(var captureId in mc.CaptureIds)
                _mcdb.ModuleCaptures.InsertOnSubmit(new ModuleCaptureDAO() { CaptureId = captureId.Key, Module = m });
                //m.ModuleCaptures.Add(new ModuleCapture() { CaptureId = captureId, Module = m});
            foreach(var imagingId in mc.ImagingIds)
                _mcdb.ModuleImagings.InsertOnSubmit(new ModuleImagingDAO() { ImagingId = imagingId.Key, Module = m });
                //m.ModuleImagings.Add(new ModuleImaging() { ImagingId = imagingId, Module = m });
            foreach(var lookupId in mc.LookupIds)
                _mcdb.ModuleLookups.InsertOnSubmit(new ModuleLookupDAO() { LookupId = lookupId.Key, Module = m });
                //m.ModuleLookups.Add(new ModuleLookup() { LookupId = lookupId, Module = m});
            foreach(var mappingId in mc.MappingIds)
                _mcdb.ModuleMappings.InsertOnSubmit(new ModuleMappingDAO() { MappingsId = mappingId.Key, Module = m });
                //m.ModuleMappings.Add(new ModuleMapping() { MappingsId = mappingId, Module = m});
            foreach(var normId in mc.NormalizerIds)
                _mcdb.ModuleNormalizers.InsertOnSubmit(new ModuleNormalizerDAO() { NormalizerId = normId.Key, Module = m });
                //m.ModuleNormalizers.Add(new ModuleNormalizer() { NormalizerId = normId, Module = m});
            foreach (var fieldId in mc.SettingIds)
                _mcdb.ModuleSettings.InsertOnSubmit(new ModuleSettingDAO() { SettingId = fieldId.Key, Module = m });
                //m.ModuleSettings.Add(new ModuleSetting() { FieldId = fieldId, Module = m });

            _mcdb.Modules.InsertOnSubmit(m);
            _mcdb.SubmitChanges();
            return m.ModuleId;
        }

        public Module Retrieve(int moduleId)
        {
            return _mcdb.Modules.Where(mm => mm.ModuleId == moduleId)
                .Select(mm => new Module(mm)).FirstOrDefault();
        }

        public IEnumerable<Module> RetrieveMany(IEnumerable<int> moduleIds)
        {
            return _mcdb.Modules.Where(mm => moduleIds.Contains(mm.ModuleId))
                .Select(mm => new Module(mm));
        }

        public bool Update(Module mc)
        {
            var m = _mcdb.Modules.Where(mm => mm.ModuleId == mc.ModuleId).FirstOrDefault();
            if (null == m)
                return false;
            m.InputModule = mc.InputModule;
            m.ModuleKind = mc.ModuleKind;
            m.Name = mc.Name;
            m.OutputModule = mc.OutputModule;
            
            // add new captures
            foreach (var captureId in mc.CaptureIds)
                if (!(m.ModuleCaptures.Any(m2c => m2c.CaptureId == captureId.Key)))
                    _mcdb.ModuleCaptures.InsertOnSubmit(new ModuleCaptureDAO() { CaptureId = captureId.Key, Module = m });
                    //m.ModuleCaptures.Add(new ModuleCapture() { CaptureId = captureId, Module = m });
            // remove old captures
            foreach (var m2c in m.ModuleCaptures)
                if (!(mc.CaptureIds.Keys.Contains(m2c.CaptureId)))
                    _mcdb.ModuleCaptures.DeleteOnSubmit(m2c);
                    //m.ModuleCaptures.Remove(m2c);

            // add new imagings
            foreach (var imagingId in mc.ImagingIds)
                if (!(m.ModuleImagings.Any(m2i => m2i.ImagingId == imagingId.Key)))
                    _mcdb.ModuleImagings.InsertOnSubmit(new ModuleImagingDAO() { ImagingId = imagingId.Key, Module = m });
                    //m.ModuleImagings.Add(new ModuleImaging() { ImagingId = imagingId, Module = m});
            // remove old imagings
            foreach (var m2i in m.ModuleImagings)
                if (!(mc.ImagingIds.Keys.Contains(m2i.ImagingId)))
                    _mcdb.ModuleImagings.DeleteOnSubmit(m2i);
                    //m.ModuleImagings.Remove(m2i);

            // add new lookups
            foreach (var lookupId in mc.LookupIds)
                if (!(m.ModuleLookups.Any(m2l => m2l.LookupId == lookupId.Key)))
                    _mcdb.ModuleLookups.InsertOnSubmit(new ModuleLookupDAO() { LookupId = lookupId.Key, Module = m });
                    //m.ModuleLookups.Add(new ModuleLookup() { LookupId = lookupId, Module = m});
            // remove old lookups
            foreach (var m2l in m.ModuleLookups)
                if (!(mc.LookupIds.Keys.Contains(m2l.LookupId)))
                    _mcdb.ModuleLookups.DeleteOnSubmit(m2l);
                    //m.ModuleLookups.Remove(m2l);

            // add new mappings
            foreach (var mappingId in mc.MappingIds)
                if (!(m.ModuleMappings.Any(m2m => m2m.MappingsId == mappingId.Key)))
                    _mcdb.ModuleMappings.InsertOnSubmit(new ModuleMappingDAO() { MappingsId = mappingId.Key, Module = m });
                    //m.ModuleMappings.Add(new ModuleMapping() { MappingsId = mappingId, Module = m});
            // remove old mappings
            foreach (var m2m in m.ModuleMappings)
                if (!(mc.MappingIds.Keys.Contains(m2m.MappingsId)))
                    _mcdb.ModuleMappings.DeleteOnSubmit(m2m);
                    //m.ModuleMappings.Remove(m2m);

            // add new normalizers
            foreach (var normId in mc.NormalizerIds)
                if (!(m.ModuleNormalizers.Any(m2n => m2n.NormalizerId == normId.Key)))
                    _mcdb.ModuleNormalizers.InsertOnSubmit(new ModuleNormalizerDAO() { NormalizerId = normId.Key, Module = m });
                    //m.ModuleNormalizers.Add(new ModuleNormalizer() { NormalizerId = normId, Module = m});
            // remove old normalizers
            foreach (var m2n in m.ModuleNormalizers)
                if (!(mc.NormalizerIds.Keys.Contains(m2n.NormalizerId)))
                    _mcdb.ModuleNormalizers.DeleteOnSubmit(m2n);
                    //m.ModuleNormalizers.Remove(m2n);

            // add new settings
            foreach (var settingId in mc.SettingIds)
                if (!(m.ModuleSettings.Any(m2s => m2s.SettingId == settingId.Key)))
                    _mcdb.ModuleSettings.InsertOnSubmit(new ModuleSettingDAO() { SettingId = settingId.Key, Module = m });
                    //m.ModuleSettings.Add(new ModuleSetting() { FieldId = fieldId, Module = m });
            // remove old normalizers
            foreach (var m2s in m.ModuleSettings)
                if (!(mc.SettingIds.Keys.Contains(m2s.SettingId)))
                    _mcdb.ModuleSettings.DeleteOnSubmit(m2s);
                    //m.ModuleSettings.Remove(m2s);

            _mcdb.SubmitChanges();
            return true;
        }

        public bool Delete(int moduleId)
        {
            var m = _mcdb.Modules.Where(mm => mm.ModuleId == moduleId).FirstOrDefault();
            if (null == m)
                return false;

            foreach (var m2c in m.ModuleCaptures)
                _mcdb.ModuleCaptures.DeleteOnSubmit(m2c);
                //m.ModuleCaptures.Remove(m2c);
            foreach (var m2i in m.ModuleImagings)
                _mcdb.ModuleImagings.DeleteOnSubmit(m2i);
                //m.ModuleImagings.Remove(m2i);
            foreach (var m2l in m.ModuleLookups)
                _mcdb.ModuleLookups.DeleteOnSubmit(m2l);
                //m.ModuleLookups.Remove(m2l);
            foreach (var m2m in m.ModuleMappings)
                _mcdb.ModuleMappings.DeleteOnSubmit(m2m);
                //m.ModuleMappings.Remove(m2m);
            foreach (var m2n in m.ModuleNormalizers)
                _mcdb.ModuleNormalizers.DeleteOnSubmit(m2n);
                //m.ModuleNormalizers.Remove(m2n);
            foreach (var m2s in m.ModuleSettings)
                _mcdb.ModuleSettings.DeleteOnSubmit(m2s);
                //m.ModuleSettings.Remove(m2s);

            _mcdb.Modules.DeleteOnSubmit(m);
            try {
                _mcdb.SubmitChanges();
                return true;
            } 
            catch(System.Data.Linq.ChangeConflictException cce)
            {
                Console.WriteLine("Concurrency error.");
                Console.WriteLine(cce.Message);
                foreach(System.Data.Linq.ObjectChangeConflict occ in _mcdb.ChangeConflicts)
                {
                    System.Data.Linq.Mapping.MetaTable mt = _mcdb.Mapping.GetTable(occ.Object.GetType());
                    Module mm = (Module)occ.Object;
                    Console.WriteLine("Table name: {0}", mt.TableName);
                    Console.Write("Customer ID: ");
                    Console.WriteLine(mm.ModuleId);
                    foreach (System.Data.Linq.MemberChangeConflict mcc in occ.MemberConflicts)
                    {
                        object currVal = mcc.CurrentValue;
                        object origVal = mcc.OriginalValue;
                        object databaseVal = mcc.DatabaseValue;
                        System.Reflection.MemberInfo mi = mcc.Member;
                        Console.WriteLine("Member: {0}", mi.Name);
                        Console.WriteLine("current value: {0}", currVal);
                        Console.WriteLine("original value: {0}", origVal);
                        Console.WriteLine("database value: {0}", databaseVal);
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        #endregion

    }
}
