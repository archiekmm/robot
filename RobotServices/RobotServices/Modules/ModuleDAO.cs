﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Modules
{
    public partial class ModuleDAO
    {
        public ModuleDAO(Module mc)
            : this()
        {
            this.InputModule = mc.InputModule;
            this.ModuleKind = mc.ModuleKind;
            this.Name = mc.Name;
            this.OutputModule = mc.OutputModule;
            foreach (var captureId in mc.CaptureIds)
                this.ModuleCaptures.Add(new ModuleCaptureDAO() { CaptureId = captureId.Key, Module = this });
            foreach (var imagingId in mc.ImagingIds)
                this.ModuleImagings.Add(new ModuleImagingDAO() { ImagingId = imagingId.Key, Module = this });
            foreach (int logId in mc.LogIds)
                this.ModuleLogs.Add(new ModuleLogDAO() { LogId = logId, Module = this });
            foreach (var lookupId in mc.LookupIds)
                this.ModuleLookups.Add(new ModuleLookupDAO() { LookupId = lookupId.Key, Module = this });
            foreach (var mappingId in mc.MappingIds)
                this.ModuleMappings.Add(new ModuleMappingDAO() { MappingsId = mappingId.Key, Module = this });
            foreach (var normalId in mc.NormalizerIds)
                this.ModuleNormalizers.Add(new ModuleNormalizerDAO() { NormalizerId = normalId.Key, Module = this });
            foreach (var settingId in mc.SettingIds)
                this.ModuleSettings.Add(new ModuleSettingDAO() { SettingId = settingId.Key, Module = this });
        }
    }
}
