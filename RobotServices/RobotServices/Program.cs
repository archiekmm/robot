﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Paragon.RobotServices;
using Paragon.RobotServices.Robots;
using Paragon.RobotServices.Captures;
using Paragon.RobotServices.Conditions;
using Paragon.RobotServices.Fields;
using Paragon.RobotServices.Imaging;
using Paragon.RobotServices.Lookups;
using Paragon.RobotServices.Mappings;
using Paragon.RobotServices.Modules;
using Paragon.RobotServices.Normalizers;
using Paragon.RobotServices.Schedules;

namespace Program
{
    class Program
    {
        private static readonly Dictionary<string, Action<ICollection<string>>> Actions
            = new Dictionary<string, Action<ICollection<string>>>(StringComparer.OrdinalIgnoreCase)
            {
                { "Help", Commands.Help },
                { "Run", Commands.Run },
                { "-service", Commands.RunAsService },
                { "install", Commands.Install },
                { "uninstall", Commands.Uninstall },
            };

        public static int Main(string[] rawArgs)
        {
            int exitCode = 1;
            try
            {
                List<string> args = new List<string>(rawArgs);
                string commandName = "Run";
                if (args.Count > 0)
                {
                    commandName = args[0];
                    args.RemoveAt(0);
                }
                Action<ICollection<string>> cmdAction;
                if (!Actions.TryGetValue(commandName, out cmdAction))
                    cmdAction = Commands.Help;

                cmdAction(args);

                exitCode = 0;
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
            Environment.ExitCode = exitCode;
            return exitCode;
            //Console.WriteLine("Testing Robot Service.  ");
            //TestRobotServiceCrud();
            //TestCaptureServiceCrud();
            //TestConditionsServiceCrud();
            //TestFieldsServiceCrud();
            //TestImagingServiceCrud();
            //TestLookupsServiceCrud();
            //TestMappingsServiceCrud();
            //TestModulesServiceCrud();
            //TestNormalizerServiceCrud();
            //TestScheduleServiceCrud();
            //TestFieldTransforms();
            //TestConditionEvaluation();
            //TestMappingEvaluation();
            //TestLookupEvaluation();
            //TestRobotRun();
            //TestCreateUnisysDocument();
            //TestServices();
            //Console.WriteLine("Test complete.  Press enter to close.");
            //Console.ReadLine();
        }

        public static IEnumerable<Type> GetServices()
        {
            Type t = typeof(Paragon.RobotServices.Robots.RobotService);
            List<Type> services = new List<Type>();
            System.Reflection.Assembly assem = t.Assembly;
            foreach (Type t2 in assem.GetTypes())
            {
                if ((t2.Name.EndsWith("Service")) &&
                        t2.GetInterfaces().Any( 
                            i => (i.Namespace == t2.Namespace && i.Name == "I" + t2.Name)))
                    services.Add(t2);
            }
            return services;
        }

        public static void LoadServices(IList<ServiceHost> list)
        {
            System.UriBuilder b = new UriBuilder() { Scheme = "net.tcp", Host = "localhost", 
                Port = Paragon.RobotServices.Properties.Settings.Default.Port};
            ServiceHost sh = null;
            foreach (Type t in GetServices())
            {
                //b.Path = @"/" + t.Name;
                b.Path = @"/" + t.FullName.Replace(".", "/");
                sh = new ServiceHost(t, b.Uri);
                list.Add(sh);
            }
        }

        public static void StartServices(IList<ServiceHost> list)
        {
            foreach (var sh in list)
            {
                foreach (var addr in sh.BaseAddresses)
                    Console.WriteLine("{0} is hosted at {1}", sh.Description, addr.ToString());
                sh.Open();
            }
        }

        public static void StopServices(IList<ServiceHost> list)
        {
            foreach (var sh in list)
                sh.Close();
        }

        public static void TestRobotServiceCrud()
        {
            RobotService rs = new Paragon.RobotServices.Robots.RobotService();
            Robot rc;
            rc = rs.Retrieve(1);
            rc.Name = rc.Name + " updated";
            rs.Update(rc);
            rc = new Robot();
            rc.Name = "test new robot";
            //rc.IsScheduled = false;
            rc.Repeats = false;
            rc.SleepTime = 0;
            //rc.ModuleIds.Add(2, "Module");
            //rc.ScheduleIds.Add(1, "Schedule");
            int newId = rs.Create(rc);
            rc = rs.Retrieve(newId);
            rs.Delete(newId);
        }

        public static void TestCaptureServiceCrud() 
        {
            CaptureService cs = new Paragon.RobotServices.Captures.CaptureService();
            Capture cc;
            cc = cs.Retrieve(1);
            cc.Name = cc.Name + " updated";
            cs.Update(cc);
            cc = new Capture();
            cc.Name = "Test Capture Config";
            cc.UserId = "User";
            cc.Password = "Password";
            int ccId = cs.Create(cc);
            cc = cs.Retrieve(ccId);
            cs.Delete(ccId);
        }
        
        public static void TestConditionsServiceCrud() 
        {
            ConditionService cs = new ConditionService();
            Condition cc;
            cc = cs.Retrieve(1);
            //cc.CompareMethod = cc.Name + " updated";
            cc.Conditional = Conditional.NotEquals;
            cc.Connector = Connector.Or;
            cs.Update(cc);
            cc = new Condition();
            cc.CompareMethod = CompareMethod.Text;
            cc.Conditional = Conditional.Equals;
            cc.Connector = Connector.And;
            cc.Operand1 = "_SourceFolder";
            cc.Operand2 = "Input";
            int ccId = cs.Create(cc);
            cc = cs.Retrieve(ccId);
            cs.Delete(ccId);
        }

        public static void TestFieldsServiceCrud()
        {
            FieldService fs = new FieldService();
            Field fc = new Field();
            fc = fs.Retrieve(1);
            fc.Name = "TestChange";
            fs.Update(fc);
            fc = new Field();
            fc.Name = "TestAdd";
            fc.Value = "[_SourceFolder]";
            int fcId = fs.Create(fc);
            fc = fs.Retrieve(fcId);
            fs.Delete(fcId);
        }

        public static void TestImagingServiceCrud()
        {
            ImagingService iis = new ImagingService();
            Imaging ic = new Imaging();
            ic = iis.Retrieve(1);
            ic.Name = "IS Name Change";
            iis.Update(ic);
            ic = new Imaging();
            ic.Name = "IS new";
            ic.UserId = "guest";
            ic.Password = string.Empty;
            ic.ConnectionString = "blank";
            int icId = iis.Create(ic);
            ic = iis.Retrieve(icId);
            iis.Delete(icId);
        }

        public static void TestLookupsServiceCrud()
        {
            LookupService ls = new LookupService();
            Lookup lc = new Lookup();
            lc = ls.Retrieve(1);
            lc.Name = "LC Name Change";
            ls.Update(lc);
            lc = new Lookup();
            lc.Name = "LC New";
            lc.ConnectionString = "blank";
            lc.ProviderName = "SQL Server";
            lc.MappingIds.Add(1, lc.Name);
            int lcId = ls.Create(lc);
            lc = ls.Retrieve(lcId);
            ls.Delete(lcId);
        }

        public static void TestMappingsServiceCrud()
        {
            MappingService ms = new MappingService();
            Mapping mc = new Mapping();
            mc = ms.Retrieve(1);
            mc.Name = "mc Name Change";
            ms.Update(mc);
            mc = new Mapping();
            mc.Name = "mc New";
            mc.ConditionIds.Add(1);
            mc.FieldIds.Add(1, "_SourceFolder");
            int mcId = ms.Create(mc);
            mc = ms.Retrieve(mcId);
            ms.Delete(mcId);
        }

        public static void TestModulesServiceCrud()
        {
            ModuleService ms = new ModuleService();
            Module mc = new Module();
            mc = ms.Retrieve(2);
            mc.Name = "MC Name Change";
            ms.Update(mc);
            mc = new Module();
            mc.Name = "MC New";
            mc.CaptureIds.Add(1, "Capture");
            mc.ImagingIds.Add(2, "Imaging");
            mc.LookupIds.Add(1, "Lookup");
            mc.MappingIds.Add(1, "Mapping");
            mc.NormalizerIds.Add(1, "Normalizer");
            mc.SettingIds.Add(1, "Setting");
            int mcId = ms.Create(mc);
            mc = ms.Retrieve(mcId);
            ms.Delete(mcId);
        }

        public static void TestNormalizerServiceCrud()
        {
            NormalizerService ns = new NormalizerService();
            Normalizer nc = new Normalizer();
            nc = ns.Retrieve(1);
            nc.Name = "nc Name Change";
            ns.Update(nc);
            nc = new Normalizer();
            nc.Name = "nc New";
            nc.BitsPerPixel = 1;
            nc.MatchXAndY = true;
            nc.ModuleIds.Add(2);
            nc.Rotation = 90;
            nc.RoundResolution = true;
            nc.TiffCompression = System.Windows.Media.Imaging.TiffCompressOption.Ccitt4;
            int ncId = ns.Create(nc);
            nc = ns.Retrieve(ncId);
            ns.Delete(ncId);
        }

        public static void TestScheduleServiceCrud()
        {
            ScheduleService ss = new ScheduleService();
            Schedule sc = new Schedule();
            sc = ss.Retrieve(1);
            sc.Name = "sc Name Change";
            ss.Update(sc);
            sc = new Schedule();
            sc.Name = "sc New";
            sc.RobotIds.Add(2);
            sc.ScheduleElementIds.Add(1);
            int scId = ss.Create(sc);
            sc = ss.Retrieve(scId);
            ss.Delete(scId);
        }

        public static void TestFieldTransforms()
        {
            FieldService fs = new FieldService();
            Dictionary<string, string> testData = new Dictionary<string, string>();
            List<int> fieldIds = new List<int>();
            List<Field> configs = new List<Field>();

            testData.Add("_SourceFolder", @"C:\temp\testfile.txt");
            testData.Add("_MapClassName", "Voodoo");

            configs.Add(new Field() { Name = "Blue", Value = "[_SourceFolder]" });
            configs.Add(new Field() { Name = "Grey", Value = "[_MapClassName]" });

            for (int i = 0; i < configs.Count; ++i)
                fieldIds.Add(fs.Create(configs[i]));

            IDictionary<string, string> results =
                fs.TranslateFields(fieldIds, testData);

            Console.WriteLine("Before:");
            foreach (var kvp in testData)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);
            Console.WriteLine("After:");
            foreach (var kvp in results)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);
        }

        public static void TestConditionEvaluation()
        {
            ConditionService cs = new ConditionService();
            Dictionary<string, string> testData = new Dictionary<string, string>();
            List<int> condIds = new List<int>();
            List<Condition> configs = new List<Condition>();

            testData.Add("_SourceFolder", @"C:\temp\testfile.txt");
            testData.Add("_MapClassName", "Voodoo");

            configs.Add(new Condition() { Operand1 = "_SourceFolder", Operand2 = @"C:\temp\testfile.txt" });
            configs.Add(new Condition() { Operand1 = "_MapClassName", Operand2 = @"Black Magic" });

            foreach (var config in configs )
                condIds.Add(cs.Create(config));

            bool results =
                cs.Evaluate(condIds, testData);

            Console.WriteLine("Before:");
            foreach (var kvp in testData)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);
            Console.WriteLine("After:");
            //foreach (var kvp in results)
            Console.WriteLine("Result:  " + results.ToString());
            Console.WriteLine("After2:");
            //foreach (var kvp in results)
            bool[] r2 = new bool[] { false, false};
            for (int i = 0; i < r2.Count(); ++i)
            {
                r2[i] = cs.Evaluate(new int[] { condIds[i] }, testData);

                Console.WriteLine("Result["+i.ToString()+"]:  " + r2[i].ToString());
            }

        }

        public static void TestMappingEvaluation()
        {
            MappingService ms = new MappingService();

            Dictionary<string, string> testData = new Dictionary<string, string>();
            List<int> mapIds = new List<int>();
            //List<ConditionConfig> configs = new List<ConditionConfig>();

            testData.Add("_SourceFolder", @"C:\temp\testfile.txt");
            testData.Add("_MapClassName", "Voodoo");

            IDictionary<string, string> results = ms.TranslateFields(2, testData);
            Console.WriteLine("Before:");
            foreach (var kvp in testData)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);
            Console.WriteLine("After:");
            foreach (var kvp in results)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);

            Console.WriteLine();
            testData["_SourceFolder"] = @"C:\test\Input";
            results = ms.TranslateFields(2, testData);
            Console.WriteLine("Before:");
            foreach (var kvp in testData)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);
            Console.WriteLine("After:");
            foreach (var kvp in results)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);

        }

        public static void TestLookupEvaluation()
        {
            LookupService ls = new LookupService();
            Dictionary<string, string> testData = new Dictionary<string,string>();
            testData.Add("_SourceFolder", @"c:\test\");
            testData.Add("Blue", "Green");
            testData.Add("_query", "[_query]");
            IDictionary<string, string> rv = ls.DoLookup(1, testData);
            foreach (var kvp in testData)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);
            foreach (var kvp in rv)
                Console.WriteLine("Key:  " + kvp.Key + ", Value:  " + kvp.Value);

        }

        public static void TestRobotRun()
        {
            RobotService rs = new RobotService();
            rs.Start(12);
            System.Threading.Thread.Sleep(120000); // two minutes
            rs.Stop(12);

        }

        public static void TestServices()
        {
            try
            {
                List<ServiceHost> list = new List<ServiceHost>();
                LoadServices(list);
                StartServices(list);
                Console.WriteLine("Services started.  Press enter to stop.");
                Console.ReadLine();
                StopServices(list);
                Console.WriteLine("Services stopped.  Press enter to close.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception while running services:  " + ex.ToString());
            }
        }

        public static void TestCreateUnisysDocument()
        { 
            ImagingService iis = new ImagingService();
            int sessionId = iis.StartSession(1);
            ImagingDocument doc = new ImagingDocument();
            doc.Name = "Test";
            doc.Class = "AADOC";
            IDictionary<string, string> index = new Dictionary<string, string>();
            index.Add("CONTRACT_NUM", "54321");
            doc.Indexes.Add(index);
            byte[] imageData = System.IO.File.ReadAllBytes(@"C:\Unisys Docs\Books\Connect Exch GetStart.pdf");
            doc.Pages.Add(new ImagingPage()
                {
                    MimeType = System.Net.Mime.MediaTypeNames.Application.Pdf,
                    ImageData = imageData,
                    Name = "TestPage",
                });
            string docId = iis.CreateDocument(sessionId, doc);
            ImagingDocument newDoc = iis.RetrieveDocument(sessionId, docId);
            iis.EndSession(sessionId);

        }
}
}
