﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;
using System.Configuration;
using System.Configuration.Install;

namespace Paragon.RobotServices
{
    public class RobotWindowsService : ServiceBase
    {
        private readonly string[] _arguments;
        private ServiceImplementation _service;

        public RobotWindowsService(IEnumerable<string> arguments)
        {
            _arguments = arguments.ToArray();
            if (_arguments.Length == 0 || string.IsNullOrEmpty(_arguments[0]))
                throw new ArgumentException("Required parameter service name not provided.");

            AutoLog = false;
            CanStop = true;
            CanShutdown = true;
            CanHandlePowerEvent = true;
            CanHandleSessionChangeEvent = true;
            CanPauseAndContinue = true;

            ServiceName = _arguments[0];
        }

        private void InitializeComponent(bool disposing)
        {
            // for Designer only:
            ServiceName = "ServiceName";
        }

        protected override void  Dispose(bool disposing)
        {
            if (_service != null)
                _service.Dispose();
            _service = null;

            base.Dispose(disposing);
        }
        
        protected override void OnStart(string[] args)
        {
            try
            {
                OnStop();
                _service = new ServiceImplementation();

                List<string> allarguments = new List<string>(_arguments);
                if (args != null && args.Length > 0)
                    allarguments.AddRange(args);

                _service.Start(allarguments);
                Resources.ServiceStarted(ServiceName);
            }
            catch (Exception e)
            {
                throw new ServiceStartFailedException(ServiceName, e);
            }
        }

        protected override void OnStop()
        {
            if (_service == null)
                return;
            try
            {
                if (_service != null)
                {
                    _service.Stop();
                    _service.Dispose();
                    _service = null;
                }
            }
            finally
            {
                Resources.ServiceStopped(ServiceName);
            }
        }

        public IEnumerable<Type> GetServices()
        {
            Type t = typeof(Paragon.RobotServices.Robots.RobotService);
            List<Type> services = new List<Type>();
            System.Reflection.Assembly assem = t.Assembly;
            foreach (Type t2 in assem.GetTypes())
            {
                if ((t2.Name.EndsWith("Service")) &&
                        t2.GetInterfaces().Any(
                            i => (i.Namespace == t2.Namespace && i.Name == "I" + t2.Name)))
                    services.Add(t2);
            }
            return services;
        }

        public IList<ServiceHost> LoadServices()
        {
            List<ServiceHost> rv = new List<ServiceHost>();
            System.UriBuilder b = new UriBuilder()
            {
                Scheme = "net.tcp",
                Host = "localhost",
                Port = Paragon.RobotServices.Properties.Settings.Default.Port
            };
            ServiceHost sh = null;
            foreach (Type t in GetServices())
            {
                //b.Path = @"/" + t.Name;
                b.Path = @"/" + t.FullName.Replace(".", "/");
                sh = new ServiceHost(t, b.Uri);
                rv.Add(sh);
            }
            return rv;
        }

        public void StartServices(IList<ServiceHost> list)
        {
            foreach (var sh in list)
            {
                foreach (var addr in sh.BaseAddresses)
                    Console.WriteLine("{0} is hosted at {1}", sh.Description, addr.ToString());
                sh.Open();
            }
        }

        public void StopServices(IList<ServiceHost> list)
        {
            foreach (var sh in list)
                sh.Close();
        }

    }

}
