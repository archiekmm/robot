﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Lookups
{
    public partial class LookupDAO
    {
        public LookupDAO(Lookup lc)
            : this()
        {
            this.LookupId = lc.LookupId;
            this.Name = lc.Name;
            this.ProviderName = lc.ProviderName;
            this.ConnectionString = lc.ConnectionString;
            foreach (var mId in lc.MappingIds)
                this.LookupMappings.Add(new LookupMappingsDAO() { MappingId = mId.Key,  LookupId = this.LookupId });
        }
    }
}
