﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Lookups
{
    // NOTE: If you change the class name "LookupService" here, you must also update the reference to "LookupService" in App.config.
    public class LookupService : ILookupService
    {
        protected LookupClassesDataContext _ldb = new LookupClassesDataContext();

        #region DB CRUD operations

        public IEnumerable<int> ListAll()
        {
            return _ldb.Lookups.Select(l => l.LookupId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _ldb.Lookups
                .Select(l => new KeyValuePair<int, string>(l.LookupId, l.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _ldb.Lookups.Where(l => ids.Contains(l.LookupId))
                .Select(l => new KeyValuePair<int, string>(l.LookupId, l.Name));
        }

        public int Create(Lookup lc)
        {
            var l = new LookupDAO(lc);
            _ldb.Lookups.InsertOnSubmit(l);
            _ldb.SubmitChanges();
            return l.LookupId;
        }

        public Lookup Retrieve(int lookupId)
        {
            return _ldb.Lookups.Where(lo => lo.LookupId == lookupId)
                .Select(l => new Lookup(l)).FirstOrDefault();
        }

        public IEnumerable<Lookup> RetrieveMany(IEnumerable<int> ids)
        {
            return _ldb.Lookups.Where(l => ids.Contains(l.LookupId))
                .Select(l => new Lookup(l));
        }

        public bool Update(Lookup lc)
        {
            var l = _ldb.Lookups.Where(lo => lo.LookupId == lc.LookupId).FirstOrDefault();
            if (null == l)
                return false;
            l.ConnectionString = lc.ConnectionString;
            l.Name = lc.Name;
            l.ProviderName = lc.ProviderName;
            // Add any mapping ids that don't already exist
            foreach (var mId in lc.MappingIds)
                if (!(l.LookupMappings.Any(lm => lm.MappingId == mId.Key)))
                    _ldb.LookupMappings.InsertOnSubmit(new LookupMappingsDAO() { MappingId = mId.Key, Lookup = l });
                    //l.LookupMappings.Add(new LookupMapping() { MappingId = mId, Lookup = l });
            // remove any mapping ids that no longer exist
            foreach (var lm in l.LookupMappings)
                if (!(lc.MappingIds.Keys.Contains(lm.MappingId)))
                    _ldb.LookupMappings.DeleteOnSubmit(lm);
                    //l.LookupMappings.Remove(lm);
            _ldb.SubmitChanges();
            return true;
        }

        public bool Delete(int lookupId)
        {
            var l = _ldb.Lookups.Where(lo => lo.LookupId == lookupId).FirstOrDefault();
            if (null == l)
                return false;
            foreach (var lm in l.LookupMappings)
                _ldb.LookupMappings.DeleteOnSubmit(lm);
                //l.LookupMappings.Remove(lm);
            _ldb.Lookups.DeleteOnSubmit(l);
            return true;
        }

        #endregion

        public IDictionary<string, string> DoLookup(int lookupId, IDictionary<string, string> inputs)
        {
            Lookup lc = this.Retrieve(lookupId);
            if (null == lc)
                return inputs;
            DbLookupWorker lw = new DbLookupWorker(lc);
            return lw.AsDictionary(inputs);
        }

    }
}
