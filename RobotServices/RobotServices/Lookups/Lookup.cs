﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Lookups
{
    [DataContract]
    public class Lookup
    {
        [DataMember]
        public int LookupId;

        [DataMember]
        public string Name;

        [DataMember]
        public string ProviderName;

        [DataMember]
        public string ConnectionString;

        [DataMember]
        public Dictionary<int, string> MappingIds = new Dictionary<int, string>();
        //public List<int> MappingIds = new List<int>();

        public Lookup(LookupDAO l)
        {
            this.LookupId = l.LookupId;
            this.Name = l.Name;
            this.ProviderName = l.ProviderName;
            this.ConnectionString = l.ConnectionString;
            //this.MappingIds = new List<int>();
            foreach (var m in l.LookupMappings)
                this.MappingIds.Add(m.MappingId, m.Mapping.Name);
        }

        public Lookup() { }
    }
}
