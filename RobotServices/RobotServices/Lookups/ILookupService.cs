﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Lookups
{
    // NOTE: If you change the interface name "ILookupService" here, you must also update the reference to "ILookupService" in App.config.
    [ServiceContract]
    public interface ILookupService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> lookupIds);
        
        [OperationContract]
        int Create(Lookup lc);

        [OperationContract]
        Lookup Retrieve(int lookupId);

        [OperationContract]
        IEnumerable<Lookup> RetrieveMany(IEnumerable<int> lookupIds);

        [OperationContract]
        bool Update(Lookup lc);

        [OperationContract]
        bool Delete(int lookupId);

        [OperationContract]
        IDictionary<string, string> DoLookup(int lookupId, IDictionary<string, string> inputs);

    }
}
