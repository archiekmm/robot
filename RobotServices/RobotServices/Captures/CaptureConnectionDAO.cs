﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.Robot.Model.Capture.KofaxAscentCapture;

namespace Paragon.RobotServices.Captures
{
    public partial class CaptureConnectionDAO
    {
        public CaptureConnectionDAO(Capture cc)
            : this()
        {
            this.Name = cc.Name;
            this.Password = cc.Password;
            this.UserId = cc.UserId;
            this.CaptureKind = cc.CaptureKind;
            foreach (var settingId in cc.SettingIds)
                this.CaptureSettings.Add(new CaptureSettingDAO() { SettingId = settingId.Key, CaptureConnection = this });
        }
    }
}
