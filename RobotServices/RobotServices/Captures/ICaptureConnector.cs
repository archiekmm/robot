﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Captures
{
    public interface ICaptureConnector
    {
        IDictionary<string, string> CreateBatch(IEnumerable<byte[]> images,
            IDictionary<string, string> inputs);
    }
}
