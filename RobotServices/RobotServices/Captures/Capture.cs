﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Paragon.RobotServices.Captures
{
    [DataContract]
    public class Capture
    {
        [DataMember]
        public int CaptureId;

        [DataMember]
        public string Name;

        [DataMember]
        public string UserId;

        [DataMember]
        public string Password;

        [DataMember]
        public string CaptureKind;

        [DataMember]
        public Dictionary<int, string> SettingIds = new Dictionary<int, string>();

        public Capture(CaptureConnectionDAO c)
        {
            this.CaptureId = c.CaptureId;
            this.Name = c.Name;
            this.UserId = c.UserId;
            this.Password = c.Password;
            this.CaptureKind = c.CaptureKind;
            foreach (var setting in c.CaptureSettings)
                this.SettingIds.Add(setting.SettingId, setting.Setting.Name);
        }

        public Capture() { }
    }
}
