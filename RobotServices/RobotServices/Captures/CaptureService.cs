﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Paragon.Robot.Model.Capture.KofaxAscentCapture;
using Paragon.RobotServices.Utilities;

namespace Paragon.RobotServices.Captures
{
    // NOTE: If you change the class name "CaptureService" here, you must also update the reference to "CaptureService" in App.config.
    public class CaptureService : ICaptureService
    {
        protected CaptureClassesDataContext _cc = new CaptureClassesDataContext();
        // I'll probably need to change this to cache the login across service sessions
        // but I don't want to incur the complexity penalty right now.

        protected static Dictionary<int, CaptureConnector> _sessions = new Dictionary<int, CaptureConnector>();

        #region ICaptureService Members

        #region CRUD Operations

        public IEnumerable<int> ListAll()
        {
            return _cc.CaptureConnections.Select(cc => cc.CaptureId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _cc.CaptureConnections.Where(cc => ids.Contains(cc.CaptureId))
                .Select(cc => new KeyValuePair<int, string>(cc.CaptureId, cc.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _cc.CaptureConnections.Select(cc => new KeyValuePair<int, string>(cc.CaptureId, cc.Name));
        }

        public int Create(Capture cc)
        {
            CaptureConnectionDAO c = new CaptureConnectionDAO(cc);
            foreach (var settingId in cc.SettingIds)
                c.CaptureSettings.Add(new CaptureSettingDAO() { SettingId = settingId.Key, CaptureConnection = c });
            _cc.CaptureConnections.InsertOnSubmit(c);
            _cc.SubmitChanges();
            return c.CaptureId;
        }

        public Capture Retrieve(int captureId)
        {
            CaptureConnectionDAO cc = _cc.CaptureConnections.Where(c => c.CaptureId == captureId).FirstOrDefault();
            if (null == cc)
                return null;
            return new Capture(cc);
        }

        public IEnumerable<Capture> RetrieveMany(IEnumerable<int> ids)
        {
            return _cc.CaptureConnections.Where(cc => ids.Contains(cc.CaptureId))
                .Select(cc => new Capture(cc));
        }

        public bool Update(Capture cc)
        {
            CaptureConnectionDAO ccc = _cc.CaptureConnections.Where(c => c.CaptureId == cc.CaptureId).FirstOrDefault();
            if (null == ccc)
                return false;
            ccc.Name = cc.Name;
            ccc.UserId = cc.UserId;
            ccc.Password = cc.Password;
            ccc.CaptureKind = cc.CaptureKind;
            // insert any new setting associations
            foreach (var settingId in cc.SettingIds)
                if (!ccc.CaptureSettings.Any(c => c.SettingId == settingId.Key))
                    ccc.CaptureSettings.Add(new CaptureSettingDAO() { SettingId = settingId.Key, CaptureConnection = ccc });
            // delete any removed setting associations
            foreach (var setting in ccc.CaptureSettings)
                if (!cc.SettingIds.ContainsKey(setting.SettingId))
                    ccc.CaptureSettings.Remove(setting);
            _cc.SubmitChanges();
            return true;
        }

        public bool Delete(int captureId)
        {
            CaptureConnectionDAO cc = _cc.CaptureConnections.Where(c => c.CaptureId == captureId).FirstOrDefault();
            if (null == cc)
                return false;
            cc.CaptureSettings.Clear();            
            _cc.CaptureConnections.DeleteOnSubmit(cc);
            _cc.SubmitChanges();
            return true;
        }

        #endregion

        public int StartSession(int captureId)
        {
            CaptureConnectionDAO dao = _cc.CaptureConnections.Where(c => c.CaptureId == captureId).FirstOrDefault();
            if (null == dao)
                return -1;
            Capture config = new Capture(dao);
            Settings.SettingService ss = new Paragon.RobotServices.Settings.SettingService();
            var settings = ss.RetrieveMany(config.SettingIds.Keys);
            int newId = -1;
            CaptureConnector conn = null;
            try
            {
                Type connType = dao.CaptureKind.LoadAssemblyAndGetType();
                conn = (CaptureConnector)System.Activator.CreateInstance(connType, config, settings);
                if (_sessions.Count == 0)
                    newId = 1;
                else
                    newId = _sessions.Max(kvp => kvp.Key) + 1;
                _sessions.Add(newId, conn);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception while creating capture connector. " + Environment.NewLine
                    + "Type:  {0}, Id:  {1}, Name:  {2}, User: {3}, " + Environment.NewLine
                    + "Message:  {4}", dao.CaptureKind, dao.CaptureId,
                    dao.Name, dao.UserId, ex.ToString());
            }
            //Login l = new Login();
            //Application app = l.Logon(cc.UserId, cc.Password);
            //int newId = _sessions.Max(sess => sess.Key) + 1;
            //_sessions.Add(newId, new CaptureSession() { Login = l, App = app });
            return newId;
        }

        public bool EndSession(int sessionId)
        {
            CaptureConnector sess = _sessions
                .Where(ss => ss.Key == sessionId).Select(ss => ss.Value).FirstOrDefault();
            if (null == sess)
                return false;
            _sessions.Remove(sessionId);
            sess.Dispose();
            sess = null;
            return true;
        }

        protected IDictionary<string, string> ErrorDictionary(int code, string shortMsg, string longMsg)
        {
            Dictionary<string, string> rv = new Dictionary<string, string>();
            rv.Add("_Error.Code", code.ToString());
            rv.Add("_Error.Short", shortMsg);
            rv.Add("_Error.Long", longMsg);
            rv.Add("_Error", string.Format("{0}({1}):  {2}", rv["_Error.Code"],
                    rv["_Error.Short"], rv["_Error.Long"]));
            return rv;
        }

        public IDictionary<string, string> ImportStreams(int sessionId, IDictionary<string, string> inputs, IEnumerable<byte[]> images)
        {
            CaptureConnector sess = _sessions
                .Where(ss => ss.Key == sessionId).Select(ss => ss.Value).FirstOrDefault();
            if (null == sess)
                return ErrorDictionary(404, "Not Found", string.Format("Unable to locate Capture session with id '{0}'", sessionId));
            
            return sess.CreateBatch(images, inputs);
        }

        #endregion

    }
}
