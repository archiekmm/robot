﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Captures
{
    // NOTE: If you change the interface name "ICaptureService" here, you must also update the reference to "ICaptureService" in App.config.
    [ServiceContract]
    public interface ICaptureService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> captureIds);

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        int Create(Capture cc);

        [OperationContract]
        Capture Retrieve(int captureId);

        [OperationContract]
        IEnumerable<Capture> RetrieveMany(IEnumerable<int> captureIds);

        [OperationContract]
        bool Update(Capture cc);

        [OperationContract]
        bool Delete(int captureId);

        [OperationContract]
        int StartSession(int captureId);

        [OperationContract]
        bool EndSession(int sessionId);

        [OperationContract]
        IDictionary<string, string> ImportStreams(int sessionId, 
            IDictionary<string, string> inputs, 
            IEnumerable<byte[]> images);
    }
}
