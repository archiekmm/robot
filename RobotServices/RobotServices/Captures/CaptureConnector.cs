﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Captures
{
    public abstract class CaptureConnector : ICaptureConnector, IDisposable
    {
        protected IEnumerable<Settings.Setting> _settings = null;
        protected IEnumerable<MappingGroups.MappingGroup> _groups = null;
        protected Capture _capture = null;

        public CaptureConnector(Capture config, IEnumerable<Settings.Setting> settings, IEnumerable<MappingGroups.MappingGroup> groups) 
        {
            _capture = config;
            _settings = settings;
            _groups = groups;
        }

        public abstract IDictionary<string, string> CreateBatch(IEnumerable<byte[]> images, IDictionary<string, string> inputs);

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }
}
