﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.MappingGroups
{
    // NOTE: If you change the class name "MappingGroups" here, you must also update the reference to "MappingGroups" in App.config.
    public class MappingGroupService : IMappingGroupService
    {
        protected MappingGroupClassesDataContext _mgcdc = new MappingGroupClassesDataContext();

        #region IMappingGroupService Members

        public IEnumerable<int> ListAll()
        {
            return _mgcdc.MappingGroups.Select(dao => dao.MappingGroupId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _mgcdc.MappingGroups
                .Select(dao => new KeyValuePair<int, string>(dao.MappingGroupId, dao.MappingGroupName));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _mgcdc.MappingGroups.Where(m => ids.Contains(m.MappingGroupId))
                .Select(dao => new KeyValuePair<int, string>(dao.MappingGroupId, dao.MappingGroupName));
        }

        public int Create(MappingGroup mg)
        {
            MappingGroupDAO dao = new MappingGroupDAO(mg);
            foreach (var id in mg.MappingIds)
                dao.MappingGroupsToMappings.Add(
                    new MappingGroupsToMappingDAO() { MappingId = id.Key, MappingGroup = dao });
            _mgcdc.MappingGroups.InsertOnSubmit(dao);
            _mgcdc.SubmitChanges();
            return dao.MappingGroupId;
        }

        public MappingGroup Retrieve(int groupId)
        {
            return _mgcdc.MappingGroups.Where(dao => dao.MappingGroupId == groupId)
                .Select(dao => new MappingGroup(dao)).FirstOrDefault();
        }

        public IEnumerable<MappingGroup> RetrieveMany(IEnumerable<int> ids)
        {
            return _mgcdc.MappingGroups.Where(dao => ids.Contains(dao.MappingGroupId))
                .Select(dao => new MappingGroup(dao));
        }

        public bool Update(MappingGroup mg)
        {
            MappingGroupDAO dao = _mgcdc.MappingGroups.Where(mmg => mmg.MappingGroupId == mg.GroupId).FirstOrDefault();
            if (null == dao)
                return false;
            dao.MappingGroupName = mg.Name;
            // add new mappings
            foreach (var mappingId in mg.MappingIds)
                if (!dao.MappingGroupsToMappings.Any(mmgm => mmgm.MappingId == mappingId.Key))
                    dao.MappingGroupsToMappings.Add(
                        new MappingGroupsToMappingDAO() { MappingId = mappingId.Key, MappingGroup = dao });
            // remove old mappings
            var toRemove = new List<MappingGroupsToMappingDAO>();
            foreach (var candidate in dao.MappingGroupsToMappings)
                if (!mg.MappingIds.Keys.Contains(candidate.MappingId))
                    toRemove.Add(candidate);
            foreach (var item in toRemove)
                dao.MappingGroupsToMappings.Remove(item);
            _mgcdc.SubmitChanges();
            return true;
        }

        public bool Delete(int groupId)
        {
            MappingGroupDAO dao = _mgcdc.MappingGroups.Where(mmg => mmg.MappingGroupId == groupId).FirstOrDefault();
            if (null == dao)
                return false;
            foreach (var mgm in dao.MappingGroupsToMappings)
                dao.MappingGroupsToMappings.Remove(mgm);
            _mgcdc.MappingGroups.DeleteOnSubmit(dao);
            _mgcdc.SubmitChanges();
            return true;
        }

        public IDictionary<string, string> ConditionalTranslateFields(int groupId, IDictionary<string, string> inputs, bool? multiMap)
        {
            MappingGroup dao = Retrieve(groupId);
            //MappingGroupDAO dao = _mgcdc.MappingGroups.Where(mmg => mmg.MappingGroupId == groupId).FirstOrDefault();
            if (null == dao)
                return inputs;
            return dao.ConditionalTranslateFields(inputs, multiMap.HasValue ? multiMap.Value : false);
        }

        #endregion
    }
}
