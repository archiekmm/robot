﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.MappingGroups
{
    // NOTE: If you change the interface name "IMappingGroups" here, you must also update the reference to "IMappingGroups" in App.config.
    [ServiceContract]
    public interface IMappingGroupService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> groupIds);
        
        [OperationContract]
        int Create(MappingGroup mg);

        [OperationContract]
        MappingGroup Retrieve(int groupId);

        [OperationContract]
        IEnumerable<MappingGroup> RetrieveMany(IEnumerable<int> groupIds);

        [OperationContract]
        bool Update(MappingGroup mg);

        [OperationContract]
        bool Delete(int groupId);

        [OperationContract]
        IDictionary<string, string> ConditionalTranslateFields(int groupId, IDictionary<string, string> inputs, bool? mapMultiple);

    }
}
