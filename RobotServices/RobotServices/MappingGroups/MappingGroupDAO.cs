﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.MappingGroups
{
    public partial class MappingGroupDAO
    {
        public MappingGroupDAO(MappingGroup model) : this()
        {
            this.MappingGroupName = model.Name;
            foreach (var id in model.MappingIds)
                this.MappingGroupsToMappings.Add(
                    new MappingGroupsToMappingDAO() { MappingId = id.Key, MappingGroup = this });
        }

        // should this be here?
    }
}
