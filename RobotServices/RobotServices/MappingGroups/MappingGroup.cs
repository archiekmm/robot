﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.MappingGroups
{
    [DataContract]
    public class MappingGroup
    {
        [DataMember]
        public int GroupId;

        [DataMember]
        public string Name;

        [DataMember]
        public Dictionary<int, string> MappingIds = new Dictionary<int, string>();
        //public List<int> MappingIds = new List<int>();

        public MappingGroup(MappingGroupDAO dao)
        {
            this.GroupId = dao.MappingGroupId;
            this.Name = dao.MappingGroupName;
            foreach (var m in dao.MappingGroupsToMappings)
                this.MappingIds.Add(m.MappingId, m.MappingGroup.MappingGroupName);
        }

        public MappingGroup() { }

        public IDictionary<string, string> ConditionalTranslateFields(IDictionary<string, string> inputs, bool mapMultiple)
        {
            IDictionary<string, string> rv = null;
            IDictionary<string, string> interim = null;
            Mappings.MappingService ms = new Paragon.RobotServices.Mappings.MappingService();
            foreach (var mappingId in this.MappingIds)
            {
                Mappings.Mapping m = ms.Retrieve(mappingId.Key);
                interim = m.TranslateFields(inputs);
                // if they don't apply, interim s/b (copy of?) inputs and not null
                // but we'll check anyway
                if (null == interim)
                    continue;
                // check if this mapping handled it
                if (interim.ContainsKey("_MapClassName") && (interim["_MapClassName"] == m.Name))
                {   // it did:
                    if (mapMultiple)
                    {
                        if (null == rv)
                            rv = interim;
                        else
                            foreach (var kvp in interim)
                                if ((kvp.Key == "_MapClassName") && rv.ContainsKey("_MapClassName"))
                                    rv[kvp.Key] += "," + kvp.Value;
                                else
                                    rv[kvp.Key] = kvp.Value;
                    }
                    else
                        return interim;
                }
            }
            if (mapMultiple)
                return rv;
            return inputs;
        }
    }
}
