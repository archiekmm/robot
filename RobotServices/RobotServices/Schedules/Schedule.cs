﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Schedules
{
    [DataContract]
    public class Schedule
    {
        [DataMember]
        public string Name;

        [DataMember]
        public int ScheduleId;

        [DataMember]
        public List<int> RobotIds = new List<int>();

        [DataMember]
        public List<int> ScheduleElementIds = new List<int>();

        public Schedule(ScheduleDAO s)
        {
            this.Name = s.Name;
            this.ScheduleId = s.ScheduleId;
            foreach (var rs in s.RobotScheduleDAOs)
                this.RobotIds.Add(rs.RobotId);
            foreach (var se in s.ScheduleToScheduleElementDAOs)
                this.ScheduleElementIds.Add(se.ScheduleElementId);
        }

        public Schedule() { }
    }
}
