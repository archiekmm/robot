﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Schedules
{
    // NOTE: If you change the class name "ScheduleService" here, you must also update the reference to "ScheduleService" in App.config.
    public class ScheduleService : IScheduleService
    {
        #region IScheduleService Members

        protected ScheduleClassesDataContext _scdc = new ScheduleClassesDataContext();

        public IEnumerable<int> ListAll()
        {
            return _scdc.ScheduleDAOs
                .Select(sch => sch.ScheduleId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _scdc.ScheduleDAOs
                .Select(sch => new KeyValuePair<int, string>(sch.ScheduleId, sch.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _scdc.ScheduleDAOs.Where(s => ids.Contains(s.ScheduleId))
                .Select(sch => new KeyValuePair<int, string>(sch.ScheduleId, sch.Name));
        }


        public int Create(Schedule sc)
        {
            ScheduleDAO s = new ScheduleDAO(sc);
            if (null == s)
                return 0;
            if (null != sc.ScheduleElementIds)
                foreach (int seId in sc.ScheduleElementIds)
                    _scdc.ScheduleToScheduleElementDAOs.InsertOnSubmit(new ScheduleToScheduleElementDAO() { ScheduleElementId = seId, ScheduleDAO = s });
            if (null != sc.RobotIds)
                foreach (int rsId in sc.RobotIds)
                    _scdc.RobotScheduleDAOs.InsertOnSubmit(new RobotScheduleDAO() { RobotId = rsId, ScheduleDAO = s });
            _scdc.ScheduleDAOs.InsertOnSubmit(s);
            _scdc.SubmitChanges();
            return s.ScheduleId;
        }

        public Schedule Retrieve(int scheduleId)
        {
            return _scdc.ScheduleDAOs.Where(ss => ss.ScheduleId == scheduleId)
                .Select(ss => new Schedule(ss)).FirstOrDefault();
        }

        public IEnumerable<Schedule> RetrieveMany(IEnumerable<int> ids)
        {
            return _scdc.ScheduleDAOs.Where(ss => ids.Contains(ss.ScheduleId))
                .Select(ss => new Schedule(ss));
        }

        public bool Update(Schedule sc)
        {
            ScheduleDAO s = _scdc.ScheduleDAOs.Where(ss => ss.ScheduleId == sc.ScheduleId).FirstOrDefault();
            if (null == s)
                return false;

            s.Name = sc.Name;

            // add new schedule elements
            if (null != sc.ScheduleElementIds)
                foreach (var seId in sc.ScheduleElementIds)
                    if (!(s.ScheduleToScheduleElementDAOs.Any(se => se.ScheduleElementId == seId)))
                        _scdc.ScheduleToScheduleElementDAOs.InsertOnSubmit(
                            new ScheduleToScheduleElementDAO() { ScheduleElementId = seId, ScheduleDAO = s });
            // remove old schedule elements
            foreach (var sse in s.ScheduleToScheduleElementDAOs)
                if (null != sc.ScheduleElementIds)
                {
                    if (!(sc.ScheduleElementIds.Contains(sse.ScheduleElementId)))
                        _scdc.ScheduleToScheduleElementDAOs.DeleteOnSubmit(sse);
                }
                else
                    _scdc.ScheduleToScheduleElementDAOs.DeleteOnSubmit(sse);

            // add new robots
            if (null != sc.ScheduleElementIds)
                foreach (var rId in sc.RobotIds)
                    if (!(s.RobotScheduleDAOs.Any(re => re.RobotId == rId)))
                        _scdc.RobotScheduleDAOs.InsertOnSubmit(
                            new RobotScheduleDAO() { RobotId = rId, ScheduleDAO = s });
            // remove old robots
            foreach (var rs in s.RobotScheduleDAOs)
                if (null != sc.ScheduleElementIds)
                {
                    if (!(sc.RobotIds.Contains(rs.RobotId)))
                        _scdc.RobotScheduleDAOs.DeleteOnSubmit(rs);
                }
                else
                    _scdc.RobotScheduleDAOs.DeleteOnSubmit(rs);

            _scdc.SubmitChanges();
            return true;
        }

        public bool Delete(int scheduleId)
        {
            ScheduleDAO s = _scdc.ScheduleDAOs.Where(ss => ss.ScheduleId == scheduleId).FirstOrDefault();
            if (null == s)
                return false;
            foreach (var sse in s.ScheduleToScheduleElementDAOs)
                _scdc.ScheduleToScheduleElementDAOs.DeleteOnSubmit(sse);
            foreach (var rs in s.RobotScheduleDAOs)
                _scdc.RobotScheduleDAOs.DeleteOnSubmit(rs);
            _scdc.ScheduleDAOs.DeleteOnSubmit(s);
            _scdc.SubmitChanges();
            return true;
        }

        public bool CheckSchedule(int scheduleId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ScheduleElement> GetElements(int scheduleId)
        {
            var item = _scdc.ScheduleDAOs.Where(s => s.ScheduleId == scheduleId).FirstOrDefault();
            var list = new List<ScheduleElement>();
            if (item == null)
                return list;
            foreach (var sse in item.ScheduleToScheduleElementDAOs)
                list.Add(new ScheduleElement(sse.ScheduleElementDAO));
            return list;
        }

        public int CreateElement(ScheduleElement el)
        {
            var dao = new ScheduleElementDAO(el);
            if (null == dao)
                return 0;
            _scdc.ScheduleElementDAOs.InsertOnSubmit(dao);
            _scdc.SubmitChanges();
            return dao.ScheduleElementId;
        }

        public ScheduleElement RetrieveElement(int scheduleElementId)
        {
            return _scdc.ScheduleElementDAOs.Where(dao => dao.ScheduleElementId == scheduleElementId).Select(s => new ScheduleElement(s)).FirstOrDefault();
        }

        public bool UpdateElement(ScheduleElement el)
        {
            ScheduleElementDAO s = _scdc.ScheduleElementDAOs.Where(ss => ss.ScheduleElementId == el.ScheduleElementId).FirstOrDefault();
            if (null == s)
                return false;
            s.Duration = el.Duration;
            if (s.Duration < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                s.Duration = new DateTime(1760, 1, 1);
            s.End = el.End;
            if (s.End < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                s.End = new DateTime(1760, 1, 1);
            s.Frequency = el.Frequency;
            if (s.Frequency < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                s.Frequency = new DateTime(1760, 1, 1);
            s.Name = el.Name;
            s.ScheduleKind = el.ScheduleKind;
            s.Start = el.Start;
            if (s.Start < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                s.Start = new DateTime(1760, 1, 1);
            _scdc.SubmitChanges();
            return true;
        }

        public bool DeleteElement(int elementId)
        {
            var item = _scdc.ScheduleElementDAOs.Where(el => el.ScheduleElementId == elementId).FirstOrDefault();
            if (null == item)
                return false;
            _scdc.ScheduleElementDAOs.DeleteOnSubmit(item);
            _scdc.SubmitChanges();
            return true;
        }

        public IEnumerable<int> ListAllElements()
        {
            return _scdc.ScheduleElementDAOs.Select(s => s.ScheduleElementId).AsEnumerable();
        }

        public IEnumerable<ScheduleElement> RetrieveManyElements(IEnumerable<int> ids)
        {
            return _scdc.ScheduleElementDAOs.Where(s => ids.Contains(s.ScheduleElementId)).Select(s => new ScheduleElement(s));
        }

        #endregion
    }
}
