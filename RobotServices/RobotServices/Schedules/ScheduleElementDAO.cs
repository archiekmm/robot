﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Schedules
{
    public partial class ScheduleElementDAO
    {
        public ScheduleElementDAO(ScheduleElement se)
            : this()
        {
            this.Duration = se.Duration;
            if (this.Duration < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                this.Duration = new DateTime(1760, 1, 1);
            this.End = se.End;
            if (this.End < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                this.End = new DateTime(1760, 1, 1);
            this.Frequency = se.Frequency;
            if (this.Frequency < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                this.Frequency = new DateTime(1760, 1, 1);
            this.Name = se.Name;
            this.ScheduleElementId = se.ScheduleElementId;
            this.ScheduleKind = se.ScheduleKind;
            this.Start = se.Start;
            if (this.Start < new DateTime(1760, 1, 1)) //1/1/1753 12:00:00 AM
                this.Start = new DateTime(1760, 1, 1);
        }
    }
}
