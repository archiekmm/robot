﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Schedules
{
    public partial class ScheduleDAO
    {
        public ScheduleDAO(Schedule sc)
            : this()
        {
            this.Name = sc.Name;
            //foreach (int robotId in sc.RobotIds)
            //    this.RobotSchedules.Add(new RobotSchedule() { RobotId = robotId, ScheduleId = this.ScheduleId });
            if (null != sc.ScheduleElementIds)
                foreach (int elementId in sc.ScheduleElementIds)
                    this._ScheduleToScheduleElementDAOs.Add(new ScheduleToScheduleElementDAO() { ScheduleElementId = elementId, ScheduleId = this.ScheduleId });
        }
    }
}
