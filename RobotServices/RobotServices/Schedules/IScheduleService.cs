﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Schedules
{
    // NOTE: If you change the interface name "IScheduleService" here, you must also update the reference to "IScheduleService" in App.config.
    [ServiceContract]
    public interface IScheduleService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> scheduleIds);

        [OperationContract]
        int Create(Schedule sc);

        [OperationContract]
        Schedule Retrieve(int scheduleId);

        [OperationContract]
        IEnumerable<Schedule> RetrieveMany(IEnumerable<int> scheduleIds);

        [OperationContract]
        bool Update(Schedule sc);

        [OperationContract]
        bool Delete(int scheduleId);

        [OperationContract]
        bool CheckSchedule(int scheduleId);

        [OperationContract]
        IEnumerable<ScheduleElement> GetElements(int scheduleId);

        [OperationContract]
        int CreateElement(ScheduleElement el);

        [OperationContract]
        ScheduleElement RetrieveElement(int scheduleElementId);

        [OperationContract]
        bool UpdateElement(ScheduleElement el);

        [OperationContract]
        bool DeleteElement(int elementId);

        [OperationContract]
        IEnumerable<int> ListAllElements();

        [OperationContract]
        IEnumerable<ScheduleElement> RetrieveManyElements(IEnumerable<int> ids);
    }
}
