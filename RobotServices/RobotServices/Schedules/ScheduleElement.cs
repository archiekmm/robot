﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Schedules
{
    [DataContract]
    public class ScheduleElement
    {
        [DataMember]
        public DateTime Duration;

        [DataMember]
        public DateTime End;

        [DataMember]
        public DateTime Frequency;

        [DataMember]
        public DateTime Start;

        [DataMember]
        public string Name;

        [DataMember]
        public int ScheduleElementId;

        [DataMember]
        public int ScheduleKind;

        public ScheduleElement(ScheduleElementDAO sed)
        {
            this.Duration = sed.Duration;
            this.End = sed.End;
            this.Frequency = sed.Frequency;
            this.Name = sed.Name;
            this.ScheduleElementId = sed.ScheduleElementId;
            this.ScheduleKind = sed.ScheduleKind;
            this.Start = sed.Start;
        }
    }
}
