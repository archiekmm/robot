﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Logs
{
    // NOTE: If you change the interface name "ILogService" here, you must also update the reference to "ILogService" in App.config.
    [ServiceContract]
    public interface ILogService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        // no names, so no ListWithNames or ListAllWithNames methods
        // should logs be named? -- no, these are log entries, not log configs

        [OperationContract]
        int Create(Log logMessage);

        [OperationContract]
        Log Retrieve(int logId);

        [OperationContract]
        IEnumerable<Log> RetrieveMany(IEnumerable<int> logIds);

        [OperationContract]
        bool Delete(int logId);
    }
}
