﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Logs
{
    // NOTE: If you change the class name "LogService" here, you must also update the reference to "LogService" in App.config.
    public class LogService : ILogService
    {
        protected LogClassesDataContext _lcdc = new LogClassesDataContext();

        #region ILogService Members

        public IEnumerable<int> ListAll()
        {
            return _lcdc.Logs.Select(l => l.LogId);
        }

        public IEnumerable<Log> RetrieveMany(IEnumerable<int> ids)
        {
            return _lcdc.Logs.Where(log => ids.Contains(log.LogId))
                .Select(log => new Log(log));
            //return _lcdc.Logs.Join(_lcdc.RobotLogs, l => l.LogId, rl => rl.LogId,
            //    (l, rl) => new { Log = l, RobotId = rl.RobotId})
            //    .Where(joined => joined.RobotId == robotId).Select(j => new Log(j.Log));
        }

        public int Create(Log logMessage)
        {
            LogDAO dao = new LogDAO(logMessage);
            if (null == dao)
                return 0;
            _lcdc.Logs.InsertOnSubmit(dao);
            _lcdc.SubmitChanges();
            return dao.LogId;
        }

        public Log Retrieve(int logId)
        {
            return _lcdc.Logs.Where(l => l.LogId == logId).Select(l => new Log(l)).FirstOrDefault();
        }

        public bool Delete(int logId)
        {
            LogDAO dao = _lcdc.Logs.Where(l => l.LogId == logId).FirstOrDefault();
            if (null == dao)
                return false;
            _lcdc.Logs.DeleteOnSubmit(dao);
            _lcdc.SubmitChanges();
            return true;
        }

        #endregion
    }
}
