﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Logs
{
    [DataContract]
    public class Log
    {
        [DataMember]
        public int LogId;

        [DataMember]
        public int Severity;

        [DataMember]
        public string Message;

        public Log(LogDAO dao)
        {
            this.LogId = dao.LogId;
            this.Severity = dao.Severity;
            this.Message = dao.Message;
        }

        public Log() { }
    }
}
