﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Logs
{
    public partial class LogDAO
    {
        public LogDAO(Log log)
        {
            this.Message = log.Message;
            this.Severity = log.Severity;
        }
    }
}
