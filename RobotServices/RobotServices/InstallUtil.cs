﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;

namespace Paragon.RobotServices
{
    class InstallUtil
    {
        private readonly AssemblyName _assemblyName;
        private readonly string _baseDirectory;
        private readonly string _fileName;
        private readonly string _installUtilExe;

        public InstallUtil(Assembly assembly)
        {
            _assemblyName = assembly.GetName();
            _fileName = Path.GetFullPath(assembly.Location);
            _baseDirectory = Path.GetDirectoryName(_fileName);

            _installUtilExe = Path.GetFullPath(Path.Combine(RuntimeEnvironment.GetRuntimeDirectory(), "InstallUtil.exe"));
            if (!File.Exists(_installUtilExe))
                throw new FileNotFoundException("InstallUtil.exe not found.", _installUtilExe);
        }

        public void Install(IEnumerable<string> moreargs)
        {
            if (0 != Run(true, moreargs))
                throw new ApplicationException(string.Format("InstallUtil failed to install {0}.", _assemblyName.Name));
        }

        public void Uninstall(IEnumerable<string> moreargs)
        {
            if (0 != Run(false, moreargs))
                throw new ApplicationException(string.Format("InstallUtil failed to uninstall {0}.", _assemblyName.Name));
        }

        private int Run(bool install, IEnumerable<string> moreargs)
        {
            string apppath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            string appdata = Path.Combine(apppath, _assemblyName.Name);
            if (!(Directory.Exists(appdata)))
                Directory.CreateDirectory(appdata);

            List<string> arguments = null;
            if (null != moreargs) 
                arguments = new List<string>(moreargs);
            else
                arguments = new List<string>();
            if (!install)
                arguments.Add("/uninstall");
            arguments.AddRange(new [] {
                "/ShowCallStack=true", 
                string.Format("/LogToConsole=true"),
                string.Format("/LogFile={0}", Path.Combine(appdata, "install.log")),
                string.Format("/InstallStateDir={0}", _baseDirectory),
                string.Format("{0}", _fileName),
            });
            int result = AppDomain.CurrentDomain.ExecuteAssembly(_installUtilExe, AppDomain.CurrentDomain.Evidence, arguments.ToArray());
            return result;
        }
    }
}
