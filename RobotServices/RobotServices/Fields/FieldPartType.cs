﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Fields
{
    internal enum FieldPartType
    {
        Literal,
        DateTime,
        Field
    }
}
