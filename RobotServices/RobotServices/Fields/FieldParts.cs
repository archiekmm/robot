﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Fields
{
    internal class FieldParts : List<FieldPart>
    {
        private const string cLeftBracket = "[";
        private const string cEscLeftBracket = @"\[";
        private const string cReplLeftBracket = "Ò";
        private const string cRightBracket = "]";
        private const string cEscRightBracket = @"\]";
        private const string cReplRightBracket = "Ó";

        public static string EncodeEscapedPivotChars(string input)
        {
            return input.Replace(cEscLeftBracket, cReplLeftBracket).Replace(cEscRightBracket, cReplRightBracket);
        }

        public static string DecodeEscapedPivotChars(string input)
        {
            return input.Replace(cReplLeftBracket, cLeftBracket).Replace(cEscRightBracket, cRightBracket);
        }

        public static FieldParts Parse(string value)
        {
            FieldParts rv = new FieldParts();

            // we get something like:  "Label-[Field:func{arg1|arg2}, func2{arg3}]-[Field3:func3]-def"
            // encode escaped "["s (i.e., the sequence "\["), and then split on the rest of the "["s
            string[] fields = EncodeEscapedPivotChars(value).Split(cLeftBracket.ToCharArray());
            for (int i = 0; i < fields.Count(); ++i)
            {
                // from our example, we get:  
                // fields[0] = "Label-"
                // fields[1] = "Field:func{arg1|arg2}, func2{arg3}]-"
                // fields[2] = "Field3:func3]-def"

                // fields[0] represents any leading literal characters
                if (i == 0)
                {   // so, we add a literal field part, then loop
                    if (fields[0] != string.Empty)
                        rv.Add(FieldPart.CreateLiteral(fields[0]));
                    // else no literal, nothing else to do for fields[0]
                    continue;
                }
                // for i > 0 each field[i] is a field (fields may contain trailing literal parts)
                // up to the next closing bracket represents a field, so we split on that
                string[] other = fields[i].Split(cRightBracket.ToCharArray());
                // e.g., from fields[2] we get other[0] = "Field3:func3" and other[1]="-def"
                // generally, our count will be one (just a field) or two (field + literal)
                int count = other.Count();
                if ((count < 1) || (count > 2))
                    rv.Add(FieldPart.CreateLiteral(fields[i])); // bad format, just treat it as a literal
                else
                {
                    FieldPart temp = FieldPart.CreateField(other[0]);
                    if (temp != null)
                        rv.Add(temp);
                    if ((count == 2) && (other[1] != string.Empty))
                        rv.Add(FieldPart.CreateLiteral(other[1]));
                }
            }
            return rv;
        }
        public void ParseValueString(string strInput)
        {
            FieldPart newPart = null;

            string strValue = strInput;
            List<string> lsSplit1 = new List<string>();
            List<string> lsSplit2 = new List<string>();
            List<string> lsSplit3 = new List<string>();

            this.Clear();

            while (true)
            {
                if (strValue == string.Empty)
                    break;

                // split on "["
                lsSplit1.Clear();
                lsSplit1.AddRange(strValue.Split("[".ToCharArray()));
                if (lsSplit1.Count == 0)
                    break;

                // if first item is not empty, there is a literal value
                if (lsSplit1[0] != string.Empty)
                {
                    newPart = new FieldPart();
                    newPart.PartType = FieldPartType.Literal;
                    newPart.LiteralText = lsSplit1[0];
                    this.Add(newPart);
                    newPart = null;
                }

                // if there is only one entry, there are no "[", and only the already-processed field literal is present
                if (lsSplit1.Count == 1)
                    break;

                // split the second portion again, this time on "]"
                lsSplit2.Clear();
                lsSplit2.AddRange(lsSplit1[1].Split("]".ToCharArray()));

                // if the count is zero, the "[" was not matched with a "]", and we are ignoring everything after the "["
                if (lsSplit2.Count == 0)
                    break;

                #region setup field
                // field name is not empty
                if (lsSplit2[0] != string.Empty)
                {
                    lsSplit3.Clear();
                    lsSplit3.AddRange(lsSplit2[0].Split(":".ToCharArray(), 2));
                    if (lsSplit3.Count == 1)
                    {
                        newPart = new FieldPart();
                        switch (lsSplit3[0].ToLowerInvariant())
                        {
                            case "cr":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = "\n";
                                break;
                            case "lf":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = "\r";
                                break;
                            case "br":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = "\n\r";
                                break;
                            case "tab":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = "\t";
                                break;
                            case "comma":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = ",";
                                break;
                            case "quote":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = "\"";
                                break;
                            case "colon":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = ":";
                                break;
                            case "semi":
                                newPart.PartType = FieldPartType.Literal;
                                newPart.LiteralText = ";";
                                break;
                            default:
                                newPart.PartType = FieldPartType.Field;
                                newPart.FieldName = lsSplit3[0];
                                break;
                        }
                        this.Add(newPart);
                        newPart = null;
                    }
                    else
                    {
                        newPart = new FieldPart();
                        switch (lsSplit3[0].ToLowerInvariant())
                        {
                            case "date":
                            case "time":
                            case "datetime":
                                newPart.PartType = FieldPartType.DateTime;
                                newPart.DateFormat = lsSplit3[1];
                                break;
                            default:
                                newPart.PartType = FieldPartType.Field;
                                newPart.FieldName = lsSplit3[0];
                                newPart.Parameters = lsSplit3[1];
                                break;
                        }
                        this.Add(newPart);
                        newPart = null;
                    }
                #endregion
                }
                if (lsSplit1.Count > 2)
                    strValue = lsSplit2[1] + "[" + string.Join("[", lsSplit1.Skip(2).ToArray());
                else
                    strValue = lsSplit2[1];
            }
        }

        public void DisplayAllParts()
        {
            //KeyValuePair<string, string> lookupPair;
            string strDisplay = string.Empty;
            string strParameters = string.Empty;

            foreach (FieldPart fieldPart in this)
            {
                switch (fieldPart.PartType)
                {
                    case FieldPartType.Literal:
                        strDisplay = "Literal:".PadRight(15) + "'" + fieldPart.LiteralText + "'";
                        break;
                    case FieldPartType.DateTime:
                        strDisplay = "Date/Time:".PadRight(15) + fieldPart.DateFormat;
                        break;
                    case FieldPartType.Field:
                        strDisplay = "Field:".PadRight(15) + fieldPart.FieldName + ":";
                        if (fieldPart.Trim)
                            strParameters = ",trim";
                        if (fieldPart.Wrap)
                            strParameters += ",wrap{" + fieldPart.WrapLength + "}";
                        if (fieldPart.DateField)
                            strParameters += ",date{" + fieldPart.DateFormat + "}";
                        if (fieldPart.Lookup)
                        {
                            strParameters += ",lookup{";
                            foreach (KeyValuePair<string, string> lookupPair in fieldPart.LookupTable)
                                strParameters += lookupPair.Key + "=" + lookupPair.Value + ";";
                            strParameters += "}";
                        }
                        if (fieldPart.Numeric)
                            strParameters += ",numeric{" + fieldPart.NumericFormat + "}";
                        if (fieldPart.Strip)
                            strParameters += ",strip{" + fieldPart.StripChars + "}";
                        if (fieldPart.Fixed)
                            strParameters += ",fixed{" + fieldPart.FixedLength + "}";
                        if (fieldPart.IndexString)
                            strParameters += ",index{" + fieldPart.IndexValue.ToString() + "|" + fieldPart.IndexDelimiter + "}";
                        if (fieldPart.ReplaceString)
                            strParameters += ",replace{" + fieldPart.ReplaceText + "|" + fieldPart.ReplaceWithText + "}";
                        if (fieldPart.StringFormat)
                            strParameters += ",format{" + fieldPart.StringFormatMask + "}";
                        if (fieldPart.Substring)
                            strParameters += ",substring{" + fieldPart.SubstringStart + "|" + fieldPart.SubstringLength + "}";
                        if (fieldPart.TruncateAfter)
                            strParameters += ",truncate{" + fieldPart.TruncateChars + "}";
                        if (fieldPart.TruncateAt)
                            strParameters += ",truncate{" + fieldPart.TruncatePos + "}";
                        if (strParameters.StartsWith(","))
                            strDisplay += strParameters.Substring(1); // skip first comma
                        break;
                }
                System.Diagnostics.Debug.Print(strDisplay);
            }
        }
    }
}
