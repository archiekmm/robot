﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Fields
{
    public partial class FieldDAO
    {
        public FieldDAO(Field fc)
            : this()
        {
            this.Name = fc.Name;
            this.Value = fc.Value;
        }

    }
}
