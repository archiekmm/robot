﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Fields
{
    // NOTE: If you change the class name "FieldService" here, you must also update the reference to "FieldService" in App.config.
    public class FieldService : IFieldService
    {
        protected FieldClassesDataContext _fdb = new FieldClassesDataContext();

        #region IFieldService Members

        public IEnumerable<int> ListAll()
        {
            return _fdb.Fields
                .Select(f => f.FieldId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _fdb.Fields
                .Select(f => new KeyValuePair<int, string>(f.FieldId, f.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _fdb.Fields.Where(f => ids.Contains(f.FieldId))
                .Select(f => new KeyValuePair<int, string>(f.FieldId, f.Name));
        }

        public int Create(Field fc)
        {
            FieldDAO f = new FieldDAO(fc);
            _fdb.Fields.InsertOnSubmit(f);
            _fdb.SubmitChanges();
            return f.FieldId;
        }

        public Field Retrieve(int fieldId)
        {
            return _fdb.Fields.Where(fi => fi.FieldId == fieldId)
                .Select(f => new Field(f)).FirstOrDefault();
        }

        public IEnumerable<Field> RetrieveMany(IEnumerable<int> fieldIds)
        {
            return _fdb.Fields.Where(f => fieldIds.Contains(f.FieldId))
                .Select(f => new Field(f));
        }

        public bool Update(Field fc)
        {
            FieldDAO f = _fdb.Fields.Where(fi => fi.FieldId == fc.FieldId).FirstOrDefault();
            if (null == f)
                return false;
            f.Name = fc.Name;
            f.Value = fc.Value;
            _fdb.SubmitChanges();
            return true;
        }

        public bool Delete(int fieldId)
        {
            FieldDAO f = _fdb.Fields.Where(fi => fi.FieldId == fieldId).FirstOrDefault();
            if (null == f)
                return false;
            _fdb.Fields.DeleteOnSubmit(f);
            _fdb.SubmitChanges();
            return true;
        }

        public IDictionary<string, string> TranslateFields(IEnumerable<int> fieldIds, IDictionary<string, string> inputs)
        {
            var rv = RetrieveMany(fieldIds).TranslateFields(inputs);
            return rv;
        }

        #endregion
    }
}
