﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Fields
{
    [DataContract]
    public class Field
    {
        [DataMember]
        public int FieldId;

        [DataMember]
        public string Name;

        [DataMember]
        public string Value
        {
            get { return _value; }
            set {
                if (value != _value)
                {
                    if (null == value)
                        _parts = null;
                    else
                        _parts = FieldParts.Parse(value);
                    _value = value;
                }
            }
        }

        protected string _value = null;

        public Field(FieldDAO f)
        {
            this.FieldId = f.FieldId;
            this.Name = f.Name;
            this.Value = f.Value;
        }

        public Field() { }

        internal FieldParts _parts = new FieldParts();

        internal FieldParts Parts
        {
            get { return _parts; }
        }

        public KeyValuePair<string, string> TranslateField(IDictionary<string, string> inputs)
        {
            string name = this.Name;
            string value = string.Empty;
            string partValue = string.Empty;

            if ((this.Value != string.Empty) && (this.Parts.Count == 0))
            {
                string temp = this.Value;
                this.Value = string.Empty;
                this.Value = temp;
            }
            foreach (FieldPart fp in this.Parts)
            {
                switch (fp.PartType)
                {
                    case FieldPartType.Literal:
                        value += fp.LiteralText;
                        break;
                    case FieldPartType.DateTime:
                        value += DateTime.Now.ToString(fp.DateFormat);
                        break;
                    case FieldPartType.Field:
                        if (inputs.ContainsKey(fp.FieldName))
                        {
                            partValue = TranslatePart(inputs[fp.FieldName], fp);
                            value += partValue;
                        }
                        break;
                }
            }
            return new KeyValuePair<string, string>(name, value);
        }

        private string TranslatePart(string startValue, FieldPart part)
        {
            List<string> subValues = new List<string>();
            //string returnValue = string.Empty;

            // operation order:
            //  1.  get indexed value
            if (part.IndexString)
            {
                subValues.Clear();
                subValues.AddRange(startValue.Split(part.IndexDelimiter.ToCharArray()));
                if ((subValues.Count == 0) || (subValues.Count < part.IndexValue))
                    startValue = string.Empty;
                else
                    startValue = subValues[part.IndexValue];
            }

            //  2.  trim
            if (part.Trim)
                startValue = startValue.Trim();

            //  3.  strip
            if (part.Strip)
            {
                for (int i = 0; i < part.StripChars.Length; ++i)
                    startValue = startValue.Replace(part.StripChars.Substring(i, 1), string.Empty);
            }

            //  4.  substring
            if (part.Substring)
            {
                int subStringEnd;
                // must start inside string
                if (part.SubstringStart < 0) part.SubstringStart = 0;
                if (part.SubstringStart >= startValue.Length) part.SubstringStart = startValue.Length - 1;
                subStringEnd = part.SubstringStart + part.SubstringLength;
                startValue = startValue.Substring(part.SubstringStart,
                    // can't take more string than there is
                    (subStringEnd < startValue.Length) ?
                    part.SubstringLength : (startValue.Length - part.SubstringStart));
            }

            //  4.  truncate at
            if (part.TruncateAt)
            {
                if ((part.TruncatePos > 1) && (part.TruncatePos <= startValue.Length))
                    startValue = startValue.Substring(0, part.TruncatePos);
            }

            //  5.  truncate after
            if (part.TruncateAfter)
            {
                if (part.TruncateChars.Length > 0)
                {
                    for (int i = 0; i < startValue.Length; ++i)
                    {
                        char ch = startValue[i];
                        if (part.TruncateChars.Contains(ch))
                        {
                            startValue = startValue.Substring(0, i);
                            break;
                        }
                    }
                }
            }

            //  6. replace
            if (part.ReplaceString)
            {
                if (part.ReplaceText.Length > 0)
                {
                    startValue = startValue.Replace(part.ReplaceText, part.ReplaceWithText);
                }
            }

            //  7.  format
            if (part.StringFormat)
            {
                string restOfMask = part.StringFormatMask.Replace("!", ""); // ignoring fill selector for now
                bool toUpper = false;
                bool toLower = false;
                bool alignLeft = false;
                //bool alignRight = true; default is to align right
                // if it has both toUpper and toLower specifiers, use whichever occurs later in the format string
                if ((restOfMask.Contains("<")) && (restOfMask.Contains(">")))
                {
                    if (restOfMask.IndexOf("<") < restOfMask.IndexOf(">"))
                        toUpper = true;
                    else
                        toLower = true;
                    restOfMask = restOfMask.Replace("<", "").Replace(">", "");
                }
                else if (restOfMask.Contains("<"))
                {
                    toLower = true;
                    restOfMask = restOfMask.Replace("<", "");
                }
                else if (restOfMask.Contains(">"))
                {
                    toUpper = true;
                    restOfMask = restOfMask.Replace(">", "");
                }

                // if it has both left and right align specifiers, use whichever occurs later in the format string
                if ((restOfMask.Contains("-")) && (restOfMask.Contains("+")))
                {
                    if (restOfMask.IndexOf("-") > restOfMask.IndexOf("+"))
                        alignLeft = true;
                    restOfMask = restOfMask.Replace("-", "").Replace("+", "");
                }
                else if (restOfMask.Contains("-"))
                {
                    alignLeft = true;
                    restOfMask = restOfMask.Replace("-", "");
                }
                else if (restOfMask.Contains("+"))
                {
                    restOfMask = restOfMask.Replace("+", "");
                }

                if (toUpper)
                    startValue = startValue.ToUpperInvariant();
                if (toLower)
                    startValue = startValue.ToLowerInvariant();
                if (restOfMask.Contains("@"))
                    startValue = string.Format("{0," + (alignLeft ? "-" : "") + restOfMask.ToCharArray().Count().ToString() + "}", startValue).Trim();
                else //if (restOfMask.Contains("&"))
                    startValue = string.Format("{0," + (alignLeft ? "-" : "") + restOfMask.ToCharArray().Count().ToString() + "}", startValue);

                //startValue = string.Format(part.StringFormatMask, startValue);
            }

            //  8.  fixed
            if (part.Fixed)
            {
                if (startValue.Length < part.FixedLength)
                    startValue = startValue.PadRight(part.FixedLength);
                else
                    startValue = startValue.Substring(0, part.FixedLength);
            }

            //  9.  DateField, Numeric, Lookup, and Wrap are mutally exclusive
            if (part.DateField)         //  9.a. datefield
            {
                DateTime temp;
                if (DateTime.TryParse(startValue, out temp))
                    startValue = temp.ToString(part.DateFormat);
            }
            else if (part.Numeric)      //  9.b. numeric
            {
                string digits = "0123456789";
                string temp = "";
                // strip non-numerics
                for (int i = 0; i < startValue.Length; ++i)
                    if (digits.Contains(startValue[i]))
                        temp += startValue[i].ToString();
                startValue = double.Parse(temp).ToString(part.NumericFormat);
            }
            else if (part.Lookup)       //  9.c. lookup
            {
                if (startValue.Length > 0)
                {
                    if (part.LookupTable.ContainsKey(startValue))
                        startValue = part.LookupTable[startValue];
                    else if (part.LookupTable.ContainsKey("*"))
                        startValue = part.LookupTable["*"];
                }
            }
            else if (part.Wrap)         //  9.d. wrap
            {
                startValue = WrapText(startValue, part.WrapLength);
            }

            return startValue;
        }

        private string WrapText(string input, int lineLength)
        {
            int spaceLeft = lineLength;
            string output = string.Empty;

            if (input.Trim().Length == 0)
                return string.Empty;
            input = input.Replace("\r\n", "\n").Replace("\n\r", "\n");
            foreach (string word in input.Split(new char[] { ' ', '\r', '\n' }))
            {
                if (word.Length > spaceLeft)
                {
                    output += System.Environment.NewLine + word + " ";
                    spaceLeft = lineLength - (word.Length + 1);
                }
                else
                {
                    output += word + " ";
                    spaceLeft -= word.Length + 1;
                }
            }

            return output;
        }
    }
}
