﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Fields
{
    // NOTE: If you change the interface name "IFieldService" here, you must also update the reference to "IFieldService" in App.config.
    [ServiceContract]
    public interface IFieldService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> fieldIds);

        [OperationContract]
        int Create(Field fc);

        [OperationContract]
        Field Retrieve(int fieldId);

        [OperationContract]
        IEnumerable<Field> RetrieveMany(IEnumerable<int> fieldIds);

        [OperationContract]
        bool Update(Field fc);

        [OperationContract]
        bool Delete(int fieldId);

        [OperationContract]
        IDictionary<string, string> TranslateFields(IEnumerable<int> fieldIds, IDictionary<string, string> inputs);

    }
}
