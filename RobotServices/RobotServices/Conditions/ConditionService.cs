﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Conditions
{
    public class ConditionService : IConditionService
    {
        protected ConditionClassesDataContext _cdb = new ConditionClassesDataContext();

        #region IConditionService Members

        #region CRUD Ops

        public IEnumerable<int> ListAll()
        {
            return _cdb.Conditions.Select(dao => dao.ConditionId);
        }

        // no names, so no ListWithNames method

        public int Create(Condition cc)
        {
            ConditionDAO c = new ConditionDAO(cc);
            _cdb.Conditions.InsertOnSubmit(c);
            _cdb.SubmitChanges();
            return c.ConditionId;
        }

        public Condition Retrieve(int condId)
        {
            ConditionDAO c = _cdb.Conditions.Where(cc => cc.ConditionId == condId).FirstOrDefault();
            if (null == c)
                return null;
            return new Condition(c);
        }

        public IEnumerable<Condition> RetrieveMany(IEnumerable<int> ids)
        {
            return _cdb.Conditions.Where(dao => ids.Contains(dao.ConditionId))
                .Select(dao => new Condition(dao));
        }

        public bool Update(Condition cc)
        {
            ConditionDAO c = _cdb.Conditions.Where(ccc => ccc.ConditionId == cc.ConditionId).FirstOrDefault();
            if (null == c)
                return false;
            c.CompareMethod = cc.CompareMethod;
            c.Conditional = cc.Conditional;
            //c.ConditionId = cc.ConditionId;
            c.Connector = cc.Connector;
            c.Operand1 = cc.Operand1;
            c.Operand2 = cc.Operand2;
            _cdb.SubmitChanges();
            return true;
        }

        public bool Delete(int condId)
        {
            ConditionDAO c = _cdb.Conditions.Where(ccc => ccc.ConditionId == condId).FirstOrDefault();
            if (null == c)
                return false;
            _cdb.Conditions.DeleteOnSubmit(c);
            _cdb.SubmitChanges();
            return true;
        }

        #endregion

        public bool Evaluate(IEnumerable<int> condIds, IDictionary<string, string> inputs)
        {
            var cList = RetrieveMany(condIds);
            
            //List<ConditionDAO> cList = new List<ConditionDAO>();
            //foreach (int condId in condIds)
            //{
            //    ConditionDAO c = _cdb.Conditions.Where(ccc => ccc.ConditionId == condId).FirstOrDefault();
            //    if (null == c)
            //        continue;
            //    cList.Add(c);
            //}
            return cList.Evaluate(inputs);
        }

        #endregion
    }
}
