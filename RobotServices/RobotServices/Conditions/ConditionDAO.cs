﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Paragon.RobotServices.Fields;

namespace Paragon.RobotServices.Conditions
{
    public enum Connector
    {
        And,
        Or
    }

    public enum CompareMethod
    {
        Text,
        Binary
    }

    public enum Conditional
    {
        Equals,
        NotEquals,
        IsBlank,
        IsNotBlank,
        StartsWith,
        EndsWith,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Contains,
        InList,
        NotInList,
        Like,
        NotLike
    }

    public partial class ConditionDAO
    {
        public ConditionDAO(Condition cc) : this()
        {
            this._CompareMethod = cc.CompareMethod;
            this._Conditional= cc.Conditional;
            this._ConditionId = cc.ConditionId;
            this._Connector = cc.Connector;
            this._Operand1 = cc.Operand1;
            this._Operand2 = cc.Operand2;
        }

    }
}
