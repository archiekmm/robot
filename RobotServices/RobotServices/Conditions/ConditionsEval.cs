﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Conditions
{
    public static class ConditionsEval
    {
        public static bool Evaluate(this IEnumerable<Condition> conditions, IDictionary<string, string> inputs)
        {
            bool rv = true;

            // No conditions === "unconditional" === always true
            if (conditions.Count() == 0)
                return true;

            foreach (var cond in conditions)
            {
                bool interim = cond.Evaluate(inputs);
                switch (cond.Connector)
                {
                    case Connector.And: // AND
                        rv = (rv && interim);
                        break;
                    case Connector.Or: // OR
                        rv = (rv || interim);
                        break;
                }
            }
            return rv;
        }
    }
}
