﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Paragon.RobotServices.Conditions
{
    [ServiceContract]
    public interface IConditionService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        int Create(Condition cc);

        [OperationContract]
        Condition Retrieve(int condId);

        [OperationContract]
        IEnumerable<Condition> RetrieveMany(IEnumerable<int> conditionIds);

        [OperationContract]
        bool Update(Condition cc);

        [OperationContract]
        bool Delete(int condId);

        [OperationContract]
        bool Evaluate(IEnumerable<int> condIds, IDictionary<string, string> inputs);

    }
}
