﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Server
{
    public class Server
    {
        public string Host { get; set; }
        public string Name { get; set; }
        public int Port { get; set; }

        public List<Robots.Robot> Robots { get; set; }
        public List<Modules.Module> Modules { get; set; }

    }
}
