﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Utilities;

namespace Paragon.RobotServices.Robots
{
    public class Robot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public bool Repeats { get; set; }
        public int SleepTime { get; set; }

        public List<Schedules.Schedule> Schedules { get; set; }
        public Notification Notifier { get; set; }
        public Normalizers.Normalizer Normalizer { get; set; }
        public string Lookup { get; set; }
        public string Mapping { get; set; }

        public int InputModuleId { get; set; }
        public int OutputModuleId { get; set; }
        public XmlDictionary<int, string> InputSettings { get; set; }
        public XmlDictionary<int, string> OutputSettings { get; set; }

    }
}
