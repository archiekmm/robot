﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Paragon.RobotServices.Robots
{
    [DataContract]
    public class RobotOld
    {
        [DataMember]
        public bool IsScheduled;

        [DataMember]
        public string Name;

        [DataMember]
        public bool Repeats;

        [DataMember]
        public int RobotId;

        [DataMember]
        public int SleepTime;

        [DataMember]
        public string Status;

        [DataMember]
        public Dictionary<int, string> ModuleIds = new Dictionary<int, string>();
        //public List<int> ModuleIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> ScheduleIds = new Dictionary<int, string>();
        //public List<int> ScheduleIds = new List<int>();

        [DataMember]
        public List<int> LogIds = new List<int>();

        [DataMember]
        public Dictionary<int, string> SettingIds = new Dictionary<int, string>();
        //public List<int> SettingIds = new List<int>();

        [DataMember]
        public List<int> ServerIds = new List<int>();

        public RobotOld(RobotDAO r)
        {
            this.IsScheduled = r.IsScheduled;
            this.Name = r.Name;
            this.Repeats = r.Repeats;
            this.RobotId = r.RobotId;
            this.SleepTime = r.SleepTime;
            foreach (var rl in r.RobotLogs)
                this.LogIds.Add(rl.LogId);
            foreach (var rm in r.RobotModules)
                this.ModuleIds.Add(rm.ModuleId, rm.Module.Name);
            foreach (var rs in r.RobotSchedules)
                this.ScheduleIds.Add(rs.ScheduleId, rs.Schedule.Name);
            foreach (var rs in r.RobotServers)
                this.ServerIds.Add(rs.ServerId);
            foreach (var rs in r.RobotSettings)
                this.SettingIds.Add(rs.SettingId, rs.Setting.Name);
            RobotService rsvc = new RobotService();
            this.Status = rsvc.GetStatus(r.RobotId);
        }

        public RobotOld() 
        {
        }
    }
}
