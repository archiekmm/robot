﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Robots
{
    public partial class RobotDAO
    {
        public RobotDAO(Robot rc)
            : this()
        {
            this.IsScheduled = rc.IsScheduled;
            this.Name = rc.Name;
            this.Repeats = rc.Repeats;
            this.SleepTime = rc.SleepTime;
            if (rc.LogIds != null)
                this.RobotLogs.AddRange(rc.LogIds.Select(logId => new RobotLogDAO() { LogId = logId, Robot = this}));
            //foreach (int logId in rc.LogIds)
            //    this.RobotLogs.Add(new RobotLogDAO() { LogId = logId, Robot = this });
            if (rc.ModuleIds != null)
                this.RobotModules.AddRange(rc.ModuleIds.Select(modId => new RobotModuleDAO() { ModuleId = modId.Key, Robot = this }));
            //foreach (var moduleId in rc.ModuleIds)
            //    this.RobotModules.Add(new RobotModuleDAO() { ModuleId = moduleId.Key, Robot = this });
            if (rc.ScheduleIds != null)
                this.RobotSchedules.AddRange(rc.ScheduleIds.Select(schedId => new RobotScheduleDAO() { ScheduleId = schedId.Key, Robot = this }));
            //foreach (var scheduleId in rc.ScheduleIds)
            //    this.RobotSchedules.Add(new RobotScheduleDAO() { ScheduleId = scheduleId.Key, Robot = this });
            if (rc.ServerIds != null)
                this.RobotServers.AddRange(rc.ServerIds.Select(servId => new ServerRobotDAO() {  ServerId =  servId, Robot = this }));
            //foreach (int serverId in rc.ServerIds)
            //    this.RobotServers.Add(new ServerRobotDAO() { ServerId = serverId, Robot = this });
            if (rc.SettingIds != null)
                this.RobotSettings.AddRange(rc.SettingIds.Select(setId => new RobotSettingDAO() { SettingId= setId.Key, Robot = this }));
            //foreach (var settingId in rc.SettingIds)
            //    this.RobotSettings.Add(new RobotSettingDAO() { SettingId = settingId.Key, Robot = this });
        }
    }
}
