﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Robots
{
    // NOTE: If you change the interface name "IRobotService" here, you must also update the reference to "IRobotService" in App.config.
    [ServiceContract]
    public interface IRobotService
    {
        [OperationContract]
        IEnumerable<int> ListAll();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListAllWithNames();

        [OperationContract]
        IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> robotIds);

        [OperationContract]
        int Create(Robot rc);

        [OperationContract]
        Robot Retrieve(int robotId);

        [OperationContract]
        IEnumerable<Robot> RetrieveMany(IEnumerable<int> robotIds);

        [OperationContract]
        bool Update(Robot rc);

        [OperationContract]
        bool Delete(int robotId);

        [OperationContract]
        void Start(int robotId);

        [OperationContract]
        void Stop(int robotId);

        [OperationContract]
        void Pause(int robotId);

        [OperationContract]
        void Resume(int robotId);

        [OperationContract]
        string GetStatus(int robotId);
    }
}
