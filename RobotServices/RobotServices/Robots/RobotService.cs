﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Paragon.RobotServices.Robots
{
    // NOTE: If you change the class name "RobotService" here, you must also update the reference to "RobotService" in App.config.
    public class RobotService : IRobotService
    {
        protected RobotClassesDataContext _rcdc = new RobotClassesDataContext();
        protected static Dictionary<int, RobotWorker> workers = new Dictionary<int, RobotWorker>();

        public IEnumerable<int> ListAll()
        {
            return _rcdc.Robots
                .Select(r => r.RobotId);
        }

        public IEnumerable<KeyValuePair<int, string>> ListAllWithNames()
        {
            return _rcdc.Robots
                .Select(r => new KeyValuePair<int, string>(r.RobotId, r.Name));
        }

        public IEnumerable<KeyValuePair<int, string>> ListWithNames(IEnumerable<int> ids)
        {
            return _rcdc.Robots.Where(r => ids.Contains(r.RobotId))
                .Select(r => new KeyValuePair<int, string>(r.RobotId, r.Name));
        }

        // need to remove this:  robots don't care what server they're on
        //  (er, this server, duh!) -- are ignorant of servers sharing the DB, too.
        // instead, need the WebApp to keep server/robot associations in its config
        public IEnumerable<Robot> ListByServer(int serverId)
        {
            var serverBots = _rcdc.ServerRobots.Where(sr => sr.ServerId == serverId).Select(sr => sr.RobotId);
            return _rcdc.Robots.Where(r => serverBots.Contains(r.RobotId)).Select(r => new Robot(r));
        }

        public int Create(Robot rc)
        {
            RobotDAO r = new RobotDAO(rc);
            _rcdc.Robots.InsertOnSubmit(r);
            _rcdc.SubmitChanges();
            return r.RobotId;
        }

        public Robot Retrieve(int robotId)
        {
            return _rcdc.Robots.Where(rr => rr.RobotId == robotId)
                .Select(r => new Robot(r)).FirstOrDefault();
        }

        public IEnumerable<Robot> RetrieveMany(IEnumerable<int> ids)
        {
            return _rcdc.Robots.Where(r => ids.Contains(r.RobotId))
                .Select(r => new Robot(r));
        }

        public bool Update(Robot rc)
        {
            var r = _rcdc.Robots.Where(rr => rr.RobotId == rc.RobotId).FirstOrDefault();
            if (null == r)
                return false;
            r.IsScheduled = rc.IsScheduled;
            r.Name = rc.Name;
            r.Repeats = rc.Repeats;
            r.SleepTime = rc.SleepTime;

            // add new modules
            foreach(var moduleId in rc.ModuleIds)
                if (!(r.RobotModules.Any(rm => rm.ModuleId == moduleId.Key)))
                    _rcdc.RobotModules.InsertOnSubmit(new RobotModuleDAO() { ModuleId = moduleId.Key, Robot = r });
            // remove old modules
            foreach (var rm in r.RobotModules)
                if (!(rc.ModuleIds.Keys.Contains(rm.ModuleId)))
                    _rcdc.RobotModules.DeleteOnSubmit(rm);

            // add new schedules
            foreach(var scheduleId in rc.ScheduleIds)
                if (!(r.RobotSchedules.Any(rs => rs.ScheduleId == scheduleId.Key)))
                    _rcdc.RobotSchedules.InsertOnSubmit(new RobotScheduleDAO() { ScheduleId = scheduleId.Key, Robot = r });
            // remove old schedules
            foreach (var rs in r.RobotSchedules)
                if (!(rc.ScheduleIds.Keys.Contains(rs.ScheduleId)))
                    _rcdc.RobotSchedules.DeleteOnSubmit(rs);

            _rcdc.SubmitChanges();
            return true;
        }

        public bool Delete(int robotId)
        {
            var r = _rcdc.Robots.Where(rr => rr.RobotId == robotId).FirstOrDefault();
            if (null == r)
                return false;

            foreach (var rm in r.RobotModules)
                _rcdc.RobotModules.DeleteOnSubmit(rm);
            foreach (var rs in r.RobotSchedules)
                _rcdc.RobotSchedules.DeleteOnSubmit(rs);
            _rcdc.Robots.DeleteOnSubmit(r);

            _rcdc.SubmitChanges();
            return true;
        }

        public void Start(int robotId)
        {
            RobotWorker rw = null;
            if (workers.ContainsKey(robotId))
                rw = workers[robotId];
            else
            {
                var r = _rcdc.Robots.Where(rr => rr.RobotId == robotId).FirstOrDefault();
                if (null == r)
                    return;
                rw = new RobotWorker(r);
                workers.Add(robotId, rw);
            }
            rw.Start();
        }

        public void Stop(int robotId)
        {
            if (workers.ContainsKey(robotId))
                workers[robotId].Stop();
        }

        public void Pause(int robotId)
        {
            if (workers.ContainsKey(robotId))
                workers[robotId].Pause();
        }

        public void Resume(int robotId)
        {
            if (workers.ContainsKey(robotId))
                workers[robotId].Resume();
        }

        public string GetStatus(int robotId)
        {
            if (workers.ContainsKey(robotId))
                return workers[robotId].Status;
            else
                return "stopped";
        }
    }
}
