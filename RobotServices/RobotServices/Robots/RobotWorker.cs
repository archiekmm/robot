﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Paragon.RobotServices.Utilities;
using Paragon.RobotServices.Plugin;

namespace Paragon.RobotServices.Robots
{
    public class RobotWorker
    {
        public string Status;
        public string Name;
        public bool Repeats;
        public bool IsScheduled;
        public TimeSpan SleepTime;
        //public PlugInBase PlugIn;
        public SynchronizedBool Abort;

        protected PlugInTaskInfo piti = null;
        protected ManualResetEvent sleepAbortHandle = null;
        protected Thread pluginThread = null;
        protected Thread contextThread = null;

        public RobotWorker(Robot r)
        {
            this.Status = "uninitialized";
            this.Name = r.Name;
            this.Repeats = r.Repeats;
            this.IsScheduled = r.IsScheduled;
            this.SleepTime = new TimeSpan(r.SleepTime * TimeSpan.TicksPerSecond);
            // or
            //this.SleepTime = new TimeSpan(0, 0, r.SleepTime);

            this.piti = new Utilities.PlugInTaskInfo()
                {   ContextHandle = new AutoResetEvent(false),
                    ControlHandle = new ManualResetEvent(false),
                    PluginHandle = new AutoResetEvent(false),
                    KeepRunning = new SynchronizedBool(true),
                    PageCount = 0 };
            this.sleepAbortHandle = new ManualResetEvent(false);
            this.Abort = new Utilities.SynchronizedBool(false);
            this.PlugIn = CreatePluginFromModule(r.RobotModules.FirstOrDefault().ModuleId);
        }

        public void Start() 
        {
            this.Status = "starting";
            this.Abort.Value = false;
            this.sleepAbortHandle.Reset();
            this.contextThread = new Thread(new ParameterizedThreadStart(RunContext));
            this.contextThread.Start(piti.ControlHandle);
            while (!this.contextThread.IsAlive)
                Thread.Sleep(0);
            this.piti.ControlHandle.Set();
            this.Status = "running";
        }
        
        public void Stop() 
        {
            this.Status = "stopping";
            this.piti.ControlHandle.Reset();
            this.Abort.Value = true;
            this.sleepAbortHandle.Set();
            this.piti.ControlHandle.Set();
            if (null != this.contextThread)
                this.contextThread.Join();
            this.Status = "stopped";
        }

        public void Pause() 
        {
            this.Status = "pausing";
            this.piti.ControlHandle.Reset();
            this.Status = "paused";
        }

        public void Resume() 
        {
            this.Status = "resuming";
            this.piti.ControlHandle.Set();
            this.Status = "running";
        }

        protected void RunContext(object state)
        {
            ManualResetEvent controller = (ManualResetEvent)state;
            bool keepGoing = true;
            piti.KeepRunning.Value = true;
            //logger.Log(NLog.LogLevel.Trace, "Starting RunPlugin thread.");
            this.pluginThread = new Thread(new ParameterizedThreadStart(RunPlugin));
            this.pluginThread.Start(this.piti);
            while (!this.pluginThread.IsAlive)
                Thread.Sleep(0);
            //logger.Log(NLog.LogLevel.Trace, "RunPlugin thread is alive.");
            while (keepGoing)
            {
                //logger.Log(LogLevel.Trace, "RunContext waiting on controller.");
                controller.WaitOne();
                keepGoing = KeepRunning();
                //logger.Log(LogLevel.Trace, "RunContext finished waiting on controller, updating keepRunning to {0}", keepGoing);
                this.piti.KeepRunning.Value = keepGoing;
                //logger.Log(LogLevel.Trace, "RunContext assigned keepRunning, signalling pluginHandle and waiting on contextHandle.");
                WaitHandle.SignalAndWait(piti.PluginHandle, piti.ContextHandle);
                //logger.Log(LogLevel.Trace, "RunContext finished waiting on contextHandle, sleeping.");
                // WaitOne with a timeout parameter waits for either a signal or a timeout,
                // returns true if the signal was received or false if the timeout passed
                // so if it returns true, we need to abort.
                if (sleepAbortHandle.WaitOne(this.SleepTime))
                {
                    // we were signalled
                    this.piti.KeepRunning.Value = keepGoing = false;
                    // wake up the plugin so it can stop
                    if (this.pluginThread.IsAlive)
                        WaitHandle.SignalAndWait(piti.PluginHandle, piti.ContextHandle);
                    // condition would fail when we tried to loop, but we'll be explicit
                    break;
                }
            }
            if (null != this.pluginThread)
                this.pluginThread.Join();
            Console.WriteLine("Context thread execution completed.");
            //logger.Log(LogLevel.Trace, "keepGoing was false or sleep was aborted, exiting thread.");
        }

        protected void RunPlugin(object state)
        {
            PlugInTaskInfo pi = (PlugInTaskInfo)state;
            while (pi.KeepRunning.Value)
            {
                //logger.Log(LogLevel.Trace, "RunPlugin blocking on waithandles.");
                // i think i need to wait on both context and control wait handles
                WaitHandle.WaitAll(new WaitHandle[] { pi.PluginHandle, pi.ControlHandle});
                //logger.Log(LogLevel.Trace, "RunPlugin finished blocking.");
                if (pi.KeepRunning.Value)
                {
                    //logger.Log(LogLevel.Trace, "RunPlugin keeps going, doing work.");
                    long pc = this.PlugIn.Run();
                    Interlocked.Exchange(ref pi.PageCount, pc);
                    //logger.Log(LogLevel.Trace, "DoWork completed {0} pages of work; signalling context.", pc);
                    pi.ContextHandle.Set();
                    //logger.Log(LogLevel.Trace, "RunPlugin signalled context.");
                }
                else
                {
                    //logger.Log(LogLevel.Trace, "RunPlugin stopping work due to keepGoing == false.");
                    Interlocked.Exchange(ref pi.PageCount, 0);
                    pi.ContextHandle.Set();
                    //logger.Log(LogLevel.Trace, "RunPlugin returning.");
                    return;
                }
                //logger.Log(LogLevel.Trace, "RunPlugin looping, keepRunning is {0}", pi.keepRunning.Value);
            }
            Console.WriteLine("Plugin thread execution completed.");
        }

        protected bool KeepRunning()
        {
            //logger.Log(NLog.LogLevel.Trace,
            //    "Context.KeepRunning inputs: abort: {0}; scheduled: {1}, repeat: {2}, schedule.Check(): {3}",
            //    abort.Value, scheduled, repeat, schedule.CheckSchedule());
            if (Abort.Value)
                return false;
            if (IsScheduled && Repeats)
                //return schedule.CheckSchedule();
                return true; // temporary until we get schedule going
            if (!IsScheduled && Repeats)
                return true;
            return false;
        }

        protected PlugInBase CreatePluginFromModule(int moduleId)
        {
            Modules.ModuleService ms = new Paragon.RobotServices.Modules.ModuleService();
            var m = ms.Retrieve(moduleId);
            //Settings.SettingService ss = new Paragon.RobotServices.Settings.SettingService();
            string dllName = m.ModuleKind;
            Type pluginType = dllName.LoadAssemblyAndGetType();
            //var settings = ss.RetrieveMany(m.SettingIds.Keys);
            return (PlugInBase)System.Activator.CreateInstance(pluginType, moduleId);
        }
    }
}
