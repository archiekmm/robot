﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices
{
    public class Repository
    {
        public ObservableCollection<Robots.Robot> Robots { get; protected set; }
        public ObservableCollection<Modules.Module> Modules { get; protected set; }

        public Repository()
        {
            Robots = new ObservableCollection<Robots.Robot>();
            Modules = new ObservableCollection<Modules.Module>();
            
        }
    }
}
