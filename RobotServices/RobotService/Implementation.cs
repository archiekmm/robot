﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using System.Reflection;
using System.Xml.Linq;

namespace Paragon.RobotServices.Service
{
    class Implementation : IDisposable
    {
        protected const string configName = "config.xml";

        public static readonly string LogPath = System.IO.Path.Combine(@"C:\Users\Public\Documents\", "robotservice.log");

        private static WebServiceHost robotHost = null;
        private static WebServiceHost moduleHost = null;
        string configPath = null;
        string path = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "robotservice.log");
        public void Start(ICollection<string> arguments)
        {
            //return;
            int port = 8080;
            string robotPath = "robots/";
            string modulePath = "modules/";
            //string LogPath = System.IO.Path.Combine(@"C:\Users\Public\Documents\", "robotservice.log");
            try
            {
                //configPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                //configPath = System.IO.Path.Combine(configPath, typeof(Implementation).Assembly.GetName().Name);
                //configPath = Path.Combine(configPath, configName);
                //if (File.Exists(configPath))
                //{
                //    XElement el = XElement.Load(configPath);
                //    if (null != el.Element("port"))
                //        if (!int.TryParse(el.Element("port").Value, out port))
                //            port = 8080;
                //    if (null != el.Element("robotPath"))
                //        robotPath = el.Element("robotPath").Value;
                //    if (null != el.Element("modulePath"))
                //        modulePath = el.Element("modulePath").Value;
                //}
                var uriBuilder = new UriBuilder();
                var hostName = System.Net.Dns.GetHostName();
                uriBuilder.Host = hostName;
                uriBuilder.Scheme = "http";
                uriBuilder.Port = port;
                uriBuilder.Path = robotPath;
                //var baseUri = "http://va-vpc-vs2010:8080/";
                //Console.WriteLine(uriBuilder.Uri);
                System.IO.File.AppendAllText(LogPath,
                    string.Format("{0}:  hosting robot service at {1}\n", 
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), uriBuilder.Uri.ToString()));
                robotHost = new WebServiceHost(typeof(RobotService), uriBuilder.Uri); // new Uri(baseUri + "robots/"));
                var ep = robotHost.AddServiceEndpoint(typeof(IRobotService), new WebHttpBinding(), "");
                var beh = robotHost.Description.Behaviors.Find<System.ServiceModel.Description.ServiceDebugBehavior>();
                beh.IncludeExceptionDetailInFaults = true;
                System.IO.File.AppendAllText(LogPath, 
                    string.Format("{0}:  robot service opened", 
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                robotHost.Open();
                uriBuilder.Path = modulePath;
                //Console.WriteLine(uriBuilder.Uri);
                System.IO.File.AppendAllText(LogPath, 
                    string.Format("{0}:  hosting module service at {1}\n",
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),uriBuilder.Uri.ToString()));
                moduleHost = new WebServiceHost(typeof(ModuleService), uriBuilder.Uri); // new Uri(baseUri + "robots/"));
                var ep2 = moduleHost.AddServiceEndpoint(typeof(IModuleService), new WebHttpBinding(), "");
                beh = moduleHost.Description.Behaviors.Find<System.ServiceModel.Description.ServiceDebugBehavior>();
                beh.IncludeExceptionDetailInFaults = true;
                moduleHost.Open();
                System.IO.File.AppendAllText(LogPath, 
                    string.Format("{0}:  module service opened",
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(LogPath, 
                    string.Format("\n{0}:  Exception:  {1}",
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.ToString()));
                throw ex;
            }
            //Console.Write("Service is running.");
            //Console.Read();
            //RobotService.StopAllRobots();
            //Console.Read();
            //host.Close();
            //host2.Close();

        }

        public void Stop()
        {
            Pause();
            robotHost = null;
            moduleHost = null;
        }

        public void Pause()
        {
            if (null != robotHost)
                robotHost.Close();
            if (null != moduleHost)
                moduleHost.Close();
        }

        public void Continue()
        {
            if ((null != robotHost) && (null != moduleHost))
            {
                robotHost.Open();
                moduleHost.Open();
            }
            else
            {
                Stop();
                Start(null);
            }
        }

        public void Dispose()
        {
            if (null != robotHost)
                robotHost.Close();
            if (null != moduleHost)
                moduleHost.Close();
            robotHost = null;
            moduleHost = null;
        }
    }
}
