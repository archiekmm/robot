﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using System.ComponentModel;

namespace Paragon.RobotServices.Service
{
    [RunInstaller(true)]
    public class Installer : System.Configuration.Install.Installer
    {
        private readonly ServiceProcessInstaller _installProcess;
        private readonly ServiceInstaller _installService;

        public Installer()
        {
            _installProcess = new ServiceProcessInstaller();
            _installProcess.Account = ServiceAccount.LocalSystem;

            _installService = new ServiceInstaller();
            _installService.StartType = ServiceStartMode.Automatic;
            _installService.DelayedAutoStart = true;
            
            _installService.Installers.Clear();

            Installers.Add(_installProcess);
            Installers.Add(_installService);
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            if (Context.Parameters.ContainsKey("username"))
                _installProcess.Account = ServiceAccount.User;

            string svcName = Context.Parameters["ServiceName"];
            string displayName = Context.Parameters["DisplayName"];
            if (string.IsNullOrEmpty(svcName))
                throw new ArgumentException("Missing required parameter 'ServiceName'.");
            _installService.ServiceName = svcName;
            _installService.DisplayName = string.IsNullOrEmpty(displayName) ? svcName : displayName;
            stateSaver.Add("ServiceName", svcName);
            if (!string.IsNullOrEmpty(displayName))
                stateSaver.Add("DisplayName", displayName);

            // run the install
            base.Install(stateSaver);
        }

        public override void Rollback(System.Collections.IDictionary savedState)
        {
            _installService.ServiceName = (string)savedState["ServiceName"];
            base.Rollback(savedState);
        }

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            _installService.ServiceName = (string)savedState["ServiceName"];
            base.Uninstall(savedState);
        }
    }
}
