﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using Paragon.RobotServices.Model;

namespace Paragon.RobotServices.Service
{
    [ServiceContract]
    public interface IModuleService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/")]
        IList<Module> List();

        [OperationContract]
        [WebGet(UriTemplate = "/{id}")]
        Module GetModule(string id);
    }
}
