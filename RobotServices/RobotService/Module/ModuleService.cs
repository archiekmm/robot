﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;
using System.ServiceModel.Web;

namespace Paragon.RobotServices.Service
{
    public class ModuleService : IModuleService
    {
        protected static List<Module> Modules;

        public IList<Module> List()
        {
            return Modules;
        }

        public Module GetModule(string id)
        {
            int modId;
            if (!int.TryParse(id, out modId))
                throw new WebFaultException<string>("Id value is not an integer.", System.Net.HttpStatusCode.BadRequest);
            var rv = Modules.Where(bot => bot.Id == modId).SingleOrDefault();
            if (null == rv)
                throw new WebFaultException<string>(string.Format("Robot with id value {0} not found.", id), System.Net.HttpStatusCode.NotFound);
            return rv;
        }

        static ModuleService()
        {
            Modules = new List<Module>();
            System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Module>));
            string location = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var fs = new System.IO.FileStream(System.IO.Path.Combine(location, "modules.xml"), System.IO.FileMode.Open);
            var newMods = (List<Paragon.RobotServices.Model.Module>)dcs.ReadObject(fs);
            Modules.AddRange(newMods);
            //var m1 = new Module()
            //{
            //    Id = 0,
            //    File = "InputFs.dll",
            //    Name = "InputFileSystem",
            //    Type = "input"
            //};
            //var m2 = new Module()
            //{
            //    Id = 1,
            //    File = "InputUnisys.dll",
            //    Name = "InputUnisys",
            //    Type = "input"
            //};
            //var m3 = new Module()
            //{
            //    Id = 2,
            //    File = "OutputFs.dll",
            //    Name = "OutputFileSystem",
            //    Type = "output"
            //};
            //var m4 = new Module()
            //{
            //    Id = 3,
            //    File = "OutputUnisys.dll",
            //    Name = "OutputUnisys",
            //    Type = "output"
            //};
            //m1.Settings.Add(new ModuleSetting() { Id = 0, Group = "One", Label = "One", SettingTypeId = 1 });
            //m1.Settings.Add(new ModuleSetting() { Id = 1, Group = "One", Label = "One", SettingTypeId = 1 });
            //m1.Settings.Add(new ModuleSetting() { Id = 2, Group = "One", Label = "One", SettingTypeId = 1 });

            //m2.Settings.Add(new ModuleSetting() { Id = 0, Group = "One", Label = "One", SettingTypeId = 1 });
            //m2.Settings.Add(new ModuleSetting() { Id = 1, Group = "One", Label = "One", SettingTypeId = 1 });
            //m2.Settings.Add(new ModuleSetting() { Id = 2, Group = "One", Label = "One", SettingTypeId = 1 });

            //m3.Settings.Add(new ModuleSetting() { Id = 0, Group = "One", Label = "One", SettingTypeId = 1 });
            //m3.Settings.Add(new ModuleSetting() { Id = 1, Group = "One", Label = "One", SettingTypeId = 1 });
            //m3.Settings.Add(new ModuleSetting() { Id = 2, Group = "One", Label = "One", SettingTypeId = 1 });

            //m4.Settings.Add(new ModuleSetting() { Id = 0, Group = "One", Label = "One", SettingTypeId = 1 });
            //m4.Settings.Add(new ModuleSetting() { Id = 1, Group = "One", Label = "One", SettingTypeId = 1 });
            //m4.Settings.Add(new ModuleSetting() { Id = 2, Group = "One", Label = "One", SettingTypeId = 1 });

            //Modules.AddRange(new Module[] { m1, m2, m3, m4 });
        }
    }
}
