﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Threading;
using Paragon.RobotServices.Model;

namespace Paragon.RobotServices.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            var uriBuilder = new UriBuilder();
            var hostName = System.Net.Dns.GetHostName();
            uriBuilder.Host = hostName;
            uriBuilder.Scheme = "http";
            uriBuilder.Port = 8081;
            uriBuilder.Path = "robots/";
            //var baseUri = "http://va-vpc-vs2010:8080/";
            Console.WriteLine(uriBuilder.Uri);
            var host = new WebServiceHost(typeof(RobotService), uriBuilder.Uri); // new Uri(baseUri + "robots/"));
            var ep = host.AddServiceEndpoint(typeof(IRobotService), new WebHttpBinding(), "");
            var beh = host.Description.Behaviors.Find<System.ServiceModel.Description.ServiceDebugBehavior>();
            beh.IncludeExceptionDetailInFaults = true;
            host.Open();
            uriBuilder.Path = "modules/";
            var host2 = new WebServiceHost(typeof(ModuleService), uriBuilder.Uri); // new Uri(baseUri + "robots/"));
            var ep2 = host2.AddServiceEndpoint(typeof(IModuleService), new WebHttpBinding(), "");
            beh = host2.Description.Behaviors.Find<System.ServiceModel.Description.ServiceDebugBehavior>();
            beh.IncludeExceptionDetailInFaults = true;
            host2.Open();
            Console.Write("Service is running.");

            Console.Write("Scheduling Service is running.");
            Thread workerThread = new Thread(ScheduleService.StartScheduling);
            workerThread.Start();

            Console.Read();
            RobotService.StopAllRobots();
            Console.Read();
            host.Close();
            host2.Close();
        }
    }
}
