﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Paragon.RobotServices.Service
{
    class ServiceProcess : ServiceBase
    {
        private readonly string[] _arguments;
        private Implementation _service = null;

        public ServiceProcess(IEnumerable<string> arguments)
        {
            _arguments = arguments.ToArray();
            if (_arguments.Length == 0 || string.IsNullOrEmpty(_arguments[0]))
                throw new ArgumentException("Required parameter 'ServiceName' not provided.");

            AutoLog = false;
            CanStop = true;
            CanShutdown = true;
            CanHandlePowerEvent = true;
            CanHandleSessionChangeEvent = false;
            CanPauseAndContinue = true;

            ServiceName = _arguments[0];
        }

        protected override void OnStart(string[] args)
        {
            //while (!System.Diagnostics.Debugger.IsAttached) System.Threading.Thread.Sleep(100);
            OnStop();
            _service = new Implementation();

            List<string> allArguments = new List<string>(_arguments);
            if (null != args && args.Length > 0)
                allArguments.AddRange(args);

            _service.Start(allArguments);
        }

        protected override void OnStop()
        {
            if (null != _service)
            {
                _service.Stop();
                _service.Dispose();
                _service = null;
            }
        }

        protected override void OnContinue()
        {
            _service.Continue();
            base.OnContinue();
        }

        protected override void OnPause()
        {
            _service.Pause();
            base.OnPause();
        }

        protected override void OnShutdown()
        {
            OnStop();
            base.OnShutdown();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (null != _service)
                _service.Dispose();
            _service = null;
            base.Dispose(disposing);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // ServiceProcess
            // 
            this.AutoLog = false;
            this.CanHandlePowerEvent = true;
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.ServiceName = "ServiceName";

        }

    }
}
