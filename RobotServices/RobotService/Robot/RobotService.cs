﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;
using System.ServiceModel.Web;
using System.Net;
using System.Net.Mail;
using LicenseManager;
using System.Net.NetworkInformation;
using System.Xml;
using System.Xml.Linq;

namespace Paragon.RobotServices.Service
{
    public class RobotService : IRobotService
    {
        public static List<Robot> Robots;
        public static Dictionary<int, RobotWorker> Workers;
        //protected static ParagonIGRedaction.clsProcess paraIGRedaction = new ParagonIGRedaction.clsProcess();
        public IList<Robot> List()
        {
            return Robots;
        }

        public Robot GetRobot(string id)
        {
            int botId;
            if (!int.TryParse(id, out botId))
                throw new WebFaultException<string>("Id value is not an integer.", System.Net.HttpStatusCode.BadRequest);
            var rv = Robots.Where(bot => bot.Id == botId).SingleOrDefault();
            if (null == rv.Status)
                throw new WebFaultException<string>(string.Format("Robot with id value {0} not found.", id), System.Net.HttpStatusCode.NotFound);
            return rv;
        }

        public bool Start(string id)
        {
            Robot rv = GetRobot(id);
            SendNotification.Notifier = rv.Notifier;
            RobotWorker worker = null;
            if (Workers.ContainsKey(rv.Id))
                worker = Workers[rv.Id];
            else
            {
                worker = new RobotWorker(rv);
                Workers.Add(rv.Id, worker);
                License lic = GetLicense(rv.LicenseKey);
                worker.License = MapLicense(lic);
            }

            if (worker.License.IsLiscenceExpired)
            {
                return false;
            }
            else
            {
                worker.Start();
                rv.Status = worker.Status;
                return true;
            }
            
        }

        public static void StopAllRobots()
        {
            foreach (var w in Workers)
                w.Value.Stop();
            System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
            string location = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var fs = new System.IO.FileStream(System.IO.Path.Combine(location, "robots.xml"), System.IO.FileMode.Create);
            dcs.WriteObject(fs, Robots);
            fs.Close();
        }

        static RobotService()
        {
            Robots = new List<Robot>();
            //paraIGRedaction.Initialize();
            Workers = new Dictionary<int, RobotWorker>();
            System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
            string location = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var fs = new System.IO.FileStream(System.IO.Path.Combine(location, "robots.xml"), System.IO.FileMode.Open);
            var newBots = (List<Paragon.RobotServices.Model.Robot>)dcs.ReadObject(fs);
            fs.Close();
            Robots.AddRange(newBots);
            GetProcessCount(Robots);
            GetLicense(Robots);
            //SetAuditQueries(Robots);
        }

        private static void GetProcessCount(List<Robot> Robots)
        {
            foreach (Robot robot in Robots)
            {
                AuditInfo auditInfo = new AuditInfo();
                robot.SuccessCount = auditInfo.GetSuccessCount(robot.Audit);
            }
        }

        private static void GetLicense(List<Robot> Robots)
        {
            foreach (Robot robot in Robots)
            {
                License license = GetLicense(robot.LicenseKey);
                if (license.Features != null)
                {
                    robot.Liscence = MapLicense(license);
                }
            }
        }

        private static License GetLicense(string licenseKey)
        {
            string macAddress = GetMacAddress();

            LicenseManager.LicenseManager licenseManager = new LicenseManager.LicenseManager();
            License liscence = licenseManager.GetLicense(licenseKey, macAddress);
            return liscence;
        }
        
        /// <summary>
        /// License mapping Code
        /// </summary>
        /// <param name="license"></param>
        public static UseLicense MapLicense(License license)
        {
            string[] features = license.Features;
            UseLicense useLicense = new UseLicense();

            if (features != null)
            {
                foreach (string feature in features.ToList())
                {
                    switch (feature)
                    {
                        case "0":
                            useLicense.CanNormalize = true;
                            break;
                        case "1":
                            useLicense.CanMap = true;
                            break;
                        case "2":
                            useLicense.CanLookup = true;
                            break;
                    }
                }
                useLicense.ExpirationDate = license.ExpirationDate;
                if (useLicense.ExpirationDate <= DateTime.Now)
                {
                    useLicense.IsLiscenceExpired = true;
                    useLicense.Message = "License has expired.";
                }
                else
                {
                    useLicense.IsLiscenceExpired = false;
                    useLicense.Message = string.Empty;
                }
            }
            return useLicense;
        }

        static string GetMacAddress()
        {
            string macAddress = string.Empty;
            foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (macAddress == String.Empty)// only return MAC Address from first card  
                {
                    if (adapter.OperationalStatus == OperationalStatus.Up)
                    {
                        IPInterfaceProperties properties = adapter.GetIPProperties();
                        macAddress = adapter.GetPhysicalAddress().ToString();
                    }
                }
            }
            return macAddress;
        }

        public void UpdateRobot(string id, Robot bot)
        {
            int botId;
            if (!int.TryParse(id, out botId))
                throw new WebFaultException<string>("Id value is not an integer.", System.Net.HttpStatusCode.BadRequest);
            if (null == bot)
                throw new WebFaultException<string>("No Robot data included with update request.", System.Net.HttpStatusCode.BadRequest);
            Robots[botId] = bot;

            StopAllRobots();
        }

        public void CreateRobot(Robot bot)
        {
            int newId = Robots.Count();
            bot.Id = newId;
            Robots.Add(bot);
        }

        public void DeleteRobot(string id)
        {
            throw new NotImplementedException();
        }

        public bool Stop(string id)
        {
            Robot rv = GetRobot(id);
            if (!Workers.ContainsKey(rv.Id))
                return false;

            Workers[rv.Id].Stop();
            rv.Status = Workers[rv.Id].Status;
            return true;
        }

        public bool Pause(string id)
        {
            Robot rv = GetRobot(id);
            if (!Workers.ContainsKey(rv.Id))
                return false;

            Workers[rv.Id].Pause();
            rv.Status = Workers[rv.Id].Status;
            return true;
        }

        public bool Resume(string id)
        {
            Robot rv = GetRobot(id);
            if (!Workers.ContainsKey(rv.Id))
                return false;

            Workers[rv.Id].Resume();
            rv.Status = Workers[rv.Id].Status;
            return true;
        }

        public bool StartScheduling(string id, string val)
        {
            Robot rv = GetRobot(id);
            rv.IsScheduled = Convert.ToBoolean(val);
            System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
            string location = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var fs = new System.IO.FileStream(System.IO.Path.Combine(location, "robots.xml"), System.IO.FileMode.Create);
            dcs.WriteObject(fs, Robots);
            fs.Close();

            return true;
        }


        public bool SaveLicenseKey(string id, string val)
        {
            Robot rv = GetRobot(id);
            rv.LicenseKey = val;
            System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
            string location = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var fs = new System.IO.FileStream(System.IO.Path.Combine(location, "robots.xml"), System.IO.FileMode.Create);
            dcs.WriteObject(fs, Robots);
            fs.Close();

            return true;
        }

        public List<LogEntry> Logs(string id)
        {
            int botId = -1;
            if (!int.TryParse(id, out botId))
            {
                var rv = new List<LogEntry>();
                rv.Add(new LogEntry() { EventDate = DateTime.Now, Id = 1, Message = string.Format("no such robot with id '{0}'", id), Severity = LogSeverity.Critical });
                return rv;
            }
            if (!Workers.ContainsKey(botId))
            {
                var rv = new List<LogEntry>();
                if (GetRobot(id) == null)
                    rv.Add(new LogEntry() { EventDate = DateTime.Now, Id = 1, Message = string.Format("no such robot with id '{0}'", id), Severity = LogSeverity.Critical });
                else
                    rv.Add(new LogEntry() { EventDate = DateTime.Now, Id = 1, Message = string.Format("There are no log entries for the robot with id '{0}'.", id), Severity = LogSeverity.Critical });
                return rv;
            }
            return Workers[botId].Logs;
        }

        public List<ChartData> GetChartData(string id, string fromDate, string toDate, string viewType)
        {
            Robot rv = GetRobot(id);
            List<ChartData> chartData = new List<ChartData>();
            AuditInfo audit = new AuditInfo();
            AuditInfo.SetAuditQueries(rv.Audit);
            StringBuilder selectQuery = new StringBuilder();
            
            switch (viewType)
            {
                case "Day":
                    selectQuery.AppendFormat(rv.Audit.SelectQueryByDay, rv.Audit.AuditTableName, fromDate, toDate);
                    rv.Audit.SelectQueryByDay = selectQuery.ToString();
                    break;
                case "Month":
                    selectQuery.AppendFormat(rv.Audit.SelectQueryByMonth, rv.Audit.AuditTableName, fromDate, toDate);
                    rv.Audit.SelectQueryByMonth = selectQuery.ToString();
                    break;
                case "Year":
                    selectQuery.AppendFormat(rv.Audit.SelectQueryByYear, rv.Audit.AuditTableName, fromDate, toDate);
                    rv.Audit.SelectQueryByYear = selectQuery.ToString();
                    break;
                default:
                    selectQuery.AppendFormat(rv.Audit.SelectQueryByDay, rv.Audit.AuditTableName, fromDate, toDate);
                    rv.Audit.SelectQueryByDay = selectQuery.ToString();
                    break;
            }
            chartData = audit.GetChartData(rv.Audit, viewType);
            return chartData;
        }

        //public List<ChartData> GetChartData(string id, string fromDate, string toDate, string viewType)
        //{
        //    Robot rv = GetRobot(id);
        //    List<ChartData> chartData = new List<ChartData>();
        //    AuditInfo audit = new AuditInfo();
        //    switch (viewType)
        //    {
        //        case "Day":
        //            rv.Audit.SelectQueryByDay = "select CAST(CAST(start_time AS DATE) as varchar(10)) as view_type, count(case status when 'Successful' then 1 end) success, count(case status when 'Error' then 1 end) error from robotinstance where CAST(start_time AS date) BETWEEN CAST('" + fromDate + "' as date) and CAST('" + toDate + "' as date) group by CAST(start_time AS DATE) order by CAST(start_time AS DATE)";
        //            break;
        //        case "Month":
        //            rv.Audit.SelectQueryByMonth = "SELECT DATENAME(month ,start_time) as view_type, count(case status when 'Successful' then 1 end) success, count(case status when 'Error' then 1 end) error from robotinstance where CAST(start_time AS date) BETWEEN CAST('" + fromDate + "' as date) and CAST('" + toDate + "' as date) group by MONTH(CAST(start_time AS DATE) ) , DATENAME(month ,start_time)";
        //            break;
        //        case "Year":
        //            rv.Audit.SelectQueryByYear = "SELECT year(CAST(start_time AS DATE)) as view_type, count(case status when 'Successful' then 1 end) success, count(case status when 'Error' then 1 end) error from robotinstance where CAST(start_time AS date) BETWEEN CAST('" + fromDate + "' as date) and CAST('" + toDate + "' as date) group by year(CAST(start_time AS DATE) )";
        //            break;
        //        default:
        //            rv.Audit.SelectQueryByDay = "select CAST(CAST(start_time AS DATE) as varchar(10)) as view_type, count(case status when 'Successful' then 1 end) success, count(case status when 'Error' then 1 end) error from robotinstance where CAST(start_time AS date) BETWEEN CAST('" + fromDate + "' as date) and CAST('" + toDate + "' as date) group by CAST(start_time AS DATE) order by CAST(start_time AS DATE)";
        //            break;
        //    }
        //    chartData = audit.GetChartData(rv.Audit, viewType);
        //    return chartData;
        //}

        
        
    }

}
