﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Paragon.RobotServices.Model;
using Paragon.RobotServices.Plugin;

namespace Paragon.RobotServices.Service
{
    public class Normalizer
    {
        protected NormalizerSettings Settings;

        public Normalizer(NormalizerSettings settings)
        {
            Settings = settings;
        }

        public Page Normalize(Page input)
        {
            return input;
        }

        public IEnumerable<Page> Normalize(IEnumerable<Page> inputs)
        {
            List<Page> rv = new List<Page>();

            foreach (var i in inputs)
                rv.Add(this.Normalize(i));

            return rv;
        }
    }
}
