﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Paragon.RobotServices.Service
{
    public enum Connector
    {
        And,
        Or
    }

    public enum CompareMethod
    {
        Text,
        Binary
    }

    public enum Conditional
    {
        Equals,
        NotEquals,
        IsBlank,
        IsNotBlank,
        StartsWith,
        EndsWith,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Contains,
        InList,
        NotInList,
        Like,
        NotLike
    }

    [DataContract]
    public class Condition
    {
        [DataMember]
        public int ConditionId;

        [DataMember]
        public string Operand1;

        [DataMember]
        public Conditional Conditional = Conditional.Equals;

        [DataMember]
        public string Operand2;

        [DataMember]
        public Connector Connector = Connector.And;

        [DataMember]
        public CompareMethod CompareMethod = CompareMethod.Text;

        //public Condition(ConditionDAO c)
        //{
        //    this.ConditionId = c.ConditionId;
        //    this.CompareMethod = c.CompareMethod;
        //    this.Conditional = c.Conditional;
        //    this.Connector = c.Connector;
        //    this.Operand1 = c.Operand1;
        //    this.Operand2 = c.Operand2;
        //}

        public Condition() { }

        public Condition(System.Xml.Linq.XElement el)
        {
            if (el.Attribute("id") != null)
                int.TryParse(el.Attribute("id").Value, out this.ConditionId);
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'id' attribute, or the attribute is not a number.", el.ToString()));
            //if (el.Attribute("order") != null)
            //    int.TryParse(el.Attribute("order").Value, out this.Order);
            //else
            //    throw new ArgumentException(string.Format("element {0} does not have an 'order' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("op1") != null)
                this.Operand1 = el.Attribute("op1").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'op1' attribute.", el.ToString()));
            if (el.Attribute("op2") != null)
                this.Operand2 = el.Attribute("op2").Value;
            else
                throw new ArgumentException(string.Format("element {0} does not have a 'op2' attribute.", el.ToString()));
            if (el.Attribute("conditionalId") != null)
            {
                //int.TryParse(el.Attribute("conditionalId").Value, out this.ConditionalId);
                if (!Enum.TryParse<Conditional>(el.Attribute("conditionalId").Value, out this.Conditional))
                    throw new ArgumentException(string.Format("element's 'conditionalId' attribute value of '{0}' could not be parsed as a valid conditional (Equal, NotEqual, etc.)", el.Attribute("conditionalId").Value));
            }
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'conditionalId' attribute, or the attribute is not a number.", el.ToString()));
            if (el.Attribute("connectorId") != null)
            {
                //int.TryParse(el.Attribute("connectorId").Value, out this.ConnectorId);
                if (!Enum.TryParse<Connector>(el.Attribute("connectorId").Value, out this.Connector))
                    throw new ArgumentException(string.Format("element's 'connectorId' attribute value of '{0}' could not be parsed as a valid connector (And, Or)", el.Attribute("conditionalId").Value));
            }
            else
                throw new ArgumentException(string.Format("element {0} does not have an 'connectorId' attribute, or the attribute is not a number.", el.ToString()));
        }

        public static List<Condition> FromElements(IEnumerable<System.Xml.Linq.XElement> els)
        {
            List<Condition> rv = new List<Condition>();
            rv.AddRange(els.Select(e => new Condition(e)));
            return rv;
        }

        public bool Evaluate(IDictionary<string, string> inputFields)
        {
            List<Field> fields = new List<Field>();
            IDictionary<string, string> results;
            string op1Value = "", op2Value;
            bool result;

            // if the left operand contains a field name, get the value directly
            if (inputFields.ContainsKey(Operand1))
                op1Value = inputFields[Operand1];
            else  //otherwise, it needs to be translated
                fields.Add(new Field() { Name = "_op1", Value = Operand1 });

            // right operand is always translated
            fields.Add(new Field() { Name = "_op2", Value = Operand2 });

            results = fields.TranslateFields(inputFields);

            // now set the translated results
            if (results.ContainsKey("_op1"))
                op1Value = results["_op1"];
            op2Value = results["_op2"];

            // perform the comparison
            switch (Conditional)
            {
                case Conditional.Equals:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant() == op2Value.ToLowerInvariant());
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) == double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) == DateTime.Parse(op2Value));
                        else
                            result = (op1Value == op2Value);
                    }
                    break;
                case Conditional.NotEquals:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant() != op2Value.ToLowerInvariant());
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) != double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) != DateTime.Parse(op2Value));
                        else
                            result = (op1Value != op2Value);
                    }
                    break;
                case Conditional.IsBlank:
                    result = (op1Value.Trim().Length == 0);
                    break;
                case Conditional.IsNotBlank:
                    result = (op1Value.Trim().Length != 0);
                    break;
                case Conditional.StartsWith:
                    if (CompareMethod == CompareMethod.Text)
                        result = op1Value.ToLowerInvariant().StartsWith(op2Value.ToLowerInvariant());
                    else
                        result = op1Value.StartsWith(op2Value);
                    break;
                case Conditional.EndsWith:
                    if (CompareMethod == CompareMethod.Text)
                        result = op1Value.ToLowerInvariant().EndsWith(op2Value.ToLowerInvariant());
                    else
                        result = op1Value.EndsWith(op2Value);
                    break;
                case Conditional.GreaterThan:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) > 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) > double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) > DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) > 0);
                    }
                    break;
                case Conditional.GreaterThanOrEqual:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) >= 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) >= double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) >= DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) >= 0);
                    }
                    break;
                case Conditional.LessThan:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) < 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) < double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) < DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) < 0);
                    }
                    break;
                case Conditional.LessThanOrEqual:
                    if (CompareMethod == CompareMethod.Text)
                        result = (op1Value.ToLowerInvariant().CompareTo(op2Value.ToLowerInvariant()) <= 0);
                    else
                    {
                        if (IsNumeric(op1Value) && IsNumeric(op2Value))
                            result = (double.Parse(op1Value) <= double.Parse(op2Value));
                        else if (IsDate(op1Value) && IsDate(op2Value))
                            result = (DateTime.Parse(op1Value) <= DateTime.Parse(op2Value));
                        else
                            result = (op1Value.CompareTo(op2Value) <= 0);
                    }
                    break;
                case Conditional.Contains:
                    if (op1Value.Length == 0)
                        result = false;
                    else
                        result = op1Value.Contains(op2Value);
                    break;
                case Conditional.InList:
                    if (op1Value.Length == 0)
                        result = false;
                    else
                        result = ("," + op2Value + ",").Contains("," + op1Value + ",");
                    break;
                case Conditional.NotInList:
                    if (op1Value.Length == 0)
                        result = false;
                    else
                        result = !("," + op2Value + ",").Contains("," + op1Value + ",");
                    break;
                case Conditional.Like:
                    result = (System.Text.RegularExpressions.Regex.Match(op1Value, op2Value).Success);
                    break;
                case Conditional.NotLike:
                    result = !(System.Text.RegularExpressions.Regex.Match(op1Value, op2Value).Success);
                    break;
                default:
                    result = false;
                    break;
            }

            return result;
        }

        private bool IsNumeric(string value)
        {
            double temp;
            return double.TryParse(value, out temp);
        }

        private bool IsDate(string value)
        {
            DateTime temp;
            return DateTime.TryParse(value, out temp);
        }

    }
}
