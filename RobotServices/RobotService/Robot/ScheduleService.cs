﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Paragon.RobotServices.Model;

namespace Paragon.RobotServices.Service
{
    static class ScheduleService
    {
        public static void StartScheduling()
        {
            List<int> runningSchedules = new List<int>();
            List<int> runningBlackout = new List<int>();
            while (true)
            {
                //read robot list from xml
                List<Robot> RobotList;  //to delete when windows service code is used - only else condition will be used
                if (RobotService.Robots.Count <= 0)
                {
                    RobotList = ReadXml();
                }
                else
                {
                    RobotList = RobotService.Robots.ToList();
                }
                foreach (Robot rb in RobotList)
                {
                    if (rb.IsScheduled)
                    {
                        RobotService service = new RobotService();
                        List<Schedule> schedules = rb.Schedules;
                        foreach (Schedule schedule in schedules)
                        {
                            foreach (ScheduleElement scheduleElement in schedule.Elements)
                            {
                                if (scheduleElement.IsBlackout)
                                {
                                    if(!runningBlackout.Contains(scheduleElement.ScheduleElementId))
                                    {
                                        //if ((scheduleElement.Start <= DateTime.Now) && (scheduleElement.End >= DateTime.Now))
                                        //{
                                        //    if (rb.Status == "running")
                                        //    {
                                        //        service.Stop(rb.Id.ToString());
                                        //        runningBlackout.Add(scheduleElement.ScheduleElementId);
                                        //    }
                                        //}



                                        switch (scheduleElement.Duration)
                                        {
                                            case "Daily":
                                                if ((scheduleElement.Start.TimeOfDay <= DateTime.Now.TimeOfDay) && (scheduleElement.End.TimeOfDay >= DateTime.Now.TimeOfDay))
                                                {
                                                    if (rb.Status == "running")
                                                    {
                                                        service.Stop(rb.Id.ToString());
                                                        runningBlackout.Add(scheduleElement.ScheduleElementId);
                                                    }
                                                }
                                                break;
                                            case "Weekly":
                                                if ((scheduleElement.Start.TimeOfDay <= DateTime.Now.TimeOfDay) && (scheduleElement.End.TimeOfDay >= DateTime.Now.TimeOfDay))
                                                {
                                                    if (scheduleElement.Start.DayOfWeek == DateTime.Now.DayOfWeek)
                                                    {
                                                        if (rb.Status == "running")
                                                        {
                                                            service.Stop(rb.Id.ToString());
                                                            runningBlackout.Add(scheduleElement.ScheduleElementId);
                                                        }
                                                    }
                                                }
                                                break;
                                            case "Monthly":
                                                if ((scheduleElement.Start.TimeOfDay <= DateTime.Now.TimeOfDay) && (scheduleElement.End.TimeOfDay >= DateTime.Now.TimeOfDay))
                                                {
                                                    if (scheduleElement.Start.Day == DateTime.Now.Day)
                                                    {
                                                        if (rb.Status == "running")
                                                        {
                                                            service.Stop(rb.Id.ToString());
                                                            runningBlackout.Add(scheduleElement.ScheduleElementId);
                                                        }
                                                    }
                                                }
                                                break;
                                            default:
                                                if ((scheduleElement.Start <= DateTime.Now) && (scheduleElement.End >= DateTime.Now))
                                                {
                                                    if (rb.Status == "running")
                                                    {
                                                        service.Stop(rb.Id.ToString());
                                                        runningBlackout.Add(scheduleElement.ScheduleElementId);
                                                    }
                                                }
                                                break;
                                        }

                                    }
                                    else
                                    {
                                        //if (scheduleElement.End <= DateTime.Now)
                                        //{
                                        //    if (runningSchedules.Count > 0)
                                        //    {
                                        //        service.Start(rb.Id.ToString());
                                        //        runningBlackout.Remove(scheduleElement.ScheduleElementId);
                                        //    }
                                        //}

                                        switch (scheduleElement.Duration)
                                        {
                                            case "Daily":
                                                if (scheduleElement.End.TimeOfDay <= DateTime.Now.TimeOfDay)
                                                {
                                                    if (runningSchedules.Count > 0)
                                                    {
                                                        service.Start(rb.Id.ToString());
                                                        runningBlackout.Remove(scheduleElement.ScheduleElementId);
                                                    }
                                                }
                                                break;
                                            case "Weekly":
                                                if (scheduleElement.End.TimeOfDay <= DateTime.Now.TimeOfDay)
                                                {
                                                    if (scheduleElement.End.DayOfWeek == DateTime.Now.DayOfWeek)
                                                    {
                                                        if (runningSchedules.Count > 0)
                                                        {
                                                            service.Start(rb.Id.ToString());
                                                            runningBlackout.Remove(scheduleElement.ScheduleElementId);
                                                        }
                                                    }
                                                }
                                                break;
                                            case "Monthly":
                                                if (scheduleElement.End.TimeOfDay <= DateTime.Now.TimeOfDay)
                                                {
                                                    if (scheduleElement.End.Day == DateTime.Now.Day)
                                                    {
                                                        if (runningSchedules.Count > 0)
                                                        {
                                                            service.Start(rb.Id.ToString());
                                                            runningBlackout.Remove(scheduleElement.ScheduleElementId);
                                                        }
                                                    }
                                                }
                                                break;
                                            default:
                                                if (scheduleElement.End <= DateTime.Now)
                                                {
                                                    if (runningSchedules.Count > 0)
                                                    {
                                                        service.Start(rb.Id.ToString());
                                                        runningBlackout.Remove(scheduleElement.ScheduleElementId);
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                    continue;
                                }
                                if (runningSchedules.Contains(scheduleElement.ScheduleElementId))
                                {
                                    CheckScheduleEndDuration(runningSchedules, rb, service, scheduleElement);
                                }
                                else
                                {
                                    CheckScheduleStartDuration(runningSchedules, rb, service, scheduleElement);
                                }
                            }
                        }
                    }
                }
                Thread.Sleep(300);
            }
        }

        private static void CheckScheduleStartDuration(List<int> runningSchedules, Robot rb, RobotService service, ScheduleElement scheduleElement)
        {
            switch (scheduleElement.Duration)
            {
                case "Daily":
                    if ((scheduleElement.Start.TimeOfDay <= DateTime.Now.TimeOfDay) && (scheduleElement.End.TimeOfDay >= DateTime.Now.TimeOfDay))
                    {
                        service.Start(rb.Id.ToString());
                        runningSchedules.Add(scheduleElement.ScheduleElementId);
                    }
                    break;
                case "Weekly":
                    if ((scheduleElement.Start.TimeOfDay <= DateTime.Now.TimeOfDay) && (scheduleElement.End.TimeOfDay >= DateTime.Now.TimeOfDay))
                    {
                        if (scheduleElement.Start.DayOfWeek == DateTime.Now.DayOfWeek)
                        {
                            service.Start(rb.Id.ToString());
                            runningSchedules.Add(scheduleElement.ScheduleElementId);
                        }
                    }
                    break;
                case "Monthly":
                    if ((scheduleElement.Start.TimeOfDay <= DateTime.Now.TimeOfDay) && (scheduleElement.End.TimeOfDay >= DateTime.Now.TimeOfDay))
                    {
                        if (scheduleElement.Start.Day == DateTime.Now.Day)
                        {
                            service.Start(rb.Id.ToString());
                            runningSchedules.Add(scheduleElement.ScheduleElementId);
                        }
                    }
                    break;
                default:
                    if ((scheduleElement.Start <= DateTime.Now) && (scheduleElement.End >= DateTime.Now))
                    {
                        service.Start(rb.Id.ToString());
                        runningSchedules.Add(scheduleElement.ScheduleElementId);
                    }
                    break;
            }
        }

        private static void CheckScheduleEndDuration(List<int> runningSchedules, Robot rb, RobotService service, ScheduleElement scheduleElement)
        {
            switch (scheduleElement.Duration)
            {
                case "Daily":
                    if (scheduleElement.End.TimeOfDay <= DateTime.Now.TimeOfDay)
                    {
                        service.Stop(rb.Id.ToString());
                        runningSchedules.Remove(scheduleElement.ScheduleElementId);
                    }
                    break;
                case "Weekly":
                    if (scheduleElement.End.TimeOfDay <= DateTime.Now.TimeOfDay)
                    {
                        if (scheduleElement.End.DayOfWeek == DateTime.Now.DayOfWeek)
                        {
                            service.Stop(rb.Id.ToString());
                            runningSchedules.Remove(scheduleElement.ScheduleElementId);
                        }
                    }
                    break;
                case "Monthly":
                    if (scheduleElement.End.TimeOfDay <= DateTime.Now.TimeOfDay)
                    {
                        if (scheduleElement.End.Day == DateTime.Now.Day)
                        {
                            service.Stop(rb.Id.ToString());
                            runningSchedules.Remove(scheduleElement.ScheduleElementId);
                        }
                    }
                    break;
                default:
                    if (scheduleElement.End <= DateTime.Now)
                    {
                        service.Stop(rb.Id.ToString());
                        runningSchedules.Remove(scheduleElement.ScheduleElementId);
                    }
                    break;
            }
        }

        //will be removed later
        static List<Robot> ReadXml()
        {
            List<Robot> Robots = new List<Robot>();
            Dictionary<int, RobotWorker> Workers = new Dictionary<int, RobotWorker>();
            System.Runtime.Serialization.DataContractSerializer dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(List<Paragon.RobotServices.Model.Robot>));
            string location = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var fs = new System.IO.FileStream(System.IO.Path.Combine(location, "robots.xml"), System.IO.FileMode.Open);
            var newBots = (List<Paragon.RobotServices.Model.Robot>)dcs.ReadObject(fs);
            fs.Close();
            Robots.AddRange(newBots);

            return Robots;
        }
    }
}
