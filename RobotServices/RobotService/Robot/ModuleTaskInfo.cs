﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Paragon.RobotServices.Service
{
    public class PlugInTaskInfo
    {
        public AutoResetEvent PluginHandle;
        public AutoResetEvent ContextHandle;
        public ManualResetEvent ControlHandle;
        public SynchronizedBool KeepRunning;
        public long PageCount;
    }
}
