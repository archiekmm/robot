﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Paragon.RobotServices.Model;
using Paragon.RobotServices.Plugin;
using NLog;
using System.Xml;
using System.Data;
using System.Xml.Linq;
using LicenseManager;

namespace Paragon.RobotServices.Service
{
    public class RobotWorker
    {
        static Logger nlogger = LogManager.GetLogger("RobotWorker");
        
        public UseLicense License = new UseLicense();

        public class RobotAbortedException : Exception
        {
        }

        protected Robot RobotData;
        public List<LogEntry> Logs = new List<LogEntry>();

        public string Status;
        public string Name;
        public bool Repeats;
        public bool IsScheduled;
        public TimeSpan SleepTime;
        public IInputModule InputModule;
        public IOutputModule OutputModule;
        //public PlugInBase PlugIn;
        public SynchronizedBool Abort;
        ParagonIGRedaction.clsProcess paraIGRedaction = new ParagonIGRedaction.clsProcess();
        bool IGRedactionModule;

        protected PlugInTaskInfo piti = null;
        protected ManualResetEvent sleepAbortHandle = null;
        protected ManualResetEventSlim pauseHandle = null;
        protected Thread pluginThread = null;
        protected Thread contextThread = null;

        protected Normalizer Norm;
        protected Lookup Lookup;

//        private const string AUDIT_INSERT_MSSQL =
//            @"INSERT INTO {0} (object_name, object_id, start_time, status, comments)
//            OUTPUT inserted.id
//            VALUES ({1}, {2}, getdate(), {3}, {4})";

//        private const string AUDIT_UPDATE_MSSQL =
//            @"UPDATE {0}
//            SET end_time = getdate(),
//            status = {1},
//            comments = {2}
//            WHERE {3} = {4}";

        public RobotWorker(Robot r)
        {
            this.RobotData = r;
            this.Status = "uninitialized";
            this.Name = r.Name;
            this.Repeats = r.Repeats;
            // this.IsScheduled = r.IsScheduled;
            this.SleepTime = new TimeSpan(r.SleepTime * TimeSpan.TicksPerSecond);
            // or
            //this.SleepTime = new TimeSpan(0, 0, r.SleepTime);

            this.piti = new PlugInTaskInfo()
            {
                ContextHandle = new AutoResetEvent(false),
                ControlHandle = new ManualResetEvent(false),
                PluginHandle = new AutoResetEvent(false),
                KeepRunning = new SynchronizedBool(true),
                PageCount = 0
            };
            this.sleepAbortHandle = new ManualResetEvent(false);
            this.Abort = new SynchronizedBool(false);
            //this.PlugIn = CreatePluginFromModule(r.RobotModules.FirstOrDefault().ModuleId);

            try
            {
                paraIGRedaction.Initialize();
            }
            catch (Exception ex)
            {
                SendNotification.Notify(SendNotification.NotifyEvent.AllError);
                nlogger.Log(LogLevel.Debug, "Dll is already instantialized.");
            }

            try
            {

                Logs.Add(new LogEntry() { Id = Logs.Count, EventDate = DateTime.Now, Message = "instantiating input module.", Severity = LogSeverity.Normal });

                nlogger.Log(LogLevel.Debug, "instantiating input module.");

                this.InputModule = (IInputModule)CreateModule(r.InputModuleId, r.InputSettings);
                Logs.Add(new LogEntry() { Id = Logs.Count, EventDate = DateTime.Now, Message = "instantiating output module.", Severity = LogSeverity.Normal });
                nlogger.Log(LogLevel.Debug, "instantiating output module.");

                this.OutputModule = (IOutputModule)CreateModule(r.OutputModuleId, r.OutputSettings);
                if (this.OutputModule.ShareConnector)
                {
                    Logs.Add(new LogEntry() { Id = Logs.Count, EventDate = DateTime.Now, Message = "sharing connector object.", Severity = LogSeverity.Normal });
                    nlogger.Log(LogLevel.Debug, "Output shares connector object.");
                    this.OutputModule.Connector = this.InputModule.Connector;
                }

                this.Norm = new Normalizer(r.Normalizer);
                this.Lookup = new Lookup(r.Lookup);
            }
            catch (Exception ex)
            {
                SendNotification.Notify(SendNotification.NotifyEvent.AllError);
                Logs.Add(new LogEntry() { Id = Logs.Count, EventDate = DateTime.Now, Message = string.Format("Exception while creating RobotWorker: {0}.", ex.ToString()), Severity = LogSeverity.Normal });
                nlogger.Log(LogLevel.Error, string.Format("Exception while creating RobotWorker: {0}.", ex.ToString()));
            }
        }

        public void Start()
        {
            Console.WriteLine(string.Format("Starting robot '{0}'", this.Name));

            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Starting robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, "Starting robot '{0}'");
            if (this.Status == "paused")
            {
                this.Resume();
                return;
            }
            this.Status = "starting";
            this.Abort.Value = false;
            //this.sleepAbortHandle.Reset();
            //this.contextThread = new Thread(new ParameterizedThreadStart(RunContext));
            //this.contextThread.Start(piti.ControlHandle);

            this.pauseHandle = new ManualResetEventSlim(true);
            this.contextThread = new Thread(new ThreadStart(ThreadMethod));
            this.contextThread.Start();
            while (!this.contextThread.IsAlive)
                Thread.Sleep(0);
            //this.piti.ControlHandle.Set();
            this.Status = "running";
            Console.WriteLine(string.Format("Started robot '{0}'", this.Name));
            //processing started
            SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStarted);
            nlogger.Log(LogLevel.Debug, "Started robot '{0}'");
            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Started robot '{0}'", this.Name)
            });
        }

        public void Stop()
        {
            Console.WriteLine(string.Format("Stopping robot '{0}'", this.Name));
            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Stopping robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, "Stopping robot '{0}'");
            this.Status = "stopping";
            //this.piti.ControlHandle.Reset();
            this.Abort.Value = true;
            //this.sleepAbortHandle.Set();
            //this.piti.ControlHandle.Set();
            if (null != this.contextThread)
                if (!this.contextThread.Join(10000)) // wait 10 seconds before abort
                    this.contextThread.Abort();
            this.Status = "stopped";
            Console.WriteLine(string.Format("Stopped robot '{0}'", this.Name));
            SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedByUser);
            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Stopping robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, "Stopped robot '{0}'");
        }

        public void Pause()
        {
            Console.WriteLine(string.Format("Pausing robot '{0}'", this.Name));
            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Pausing robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, "Pausing robot '{0}'");
            this.Status = "pausing";
            this.pauseHandle.Reset();
            this.Status = "paused";
            Console.WriteLine(string.Format("Paused robot '{0}'", this.Name));
            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Paused robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, "Paused robot '{0}'");
        }

        public void Resume()
        {
            Console.WriteLine(string.Format("Resuming robot '{0}'", this.Name));
            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Resuming robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, "Resuming robot '{0}'");
            this.Status = "resuming";
            this.pauseHandle.Set();
            this.Status = "running";
            Console.WriteLine(string.Format("Resuming robot '{0}'", this.Name));
            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Resumed robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, "Resumed robot '{0}'");
        }

        protected void RunContext(object state)
        {
            ManualResetEvent controller = (ManualResetEvent)state;
            bool keepGoing = true;
            piti.KeepRunning.Value = true;
            //logger.Log(NLog.LogLevel.Trace, "Starting RunPlugin thread.");
            this.pluginThread = new Thread(new ParameterizedThreadStart(RunPlugin));
            this.pluginThread.Start(this.piti);
            while (!this.pluginThread.IsAlive)
                Thread.Sleep(0);
            //logger.Log(NLog.LogLevel.Trace, "RunPlugin thread is alive.");
            while (keepGoing)
            {
                //logger.Log(LogLevel.Trace, "RunContext waiting on controller.");
                controller.WaitOne();
                keepGoing = KeepRunning();
                //logger.Log(LogLevel.Trace, "RunContext finished waiting on controller, updating keepRunning to {0}", keepGoing);
                this.piti.KeepRunning.Value = keepGoing;
                //logger.Log(LogLevel.Trace, "RunContext assigned keepRunning, signalling pluginHandle and waiting on contextHandle.");
                WaitHandle.SignalAndWait(piti.PluginHandle, piti.ContextHandle);
                //logger.Log(LogLevel.Trace, "RunContext finished waiting on contextHandle, sleeping.");
                // WaitOne with a timeout parameter waits for either a signal or a timeout,
                // returns true if the signal was received or false if the timeout passed
                // so if it returns true, we need to abort.
                if (sleepAbortHandle.WaitOne(this.SleepTime))
                {
                    // we were signalled
                    this.piti.KeepRunning.Value = keepGoing = false;
                    // wake up the plugin so it can stop
                    if (this.pluginThread.IsAlive)
                        WaitHandle.SignalAndWait(piti.PluginHandle, piti.ContextHandle);
                    // condition would fail when we tried to loop, but we'll be explicit
                    break;
                }
            }
            if (null != this.pluginThread)
                if (!this.pluginThread.Join(6000))
                    this.pluginThread.Abort(); // abort after 6 seconds
            Console.WriteLine("Context thread execution completed.");
            //logger.Log(LogLevel.Trace, "keepGoing was false or sleep was aborted, exiting thread.");
        }

        protected void RunPlugin(object state)
        {
            PlugInTaskInfo pi = (PlugInTaskInfo)state;
            while (pi.KeepRunning.Value)
            {
                //logger.Log(LogLevel.Trace, "RunPlugin blocking on waithandles.");
                // i think i need to wait on both context and control wait handles
                WaitHandle.WaitAll(new WaitHandle[] { pi.PluginHandle, pi.ControlHandle });
                //logger.Log(LogLevel.Trace, "RunPlugin finished blocking.");
                if (pi.KeepRunning.Value)
                {
                    //logger.Log(LogLevel.Trace, "RunPlugin keeps going, doing work.");
                    // July 9th, commented the next two lines to compile
                    long pc = this.RunRobot();
                    Interlocked.Exchange(ref pi.PageCount, pc);
                    //logger.Log(LogLevel.Trace, "DoWork completed {0} pages of work; signalling context.", pc);
                    pi.ContextHandle.Set();
                    //logger.Log(LogLevel.Trace, "RunPlugin signalled context.");
                }
                else
                {
                    //logger.Log(LogLevel.Trace, "RunPlugin stopping work due to keepGoing == false.");
                    Interlocked.Exchange(ref pi.PageCount, 0);
                    pi.ContextHandle.Set();
                    //logger.Log(LogLevel.Trace, "RunPlugin returning.");
                    return;
                }
                //logger.Log(LogLevel.Trace, "RunPlugin looping, keepRunning is {0}", pi.keepRunning.Value);
            }
            Console.WriteLine("Plugin thread execution completed.");
        }

        protected bool KeepRunning()
        {
            //logger.Log(NLog.LogLevel.Trace,
            //    "Context.KeepRunning inputs: abort: {0}; scheduled: {1}, repeat: {2}, schedule.Check(): {3}",
            //    abort.Value, scheduled, repeat, schedule.CheckSchedule());
            if (Abort.Value)
            {
                SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                return false;
            }
            else
                return false; // temporary until we get schedule going
            //if (!IsScheduled && Repeats)
            //    return true;
            //return false;
        }

        //protected PlugInBase CreatePluginFromModule(int moduleId)
        //{
        //    Modules.ModuleService ms = new Paragon.RobotServices.Modules.ModuleService();
        //    var m = ms.Retrieve(moduleId);
        //    //Settings.SettingService ss = new Paragon.RobotServices.Settings.SettingService();
        //    string dllName = m.ModuleKind;
        //    Type pluginType = dllName.LoadAssemblyAndGetType();
        //    //var settings = ss.RetrieveMany(m.SettingIds.Keys);
        //    return (PlugInBase)System.Activator.CreateInstance(pluginType, moduleId);
        //}

        protected object CreateModule(int moduleId, IDictionary<int, string> settings)
        {
            try
            {
                var ms = new ModuleService();
                var mod = ms.GetModule(moduleId.ToString());
                Type type = Type.GetType(mod.File.Replace(".dll", ""));
                if (null == type)
                {
                    var aName = System.Reflection.AssemblyName.GetAssemblyName(mod.File);
                    var assem = System.Reflection.Assembly.Load(aName);
                    type = assem.GetTypes().Where(t => t.Name == mod.Name).Single();
                    if (mod.Name.IndexOf("Redaction") > 0)
                        IGRedactionModule = true;

                }
                var rv = System.Activator.CreateInstance(type, new object[] { settings });
                return rv;
            }
            catch (Exception ex)
            {
                SendNotification.Notify(SendNotification.NotifyEvent.AllError);
                ex.ToString();
                return null;
            }
        }

        protected long RunRobot()
        {
            long rv;
            long batchSize = 0;
            bool batchItems = false;
            // read batch flag, size from somewhere ...

            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Connecting robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, string.Format("Connecting robot '{0}'", this.Name));

            if (this.InputModule.RequiresConnection)
                this.InputModule.Connect();
            if (this.OutputModule.RequiresConnection)
            {
                if (IGRedactionModule)
                {

                }
                if (!this.OutputModule.ShareConnector)
                    this.OutputModule.Connect();

            }

            //

            if ((batchItems) && (batchSize > 1) && (OutputModule.SupportsBatching))
                rv = ProcessBatches();
            else
                rv = ProcessItems();


            this.Logs.Add(new LogEntry()
            {
                Severity = LogSeverity.Normal,
                EventDate = DateTime.Now,
                Id = this.Logs.Count,
                Message = string.Format("Disconnecting robot '{0}'", this.Name)
            });
            nlogger.Log(LogLevel.Debug, string.Format("Disconnecting robot '{0}'", this.Name));

            if (this.OutputModule.RequiresConnection)
                if (!this.OutputModule.ShareConnector)
                    this.OutputModule.Disconnect();
            if (this.InputModule.RequiresConnection)
                this.InputModule.Disconnect();
            return rv;
        }

        protected long ProcessItems()
        {
            long rv = 0;
            
            while (true)
            {
                try
                {
                    if (License.ExpirationDate <= DateTime.Now)
                    {
                        Abort.Value = true;
                        RobotData.Liscence.Message = "License has expired.";
                    }
                    if (Abort.Value)
                    {
                        SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                        break;
                    }

                    var curItem = InputModule.Get();
                    
                    //StringBuilder insertQuery = new StringBuilder();
                    //AuditInfo.SetAuditQueries(RobotData.Audit);
                    ////insertQuery.AppendFormat(AUDIT_INSERT_MSSQL, RobotData.Audit.AuditTableName, "'" + curItem.Name + "'", curItem.Id, "'" + curItem.Status + "'", "'" + curItem.Status + "'");
                   
                    //    insertQuery.AppendFormat(RobotData.Audit.InsertQuery, RobotData.Audit.AuditTableName, "'" + curItem.Name + "'", curItem.Id, "'" + curItem.Status + "'", "'" + curItem.Status + "'");

                    //    RobotData.Audit.InsertQuery = insertQuery.ToString();
                    //    AuditInfo audit = new AuditInfo();
                    //    Guid insertedId = audit.InsertAuditInfo(RobotData.Audit);
                    
                    if (null == curItem)
                    {
                        SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedNormally);
                        this.Logs.Add(new LogEntry()
                        {
                            Severity = LogSeverity.Normal,
                            EventDate = DateTime.Now,
                            Id = this.Logs.Count,
                            Message = string.Format("Item was null, exiting loop. robot '{0}'", this.Name)
                        });
                        nlogger.Log(LogLevel.Debug, string.Format("Item was null, exiting loop. robot '{0}'", this.Name));
                        break;
                    }
                    else
                    {
                        this.Logs.Add(new LogEntry()
                        {
                            Severity = LogSeverity.Normal,
                            EventDate = DateTime.Now,
                            Id = this.Logs.Count,
                            Message = string.Format("retrieved item with id of {0}.", curItem.Id)
                        });
                        nlogger.Log(LogLevel.Debug, string.Format("retrieved item with id of {0}.", curItem.Id));
                    }

                    ProcessItem(curItem);

                    if (curItem.Status != WorkitemStatus.Error)
                    {
                        rv += OutputModule.Process(curItem);
                        this.Logs.Add(new LogEntry()
                        {
                            Severity = LogSeverity.Normal,
                            EventDate = DateTime.Now,
                            Id = this.Logs.Count,
                            Message = string.Format("Item processed successfully", this.Name)
                        });
                        nlogger.Log(LogLevel.Debug, string.Format("Item processed successfully", this.Name));
                    }
                    else
                    {
                        SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                        this.Logs.Add(new LogEntry()
                        {
                            Severity = LogSeverity.Normal,
                            EventDate = DateTime.Now,
                            Id = this.Logs.Count,
                            Message = string.Format("Item {0} status resulted in error.", curItem.Id)

                        });
                        nlogger.Log(LogLevel.Error, string.Format("Item {0} status resulted in error.", curItem.Id));
                    }
                    
                    //StringBuilder updateQuery = new StringBuilder();
                    
                    //updateQuery.AppendFormat(RobotData.Audit.UpdateQuery, RobotData.Audit.AuditTableName, "'" + curItem.Status + "'", "'"+ curItem.Status+"'", RobotData.Audit.IdFieldName, "'"+insertedId+"'");

                    //RobotData.Audit.UpdateQuery = updateQuery.ToString();
                    //audit.UpdateAuditInfo(RobotData.Audit);
                    
                    if (Abort.Value)
                    {
                        SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                        break;
                    }

                    //if (curItem.Status != WorkitemStatus.Error)
                    //{
                    InputModule.PostProcess(curItem);
                    this.Logs.Add(new LogEntry()
                    {
                        Severity = LogSeverity.Normal,
                        EventDate = DateTime.Now,
                        Id = this.Logs.Count,
                        Message = string.Format("Item postprocessing complete.", this.Name)
                    });
                    nlogger.Log(LogLevel.Error, string.Format("Item postprocessing complete.", this.Name));
                    
                    if (rv >= 100)
                        break;

                }
                catch (Exception ex)
                {
                    SendNotification.Notify(SendNotification.NotifyEvent.AllError);
                    Logs.Add(new LogEntry() { Id = Logs.Count, EventDate = DateTime.Now, Message = string.Format("Exception while processing items: {0}.", ex.ToString()), Severity = LogSeverity.Normal });
                    nlogger.Log(LogLevel.Error, string.Format("Exception while processing items: {0}.", ex.ToString()));
                }
            }

            return rv;
        }

        protected long ProcessBatches()
        {
            throw new NotImplementedException();
        }

        protected void ProcessItem(IWorkitem item)
        {
            //long rv;

            if (null == item)
                return;

            try
            {
                if (Abort.Value)
                {
                    SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                    return;
                }

                //throw new RobotAbortedException();
                if ((License.CanNormalize) && (item.Status != WorkitemStatus.Error) && (item is Document) && (RobotData.Normalizer.Normalize))
                {
                    Normalizer norm = new Normalizer(RobotData.Normalizer);
                    Document doc = item as Document;
                    for (int i = 0; i < doc.Images.Count; i++)
                        doc.Images[i] = norm.Normalize(doc.Images[i]);
                }

                if (Abort.Value)
                {
                    SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                    return;
                }
                //throw new RobotAbortedException();

                if ((License.CanLookup) && (item.Status != WorkitemStatus.Error)) // && (RobotData.DoLookup))
                    item.Status = item.Status; // fix this

                if (Abort.Value)
                {
                    SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                    return;
                }
                //throw new RobotAbortedException();

                if ((License.CanMap) && (item.Status != WorkitemStatus.Error)) // && (RobotData.DoMapping))
                    item.Status = item.Status; // fix this

                item.Status = WorkitemStatus.Successful;
                //rv = 1;
            }
            catch (Exception ex)
            {
                SendNotification.Notify(SendNotification.NotifyEvent.AllError);
                // log exception
                item.Status = WorkitemStatus.Error;
                this.Logs.Add(new LogEntry()
                {
                    Severity = LogSeverity.Error,
                    EventDate = DateTime.Now,
                    Id = this.Logs.Count,
                    Message = string.Format("robot '{0}' encountered exception {1}", this.Name, ex.ToString())
                });
                //rv = 0;
            }

            return;// rv;

        }

        public void ThreadMethod()
        {
            while (!Abort.Value)
            {
                this.Logs.Add(new LogEntry()
                {
                    Severity = LogSeverity.Error,
                    EventDate = DateTime.Now,
                    Id = this.Logs.Count,
                    Message = string.Format("threadmethod looping")
                });
                pauseHandle.Wait();
                var counter = RunRobot();
                WaitWithAbort(RobotData.SleepTime);
                Console.WriteLine("abort value is {0}", Abort.Value);
            }
        }

        public bool WaitWithAbort(int seconds)
        {
            for (int i = 0; i <= (seconds * 10); i++)  // 10 iterations per second, so iterate 10 * #seconds
            {
                if (Abort.Value)
                {
                    SendNotification.Notify(SendNotification.NotifyEvent.ProcessingStoppedDueToError);
                    return true;
                } // ... or abort
                Thread.Sleep(100); // sleep 100 milliseconds per iteration
            }
            return false;
        }

        
    }
}
