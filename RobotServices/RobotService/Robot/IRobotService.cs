﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using Paragon.RobotServices.Model;

namespace Paragon.RobotServices.Service
{
    [ServiceContract]
    public interface IRobotService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/")]
        IList<Robot> List();

        [OperationContract]
        [WebGet(UriTemplate = "/{id}")]
        Robot GetRobot(string id);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/{id}")]
        void UpdateRobot(string id, Robot bot);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/")]
        void CreateRobot(Robot bot);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "/{id}")]
        void DeleteRobot(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/start/{id}")]
        bool Start(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/stop/{id}")]
        bool Stop(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/pause/{id}")]
        bool Pause(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/resume/{id}")]
        bool Resume(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/startscheduling?id={id}&val={val}")]
        bool StartScheduling(string id, string val);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/SaveLicenseKey?id={id}&val={val}")]
        bool SaveLicenseKey(string id, string val);

        [OperationContract]
        [WebGet(UriTemplate = "/{id}/logs")]
        List<LogEntry> Logs(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/chartdata?id={id}&fromDate={fromDate}&toDate={toDate}&viewType={viewType}")]
        List<ChartData> GetChartData(string id, string fromDate, string toDate, string viewType);
        
    }
}
