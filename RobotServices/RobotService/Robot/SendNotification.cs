﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;
using System.Net.Mail;

namespace Paragon.RobotServices.Service
{
    static class SendNotification
    {
        public static NotificationSettings Notifier;

        public static string emailSubject = string.Empty;
        public static string emailBody = string.Empty;
        
        public enum NotifyEvent
        {
            ProcessingStarted = 1,
            ProcessingStoppedNormally = 2,
            ProcessingStoppedDueToError = 3,
            ProcessingStoppedByUser = 4,
            AllError = 5
        };

        public static void Notify(NotifyEvent notifyEvent)
        {
            switch (notifyEvent)
            {
                case NotifyEvent.ProcessingStarted:
                    if (Notifier.NotifyStartEmail)
                    {
                        emailSubject = "Processing started";
                        emailBody = "Processing started";
                        SendMail();
                    }
                    break;
                case NotifyEvent.ProcessingStoppedNormally:
                    if (Notifier.NotifyStopEmail)
                    {
                        emailSubject = "Processing stopped normally";
                        emailBody = "Processing stopped normally";
                        SendMail();
                    }
                    break;
                case NotifyEvent.ProcessingStoppedDueToError:
                    if (Notifier.NotifyErrorAbortEmail)
                    {
                        emailSubject = "Processing stopped due to error";
                        emailBody = "Processing stopped due to error";
                        SendMail();
                    }
                    break;
                case NotifyEvent.ProcessingStoppedByUser:
                    if (Notifier.NotifyUserAbortEmail)
                    {
                        emailSubject = "Processing stopped by user";
                        emailBody = "Processing stopped by user";
                        SendMail();
                    }
                    break;
                case NotifyEvent.AllError:
                    if (Notifier.NotifyAnyErrorEmail)
                    {
                        emailSubject = "All error messages";
                        emailBody = "All error messages";
                        SendMail();
                    }
                    break;
                default:
                    break;
            }
        }

        static void SendMail()
        {
            var fromAddress = Notifier.From;
            var toAddress = Notifier.Recipients;
            SmtpClient client = new SmtpClient(Notifier.SmtpServer, Notifier.SmtpPort);
            MailMessage mail_msg = new MailMessage();
            MailAddress fromAdd = new MailAddress(fromAddress); 
            mail_msg.From = fromAdd;
            mail_msg.To.Add(toAddress);  
            mail_msg.Subject = emailSubject;
            mail_msg.IsBodyHtml = true;
            mail_msg.Body = emailBody;

            System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential("username", "password");
            client.EnableSsl = true;
            client.UseDefaultCredentials = true;
            client.Credentials = basicCredential;
            client.Send(mail_msg);
        }
    }
}
