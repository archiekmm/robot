﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;

namespace Paragon.RobotServices.Service
{
    static class Commands
    {
        public static void Help(ICollection<string> args)
        {
        }

        public static void Run(ICollection<string> args)
        {
            using (Implementation svc = new Implementation())
            {
                svc.Start(args);
                Console.WriteLine("Press [Enter] to exit...");
                Console.ReadLine();
                RobotService.StopAllRobots();
                svc.Stop();
            }
        }

        public static void RunAsService(ICollection<string> args)
        {
            if (args.Count == 0)
                args.Add("RobotService");
            ServiceBase.Run(new ServiceProcess(args));
        }

        public static void Install(ICollection<string> args)
        {
            new InstallUtil(typeof(Program).Assembly).Install(args);
        }

        public static void UnInstall(ICollection<string> args)
        {
            new InstallUtil(typeof(Program).Assembly).Uninstall(args);
        }
    }
}
