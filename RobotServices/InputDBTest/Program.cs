﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Plugin;

namespace InputDBTest
{
    class Program
    {
         static void Main2(string[] args)
        {
            Dictionary<int, string> outputSettings = new Dictionary<int, string>();
            outputSettings.Add(1, "paraadm1");
            outputSettings.Add(2, "paraadm");
            outputSettings.Add(3, "va-vpc-pidemo");
            outputSettings.Add(4, "ParagonImagingInterface/ParagonImaging");

            List<Paragon.RobotServices.Model.LogEntry> Log = new List<Paragon.RobotServices.Model.LogEntry>();

            OutputIGRedaction opi = new OutputIGRedaction(outputSettings);
            opi.Logger = Log;

            opi.Connect();

            Document doc = new Document();
            doc.Class = "CONTRACT";
            doc.Name = "OPI Test " + DateTime.Now.ToString("yyyy-mm-dd-hh-MM");
            doc.Indexes.Add(new Dictionary<string, string>());
            doc.Indexes[0].Add("First Name", "Bobby");
            doc.Indexes[0].Add("Last Name", "Miller");
            doc.Indexes[0].Add("Loan Amount", "$1,000.00");
            doc.Indexes[0].Add("Contract Date", "08/27/2012");

            doc.Images.AddRange(Page.GetPages("4PageDoc.tif"));  //.Add(new Page("4PageDoc.tif"));
            
            opi.Process(doc);
            Console.WriteLine("Doc status is {0}", doc.Status);
            if (doc.Status == WorkitemStatus.Error)
            {
                Console.WriteLine("Source is {0}", doc.ErrorSource);
                Console.WriteLine("Error is {0}", doc.ErrorDescription);
            }
            else
                Console.WriteLine("Doc id is {0}", doc.Id);

            Console.WriteLine("Done.");
            Console.ReadLine();

        }

        static void Main(string[] args)
        {
            Dictionary<int, string> inputSettings = new Dictionary<int, string>();
            inputSettings.Add(1, @"C:\Test\Work");
            inputSettings.Add(2, "{SQL Server}");
            inputSettings.Add(3, "ma-srv-webimg");
            inputSettings.Add(4, "AAARedaction");
            inputSettings.Add(5, "redaction");
            inputSettings.Add(6, "redaction1");
            inputSettings.Add(7, "select top 1 * from AAAHistoricalData where status is null");
            inputSettings.Add(8, "update AAAHistoricalData set status = 'S' where ObjectID =");
            inputSettings.Add(9, "update AAAHistoricalData set status = 'E' where ObjectID =");
            inputSettings.Add(10, "true");
            inputSettings.Add(11, @"C:\Test\Archive");
            inputSettings.Add(12, @"C:\Test\Archive");
            inputSettings.Add(13, "");
            IInputModule id = new InputDB(inputSettings);
            id.Connect();
            
            Dictionary<int, string> outputSettings = new Dictionary<int, string>();
            outputSettings.Add(1, @"C:\AAAOutput");


           List<Paragon.RobotServices.Model.LogEntry> Log = new List<Paragon.RobotServices.Model.LogEntry>();

            OutputIGRedaction opi = new OutputIGRedaction(outputSettings);
            opi.Logger = Log;

            opi.Connect();

            IWorkitem item = id.Get();

            //item.Class = "CONTRACT";
            opi.Process(item);

            if (item.Status == WorkitemStatus.Successful)
            {
                Console.WriteLine("Workitem processed successfully.");
                id.PostProcess(item);
            }
            else
                Console.WriteLine("Workitem errored:\n  {0}\n  {1}\n", item.ErrorSource, item.ErrorDescription);
                

            Console.WriteLine("Done.");
            Console.ReadLine();
        }
    }

}
