﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Paragon.RobotServices.Plugin
{
    public class Document : IWorkitem
    {
        #region IWorkitem Members

        public string Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public IList<IDictionary<string, string>> Indexes { get; protected set; }

        public WorkitemStatus Status { get; set; }
        public bool ForceNewBatch { get; set; }

        public string ErrorDescription { get; set; }
        public string ErrorSource { get; set; }

        public string DestId { get; set;}
        public string DestName { get; set; }
        public string DestBatchId { get; set; }
        public string DestBatchName { get; set; }

        public long DocumentCount { get; set; }
        public long PageCount { get; set; }

        #endregion

        public Document()
        {
            Indexes = new List<IDictionary<string, string>>();
            //Indexes.Add(new Dictionary<string, string>());
            Images = new List<Page>();
            Attachments = new List<Page>();
            Status = WorkitemStatus.NotProcessed;
        }

        public List<Page> Images { get; protected set; }
        public List<Page> Attachments { get; protected set; }

        public bool SaveImagesToTiff(string filename, bool overwriteExisting = false)
        {
            bool rv = false;
            if (this.Images.Count <= 0)
                throw new InvalidOperationException("The document has no images.");

            if (!filename.EndsWith(".tif"))
                filename += ".tif";
            if (File.Exists(filename))
            {
                if (overwriteExisting)
                    File.Delete(filename);
                else
                    throw new ArgumentException(string.Format("File '{0}' already exists but overwriteExisting is false.", filename));
            }

            // copy first image to temp file
            MemoryStream imageStream = new MemoryStream();
            Image first = Image.FromStream(new MemoryStream(this.Images[0].Data));
            first.Save(imageStream, ImageFormat.Tiff);

            Image tiff = Image.FromStream(imageStream);
            ImageCodecInfo tiffInfo = ImageCodecInfo.GetImageEncoders().First(ici => ici.MimeType == "image/tiff");
            EncoderParameters firstParams = new EncoderParameters(2);
            firstParams.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            firstParams.Param[1] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
            tiff.Save(filename, tiffInfo, firstParams);

            // copy subsequent images to temp file

            EncoderParameters restParams = new EncoderParameters(2);
            restParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
            restParams.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            for (int i = 1; i < this.Images.Count; i++)
            {
                tiff.SaveAdd(Image.FromStream(new MemoryStream(this.Images[i].Data)), restParams);
            }

            // flush the file
            EncoderParameters flushParams = new EncoderParameters(1);
            flushParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.Flush);
                           tiff.SaveAdd(flushParams);

            return rv;

        }

    }
}
