﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;

namespace Paragon.RobotServices.Plugin
{
    public interface IOutputModule : IModule, IConnector
    {
        long Process(IWorkitem item);
        long ProcessBatch(IEnumerable<IWorkitem> items);
        bool SupportsBatching { get; }
        bool RequiresConnection { get; }
        IConnector Connector { get; set; }
        bool ShareConnector { get; }
        IList<LogEntry> Logger { get; set; }
    }
}
