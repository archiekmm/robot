﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Model;

namespace Paragon.RobotServices.Plugin
{
    public abstract class ImagingConnector : IImagingConnector, IDisposable
    {
        protected IDictionary<string, string> _login;

        public ImagingConnector(IDictionary<string, string> login)
        {
            if (null == login)
                throw new ArgumentNullException("login");
            _login = login;
        }

        public ImagingConnector(System.Data.Common.DbConnectionStringBuilder dcsb)
        {
            _login = new Dictionary<string, string>();
            foreach (var key in dcsb.Keys)
                _login.Add((string)key, (string)dcsb[(string)key]);

        }

        public abstract bool Connect();
        public abstract void Disconnect();
        public abstract bool IsConnected { get; }

        public abstract bool UpdateIndexes(IWorkitem doc);
        public abstract bool DeleteDocument(string docId);

        public abstract IEnumerable<Document> FindDocuments(IDictionary<string, string> searchIndexes);
        public abstract string CreateDocument(Document doc);
        public abstract Document RetrieveDocument(string docId);
        public abstract bool UpdateDocument(Document doc, bool replacePages);
        public abstract bool ImportPage(string docId, string pageName, string mimeType, byte[] pageData);
        public abstract byte[] ExportPage(string docId, string pageName);

        public abstract IEnumerable<Folder> FindFolders(IDictionary<string, string> searchIndexes);
        public abstract string CreateFolder(Folder folder);
        public abstract Folder RetrieveFolder(string folderId);
        public abstract bool UpdateFolder(Folder folder);
        public abstract bool DeleteFolder(string folderId);
        public abstract bool AddToFolder(string folderId, string docId);
        public abstract bool RemoveFromFolder(string folderId, string docId);

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }
}
