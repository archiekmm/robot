﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public interface IModule
    {
        int Id { get; set; }
        string Name { get; set; }
        string FileName { get; set; }
        string Type { get; set; }
        bool RequiresConnection { get; }
    }
}
