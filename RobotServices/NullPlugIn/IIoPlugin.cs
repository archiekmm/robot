﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public interface IIoPlugin
    {
        IInputModule InputModule { get; }
        IOutputModule OutputModule { get; }
    }
}
