﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public interface IImagingConnector : IConnector
    {
        IEnumerable<Document> FindDocuments(IDictionary<string, string> searchIndexes);
        string CreateDocument(Document doc);
        Document RetrieveDocument(string docId);
        bool UpdateDocument(Document doc, bool replacePages);
        bool ImportPage(string docId, string pageName, string mimeType, byte[] pageData);
        byte[] ExportPage(string docId, string pageName);

        bool UpdateIndexes(IWorkitem doc);
        bool DeleteDocument(string docId);
        bool DeleteFolder(string folderId);
        
        IEnumerable<Folder> FindFolders(IDictionary<string, string> searchIndexes);
        string CreateFolder(Folder folder);
        Folder RetrieveFolder(string folderId);
        bool UpdateFolder(Folder folder);
        bool AddToFolder(string folderId, string docId);
        bool RemoveFromFolder(string folderId, string docId);
    }
}
