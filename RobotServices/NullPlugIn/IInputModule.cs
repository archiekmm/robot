﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Paragon.RobotServices.Plugin
{
    public interface IInputModule : IModule, IConnector
    {
        IWorkitem Get();
        IEnumerable<IWorkitem> GetBatch();
        void Delete(IWorkitem item);
        void Move(IWorkitem item, DirectoryInfo whereTo);
        void UpdateStatus(IWorkitem item, WorkitemStatus status);
        bool SupportsMove { get; }
        bool SupportsDelete { get; }
        bool SupportsUpdateStatus { get; }
        bool SupportsBatching { get; }
        void PostProcess(IWorkitem item);
        //bool RequiresConnection { get; }
        IConnector Connector { get; }
    }
}
