﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public class Page
    {
        public string Name
        {
            get;
            set;
        }

        public string MimeType
        {
            get;
            set;
        }

        public byte[] Data
        {
            get;
            protected set;
        }

        public static IEnumerable<Page> GetPages(string fileName, string pageNamePrefix, int pageNumberSuffixStart)
        {
            List<Page> rv = new List<Page>();

            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException();
            if (!System.IO.File.Exists(fileName))
                throw new System.IO.FileNotFoundException(fileName);

            string ext = System.IO.Path.GetExtension(fileName);
            string MimeType = "application/octet-stream";
            if (FileExtensions.MimeTypes.ContainsKey(ext.Substring(1).ToLower()))
                MimeType = FileExtensions.MimeTypes[ext.Substring(1).ToLower()];
            if (MimeType.StartsWith("image/"))
            {
                System.IO.MemoryStream imageStream = new System.IO.MemoryStream(System.IO.File.ReadAllBytes(fileName));
                System.Drawing.Image image = System.Drawing.Image.FromStream(imageStream);
                for (int i = 0; i < image.GetFrameCount(System.Drawing.Imaging.FrameDimension.Page); i++)
                {
                    var ms = new System.IO.MemoryStream();
                    image.SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Page, i);
                    image.Save(ms, image.RawFormat);
                    rv.Add(new Page(pageNamePrefix + (pageNumberSuffixStart + i).ToString(), MimeType, ms.GetBuffer()));
                }
            }
            else
            {
                var ms = new System.IO.MemoryStream();
                var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                fs.CopyTo(ms);
                rv.Add(new Page(pageNamePrefix + pageNumberSuffixStart.ToString(), MimeType, ms.GetBuffer()));
                fs.Close();
                fs = null;
            }

            return rv;
        }

        public static IEnumerable<Page> GetPages(string filename, string pageNamePrefix)
        {
            return GetPages(filename, pageNamePrefix, 1);
        }

        public static IEnumerable<Page> GetPages(string filename, int pageNumberSuffixStart)
        {
            return GetPages(filename, "Page", pageNumberSuffixStart);
        }

        public static IEnumerable<Page> GetPages(string filename)
        {
            return GetPages(filename, "Page", 1);
        }

        public Page(string name, string mimeType, byte[] data)
        {
            Name = name;
            MimeType = mimeType;
            Data = data;
        }

        //public Page(string fileName)
        //{
        //    if (string.IsNullOrEmpty(fileName))
        //        throw new ArgumentNullException();
        //    if (!System.IO.File.Exists(fileName))
        //        throw new System.IO.FileNotFoundException(fileName);

        //    Name = System.IO.Path.GetFileNameWithoutExtension(fileName);
        //    string ext = System.IO.Path.GetExtension(fileName);
        //    MimeType = FileExtensions.MimeTypes[ext.Substring(1)];
        //    var ms = new System.IO.MemoryStream();
        //    if (MimeType.StartsWith("image/"))
        //    {
        //        System.Drawing.Image image = System.Drawing.Image.FromFile(fileName);
        //        MimeType = image.GetMimeType();
        //        image.Save(ms, image.RawFormat);
        //        Data = ms.GetBuffer();
        //    } else {
        //        var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        //        fs.CopyTo(ms);
        //    }
        //    Data = ms.GetBuffer();
        //}

        public Page(string name, string mimeType, System.IO.Stream data)
        {
            Name = name;
            MimeType = mimeType;
            var ms = new System.IO.MemoryStream();
            data.CopyTo(ms);
            Data = ms.GetBuffer();
        }
    }
}
