﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paragon.RobotServices.Captures;
using Paragon.RobotServices.Fields;
using Paragon.RobotServices.Lookups;
using Paragon.RobotServices.Mappings;
using Paragon.RobotServices.Modules;
using Paragon.RobotServices.Settings;
//using Paragon.RobotServices.Imagings;
//using Paragon.RobotServices.Normalizers;
using Paragon.RobotServices.Utilities;

namespace Paragon.RobotServices.Plugin
{
    public class AutoImport : PlugInBase
    {
        protected int _moduleId;
        protected List<int> _captures = new List<int>();
        protected List<int> _imagings = new List<int>();
        protected List<int> _lookups = new List<int>();
        protected List<int> _mappings = new List<int>();
        protected List<int> _normalizers = new List<int>();
        protected List<int> _settings = new List<int>();
        protected string _name;
        
        // the services
        protected ModuleService _ms = new ModuleService();
        protected CaptureService _cs = new CaptureService();
        //protected ImagingService _is = new ImagingService();
        protected LookupService _ls = new LookupService();
        protected MappingService _mapSvc = new MappingService();
        //protected NormalizerService _ns = new NormalizerService();
        //protected FieldService _fs = new FieldService();
        protected SettingService _ss = new SettingService();

        // derived items
        protected IDictionary<string, string> settings = new Dictionary<string, string>();

        public const string cNormalizeImages = "NormalizeImages";
        public const string cNormalizerId = "NormalizerId";
        public const string cDoLookups = "DoLookups";
        public const string cLookupId = "LookupId";
        public const string cDoMappings = "DoMappings";
        public const string cMappingId = "MappingId";
        public const string cInputModuleName = "InputModule";
        public const string cOutputModuleName = "OutputModule";
        public const string cBatchWorkItems = "BatchOptions.BatchItems";
        public const string cBatchSize = "BatchOptions.BatchSize";
        public const string cBatchWaitTime = "BatchOptions.WaitTime";
        public const int cDefaultBatchSize = 20;
        public const int cDefaultBatchWaitTime = 10;

        protected bool _normalize = false;
        protected bool _doLookup = false;
        protected bool _doMapping = false;
        protected int _normalizerId = 0;
        protected int _lookupId = 0;
        protected int _mappingId = 0;
        protected string _inputModuleName;
        protected IInputModule _inputModule;
        protected string _outputModuleName;
        protected IOutputModule _outputModule;
        protected bool _batchItems = false;
        protected int _batchSize = cDefaultBatchSize;
        protected int _batchWaitTime = cDefaultBatchWaitTime;
        IDictionary<string, string> inputSettings;
        IDictionary<string, string> outputSettings;

        public AutoImport(int moduleId)
        {
            _moduleId = moduleId;
            Module mc = _ms.Retrieve(_moduleId);
            _captures.AddRange(mc.CaptureIds.Keys);
            _imagings.AddRange(mc.ImagingIds.Keys);
            _lookups.AddRange(mc.LookupIds.Keys);
            _mappings.AddRange(mc.MappingIds.Keys);
            _normalizers.AddRange(mc.NormalizerIds.Keys);
            _settings.AddRange(mc.SettingIds.Keys);
            //Field fc = null;
            Setting ss = null;
            foreach (var setting in mc.SettingIds)
            {
                ss = _ss.Retrieve(setting.Key);
                settings.Add(ss.Name, ss.Value);
            }
            _name = mc.Name;
            ProcessSettings();
            _inputModuleName = settings["Input Module"];
            _outputModuleName = settings["Output Module"];
            _inputModule = _inputModuleName.LoadAssemblyAndGetType().CreateInstance<IInputModule>(new object[] { inputSettings });
            _outputModule = _outputModuleName.LoadAssemblyAndGetType().CreateInstance<IOutputModule>(new object[] { outputSettings });
        }

        public override long Run()
        {
            if (_batchItems)
            {
                if (_outputModule.SupportsBatching)
                 {
                if (_batchSize > 0)
                    return ProcessBatches();
                //else
                    // log warning:  batching selected but batch size <= 0
                 }
                // else
                    // log warning:  batching selected but output module does not support batching.  Output will not be batched.
            }
            return ProcessItems();
        }

        protected void ProcessSettings()
        {
            if (settings.ContainsKey(cNormalizeImages))
                bool.TryParse(settings[cNormalizeImages], out _normalize);
            if (settings.ContainsKey(cDoLookups))
                bool.TryParse(settings[cDoLookups], out _doLookup);
            if (settings.ContainsKey(cDoMappings))
                bool.TryParse(settings[cDoMappings], out _doMapping);
            if (settings.ContainsKey(cNormalizerId))
                int.TryParse(settings[cNormalizerId], out _normalizerId);
            if (settings.ContainsKey(cLookupId))
                int.TryParse(settings[cLookupId], out _lookupId);
            if (settings.ContainsKey(cMappingId))
                int.TryParse(settings[cMappingId], out _mappingId);
            if (settings.ContainsKey(cInputModuleName))
                _inputModuleName = settings[cInputModuleName];
            if (settings.ContainsKey(cOutputModuleName))
                _outputModuleName = settings[cOutputModuleName];
            if (settings.ContainsKey(cBatchWorkItems))
                bool.TryParse(settings[cBatchWorkItems], out _batchItems);
            if (settings.ContainsKey(cBatchSize))
                int.TryParse(settings[cBatchSize], out _batchSize);
            if (settings.ContainsKey(cBatchWaitTime))
                int.TryParse(settings[cBatchWaitTime], out _batchWaitTime);
            inputSettings = new Dictionary<string, string>();
            outputSettings = new Dictionary<string, string>();
            foreach (var kvp in settings)
            {
                if (kvp.Key.StartsWith("Input."))
                    inputSettings.Add(kvp.Key.Substring("Input.".Length), kvp.Value);
                if (kvp.Key.StartsWith("Output."))
                    outputSettings.Add(kvp.Key.Substring("Output.".Length), kvp.Value);
            }
        }

        protected long ProcessBatches()
        {
            return 0;
        }

        protected long ProcessItems()
        {
            long rv = 0;
            // check available page count

            // check for abort

            IWorkitem item = _inputModule.Get();
            if (null == item)
                return 0;

            // check for abort

            
            NormalizeStreams(item);
            DoLookups(item);
            DoMappings(item);
            rv = ProcessItem(item);
            PostProcessItem(item);
            return rv;
        }

        protected void NormalizeStreams(IWorkitem item)
        {
            if (item.Status != WorkitemStatus.Error)
            {
                if (_normalize)
                {
                    if (_normalizerId > 0)
                    {
                        //foreach (System.IO.Stream s in item.Streams)
                        //s = ns.NormalizeStream(_normalizerId, s);
                    }
                    //else
                    // log warning:  normalization selected but no normalizer chosen
                }
            }
        }

        protected void DoLookups(IWorkitem item)
        {
            if (item.Status != WorkitemStatus.Error)
            {
                if (_doLookup)
                {
                    if (_lookupId> 0)
                    {
                        IDictionary<string, string> indexFields = item.Indexes.FirstOrDefault(); ;
                        foreach(var kvp in _ls.DoLookup(_lookupId, indexFields))
                            indexFields[kvp.Key] = kvp.Value;
                    }
                    //else
                    // log warning:  lookups selected but no lookup chosen
                }
            }
        }

        protected void DoMappings(IWorkitem item)
        {
            if (item.Status != WorkitemStatus.Error)
            {
                if (_doMapping)
                {
                    // foreach (int mappingId in _mappingIds)
                    if (_mappingId > 0)
                    {
                        IDictionary<string, string> indexFields = item.Indexes.FirstOrDefault();
                        foreach (var kvp in _mapSvc.TranslateFields(_mappingId, indexFields))
                            indexFields[kvp.Key] = kvp.Value;
                    }
                    //else
                    // log warning:  lookups selected but no lookup chosen
                }
            }
        }

        protected long ProcessItem(IWorkitem item)
        {
            long rv = 0;
            if (item.Status != WorkitemStatus.Error)
            {
                rv = _outputModule.Process(item);
            }
            return rv;
        }

        protected void PostProcessItem(IWorkitem item)
        {
            foreach (var image in item.Images)
                image.Dispose();
            item.Images.Clear();
            _inputModule.Delete(item);
        }
    }
}
