﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Net.Mail;

namespace Paragon.RobotServices.Plugin
{
    public interface IWorkitem
    {
        string Id { get; set; }
        string Name { get; set; }
        string Class { get; set; }
        IList<IDictionary<string, string>> Indexes { get; }

        WorkitemStatus Status { get; set; }
        bool ForceNewBatch { get; set; }
        
        string ErrorDescription { get; set; }
        string ErrorSource { get; set; }
        
        string DestId { get; set; }
        string DestName { get; set; }
        string DestBatchId { get; set; }
        string DestBatchName { get; set; }

        long DocumentCount { get; set; }
        long PageCount { get; set; }

        //IList<IWorkitemPage> Images { get; }
        //IList<IWorkitemPage> Attachments { get; }
    }
}
