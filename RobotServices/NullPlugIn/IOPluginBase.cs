﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public abstract class IOPluginBase : PlugInBase, IIoPlugin
    {
        public abstract override long Run();

        #region IIoPlugin Members

        public abstract IInputModule InputModule  { get; }

        public abstract IOutputModule OutputModule { get; }

        #endregion
    }
}
