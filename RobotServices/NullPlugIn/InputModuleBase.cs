﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Paragon.RobotServices.Plugin
{
    public abstract class InputModuleBase: IInputModule, INotifyPropertyChanged
    {

        public abstract IWorkitem Get();

        public abstract IEnumerable<IWorkitem> GetBatch();

        public abstract void Delete(IWorkitem item);

        public abstract void Move(IWorkitem item, System.IO.DirectoryInfo whereTo);

        public void UpdateStatus(IWorkitem item, WorkitemStatus status)
        {
            throw new NotImplementedException();
        }

        public bool SupportsMove
        {
            get { throw new NotImplementedException(); }
        }

        public bool SupportsDelete
        {
            get { throw new NotImplementedException(); }
        }

        public bool SupportsUpdateStatus
        {
            get { throw new NotImplementedException(); }
        }

        public bool SupportsBatching
        {
            get { throw new NotImplementedException(); }
        }

        public int Id
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string FileName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Type
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
