﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public class ImagingDocument
    {
        public string DocumentId;
        public string Name;
        public string Class;
        public IList<IDictionary<string, string>> Indexes = new List<IDictionary<string, string>>();
        public IList<ImagingPage> Pages = new List<ImagingPage>();
    }

    public class ImagingPage
    {
        public string Name;
        public string PageId;
        public string MimeType;
        public byte[] ImageData;
    }

    public class ImagingFolder
    {
        public string FolderId;
        public string Name;
        public string Class;
        public IList<IDictionary<string, string>> Indexes;
        public IList<string> DocumentIds;
        public IList<string> FolderIds;
    }
}
