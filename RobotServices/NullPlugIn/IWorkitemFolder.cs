﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public interface IWorkitemFolder
    {
        string Id { get; set; }
        string Name { get; set; }
        string Class { get; set; }

        IList<IDictionary<string, string>> Indexes { get; }
        IList<IWorkitem> Documents { get; }
        IList<IWorkitemFolder> Folders { get; }
    }
}
