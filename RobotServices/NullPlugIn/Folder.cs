﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public class Folder : IWorkitem
    {
        #region IWorkitem Members

        public string Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public IList<IDictionary<string, string>> Indexes { get; protected set; }

        public WorkitemStatus Status { get; set; }
        public bool ForceNewBatch { get; set; }

        public string ErrorDescription { get; set; }
        public string ErrorSource { get; set; }

        public string DestId { get; set; }
        public string DestName { get; set; }
        public string DestBatchId { get; set; }
        public string DestBatchName { get; set; }

        public long DocumentCount { get; set; }
        public long PageCount { get; set; }

        #endregion

        public IList<Document> Documents { get; protected set; }
        public IList<Folder> Folders { get; protected set; }

        public Folder()
        {
            Indexes = new List<IDictionary<string, string>>();
            Documents = new List<Document>();
            Folders = new List<Folder>();
        }

    }
}
