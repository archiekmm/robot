﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public interface IWorkitemPage
    {
        string Name { get; }
        string MimeType { get; }
        byte[] Data { get; }
    }
}
