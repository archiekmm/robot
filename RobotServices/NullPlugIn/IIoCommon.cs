﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public interface IConnector
    {
        bool Connect();
        bool IsConnected { get; }
        void Disconnect();
    }

    public enum IndexFieldPairSeparators
    {
        CrOrLf,
        Cr,
        Lf,
        CrLf,
        Other
    }
}
