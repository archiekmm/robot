﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public enum WorkitemStatus
    {
        NotProcessed,
        Successful,
        Error
    }
}
