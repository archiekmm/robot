﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paragon.RobotServices.Plugin
{
    public static class ImageExtensions
    {
        public static bool IsMultiPage(string fileName)
        {
            try
            {
                return (System.Drawing.Image.FromFile(fileName).GetFrameCount(System.Drawing.Imaging.FrameDimension.Page) > 1) ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetMimeType(this System.Drawing.Image image)
        {
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp))
                return "image/bmp";
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Emf))
                return "application/x-msmetafile";
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif))
                return "image/gif";
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Icon))
                return "image/vnd.microsoft.icon";
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg))
                return "image/jpeg";
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png))
                return "image/png";
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Tiff))
                return "image/tiff";
            if (image.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Wmf))
                return "application/x-msmetafile";
            return "application/octet-stream";
        }

        public static IEnumerable<byte[]> AsBuffers(this System.Drawing.Image image)
        {
            List<byte[]> buffers = new List<byte[]>();
            for(int i = 0; i < image.GetFrameCount(System.Drawing.Imaging.FrameDimension.Page); i++)
            {
                var ms = new System.IO.MemoryStream();
                image.SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Page, i);
                image.Save(ms, image.RawFormat);
                buffers.Add(ms.GetBuffer());
            }
            return buffers;
        }
    }
}
