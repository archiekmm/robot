﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace TestLog4jNet
{
    class Program
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(
          System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

         [STAThread]
        public static int Main(string[] args)
         {
            try
             {
                log.Info("Logging is set");        
             }
            catch (Exception exc)
             {
                log.Fatal(exc.ToString());
             }
            return 1;
         }

    }
}
